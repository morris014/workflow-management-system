-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 30, 2015 at 10:55 PM
-- Server version: 5.5.44
-- PHP Version: 5.4.45-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mitsukoshi_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `approval_status`
--

CREATE TABLE IF NOT EXISTS `approval_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `approval_status`
--

INSERT INTO `approval_status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Approved'),
(3, 'Rejected');

-- --------------------------------------------------------

--
-- Table structure for table `employe_request`
--

CREATE TABLE IF NOT EXISTS `employe_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `requestor_id` varchar(45) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  `date_request` datetime DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `ref_no` varchar(45) DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `total_completed` decimal(10,0) DEFAULT '0',
  `total_tasks` decimal(10,0) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `tat` decimal(10,2) DEFAULT NULL,
  `date_ended` datetime DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `fk_employe_request_mmpi_table_of_organization1_idx` (`requestor_id`),
  KEY `fk_employe_request_ref_workflow1_idx` (`workflow_id`),
  KEY `fk_employe_request_request_status1_idx` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `employe_request`
--

INSERT INTO `employe_request` (`request_id`, `requestor_id`, `workflow_id`, `date_request`, `date_created`, `date_updated`, `ref_no`, `comments`, `total_completed`, `total_tasks`, `status`, `tat`, `date_ended`) VALUES
(34, '104002', 2, '2015-09-30 20:19:29', '2015-09-30 12:19:43', NULL, 'CM-2015093008193334', '', 0, 8, 1, 4.00, NULL),
(35, '104002', 15, '2015-09-30 22:26:08', '2015-09-30 14:26:23', NULL, 'CM-2015093010261235', 'Try', 0, 2, 1, 6.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mmpi_group_matrix`
--

CREATE TABLE IF NOT EXISTS `mmpi_group_matrix` (
  `group_id` int(11) NOT NULL,
  `employee_id` varchar(45) NOT NULL,
  PRIMARY KEY (`group_id`,`employee_id`),
  KEY `fk_ref_group_has_mmpi_table_of_organization_mmpi_table_of_o_idx` (`employee_id`),
  KEY `fk_ref_group_has_mmpi_table_of_organization_ref_group1_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mmpi_table_of_organization`
--

CREATE TABLE IF NOT EXISTS `mmpi_table_of_organization` (
  `employee_id` varchar(45) NOT NULL,
  `firs_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `contact_num` varchar(45) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `immediate_superior` varchar(45) DEFAULT NULL,
  `immediate_superior_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`employee_id`,`location_id`,`designation_id`,`department_id`,`status_id`),
  KEY `fk_mmpi_table_of_organization_ref_location1_idx` (`location_id`),
  KEY `fk_mmpi_table_of_organization_ref_designation1_idx` (`designation_id`),
  KEY `fk_mmpi_table_of_organization_ref_department1_idx` (`department_id`),
  KEY `fk_mmpi_table_of_organization_ref_status1_idx` (`status_id`),
  KEY `fk_mmpi_table_of_organization_ref_position1_idx` (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mmpi_table_of_organization`
--

INSERT INTO `mmpi_table_of_organization` (`employee_id`, `firs_name`, `middle_name`, `last_name`, `email`, `contact_num`, `location_id`, `designation_id`, `department_id`, `status_id`, `position_id`, `date_updated`, `date_created`, `immediate_superior`, `immediate_superior_id`) VALUES
('100114098', 'JP', 'Ramos', 'Bernedo', 'jp@sdfsdf.ph', '639987136650', 24, 335, 76, 1, 1, '2015-09-30 06:13:36', '2015-09-29 22:13:37', NULL, '200513977'),
('1003', 'Rodel', 'Lee', 'Peñaredondo', 'rodel.peñaredondo@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:53', 'Victoriano Unay', '200513977'),
('1004022', 'Rommel', 'Marzan', 'Patricio', 'rommel.patricio@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:24', 'Richmond Ngan', '200513977'),
('1006055', 'Junelyn', 'Reyes', 'Garcia', 'junelyn.garcia@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:24', 'Richmond Ngan', '200513977'),
('1007098', 'Nicolas', 'Casio', 'Mejarito', 'nicolas.mejarito@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:08', 'Richmond Ngan', '200513977'),
('1011018', 'Robert', 'Del Rosario', 'Domingo', 'robert.domingo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:25', 'Richmond Ngan', '200513977'),
('1011053', 'Johan', 'Guintu', 'Alejandro', 'johan.alejandro@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:27', 'Richmond Ngan', '200513977'),
('104002', 'Lito', 'Agao', 'Estabaya', 'lito.estabaya@mitsukoshimotors.com', '7223333', 23, 310, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:05', 'Richmond Ngan', '200513977'),
('105001', 'Mae Christy', 'Bayog', 'Pido', 'maechristy.pido@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:02', 'Richmond Ngan', '200513977'),
('107001', 'Edgar', 'Salon', 'Arillaga', 'edgar.arillaga@mitsukoshimotors.com', '7223333', 24, 323, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:57', 'Victoriano Unay', '200513977'),
('108002', 'Rolando', 'Atencio', 'Morales', 'rolando.morales@mitsukoshimotors.com', '7223333', 23, 379, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:46', 'Richmond Ngan', '200513977'),
('108004', 'Queenie', 'Alberca', 'Castillo', 'queenie.castillo@mitsukoshimotors.com', '7223333', 23, 380, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:48', 'Richmond Ngan', '200513977'),
('109991231', 'Mando', 'Motmot', 'Masahista', 'mando@rebar.ph', '639981231498', 25, 14, 85, 1, 1, '2015-09-30 05:47:29', '2015-09-29 21:47:31', NULL, '200513977'),
('1101048', 'Raymond', 'Muyargas', 'Alimurong', 'raymond.alimurong@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:28', 'Richmond Ngan', '200513977'),
('1101105', 'Lowell', 'Garil', 'Red', 'lowell.red@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:09', 'Richmond Ngan', '200513977'),
('1102003', 'Juan', 'Revilla', 'Quimio Jr.', 'juan.quimiojr@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:30', 'Richmond Ngan', '200513977'),
('1102124', 'Agnes', 'Verdejo', 'Flores', 'agnes.flores@mitsukoshimotors.com', '7223333', 23, 358, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:51', 'Richmond Ngan', '200513977'),
('1103042', 'Laureano', 'Silagpo', 'Valdez Jr.', 'laureano.valdezjr@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:03', 'Victoriano Unay', '200513977'),
('1103060', 'Junior', 'Catindoy', 'Sibolboro', 'junior.sibolboro@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:31', 'Richmond Ngan', '200513977'),
('1103180', 'Debbie', 'Millamina', 'Magallon', 'debbie.magallon@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:05', 'Richmond Ngan', '200513977'),
('1104012', 'Gerald', 'Dilan', 'Marzan', 'gerald.marzan@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:33', 'Richmond Ngan', '200513977'),
('1104027', 'Antony', 'Guadalupe', 'Bonifacio', 'antony.bonifacio@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:04', 'Victoriano Unay', '200513977'),
('1104029', 'Joseph', 'Casiquin', 'Suplido', 'joseph.suplido@mitsukoshimotors.com', '7223333', 23, 379, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:11', 'Richmond Ngan', '200513977'),
('1104030', 'Peter Charlotte', 'Arevalo', 'Cable', 'petercharlotte.cable@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:12', 'Richmond Ngan', '200513977'),
('1104046', 'Myla', 'Lavarrete', 'Doctolero', 'myla.doctolero@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:06', 'Richmond Ngan', '200513977'),
('1104098', 'Cecille', 'Isiang', 'Estanio', 'cecille.estanio@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:52', 'Richmond Ngan', '200513977'),
('1105087', 'Jorem', 'Ibañez', 'Morales', 'jorem.morales@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:14', 'Richmond Ngan', '200513977'),
('1105123', 'Roger', 'Bonifacio', 'Villaruel', 'roger.villaruel@mitsukoshimotors.com', '7223333', 24, 327, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:06', 'Victoriano Unay', '200513977'),
('1107017', 'Benedick', 'Tangonan', 'Galvez', 'benedick.galvez@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:15', 'Richmond Ngan', '200513977'),
('1107053', 'Sunshine', 'Calara', 'Aldave', 'sunshine.aldave@mitsukoshimotors.com', '7223333', 23, 16, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:32', 'Richmond Ngan', '200513977'),
('1109071', 'Ronnel', 'Gito', 'Obina', 'ronnel.obina@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:52', 'Victoriano Unay', '200513977'),
('1110023', 'Reynaldo', 'Curang', 'Morales', 'reynaldo.morales@mitsukoshimotors.com', '7223333', 24, 328, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:07', 'Victoriano Unay', '200513977'),
('1110031', 'Charlie', 'Abiza', 'Rañada', 'charlie.rañada@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:54', 'Victoriano Unay', '200513977'),
('1110038', 'Michael', 'Ramos', 'Bautista', 'michael.bautista@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:34', 'Richmond Ngan', '200513977'),
('1111073', 'Jonard', 'Namoc', 'Intoy', 'jonard.intoy@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:16', 'Richmond Ngan', '200513977'),
('1112027', 'Marcial', 'Turbanada', 'Lobos Jr.', 'marcial.lobosjr@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:09', 'Victoriano Unay', '200513977'),
('1112084', 'Jaime', 'Felia', 'Paulino Jr', 'jaime.paulinojr@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:18', 'Richmond Ngan', '200513977'),
('1201007', 'Ileen', 'Buenaventura', 'Villanueva', 'ileen.villanueva@mitsukoshimotors.com', '7223333', 23, 361, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:28', 'Richmond Ngan', '200513977'),
('1201019', 'Homer', 'Flaviano', 'Panganiban', 'homer.panganiban@mitsukoshimotors.com', '7223333', 24, 329, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:10', 'Victoriano Unay', '200513977'),
('1201021', 'Norwin', 'Nungay', 'Tingzon', 'norwin.tingzon@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:12', 'Victoriano Unay', '200513977'),
('1201022', 'Paulito', 'Rosales', 'Salingsing Jr', 'paulito.salingsingjr@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:13', 'Victoriano Unay', '200513977'),
('1201035', 'Samson', 'Rabie', 'Soliven', 'samson.soliven@mitsukoshimotors.com', '7223333', 24, 323, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:14', 'Victoriano Unay', '200513977'),
('1201100', 'Melvin', 'Lucelo', 'Samoranos', 'melvin.samoranos@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:16', 'Victoriano Unay', '200513977'),
('1201124', 'Roderick', 'Ayon', 'Compas', 'roderick.compas@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:55', 'Victoriano Unay', '200513977'),
('1201126', 'Cristian', 'Perez', 'Morales', 'cristian.morales@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:17', 'Victoriano Unay', '200513977'),
('1202096', 'Von Amir', 'Sabino', 'Benitez', 'vonamir.benitez@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:19', 'Victoriano Unay', '200513977'),
('1202132', 'Gerardo', 'Doque', 'Felipe', 'gerardo.felipe@mitsukoshimotors.com', '7223333', 24, 330, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:20', 'Victoriano Unay', '200513977'),
('1202137', 'Nicanor', 'Corpuz', 'Garcia Jr', 'nicanor.garciajr@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:56', 'Victoriano Unay', '200513977'),
('1203014', 'Andro', 'Tenebroso', 'Lagrimas', 'andro.lagrimas@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:58', 'Victoriano Unay', '200513977'),
('1203102', 'Jay', 'Escoltor', 'Iligan', 'jay.iligan@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:22', 'Victoriano Unay', '200513977'),
('1203125', 'Reynaldo', 'Feria', 'Rivera', 'reynaldo.rivera@mitsukoshimotors.com', '7223333', 24, 331, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:23', 'Victoriano Unay', '200513977'),
('1203169', 'Ruel', 'Gan', 'Niro', 'ruel.niro@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:28', 'Richmond Ngan', '200513977'),
('1203174', 'Daniel', 'Nuñeza', 'Viñas', 'daniel.viñas@mitsukoshimotors.com', '7223333', 24, 332, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:25', 'Victoriano Unay', '200513977'),
('1204036', 'Maria Ellaine', 'Paguio', 'Santos', 'mariaellaine.santos@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:26', 'Richmond Ngan', '200513977'),
('1204080', 'Leah', 'Artillaga', 'Silva', 'leah.silva@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:08', 'Richmond Ngan', '200513977'),
('1204121', 'Limuel', 'Alejandria', 'Compuesto', 'limuel.compuesto@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:35', 'Richmond Ngan', '200513977'),
('1204165', 'Jose Rogie', 'Surio', 'Tan', 'joserogie.tan@mitsukoshimotors.com', '7223333', 24, 333, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:27', 'Victoriano Unay', '200513977'),
('1204179', 'Rachel Ian', 'Pascual', 'Erasmo', 'rachelian.erasmo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:28', 'Victoriano Unay', '200513977'),
('1205008', 'Mary Anjenette', 'Magayam', 'Maaba', 'maryanjenette.maaba@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:29', 'Richmond Ngan', '200513977'),
('1205187', 'Mark Joseph', 'Credo', 'Picar', 'markjoseph.picar@mitsukoshimotors.com', '7223333', 24, 16, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:59', 'Victoriano Unay', '200513977'),
('1205259', 'Robert', 'Santillan', 'Mangrobang Jr.', 'robert.mangrobangjr@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:01', 'Victoriano Unay', '200513977'),
('1205260', 'Rogevin', 'Galarrita', 'Salurio', 'rogevin.salurio@mitsukoshimotors.com', '7223333', 23, 18, 83, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:40', 'Richmond Ngan', '200513977'),
('1205272', 'Darwin', 'Cantarona', 'Isaal', 'darwin.isaal@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:02', 'Victoriano Unay', '200513977'),
('1206048', 'Benjamin', 'Gara', 'Cuyag Jr.', 'benjamin.cuyagjr@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:29', 'Victoriano Unay', '200513977'),
('1206050', 'Kimwell', 'Polintan', 'Torres', 'kimwell.torres@mitsukoshimotors.com', '7223333', 24, 394, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:04', 'Victoriano Unay', '200513977'),
('1206103', 'Ricardo', 'Bermudez', 'Franco Jr.', 'ricardo.francojr@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:30', 'Richmond Ngan', '200513977'),
('1206201', 'Leover', 'Villamena', 'Pareja', 'leover.pareja@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:19', 'Richmond Ngan', '200513977'),
('1207193', 'Joy', 'Frondozo', 'Pelo', 'joy.pelo@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:09', 'Richmond Ngan', '200513977'),
('1208001', 'Elsie', 'Viesca', 'Castillo', 'elsie.castillo@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:30', 'Richmond Ngan', '200513977'),
('1208002', 'Aldrin Bradly', 'Dugay', 'Doctor', 'aldrinbradly.doctor@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:31', 'Richmond Ngan', '200513977'),
('1209089', 'Mary Rose', 'Taganahan', 'Alforte', 'maryrose.alforte@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:27', 'Richmond Ngan', '200513977'),
('1210052', 'Randy', 'Arellano', 'Villarete', 'randy.villarete@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:28', 'Richmond Ngan', '200513977'),
('1211002', 'Julius Kim', 'Villaruel', 'Sabino', 'juliuskim.sabino@mitsukoshimotors.com', '7223333', 24, 334, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:31', 'Victoriano Unay', '200513977'),
('1211024', 'Jonh Jayson', 'Caoagdan', 'Ignacio', 'jonhjayson.ignacio@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:21', 'Richmond Ngan', '200513977'),
('1211154', 'Mark Ishmael', 'Tarlit', 'Guilas', 'markishmael.guilas@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:30', 'Richmond Ngan', '200513977'),
('1211155', 'Gian Fernando', 'Revilla', 'Villarmino', 'gianfernando.villarmino@mitsukoshimotors.com', '7223333', 23, 387, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:22', 'Richmond Ngan', '200513977'),
('1211158', 'Adrian', 'Cuenca', 'Tabucon', 'adrian.tabucon@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:37', 'Richmond Ngan', '200513977'),
('1212030', 'Alvin', 'Anastacio', 'Albis', 'alvin.albis@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:33', 'Richmond Ngan', '200513977'),
('123456', 'Majida', 'Almazar', 'Fashafsheh', 'jam@rebar.ph', '09999999999', 23, 17, 78, 1, 1, '2015-09-30 14:58:00', '2015-09-30 06:58:01', NULL, '200513977'),
('1301027', 'Vincent Paul', 'Dimal', 'Fajardo', 'vincentpaul.fajardo@mitsukoshimotors.com', '7223333', 23, 376, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:24', 'Richmond Ngan', '200513977'),
('1301048', 'Rubelyn', 'Dalaodao', 'Andaya', 'rubelyn.andaya@mitsukoshimotors.com', '7223333', 23, 16, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:33', 'Richmond Ngan', '200513977'),
('1301054', 'Sheila', 'Dela Cruz', 'Calusayan', 'sheila.calusayan@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:54', 'Richmond Ngan', '200513977'),
('1301056', 'Brandon', 'Tubay', 'Garcia', 'brandon.garcia@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:32', 'Richmond Ngan', '200513977'),
('1302140', 'Joan', 'Manuevo', 'Flores', 'joan.flores@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:33', 'Richmond Ngan', '200513977'),
('1302141', 'Romeo', 'Ortiz', 'Amparo', 'romeo.amparo@mitsukoshimotors.com', '7223333', 24, 335, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:32', 'Victoriano Unay', '200513977'),
('1302184', 'Benjie', 'Guillermo', 'Nacario', 'benjie.nacario@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:25', 'Richmond Ngan', '200513977'),
('1303102', 'Estefanie', 'Utlang', 'Enquig', 'estefanie.enquig@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:32', 'Richmond Ngan', '200513977'),
('1304055', 'Fernando', 'Gabas', 'Dela Peña', 'fernando.delapeña@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:05', 'Victoriano Unay', '200513977'),
('1304115', 'Donald', 'Navarro', 'Soliman', 'donald.soliman@mitsukoshimotors.com', '7223333', 23, 359, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:55', 'Richmond Ngan', '200513977'),
('1304122', 'King Ramces', 'Tolentino', 'Calderon', 'kingramces.calderon@mitsukoshimotors.com', '7223333', 23, 16, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:54', 'Richmond Ngan', '200513977'),
('1304127', 'Annabel', 'Tayoto', 'Oclares', 'annabel.oclares@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:57', 'Richmond Ngan', '200513977'),
('1304130', 'Ma. Kharla', 'Opon', 'Mendoza', 'makharla.mendoza@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:33', 'Richmond Ngan', '200513977'),
('1305068', 'Rosechell', 'Madarang', 'Mendoza', 'rosechell.mendoza@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:34', 'Richmond Ngan', '200513977'),
('1305079', 'Maribel', 'Mesta', 'Albao', 'maribel.albao@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:10', 'Richmond Ngan', '200513977'),
('1305080', 'Ma. Marla', 'De Chavez', 'Miña', 'mamarla.mina@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:36', 'Richmond Ngan', '200513977'),
('1305081', 'Jesica', 'Caligaya', 'Andaya', 'jesica.andaya@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:26', 'Richmond Ngan', '200513977'),
('1305087', 'Melanie', 'Pabrua', 'Beso', 'melanie.beso@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:37', 'Richmond Ngan', '200513977'),
('1305089', 'Adelo', 'Cabillian', 'Valdemar', 'adelo.valdemar@mitsukoshimotors.com', '7223333', 24, 336, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:34', 'Victoriano Unay', '200513977'),
('1306077', 'Ronald', 'Padua', 'Tagalog', 'ronald.tagalog@mitsukoshimotors.com', '7223333', 24, 395, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:07', 'Victoriano Unay', '200513977'),
('1306082', 'Edmond Ralph', 'Conanan', 'Morales', 'edmondralph.morales@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:34', 'Richmond Ngan', '200513977'),
('1306107', 'Jeremie', 'Ceballo', 'Bongat', 'jeremie.bongat@mitsukoshimotors.com', '7223333', 23, 16, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:35', 'Richmond Ngan', '200513977'),
('1307025', 'Jone', 'Siaotong', 'Labrador', 'jone.labrador@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:38', 'Richmond Ngan', '200513977'),
('1307026', 'Allan Paolo', 'Quesada', 'Burce', 'allanpaolo.burce@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:28', 'Richmond Ngan', '200513977'),
('1307079', 'Ryan', 'Fernandez', 'Bumatay', 'ryan.bumatay@mitsukoshimotors.com', '7223333', 23, 388, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:29', 'Richmond Ngan', '200513977'),
('1307107', 'Mylene', 'Venus', 'Abadilla', 'mylene.abadilla@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:12', 'Richmond Ngan', '200513977'),
('1307150', 'Elmer', 'Pellero', 'Masgong II', 'elmer.masgongii@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:58', 'Richmond Ngan', '200513977'),
('1308001', 'Victoriano', 'Altiz', 'Unay', 'victoriano.unay@mitsukoshimotors.com', '7223333', 24, 337, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:36', 'Richmond Ngan', '200513977'),
('1308136', 'Rodel', 'Aquino', 'Ancheta', 'rodel.ancheta@mitsukoshimotors.com', '7223333', 24, 335, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:37', 'Victoriano Unay', '200513977'),
('1309012', 'Mickleson', '-', 'Betache', 'mickleson.betache@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:31', 'Richmond Ngan', '200513977'),
('1309130', 'Lady Lyka', 'Reyes', 'Villalobos', 'ladylyka.villalobos@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:59', 'Richmond Ngan', '200513977'),
('1310023', 'Byron', 'Liporada', 'Rabanera', 'byron.rabanera@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:32', 'Richmond Ngan', '200513977'),
('1310051', 'Wilson', 'Barahim', 'Bumanglag', 'wilson.bumanglag@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:33', 'Richmond Ngan', '200513977'),
('1310078', 'Paul', 'Bala', 'Lamsen', 'paul.lamsen@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:39', 'Victoriano Unay', '200513977'),
('1310098', 'Angelo', 'Bordeos', 'Buelo', 'angelo.buelo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:40', 'Richmond Ngan', '200513977'),
('1310115', 'Rommel', 'Lagrosa', 'Lampara', 'rommel.lampara@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:08', 'Victoriano Unay', '200513977'),
('1311011', 'Maria Reina', 'Cabahug', 'Coronel', 'mariareina.coronel@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:35', 'Richmond Ngan', '200513977'),
('1311048', 'Sarah', 'De Guzman', 'Dichoso', 'sarah.dichoso@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:13', 'Richmond Ngan', '200513977'),
('1311049', 'Sizzle', 'Balleta', 'Donguya', 'sizzle.donguya@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:35', 'Richmond Ngan', '200513977'),
('1311067', 'Jerry', 'Idoy', 'Quirante', 'jerry.quirante@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:40', 'Victoriano Unay', '200513977'),
('1311146', 'Rocel', 'Dela Vega', 'Guadalupe', 'rocel.guadalupe@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:41', 'Victoriano Unay', '200513977'),
('1312107', 'Mark', 'Dela Cruz', 'Tenorio', 'mark.tenorio@mitsukoshimotors.com', '7223333', 23, 362, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:36', 'Richmond Ngan', '200513977'),
('1401009', 'Francisco', 'Tavas', 'Veroya Jr.', 'francisco.veroyajr@mitsukoshimotors.com', '7223333', 23, 369, 79, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:03', 'Richmond Ngan', '200513977'),
('1401028', 'Harvin', 'Morales', 'Revilla', 'harvin.revilla@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:10', 'Victoriano Unay', '200513977'),
('1402074', 'Rey', 'Diay', 'Laos', 'rey.laos@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:11', 'Victoriano Unay', '200513977'),
('1402212', 'Markwin', 'Aranda', 'Tagle', 'markwin.tagle@mitsukoshimotors.com', '7223333', 24, 16, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:12', 'Victoriano Unay', '200513977'),
('1403077', 'Ariel', 'Colets', 'Vijuan', 'ariel.vijuan@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:14', 'Victoriano Unay', '200513977'),
('1403083', 'Ma. Luisa', 'Lutap', 'Acido', 'maluisa.acido@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:01', 'Richmond Ngan', '200513977'),
('1403084', 'Diana Jessica', 'Zubiri', 'De Lara', 'dianajessica.delara@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:02', 'Richmond Ngan', '200513977'),
('1403089', 'Richard', 'Pid', 'Nabua', 'richard.nabua@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:35', 'Richmond Ngan', '200513977'),
('1403096', 'Leonardo', 'Moje', 'Florendo Jr.', 'leonardo.florendojr@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:43', 'Victoriano Unay', '200513977'),
('1403098', 'Jestoni', 'Villacencio', 'Hugo', 'jestoni.hugo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:45', 'Victoriano Unay', '200513977'),
('1403099', 'Eduardo', 'Navales', 'Esquejo', 'eduardo.esquejo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:46', 'Victoriano Unay', '200513977'),
('1403100', 'Jayson', 'Magno', 'Yongco', 'jayson.yongco@mitsukoshimotors.com', '7223333', 24, 339, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:48', 'Victoriano Unay', '200513977'),
('1403109', 'Jonalyn', 'Nuguit', 'Cepillo', 'jonalyn.cepillo@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:03', 'Richmond Ngan', '200513977'),
('1403111', 'Jenar', 'Lucero', 'Luzon', 'jenar.luzon@mitsukoshimotors.com', '7223333', 23, 377, 83, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:41', 'Richmond Ngan', '200513977'),
('1403112', 'Ronnie', '-', 'Timkang', 'ronnie.timkang@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:15', 'Victoriano Unay', '200513977'),
('1403127', 'Ivan Samuel', 'Lauta', 'Nas', 'ivansamuel.nas@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:36', 'Richmond Ngan', '200513977'),
('1403128', 'Cherrylyn', 'Antolin', 'Tandoy', 'cherrylyn.tandoy@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:05', 'Richmond Ngan', '200513977'),
('1403131', 'Ronnie', 'Pimentel', 'Cubol', 'ronnie.cubol@mitsukoshimotors.com', '7223333', 23, 316, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:42', 'Richmond Ngan', '200513977'),
('1403200', 'Ernie', 'Flores', 'Belegantol', 'ernie.belegantol@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:17', 'Victoriano Unay', '200513977'),
('1403201', 'Othelo', 'B.', 'Teope', 'othelo.teope@mitsukoshimotors.com', '7223333', 24, 340, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:49', 'Victoriano Unay', '200513977'),
('1403203', 'Andy', 'Sedeño', 'Vega', 'andy.vega@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:18', 'Victoriano Unay', '200513977'),
('1404004', 'Mario Vincent', 'Avila', 'Reyes Jr.', 'mariovincent.reyesjr@mitsukoshimotors.com', '7223333', 23, 316, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:43', 'Richmond Ngan', '200513977'),
('1404014', 'Eden', 'Copla', 'Cortiz', 'eden.cortiz@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:39', 'Richmond Ngan', '200513977'),
('1404043', 'Mark', 'Francisco', 'Sangangbayan', 'mark.sangangbayan@mitsukoshimotors.com', '7223333', 23, 316, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:45', 'Richmond Ngan', '200513977'),
('1404115', 'Edward', 'Saria', 'Tibayan', 'edward.tibayan@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:46', 'Richmond Ngan', '200513977'),
('1405003', 'Charmaine', 'Gapate', 'Zaulda', 'charmaine.zaulda@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:14', 'Richmond Ngan', '200513977'),
('1405009', 'Kris Robert', 'Umagat', 'Gonzales', 'krisrobert.gonzales@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:47', 'Richmond Ngan', '200513977'),
('1405010', 'Randy', 'Gonzalo', 'Segundo', 'randy.segundo@mitsukoshimotors.com', '7223333', 23, 317, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:49', 'Richmond Ngan', '200513977'),
('1405012', 'Zalde', 'Apao', 'Galas', 'zalde.galas@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:50', 'Richmond Ngan', '200513977'),
('1405017', 'Krisialie', 'Albo', 'Tacalan', 'krisialie.tacalan@mitsukoshimotors.com', '7223333', 23, 16, 87, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:59', 'Richmond Ngan', '200513977'),
('1405065', 'Sarah Gaile', 'Sales', 'Reyes', 'sarahgaile.reyes@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:16', 'Richmond Ngan', '200513977'),
('1405091', 'Ronald', 'David', 'Zapanta', 'ronald.zapanta@mitsukoshimotors.com', '7223333', 23, 316, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:52', 'Richmond Ngan', '200513977'),
('1405093', 'Mario', 'Ganiban', 'Simon', 'mario.simon@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:53', 'Richmond Ngan', '200513977'),
('1405128', 'Florante', 'Sabino', 'Berido', 'florante.berido@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:51', 'Victoriano Unay', '200513977'),
('1405172', 'Merson', 'Macopia', 'Baccay', 'merson.baccay@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:37', 'Richmond Ngan', '200513977'),
('1405217', 'Mico', 'Umali', 'Saligumba', 'mico.saligumba@mitsukoshimotors.com', '7223333', 24, 331, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:52', 'Victoriano Unay', '200513977'),
('1405220', 'Mervin', 'Santillar', 'Cortez', 'mervin.cortez@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:53', 'Victoriano Unay', '200513977'),
('1405226', 'Ramon', 'Amores', 'Federipe', 'ramon.federipe@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:55', 'Victoriano Unay', '200513977'),
('1406018', 'Mendel Gregor', 'A.', 'Nicolas', 'mendelgregor.nicolas@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:37', 'Richmond Ngan', '200513977'),
('1406119', 'Stephanie Joy', 'Palaruan', 'Mantala', 'stephaniejoy.mantala@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:40', 'Richmond Ngan', '200513977'),
('1406122', 'Joana', 'Marin', 'Mendoza', 'joana.mendoza@mitsukoshimotors.com', '7223333', 23, 359, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:06', 'Richmond Ngan', '200513977'),
('1407001', 'Elgin', 'Pelicano', 'Fulgencio', 'elgin.fulgencio@mitsukoshimotors.com', '7223333', 23, 5, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:54', 'Richmond Ngan', '200513977'),
('1407002', 'Harry', 'Santos', 'Militar', 'harry.militar@mitsukoshimotors.com', '7223333', 23, 318, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:56', 'Richmond Ngan', '200513977'),
('1407004', 'Zemirah', 'Catubigan', 'Rodenas', 'zemirah.rodenas@mitsukoshimotors.com', '7223333', 23, 390, 86, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:53', 'Richmond Ngan', '200513977'),
('1407026', 'Dennis', 'Villamin', 'Reyes', 'dennis.reyes@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:57', 'Richmond Ngan', '200513977'),
('1407031', 'Rodolfo', 'Cubol', 'Colina', 'rodolfo.colina@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:56', 'Victoriano Unay', '200513977'),
('1407033', 'Edwin', 'Pacificar', 'Cubol', 'edwin.cubol@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:58', 'Victoriano Unay', '200513977'),
('1407034', 'Ralph Alvin', 'Roquiño', 'Panuncio', 'ralphalvin.panuncio@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:59', 'Victoriano Unay', '200513977'),
('1407040', 'Carfil', 'Aranda', 'Memije', 'carfil.memije@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:01', 'Victoriano Unay', '200513977'),
('1407042', 'Henry', 'Endraca', 'Felices', 'henry.felices@mitsukoshimotors.com', '7223333', 23, 318, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:59', 'Richmond Ngan', '200513977'),
('1407065', 'Santiago', 'Sarmiento', 'Gonzales', 'santiago.gonzales@mitsukoshimotors.com', '7223333', 23, 376, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:37', 'Richmond Ngan', '200513977'),
('1407250', 'Jervie', 'Alcanse', 'Abdon', 'jervie.abdon@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:02', 'Victoriano Unay', '200513977'),
('1408002', 'Alfonso', 'Macinas', 'Balane Jr.', 'alfonso.balanejr@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:00', 'Richmond Ngan', '200513977'),
('1408003', 'Jandilbert', 'Camile', 'Arca', 'jandilbert.arca@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:02', 'Richmond Ngan', '200513977'),
('1408006', 'Hernando', 'Santos', 'Vega Jr.', 'hernando.vegajr@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:03', 'Richmond Ngan', '200513977'),
('1408048', 'Cherry', 'Laguna', 'Alaba', 'cherry.alaba@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:17', 'Richmond Ngan', '200513977'),
('1408067', 'Neil', 'Ramones', 'Montilla', 'neil.montilla@mitsukoshimotors.com', '7223333', 23, 5, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:04', 'Richmond Ngan', '200513977'),
('1408073', 'Pascual', 'Cabanias', 'Dagala', 'pascual.dagala@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:19', 'Victoriano Unay', '200513977'),
('1408089', 'Renato', 'Lucelo', 'Pamanian', 'renato.pamanian@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:21', 'Victoriano Unay', '200513977'),
('1408102', 'Ryan', 'De Guzman', 'Maningas', 'ryan.maningas@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:06', 'Richmond Ngan', '200513977'),
('1409104', 'Melvin Roy', 'Garrote', 'Dablo', 'melvinroy.dablo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:07', 'Richmond Ngan', '200513977'),
('1409107', 'Jhomar', 'Momo', 'Alinsunorin', 'jhomar.alinsunorin@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:09', 'Richmond Ngan', '200513977'),
('1409110', 'Anthony', 'Hangad', 'Gomez', 'anthony.gomez@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:10', 'Richmond Ngan', '200513977'),
('1409111', 'Marvin', 'B.', 'Vales', 'marvin.vales@mitsukoshimotors.com', '7223333', 23, 5, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:11', 'Richmond Ngan', '200513977'),
('1409113', 'Leonilo', 'A.', 'Tano', 'leonilo.tano@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:13', 'Richmond Ngan', '200513977'),
('1409114', 'Alger', 'M.', 'Dabalos', 'alger.dabalos@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:14', 'Richmond Ngan', '200513977'),
('1409119', 'Melvin', 'Turbanada', 'Lobos', 'melvin.lobos@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:39', 'Richmond Ngan', '200513977'),
('1409175', 'Rachel', 'Salado', 'Ibrahim', 'rachel.ibrahim@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:08', 'Richmond Ngan', '200513977'),
('1410030', 'Kimberly', 'Rances', 'Leal', 'kimberly.leal@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:38', 'Richmond Ngan', '200513977'),
('1410031', 'Salome', 'De Guzman', 'Nevado', 'salome.nevado@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:18', 'Richmond Ngan', '200513977'),
('1410122', 'John Michael', 'Rapsing', 'Rosales', 'johnmichael.rosales@mitsukoshimotors.com', '7223333', 24, 334, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:04', 'Victoriano Unay', '200513977'),
('1410129', 'Jayrald', 'Restaba', 'Emprese', 'jayrald.emprese@mitsukoshimotors.com', '7223333', 24, 329, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:05', 'Victoriano Unay', '200513977'),
('1410132', 'Felipe', 'Molos', 'Leonor', 'felipe.leonor@mitsukoshimotors.com', '7223333', 24, 334, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:06', 'Victoriano Unay', '200513977'),
('1410170', 'Jan Erika', 'Bontia', 'Lopez', 'janerika.lopez@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:39', 'Richmond Ngan', '200513977'),
('1410203', 'Leonardo', 'Garcia', 'Manuel', 'leonardo.manuel@mitsukoshimotors.com', '7223333', 25, 13, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:13', 'Quintos Villoso', '200513977'),
('1411083', 'Nessa', 'Huab', 'Villa', 'nessa.villa@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:09', 'Richmond Ngan', '200513977'),
('1411104', 'Eljay', 'Villanueva', 'Millare', 'eljay.millare@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:08', 'Victoriano Unay', '200513977'),
('1411106', 'Jonathan', 'Bonifacio', 'Paler', 'jonathan.paler@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:09', 'Victoriano Unay', '200513977'),
('1411115', 'Rey', '-', 'Lopez', 'rey.lopez@mitsukoshimotors.com', '7223333', 24, 342, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:11', 'Victoriano Unay', '200513977'),
('1411125', 'Nestor', 'Galingan', 'Mangubat Jr.', 'nestor.mangubatjr@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:12', 'Victoriano Unay', '200513977'),
('1411126', 'Cyrill', 'Mangalino', 'Elago', 'cyrill.elago@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:13', 'Victoriano Unay', '200513977'),
('1411133', 'Harold', 'Lumapag', 'Bernaldez', 'harold.bernaldez@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:15', 'Victoriano Unay', '200513977'),
('1411149', 'Simplicio', 'Romulo', 'Garay Jr.', 'simplicio.garayjr@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:15', 'Richmond Ngan', '200513977'),
('1411155', 'Jingky', 'Avila', 'Denosta', 'jingky.denosta@mitsukoshimotors.com', '7223333', 23, 320, 75, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:51', 'Richmond Ngan', '200513977'),
('1411234', 'Kenneth', 'De Jesus', 'Carreon', 'kenneth.carreon@mitsukoshimotors.com', '7223333', 25, 372, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:14', 'Quintos Villoso', '200513977'),
('1411255', 'Jonathan', 'Quiambao', 'Castillo', 'jonathan.castillo@mitsukoshimotors.com', '7223333', 23, 365, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:56', 'Richmond Ngan', '200513977'),
('1411276', 'Jerry', 'Agravante', 'Villanueva', 'jerry.villanueva@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:16', 'Victoriano Unay', '200513977'),
('1411277', 'Louie', 'Villanueva', 'Fabon', 'louie.fabon@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:18', 'Victoriano Unay', '200513977'),
('1411278', 'Rodel', 'Eleserio', 'Villaganas', 'rodel.villaganas@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:19', 'Victoriano Unay', '200513977'),
('1411283', 'Juanito', 'Belen', 'Castro', 'juanito.castro@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:22', 'Victoriano Unay', '200513977'),
('1411284', 'Dennis', 'Lambo', 'Tan', 'dennis.tan@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:21', 'Victoriano Unay', '200513977'),
('1412007', 'Salne', 'Naret', 'Dahuya', 'salne.dahuya@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:17', 'Richmond Ngan', '200513977'),
('1412012', 'Quinn Ojie', '-', 'Tocmoc', 'quinnojie.tocmoc@mitsukoshimotors.com', '7223333', 24, 343, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:22', 'Victoriano Unay', '200513977'),
('1412041', 'Al Jonn', 'Abaigar', 'Santos', 'aljonn.santos@mitsukoshimotors.com', '7223333', 25, 372, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:16', 'Quintos Villoso', '200513977'),
('1412109', 'Herald', 'Rosario', 'Magleo', 'herald.magleo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:18', 'Richmond Ngan', '200513977'),
('1412177', 'Charisel', 'Tumaneng', 'Guillermo', 'charisel.guillermo@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:24', 'Victoriano Unay', '200513977'),
('1412178', 'Romaldo', 'Quivedo', 'Rivas Jr.', 'romaldo.rivasjr@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:25', 'Victoriano Unay', '200513977'),
('1501024', 'Judy Ann', 'Valle', 'Salamat', 'judyann.salamat@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:11', 'Richmond Ngan', '200513977'),
('1501079', 'Antonio', 'Abas', 'Vargas', 'antonio.vargas@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:20', 'Richmond Ngan', '200513977'),
('1501094', 'Sheila May', 'Lazado', 'Bonilla', 'sheilamay.bonilla@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:20', 'Richmond Ngan', '200513977'),
('1502006', 'Rosell Ann', 'Santos', 'Liwanag', 'rosellann.liwanag@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:12', 'Richmond Ngan', '200513977'),
('1502007', 'Rosal', 'Barcebal', 'Quiñones', 'rosal.quiñones@mitsukoshimotors.com', '7223333', 23, 370, 80, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:05', 'Richmond Ngan', '200513977'),
('1502060', 'Hubert', 'Manalo', 'Geronimo', 'hubert.geronimo@mitsukoshimotors.com', '7223333', 23, 317, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:21', 'Richmond Ngan', '200513977'),
('1502113', 'Gia', 'Mission', 'Dy', 'gia.dy@mitsukoshimotors.com', '7223333', 23, 16, 87, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:00', 'Richmond Ngan', '200513977'),
('1502125', 'Aiza', 'Enciso', 'Villareal', 'aiza.villareal@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:40', 'Richmond Ngan', '200513977'),
('1502126', 'Francis', 'Aquino', 'Trinidad', 'francis.trinidad@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:42', 'Richmond Ngan', '200513977'),
('1502193', 'Joel', 'Alfonso', 'Cetra', 'joel.cetra@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:24', 'Victoriano Unay', '200513977'),
('1502194', 'Arthur Gerald', 'Loja', 'Longares', 'arthurgerald.longares@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:25', 'Victoriano Unay', '200513977'),
('1502196', 'Isabelo', 'Salisi', 'Masaclao', 'isabelo.masaclao@mitsukoshimotors.com', '7223333', 24, 341, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:27', 'Victoriano Unay', '200513977'),
('1503081', 'Edison', 'Coke', 'Pizarra', 'edison.pizarra@mitsukoshimotors.com', '7223333', 23, 366, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:57', 'Richmond Ngan', '200513977'),
('1503104', 'Isabel Francheska', 'Espiña', 'Reyes', 'isabelfrancheska.reyes@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:13', 'Richmond Ngan', '200513977'),
('1503105', 'Evelyn', 'Garces', 'Ronquillo', 'evelyn.ronquillo@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:21', 'Richmond Ngan', '200513977'),
('1503107', 'Giraulie', 'Cerezo', 'Osma', 'giraulie.osma@mitsukoshimotors.com', '7223333', 24, 16, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:27', 'Victoriano Unay', '200513977'),
('1503108', 'Dante', 'Danganan', 'Pangan', 'dante.pangan@mitsukoshimotors.com', '7223333', 23, 363, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:58', 'Richmond Ngan', '200513977'),
('1503110', 'Andres', 'Drilon', 'Japona Jr.', 'andres.japonajr@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:28', 'Victoriano Unay', '200513977'),
('1503111', 'Jerome', 'Austria', 'De Guzman', 'jerome.deguzman@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:15', 'Richmond Ngan', '200513977'),
('1503118', 'Richard', 'Madayag', 'Orongan', 'richard.orongan@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:30', 'Victoriano Unay', '200513977'),
('1503132', 'Jefferson', 'Famoso', 'Vasquez', 'jefferson.vasquez@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:31', 'Victoriano Unay', '200513977'),
('1503135', 'Raymart', 'Gutierrez', 'Amarillo', 'raymart.amarillo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:32', 'Victoriano Unay', '200513977'),
('1503145', 'Walter', 'Orbeta', 'Micosa', 'walter.micosa@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:34', 'Victoriano Unay', '200513977'),
('1503149', 'Manolo', 'Claveria', 'An Jr.', 'manolo.anjr@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:35', 'Victoriano Unay', '200513977'),
('1503151', 'Jessie', 'Lucas', 'Lubasan', 'jessie.lubasan@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:37', 'Victoriano Unay', '200513977'),
('1503178', 'Abundio', 'Gonzales', 'Edem', 'abundio.edem@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:28', 'Victoriano Unay', '200513977'),
('1503220', 'Kheyvee', 'Bilog', 'Lanting', 'kheyvee.lanting@mitsukoshimotors.com', '7223333', 24, 344, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:38', 'Victoriano Unay', '200513977'),
('1503228', 'Vencent', 'Orlanda', 'Montiadora', 'vencent.montiadora@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:40', 'Victoriano Unay', '200513977'),
('1503233', 'Niño Ulyses', 'Tamayo', 'Magnase', 'niñoulyses.magnase@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:41', 'Victoriano Unay', '200513977'),
('1503243', 'Mikko Robert', 'Romano', 'Denum', 'mikkorobert.denum@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:42', 'Victoriano Unay', '200513977'),
('1503244', 'Edwin', 'Emano', 'Manabat', 'edwin.manabat@mitsukoshimotors.com', '7223333', 24, 16, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:29', 'Victoriano Unay', '200513977'),
('1503252', 'Esmael', 'Dela Cruz', 'Banaag', 'esmael.banaag@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:44', 'Victoriano Unay', '200513977'),
('1503257', 'Reynaldo', 'Marasigan', 'Honra Jr.', 'reynaldo.honrajr@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:45', 'Victoriano Unay', '200513977'),
('1503258', 'Jobet', 'Sandoval', 'Natividad', 'jobet.natividad@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:47', 'Victoriano Unay', '200513977'),
('1503260', 'Francis Jay', 'Santos', 'Jadion', 'francisjay.jadion@mitsukoshimotors.com', '7223333', 24, 346, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:48', 'Victoriano Unay', '200513977'),
('1503261', 'Sherwin', 'Barangas', 'Añonuevo', 'sherwin.añonuevo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:50', 'Victoriano Unay', '200513977'),
('1503262', 'Joshua', 'Tabor', 'Magadon', 'joshua.magadon@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:51', 'Victoriano Unay', '200513977'),
('1503265', 'Ronnel', 'Aquias', 'Lontoc', 'ronnel.lontoc@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:52', 'Victoriano Unay', '200513977'),
('1503266', 'Gabriel', '-', 'Bulac', 'gabriel.bulac@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:54', 'Victoriano Unay', '200513977'),
('1503274', 'Leo', 'Servano', 'Lucendo', 'leo.lucendo@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:55', 'Victoriano Unay', '200513977'),
('1503276', 'Saturnino', 'Sain', 'Fetalvo Jr.', 'saturnino.fetalvojr@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:31', 'Victoriano Unay', '200513977'),
('1503277', 'Joemar', 'Balboa', 'Datoon', 'joemar.datoon@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:57', 'Victoriano Unay', '200513977'),
('1503280', 'Patrick', 'Servito', 'Prado', 'patrick.prado@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:58', 'Victoriano Unay', '200513977'),
('1504003', 'Anthony', 'Carandang', 'Reyes', 'anthony.reyes@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:22', 'Richmond Ngan', '200513977'),
('1504040', 'Rochelle', 'Cuales', 'Cabiltes', 'rochelle.cabiltes@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:42', 'Richmond Ngan', '200513977');
INSERT INTO `mmpi_table_of_organization` (`employee_id`, `firs_name`, `middle_name`, `last_name`, `email`, `contact_num`, `location_id`, `designation_id`, `department_id`, `status_id`, `position_id`, `date_updated`, `date_created`, `immediate_superior`, `immediate_superior_id`) VALUES
('1504041', 'Paolo Roberto', 'Acosta', 'Perez', 'paoloroberto.perez@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:43', 'Richmond Ngan', '200513977'),
('1504042', 'Mhar Vane', 'Alvarez', 'Villaflor', 'mharvane.villaflor@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:44', 'Richmond Ngan', '200513977'),
('1504097', 'Gerby', 'Sudoy', 'Edrial', 'gerby.edrial@mitsukoshimotors.com', '7223333', 23, 310, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:46', 'Richmond Ngan', '200513977'),
('1504098', 'Nhel Patrick', 'Abarro', 'Sacramento', 'nhelpatrick.sacramento@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:16', 'Richmond Ngan', '200513977'),
('1504099', 'Don Abraham', 'Aquino', 'Manubay', 'donabraham.manubay@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:38', 'Richmond Ngan', '200513977'),
('1504100', 'John Vincent', 'Salamat', 'Quitlong', 'johnvincent.quitlong@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:40', 'Richmond Ngan', '200513977'),
('1504126', 'Marinel', 'Galupo', 'Rivera', 'marinel.rivera@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:41', 'Richmond Ngan', '200513977'),
('1505029', 'Hadasa', 'Encinas', 'Nueve', 'hadasa.nueve@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:23', 'Richmond Ngan', '200513977'),
('1505030', 'Algen', 'Langcamon', 'Gantiao', 'algen.gantiao@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:41', 'Richmond Ngan', '200513977'),
('1505031', 'John Paul', 'Talosig', 'Sol', 'johnpaul.sol@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:17', 'Richmond Ngan', '200513977'),
('1505032', 'Edsa Meriel', 'Ogsimer', 'Granadoso', 'edsameriel.granadoso@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:24', 'Richmond Ngan', '200513977'),
('1505053', 'Cris John', 'Palad', 'Felix', 'crisjohn.felix@mitsukoshimotors.com', '7223333', 25, 373, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:17', 'Quintos Villoso', '200513977'),
('1505066', 'Dennis', 'Ballenjare', 'Bumaya', 'dennis.bumaya@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:32', 'Victoriano Unay', '200513977'),
('1505067', 'Bernardo', 'Guris', 'Pucio', 'bernardo.pucio@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:33', 'Victoriano Unay', '200513977'),
('1505101', 'John Brylle', 'Capistrano', 'Cabrera', 'johnbrylle.cabrera@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:37:59', 'Victoriano Unay', '200513977'),
('1505102', 'Jayson', 'De Lima', 'Dimanarig', 'jayson.dimanarig@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:01', 'Victoriano Unay', '200513977'),
('1505103', 'Jecky', 'Patalita', 'Inso', 'jecky.inso@mitsukoshimotors.com', '7223333', 24, 347, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:02', 'Victoriano Unay', '200513977'),
('1505104', 'Eloisa', 'Battad', 'Domingo', 'eloisa.domingo@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:25', 'Richmond Ngan', '200513977'),
('1505109', 'Arianne Mae', 'Millena', 'Garay', 'ariannemae.garay@mitsukoshimotors.com', '7223333', 23, 16, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:38', 'Richmond Ngan', '200513977'),
('1505117', 'Melanie', 'Binibini', 'Domingo', 'melanie.domingo@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:43', 'Richmond Ngan', '200513977'),
('1505118', 'Rex', 'Catibog', 'Matala', 'rex.matala@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:42', 'Richmond Ngan', '200513977'),
('1505119', 'Mark Irvin', 'Cruz', 'Veneracion', 'markirvin.veneracion@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:24', 'Richmond Ngan', '200513977'),
('1505120', 'Enday Nita', 'Lleno', 'Gonzales', 'endaynita.gonzales@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:27', 'Richmond Ngan', '200513977'),
('1505144', 'Raymond', 'Quiñones', 'Onnagan', 'raymond.onnagan@mitsukoshimotors.com', '7223333', 23, 391, 86, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:55', 'Richmond Ngan', '200513977'),
('1505145', 'Ron Braylle', 'Basister', 'Borromeo', 'ronbraylle.borromeo@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:45', 'Richmond Ngan', '200513977'),
('1505162', 'Ar-Jay', 'Laveña', 'Regonaos', 'ar-jay.regonaos@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:04', 'Victoriano Unay', '200513977'),
('1505163', 'Dennis Rod', 'Panganiban', 'Alcantara', 'dennisrod.alcantara@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:05', 'Victoriano Unay', '200513977'),
('1505164', 'Jofrey', 'Costin', 'Bugarin', 'jofrey.bugarin@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:06', 'Victoriano Unay', '200513977'),
('1505165', 'Jay Paul', 'Castor', 'Padernal', 'jaypaul.padernal@mitsukoshimotors.com', '7223333', 24, 348, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:08', 'Victoriano Unay', '200513977'),
('1505166', 'Henry', 'Pilo', 'Yu', 'henry.yu@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:09', 'Victoriano Unay', '200513977'),
('1505167', 'Mark Joseph', 'Balares', 'Macalalad', 'markjoseph.macalalad@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:11', 'Victoriano Unay', '200513977'),
('1505168', 'John Carlo', 'Galanida', 'Masanga', 'johncarlo.masanga@mitsukoshimotors.com', '7223333', 24, 349, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:12', 'Victoriano Unay', '200513977'),
('1505169', 'Roy', 'De Guzman', 'Garcia', 'roy.garcia@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:14', 'Victoriano Unay', '200513977'),
('1505170', 'Ronnie', 'Beniza', 'Balawang', 'ronnie.balawang@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:15', 'Victoriano Unay', '200513977'),
('1505171', 'Aldrich Allen', 'Mesa', 'Lagana', 'aldrichallen.lagana@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:16', 'Victoriano Unay', '200513977'),
('1505173', 'Alvin', 'Segaya', 'Alay-ay', 'alvin.alay-ay@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:18', 'Victoriano Unay', '200513977'),
('1505174', 'Kederson Keith', 'Magno', 'Adrales', 'kedersonkeith.adrales@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:19', 'Victoriano Unay', '200513977'),
('1505175', 'Fernando', 'Oliver', 'Dichoso Jr.', 'fernando.dichosojr@mitsukoshimotors.com', '7223333', 24, 350, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:21', 'Victoriano Unay', '200513977'),
('1505176', 'Jay-Ar', 'Orubia', 'Pakingan', 'jay-ar.pakingan@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:23', 'Victoriano Unay', '200513977'),
('1505179', 'Jeffrey', 'Joson', 'Aranda', 'jeffrey.aranda@mitsukoshimotors.com', '7223333', 24, 345, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:24', 'Victoriano Unay', '200513977'),
('1505180', 'Havib', 'Creer', 'Bilog', 'havib.bilog@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:25', 'Victoriano Unay', '200513977'),
('1505181', 'Donnie', 'Palencia', 'De Lima', 'donnie.delima@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:27', 'Victoriano Unay', '200513977'),
('1505182', 'Nestor', 'Bueno', 'Mañibo', 'nestor.mañibo@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:28', 'Victoriano Unay', '200513977'),
('1505183', 'Gerardo', 'Perez', 'Macalua Jr.', 'gerardo.macaluajr@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:29', 'Victoriano Unay', '200513977'),
('1505184', 'Arnold', 'Lunasco', 'Geneblazo', 'arnold.geneblazo@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:31', 'Victoriano Unay', '200513977'),
('1505185', 'Rhoiel', 'Reyes', 'Ranido', 'rhoiel.ranido@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:32', 'Victoriano Unay', '200513977'),
('1505186', 'Eugene', 'Formento', 'Merilles', 'eugene.merilles@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:34', 'Victoriano Unay', '200513977'),
('1505187', 'Jomar', 'Magnanao', 'Tamor', 'jomar.tamor@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:35', 'Victoriano Unay', '200513977'),
('1505189', 'James', 'Cagadoc', 'Pagulayan', 'james.pagulayan@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:36', 'Victoriano Unay', '200513977'),
('1505190', 'Renz Allen', 'De Jesus', 'Perdonio', 'renzallen.perdonio@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:38', 'Victoriano Unay', '200513977'),
('1505191', 'Arjohn', 'De Jesus', 'Perez', 'arjohn.perez@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:39', 'Victoriano Unay', '200513977'),
('1505192', 'Jhasfer', 'Micosa', 'Mercado', 'jhasfer.mercado@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:41', 'Victoriano Unay', '200513977'),
('1505193', 'Angelito', 'Gutierrez', 'Pintor', 'angelito.pintor@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:42', 'Victoriano Unay', '200513977'),
('1505194', 'Raymund', 'Anaño', 'Adaptante', 'raymund.adaptante@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:44', 'Victoriano Unay', '200513977'),
('1505195', 'Mark Aldrin', 'De Ramos', 'Bautista', 'markaldrin.bautista@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:45', 'Victoriano Unay', '200513977'),
('1505197', 'Erwin', 'Pangilinan', 'Galura', 'erwin.galura@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:46', 'Victoriano Unay', '200513977'),
('1505198', 'Terence', 'De Laryarte', 'Andres', 'terence.andres@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:48', 'Victoriano Unay', '200513977'),
('1505200', 'Joel', 'Umandap', 'Ramos', 'joel.ramos@mitsukoshimotors.com', '7223333', 24, 351, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:49', 'Victoriano Unay', '200513977'),
('1505206', 'Alvin John', 'Aranda', 'Mapalad', 'alvinjohn.mapalad@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:51', 'Victoriano Unay', '200513977'),
('1505207', 'Wilfredo', 'Levardo', 'Restrivera', 'wilfredo.restrivera@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:35', 'Victoriano Unay', '200513977'),
('1505208', 'Paulo', 'Fullente', 'Agao', 'paulo.agao@mitsukoshimotors.com', '7223333', 24, 346, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:52', 'Victoriano Unay', '200513977'),
('1505209', 'Ernie', 'Catalan', 'Serquillos', 'ernie.serquillos@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:54', 'Victoriano Unay', '200513977'),
('1506001', 'Manilyn', 'Martinez', 'Villas', 'manilyn.villas@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:28', 'Richmond Ngan', '200513977'),
('1506002', 'John Philip', 'Clorado', 'Lapiguira', 'johnphilip.lapiguira@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:46', 'Richmond Ngan', '200513977'),
('1506003', 'Gerald', 'Palir', 'Capanas', 'gerald.capanas@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:44', 'Richmond Ngan', '200513977'),
('1506004', 'Julius', 'Tamayo', 'Montalban', 'julius.montalban@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:47', 'Richmond Ngan', '200513977'),
('1506033', 'Angela Mae', 'Jacobo', 'Curioso', 'angelamae.curioso@mitsukoshimotors.com', '7223333', 25, 374, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:19', 'Quintos Villoso', '200513977'),
('1506056', 'Rosalito', 'Rosales', 'Loyola', 'rosalito.loyola@mitsukoshimotors.com', '7223333', 24, 346, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:56', 'Victoriano Unay', '200513977'),
('1506057', 'Ryan', 'Gonzales', 'Rosaldo', 'ryan.rosaldo@mitsukoshimotors.com', '7223333', 23, 367, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:00', 'Richmond Ngan', '200513977'),
('1506064', 'Ruel', 'Quintos', 'Villoso', 'ruel.villoso@mitsukoshimotors.com', '7223333', 25, 9, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:20', 'Richmond Ngan', '200513977'),
('1506136', 'Alberto', 'Gemeniano', 'Setias Jr.', 'alberto.setiasjr@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:25', 'Richmond Ngan', '200513977'),
('1506140', 'Raymund Angelo', '-', 'Buaya', 'raymundangelo.buaya@mitsukoshimotors.com', '7223333', 25, 372, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:22', 'Quintos Villoso', '200513977'),
('1506155', 'Marijune', 'Verana', 'Lucero', 'marijune.lucero@mitsukoshimotors.com', '7223333', 24, 352, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:58', 'Victoriano Unay', '200513977'),
('1506157', 'Michael John', 'Torrion', 'Makinano', 'michaeljohn.makinano@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:38:59', 'Victoriano Unay', '200513977'),
('1506158', 'Ermelito', 'Enrina', 'Villanueva', 'ermelito.villanueva@mitsukoshimotors.com', '7223333', 24, 321, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:01', 'Victoriano Unay', '200513977'),
('1506159', 'Shaun Lucky', 'Ergino', 'De Castro', 'shaunlucky.decastro@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:02', 'Victoriano Unay', '200513977'),
('1506160', 'Diomher', 'Copino', 'Peñero', 'diomher.peñero@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:03', 'Victoriano Unay', '200513977'),
('1506162', 'Daryl', 'Serra', 'Penson', 'daryl.penson@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:05', 'Victoriano Unay', '200513977'),
('1506163', 'Adrian', 'Sulatorio', 'Naynes', 'adrian.naynes@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:06', 'Victoriano Unay', '200513977'),
('1506165', 'Daisa', 'Pacaldo', 'Mainit', 'daisa.mainit@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:19', 'Richmond Ngan', '200513977'),
('1506166', 'Ge-ann', 'Buenaventura', 'Quiboal', 'ge-ann.quiboal@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:49', 'Richmond Ngan', '200513977'),
('1506189', 'Ronnel', 'Gudao', 'Gulane', 'ronnel.gulane@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:08', 'Victoriano Unay', '200513977'),
('1506241', 'Jhefry', 'Buscas', 'David', 'jhefry.david@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:09', 'Victoriano Unay', '200513977'),
('1506243', 'Irene', 'Reyes', 'Balay', 'irene.balay@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:29', 'Richmond Ngan', '200513977'),
('1506244', 'Karren Mae', 'Guadayo', 'Cruz', 'karrenmae.cruz@mitsukoshimotors.com', '7223333', 23, 16, 84, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:43', 'Richmond Ngan', '200513977'),
('1506287', 'Anthony', 'Abesamis', 'Felipe', 'anthony.felipe@mitsukoshimotors.com', '7223333', 25, 372, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:23', 'Quintos Villoso', '200513977'),
('1506312', 'Lovely', 'Bartolome', 'Antonio', 'lovely.antonio@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:50', 'Richmond Ngan', '200513977'),
('1506313', 'Margin', 'Pico', 'Burce', 'margin.burce@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:31', 'Richmond Ngan', '200513977'),
('1506314', 'Jio Carlo', 'Balboa', 'De Leon', 'jiocarlo.deleon@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:52', 'Richmond Ngan', '200513977'),
('1506339', 'Glenn', 'Basatan', 'Reyes', 'glenn.reyes@mitsukoshimotors.com', '7223333', 24, 16, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:36', 'Victoriano Unay', '200513977'),
('1507051', 'Chat Rea', 'Cabaltea', 'Orpilla', 'chatrea.orpilla@mitsukoshimotors.com', '7223333', 25, 374, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:25', 'Quintos Villoso', '200513977'),
('1507052', 'Dario', 'Lacar', 'Mingala Jr.', 'dario.mingalajr@mitsukoshimotors.com', '7223333', 23, 314, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:47', 'Richmond Ngan', '200513977'),
('1507084', 'Albert', 'Pajuay', 'Denulan', 'albert.denulan@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:10', 'Victoriano Unay', '200513977'),
('1507087', 'Kenneth James', 'Mangandi', 'Albofuera', 'kennethjames.albofuera@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:12', 'Victoriano Unay', '200513977'),
('1507088', 'Alvin', 'Ocmer', 'Boco', 'alvin.boco@mitsukoshimotors.com', '7223333', 24, 353, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:13', 'Victoriano Unay', '200513977'),
('1507090', 'Dexzon', 'Suganob', 'Monares', 'dexzon.monares@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:15', 'Victoriano Unay', '200513977'),
('1507091', 'Reynan', 'Mercado', 'Ramos', 'reynan.ramos@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:16', 'Victoriano Unay', '200513977'),
('1507092', 'John Cris', 'Dioneda', 'Parilla', 'johncris.parilla@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:18', 'Victoriano Unay', '200513977'),
('1507119', 'Joy', 'Carado', 'Ygrubay', 'joy.ygrubay@mitsukoshimotors.com', '7223333', 25, 13, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:26', 'Quintos Villoso', '200513977'),
('1507133', 'Jerine', 'Mandap', 'Basco', 'jerine.basco@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:42', 'Richmond Ngan', '200513977'),
('1507147', 'Roda', 'Padilla', 'Lamonte', 'roda.lamonte@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:53', 'Richmond Ngan', '200513977'),
('1507153', 'Reynaldo', 'Cenizal', 'Recido', 'reynaldo.recido@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:19', 'Victoriano Unay', '200513977'),
('1507192', 'Rolie', 'Yabis', 'Ranjo', 'rolie.ranjo@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:54', 'Richmond Ngan', '200513977'),
('1507193', 'Charissa', 'Lugo', 'Miras', 'charissa.miras@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:43', 'Richmond Ngan', '200513977'),
('1507195', 'Pinky', 'Cea', 'Arizala', 'pinky.arizala@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:32', 'Richmond Ngan', '200513977'),
('1507196', 'Mikko', 'Sibal', 'Concepcion', 'mikko.concepcion@mitsukoshimotors.com', '7223333', 23, 368, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:02', 'Richmond Ngan', '200513977'),
('1507251', 'Romel', 'Santiago', 'Arce', 'romel.arce@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:21', 'Victoriano Unay', '200513977'),
('1507253', 'Leoreto', 'Encabo', 'Reyes', 'leoreto.reyes@mitsukoshimotors.com', '7223333', 24, 396, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:38', 'Victoriano Unay', '200513977'),
('1508002', 'Rynell Troy', 'Lofamia', 'Dizon', 'rynelltroy.dizon@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:45', 'Richmond Ngan', '200513977'),
('1508008', 'Gabriel', 'Cullo', 'Pilit', 'gabriel.pilit@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:22', 'Victoriano Unay', '200513977'),
('1508009', 'Jhon-rey', 'Restaba', 'Emprese', 'jhon-rey.emprese@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:23', 'Victoriano Unay', '200513977'),
('1508010', 'Angelo', 'Restaba', 'Manabat', 'angelo.manabat@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:25', 'Victoriano Unay', '200513977'),
('1508070', 'Christofer John', 'Lemu', 'Rivero', 'christoferjohn.rivero@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:20', 'Richmond Ngan', '200513977'),
('1508080', 'Maria Gracia', 'Jazmin', 'Luna', 'mariagracia.luna@mitsukoshimotors.com', '7223333', 23, 16, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:56', 'Richmond Ngan', '200513977'),
('1508128', 'Geezel Janz', 'Acupan', 'Dimacisil', 'geezeljanz.dimacisil@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:34', 'Richmond Ngan', '200513977'),
('1508129', 'Kim', 'Corsiga', 'Buitizon', 'kim.buitizon@mitsukoshimotors.com', '7223333', 24, 353, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:26', 'Victoriano Unay', '200513977'),
('1508130', 'Jessie', 'Tenorio', 'Nacario', 'jessie.nacario@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:28', 'Victoriano Unay', '200513977'),
('1508131', 'Filmark', 'Sobremonte', 'Bautista', 'filmark.bautista@mitsukoshimotors.com', '7223333', 24, 352, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:29', 'Victoriano Unay', '200513977'),
('1508132', 'Levi', 'Mates', 'Sale', 'levi.sale@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:30', 'Victoriano Unay', '200513977'),
('1508133', 'Amante', 'Solayao', 'Garcia Jr.', 'amante.garciajr@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:32', 'Victoriano Unay', '200513977'),
('1508134', 'Mark Anthony', 'Baldisimo', 'Kalahatian', 'markanthony.kalahatian@mitsukoshimotors.com', '7223333', 24, 344, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:33', 'Victoriano Unay', '200513977'),
('1508135', 'Virgilio', 'Parec', 'Bagasol Jr.', 'virgilio.bagasoljr@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:34', 'Victoriano Unay', '200513977'),
('1508136', 'Larry', 'Rosales', 'Salamoding', 'larry.salamoding@mitsukoshimotors.com', '7223333', 24, 326, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:36', 'Victoriano Unay', '200513977'),
('1508137', 'Rudy Jay', 'Ascano', 'Obena', 'rudyjay.obena@mitsukoshimotors.com', '7223333', 24, 338, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:37', 'Victoriano Unay', '200513977'),
('1509001', 'Mariecor Syrah', 'Hernandez', 'Genciagan', 'mariecorsyrah.genciagan@mitsukoshimotors.com', '7223333', 24, 354, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:39', 'Victoriano Unay', '200513977'),
('1509002', 'Joseph Rizmel', 'Ignacio', 'Clemente', 'josephrizmel.clemente@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:47', 'Richmond Ngan', '200513977'),
('1509006', 'Hannah Grace', 'Garcia', 'Maldupana', 'hannahgrace.maldupana@mitsukoshimotors.com', '7223333', 23, 390, 86, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:56', 'Richmond Ngan', '200513977'),
('1509050', 'Michael', 'Impas', 'Zabat', 'michael.zabat@mitsukoshimotors.com', '7223333', 25, 373, 81, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:27', 'Quintos Villoso', '200513977'),
('1509068', 'Jhoed', 'Delos Reyes', 'Almogela', 'jhoed.almogela@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:48', 'Richmond Ngan', '200513977'),
('1509069', 'Bernard', 'Pastor', 'Villamor', 'bernard.villamor@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:50', 'Richmond Ngan', '200513977'),
('1509108', 'Christian', 'Tagara', 'Dela Cruz', 'christian.delacruz@mitsukoshimotors.com', '7223333', 24, 353, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:40', 'Victoriano Unay', '200513977'),
('1509110', 'Mary Grace', 'Rodriguez', 'Rodelas', 'marygrace.rodelas@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:35', 'Richmond Ngan', '200513977'),
('1509137', 'Edfra', 'Rotaquio', 'Soltones', 'edfra.soltones@mitsukoshimotors.com', '7223333', 24, 344, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:42', 'Victoriano Unay', '200513977'),
('1509163', 'Antonette', 'Rios', 'Rafol', 'antonette.rafol@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:22', 'Richmond Ngan', '200513977'),
('1509164', 'Cecile', 'Malay', 'Maaba', 'cecile.maaba@mitsukoshimotors.com', '7223333', 23, 16, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:23', 'Richmond Ngan', '200513977'),
('1509177', 'Ellen Gay', 'Dela Cruz', 'Alipio', 'ellengay.alipio@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:45', 'Richmond Ngan', '200513977'),
('200', 'zzzzz', 'chavez', 'zzzzz', 'morris_zablay@yahoo.com', '09166222515', 24, 363, 77, 1, 1, '2015-09-30 23:01:10', '2015-09-30 15:01:23', NULL, '200513977'),
('201001', 'Rogelio', 'Gemina', 'Grabulan', 'rogelio.grabulan@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:38', 'Victoriano Unay', '200513977'),
('201213804', 'Michael', 'Ya', 'Yambao', 'michaeljy5@hotmail.com', '1234667', 24, 321, 80, 1, 1, '2015-09-30 22:39:18', '2015-09-30 14:39:33', NULL, '200513977'),
('204001', 'Michael', 'Duropan', 'Ganitano', 'michael.ganitano@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:49', 'Richmond Ngan', '200513977'),
('204002', 'Luzviminda', 'Roxas', 'Endozo', 'luzviminda.endozo@mitsukoshimotors.com', '7223333', 23, 375, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:29', 'Richmond Ngan', '200513977'),
('205002', 'Ma. Prea', 'Picar', 'Collas', 'maprea.collas@mitsukoshimotors.com', '7223333', 23, 16, 87, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:58', 'Richmond Ngan', '200513977'),
('2423234234', 'Boy', 'Motmot', 'Kamot', 'kamot@rebar.ph', '639958710042', 23, 12, 82, 1, 1, '2015-09-30 06:01:50', '2015-09-29 22:01:51', NULL, '200513977'),
('3002', 'Nelson', 'Cubelo', 'Gepulla', 'nelson.gepulla@mitsukoshimotors.com', '7223333', 24, 322, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:55', 'Victoriano Unay', '200513977'),
('305002', 'Lino', 'Socorro', 'Lobos', 'lino.lobos@mitsukoshimotors.com', '7223333', 23, 381, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:51', 'Richmond Ngan', '200513977'),
('307001', 'Ernalyn', 'Manipon', 'Paras', 'ernalyn.paras@mitsukoshimotors.com', '7223333', 23, 356, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:48', 'Richmond Ngan', '200513977'),
('312001', 'Ramon', 'Cula', 'Besana', 'ramon.besana@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:57', 'Richmond Ngan', '200513977'),
('324234234', 'Zablan', 'Zablan', 'Zablan', 'zablan@gmail.com', '09166222515', 24, 15, 82, 1, 1, '2015-09-30 23:20:03', '2015-09-30 15:20:16', NULL, '200513977'),
('401010', 'Rhona', 'Santos', 'Balino', 'rhona.balino@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:52', 'Richmond Ngan', '200513977'),
('401011', 'Rafael', 'Ganitano', 'Esperanza', 'rafael.esperanza@mitsukoshimotors.com', '7223333', 23, 382, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:53', 'Richmond Ngan', '200513977'),
('401023', 'Jose', 'Eugenio', 'Nobleza', 'jose.nobleza@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:55', 'Richmond Ngan', '200513977'),
('402003', 'Romeo', 'Ramos', 'Turla', 'romeo.turla@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:59', 'Richmond Ngan', '200513977'),
('402008', 'Bernie', 'Idoy', 'Quirante', 'bernie.quirante@mitsukoshimotors.com', '7223333', 24, 392, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:39', 'Victoriano Unay', '200513977'),
('402033', 'Adonis', 'Miranda', 'Nievarez', 'adonis.nievarez@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:00', 'Richmond Ngan', '200513977'),
('403010', 'Jenny', 'Calleja', 'Marasigan', 'jenny.marasigan@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:01', 'Richmond Ngan', '200513977'),
('405004', 'Mark Anthony', 'Dajao', 'Panolino', 'markanthony.panolino@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:56', 'Richmond Ngan', '200513977'),
('407002', 'Ma. Carmhel', 'Crisostomo', 'Ero', 'macarmhel.ero@mitsukoshimotors.com', '7223333', 24, 393, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:41', 'Victoriano Unay', '200513977'),
('409008', 'Ma. Luisa', 'Caillo', 'Cleofe', 'maluisa.cleofe@mitsukoshimotors.com', '7223333', 23, 383, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:58', 'Richmond Ngan', '200513977'),
('411003', 'Gerald', 'Duque', 'Felipe', 'gerald.felipe@mitsukoshimotors.com', '7223333', 23, 384, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:59', 'Richmond Ngan', '200513977'),
('412011', 'Julito', 'Ayuban', 'Asas', 'julito.asas@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:42', 'Victoriano Unay', '200513977'),
('41234234234234', 'Buboy', 'Schwarzenneger', 'Kalikot', 'mdfbldb@rebar.ph', '639810456647', 24, 19, 78, 1, 1, '2015-09-30 06:06:33', '2015-09-29 22:06:35', NULL, '200513977'),
('502017', 'Vic', 'Valdez', 'De Vera', 'vic.devera@mitsukoshimotors.com', '7223333', 23, 315, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:03', 'Richmond Ngan', '200513977'),
('503017', 'Gilbert', 'Cabatingan', 'Horner', 'gilbert.horner@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:04', 'Richmond Ngan', '200513977'),
('506006', 'Mary Ann', 'Tumazar', 'Gabuat', 'maryann.gabuat@mitsukoshimotors.com', '7223333', 23, 15, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:03', 'Richmond Ngan', '200513977'),
('507011', 'Michael', 'Catubig', 'Estampa', 'michael.estampa@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:44', 'Victoriano Unay', '200513977'),
('508006', 'Joel', 'Ulang', 'Rizalte', 'joel.rizalte@mitsukoshimotors.com', '7223333', 23, 15, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:20', 'Richmond Ngan', '200513977'),
('509007', 'Noel', 'Bermas', 'Buates', 'noel.buates@mitsukoshimotors.com', '7223333', 23, 15, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:22', 'Richmond Ngan', '200513977'),
('509019', 'Quintin', 'Sosing', 'Balanquit', 'quintin.balanquit@mitsukoshimotors.com', '7223333', 23, 385, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:01', 'Richmond Ngan', '200513977'),
('601003', 'Dexter', 'L.', 'Lorca', 'dexter.lorca@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:06', 'Richmond Ngan', '200513977'),
('603018', 'Benjamin', 'Villeno', 'Querimit', 'benjamin.querimit@mitsukoshimotors.com', '7223333', 23, 363, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:47', 'Richmond Ngan', '200513977'),
('604016', 'Rizell', 'Talosig', 'Segovia', 'rizell.segovia@mitsukoshimotors.com', '7223333', 23, 15, 2, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:23', 'Richmond Ngan', '200513977'),
('607028', 'Jeffrey', 'Sabiniano', 'Bacani', 'jeffrey.bacani@mitsukoshimotors.com', '7223333', 24, 311, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:45', 'Victoriano Unay', '200513977'),
('608016', 'Bunny', 'Altuna', 'Castillo', 'bunny.castillo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:07', 'Richmond Ngan', '200513977'),
('608064', 'Fermin', 'Felipe', 'Escueta', 'fermin.escueta@mitsukoshimotors.com', '7223333', 23, 16, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:48', 'Richmond Ngan', '200513977'),
('609019', 'Michael', 'Acapuyan', 'Baculo', 'michael.baculo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:09', 'Richmond Ngan', '200513977'),
('611041', 'Walter', 'Pascua', 'Parada', 'walter.parada@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:10', 'Richmond Ngan', '200513977'),
('636363', 'Jamesss', 'P', 'Aguilarssss', 'james@rebar.ph', '09183372501', 24, 17, 81, 1, 1, '2015-09-27 17:35:10', '2015-09-27 09:35:15', NULL, '200513977'),
('646464', 'James', 'P', 'Aguilarss', 'james@rebar.ph', '09173372501', 23, 11, 83, 1, 1, '2015-09-27 17:33:55', '2015-09-27 09:33:59', NULL, '200513977'),
('656565', 'Jim', 'Panes', 'Aguilars', 'james@rebar.ph', '09183372501', 23, 20, 84, 1, 1, '2015-09-27 17:31:18', '2015-09-27 09:31:22', NULL, '200513977'),
('676767', 'Jim', 'Panes', 'Aguilar', 'james@rebar.ph', '09183372501', 23, 19, 83, 1, 1, '2015-09-27 21:53:12', '2015-09-27 08:58:33', NULL, '200513977'),
('686868', 'Steven', 'Panes', 'Aguilar', 'james@build8.asia', '639064262536', 23, 16, 84, 1, 1, '2015-09-25 14:54:58', '2015-09-25 06:53:59', NULL, '200513977'),
('696969', 'James', 'Panes', 'Aguilar', 'james@rebar.ph', '639064262536', 23, 4, 2, 1, 1, '2015-09-25 14:18:12', '2015-09-25 06:17:13', NULL, '200513977'),
('705010', 'Jerson', 'Medel', 'Kwan', 'jerson.kwan@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:02', 'Richmond Ngan', '200513977'),
('706008', 'Froilan', 'Taguiam', 'Taguinod', 'froilan.taguinod@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:11', 'Richmond Ngan', '200513977'),
('707018', 'Leonil', 'Teope', 'Campollo', 'leonil.campollo@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:13', 'Richmond Ngan', '200513977'),
('707088', 'Theresa', 'Tan', 'Geronimo', 'theresa.geronimo@mitsukoshimotors.com', '7223333', 23, 357, 77, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:49', 'Richmond Ngan', '200513977'),
('707089', 'Precy', 'Cabaccan', 'Cammayo', 'precy.cammayo@mitsukoshimotors.com', '7223333', 23, 16, 82, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:30', 'Richmond Ngan', '200513977'),
('708061', 'Primrose', 'Tagatac', 'Tabaniag', 'primrose.tabaniag@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:14', 'Richmond Ngan', '200513977'),
('708075', 'Eduardo', 'Famisaran', 'Calusayan Jr.', 'eduardo.calusayanjr@mitsukoshimotors.com', '7223333', 23, 364, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:50', 'Richmond Ngan', '200513977'),
('709022', 'Julius', 'Reyes', 'Flores', 'julius.flores@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:16', 'Richmond Ngan', '200513977'),
('710013', 'Larry', 'Raipen', 'Francisco', 'larry.francisco@mitsukoshimotors.com', '7223333', 23, 315, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:17', 'Richmond Ngan', '200513977'),
('710050', 'Jesus', '---', 'Besinga', 'jesus.besinga@mitsukoshimotors.com', '7223333', 24, 16, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:58', 'Victoriano Unay', '200513977'),
('711033', 'Jerome', 'Ancajas', 'Bayron', 'jerome.bayron@mitsukoshimotors.com', '7223333', 23, 319, 74, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:35:27', 'Richmond Ngan', '200513977'),
('711116', 'Paul', 'Bunagan', 'Maraggun', 'paul.maraggun@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:18', 'Richmond Ngan', '200513977'),
('712010', 'Diosdado', 'Belleza', 'Collantes Jr.', 'diosdado.collantesjr@mitsukoshimotors.com', '7223333', 23, 386, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:04', 'Richmond Ngan', '200513977'),
('7903001', 'Tranquilino', 'Dones', 'Fullente Jr.', 'tranquilino.fullentejr@mitsukoshimotors.com', '7223333', 24, 313, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:39', 'Victoriano Unay', '200513977'),
('802054', 'Alvin', 'Basto', 'Panugao', 'alvin.panugao@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:05', 'Richmond Ngan', '200513977'),
('803024', 'Mark Anthony', 'Gotingco', 'Baloran', 'markanthony.baloran@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:20', 'Richmond Ngan', '200513977'),
('803035', 'Alberto', 'Franco', 'Alferez', 'alberto.alferez@mitsukoshimotors.com', '7223333', 24, 324, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:00', 'Victoriano Unay', '200513977'),
('804005', 'Juderico', 'Olino', 'Penero', 'juderico.penero@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:47', 'Victoriano Unay', '200513977'),
('804067', 'Matt', 'Cadion', 'Concepcion', 'matt.concepcion@mitsukoshimotors.com', '7223333', 23, 314, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:51', 'Richmond Ngan', '200513977'),
('805042', 'Charlon', 'Senina', 'Cadotdot', 'charlon.cadotdot@mitsukoshimotors.com', '7223333', 24, 311, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:48', 'Victoriano Unay', '200513977'),
('805053', 'Romelyn', 'Ordillano', 'Paez', 'romelyn.paez@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:25', 'Richmond Ngan', '200513977'),
('805107', 'Ma. Luz', 'Abay-abay', 'Nahial', 'maluz.nahial@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:21', 'Richmond Ngan', '200513977'),
('807002', 'Mark Anthony', 'Abaño', 'Rolle', 'markanthony.rolle@mitsukoshimotors.com', '7223333', 23, 311, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:07', 'Richmond Ngan', '200513977'),
('807048', 'Mark Ezekiel', 'Destreza', 'Lolos', 'markezekiel.lolos@mitsukoshimotors.com', '7223333', 23, 8, 73, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:34:23', 'Richmond Ngan', '200513977'),
('808061', 'Enriqueto', 'Mejica', 'Dula Jr.', 'enriqueto.dulajr@mitsukoshimotors.com', '7223333', 23, 314, 78, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:53', 'Richmond Ngan', '200513977'),
('809039', 'Renato', 'Gatcho', 'Agrida Jr.', 'renato.agridajr@mitsukoshimotors.com', '7223333', 24, 311, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:49', 'Victoriano Unay', '200513977'),
('8602001', 'Pastor', 'Patricio', 'Sabino', 'pastor.sabino@mitsukoshimotors.com', '7223333', 23, 313, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:49', 'Richmond Ngan', '200513977'),
('8802001', 'Irenia', 'Calas', 'Tanoja', 'irenia.tanoja@mitsukoshimotors.com', '7223333', 23, 13, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:08', 'Richmond Ngan', '200513977'),
('8805001', 'Tirso', 'Agao', 'Perol', 'tirso.perol@mitsukoshimotors.com', '7223333', 23, 312, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:10', 'Richmond Ngan', '200513977'),
('8902001', 'Imelda', 'Gomez', 'Bardon', 'imelda.bardon@mitsukoshimotors.com', '7223333', 23, 11, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:11', 'Richmond Ngan', '200513977'),
('8903001', 'Anito', 'Jumamoy', 'Sabandal', 'anito.sabandal@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:40', 'Victoriano Unay', '200513977'),
('8909001', 'Nelia', 'Quibral', 'Cortel', 'nelia.cortel@mitsukoshimotors.com', '7223333', 23, 313, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:13', 'Richmond Ngan', '200513977'),
('9001', 'Carmen', 'Santillan', 'Vicencio', 'carmen.vicencio@mitsukoshimotors.com', '7223333', 23, 378, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:45', 'Richmond Ngan', '200513977'),
('9005001', 'Marivic', 'Yarte', 'Mampo', 'marivic.mampo@mitsukoshimotors.com', '7223333', 23, 16, 88, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:36', 'Richmond Ngan', '200513977'),
('907016', 'Rolando', 'P.', 'Cataluña', 'rolando.cataluña@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:36:01', 'Victoriano Unay', '200513977'),
('907021', 'Jojo', 'Cleofas', 'Miciano', 'jojo.miciano@mitsukoshimotors.com', '7223333', 23, 16, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:07', 'Richmond Ngan', '200513977'),
('910036', 'Edwin', 'Tercan', 'Altesing', 'edwin.altesing@mitsukoshimotors.com', '7223333', 24, 311, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:43:51', 'Victoriano Unay', '200513977'),
('9110001', 'Wilfredo', 'Albino', 'Geronimo', 'wilfredo.geronimo@mitsukoshimotors.com', '7223333', 23, 371, 80, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:07', 'Richmond Ngan', '200513977'),
('9110002', 'Joseph', 'Las Piñas', 'Labayo', 'joseph.labayo@mitsukoshimotors.com', '7223333', 23, 335, 80, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:08', 'Richmond Ngan', '200513977'),
('911019', 'Aina', 'Benghilo', 'Zamora', 'aina.zamora@mitsukoshimotors.com', '7223333', 23, 360, 1, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:40:26', 'Richmond Ngan', '200513977'),
('9206001', 'Alex', 'Planco', 'Dones', 'alex.dones@mitsukoshimotors.com', '7223333', 24, 310, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:42', 'Victoriano Unay', '200513977'),
('9301001', 'Dominador', 'Narredo', 'Suliva', 'dominador.suliva@mitsukoshimotors.com', '7223333', 23, 16, 80, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:10', 'Richmond Ngan', '200513977'),
('9305001', 'Fernando', 'Gueriba', 'Mampo', 'fernando.mampo@mitsukoshimotors.com', '7223333', 23, 389, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:50', 'Richmond Ngan', '200513977'),
('9401002', 'Jessie', 'Dela Gracia', 'Juanico', 'jessie.juanico@mitsukoshimotors.com', '7223333', 23, 16, 80, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:41:11', 'Richmond Ngan', '200513977'),
('9404001', 'Arnold', 'Napitan', 'Cillano', 'arnold.cillano@mitsukoshimotors.com', '7223333', 23, 311, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:14', 'Richmond Ngan', '200513977'),
('9406002', 'Joel', 'Co', 'Del Rosario', 'joel.delrosario@mitsukoshimotors.com', '7223333', 23, 314, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:16', 'Richmond Ngan', '200513977'),
('9604001', 'Gene', 'Ponce', 'Ero', 'gene.ero@mitsukoshimotors.com', '7223333', 24, 397, 89, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:44:43', 'Victoriano Unay', '200513977'),
('9709001', 'Rowen', 'Bonifacio', 'Villaruel', 'rowen.villaruel@mitsukoshimotors.com', '7223333', 24, 323, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:43', 'Victoriano Unay', '200513977'),
('9710001', 'Ronick', 'Bocaling', 'Nacasi', 'ronick.nacasi@mitsukoshimotors.com', '7223333', 24, 325, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:44', 'Victoriano Unay', '200513977'),
('9801001', 'Ronald', 'Madelo', 'Legaspina', 'ronald.legaspina@mitsukoshimotors.com', '7223333', 23, 16, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:17', 'Richmond Ngan', '200513977'),
('9811001', 'Luzvi', 'Tenorio', 'Orozco', 'luzvi.orozco@mitsukoshimotors.com', '7223333', 23, 377, 85, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:42:51', 'Richmond Ngan', '200513977'),
('9903001', 'Alexander', 'Formalejo', 'Castillo Jr.', 'alexander.castillojr@mitsukoshimotors.com', '7223333', 24, 355, 76, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:39:46', 'Victoriano Unay', '200513977'),
('9906001', 'Julieto', 'Demetita', 'Panolino', 'julieto.panolino@mitsukoshimotors.com', '7223333', 23, 310, 72, 1, 1, '2015-09-24 20:32:49', '2015-09-24 12:33:19', 'Richmond Ngan', '200513977'),
('999999', 'Steve', 'Panes', 'Aguilar', 'jasminsimbulan16@yahoo.com', '09183372501', 23, 17, 82, 1, 1, '2015-09-29 21:35:49', '2015-09-29 13:35:52', NULL, '200513977');

-- --------------------------------------------------------

--
-- Table structure for table `mmpi_user_access`
--

CREATE TABLE IF NOT EXISTS `mmpi_user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `security_key` varchar(45) DEFAULT NULL,
  `validated` varchar(45) DEFAULT NULL,
  `task` tinyint(1) DEFAULT '0',
  `request` tinyint(1) DEFAULT '0',
  `admin` tinyint(1) DEFAULT '0',
  `user_mgt` tinyint(1) DEFAULT '0',
  `cluster_mgt` tinyint(1) DEFAULT '0',
  `flow_mgt` tinyint(1) DEFAULT '0',
  `settings` tinyint(1) DEFAULT '0',
  `group_mgt` tinyint(1) DEFAULT '0',
  `reports` tinyint(1) DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `creator_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_id_UNIQUE` (`employee_id`),
  KEY `fk_mmpi_user_access_mmpi_table_of_organization1_idx` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1643 ;

--
-- Dumping data for table `mmpi_user_access`
--

INSERT INTO `mmpi_user_access` (`id`, `employee_id`, `username`, `password`, `security_key`, `validated`, `task`, `request`, `admin`, `user_mgt`, `cluster_mgt`, `flow_mgt`, `settings`, `group_mgt`, `reports`, `date_created`, `date_updated`, `creator_id`) VALUES
(1138, '104002', 'LE104002', 'QQNSmko8', '7d3024ef4a59d7cb2fe2c31811480534', 'Sample', 1, 1, 1, 1, 1, 1, 1, 1, 1, '2015-09-24 12:33:06', NULL, '200513977'),
(1139, '807002', 'MR807002', 'Y6XtrgNM', '5f3bd21a6514aafafd4392ce62e37da9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:07', NULL, '200513977'),
(1140, '8802001', 'IT8802001', 'JDSmQWQK', '61d9db783a5ea3a25c478226a744ee99', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:09', NULL, '200513977'),
(1141, '8805001', 'TP8805001', 'GiQTkjT1', '71e02d9e86bec933007888999a815355', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:10', NULL, '200513977'),
(1142, '8902001', 'IB8902001', 'uD9IWf9S', '38658c4fdc81ad0e8838c424fed5f93a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:12', NULL, '200513977'),
(1143, '8909001', 'NC8909001', 'smPqwDxc', 'f44803b87f20d5da13f981af4b6b020c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:13', NULL, '200513977'),
(1144, '9404001', 'AC9404001', 'hrBsbydi', '57e05b60d04b977c0a53ece16cb4d612', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:15', NULL, '200513977'),
(1145, '9406002', 'JD9406002', 'i9QAG0Ng', '62d54f692b8184b8e9309da529a127e8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:16', NULL, '200513977'),
(1146, '9801001', 'RL9801001', 'RozGjlPK', '0948ddb7167d1e49dd6fb4402b32b986', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:18', NULL, '200513977'),
(1147, '9906001', 'JP9906001', 'STiTI6PI', 'b14f63b90003dacf73a219df47692806', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:19', NULL, '200513977'),
(1148, '508006', 'JR508006', 'ihwDP9mE', 'eacbcbbb90ced1c4dd95de4fedd5ee29', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:20', NULL, '200513977'),
(1149, '509007', 'NB509007', 'oSQMsaqm', 'd0a5eb8b3c644a775426f6ebdd8c757e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:22', NULL, '200513977'),
(1150, '604016', 'RS604016', 'N7GxR8xX', '012ca56b1f1fd96e14b98bdaa3c5d0cd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:23', NULL, '200513977'),
(1151, '1006055', 'JG1006055', 'ci5dwMTb', '7b1a583e03998fb65c7b5e3b5c90f174', 'Sample', 1, 1, 0, 0, 0, 0, 0, 0, 0, '2015-09-24 12:33:25', NULL, '200513977'),
(1152, '1204036', 'MS1204036', 'mSYkSZ9L', 'cd6d3deb40800add107a44f92a378641', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:26', NULL, '200513977'),
(1153, '1209089', 'MA1209089', 'FQkyUELT', '7e409e4fed20686cb77dd217bfbbe969', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:27', NULL, '200513977'),
(1154, '1210052', 'RV1210052', 'EgyspyRd', 'de4b358a09f9e95c678a78e453f309ad', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:29', NULL, '200513977'),
(1155, '1211154', 'MG1211154', 'REodUhtU', '0d6a36d9b4de60e0d684129f0a6e251d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:30', NULL, '200513977'),
(1156, '1301056', 'BG1301056', 'AqMzUJU7', '325a9b317b74a804c25d50bf675a3b5e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:32', NULL, '200513977'),
(1157, '1302140', 'JF1302140', 'lShuJNgX', '440801c1ab17994691b5f4d05a577f9f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:33', NULL, '200513977'),
(1158, '1305068', 'RM1305068', 'fMNdk5SL', '665ac10497b4ed7cf1cf30a573979466', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:35', NULL, '200513977'),
(1159, '1305080', 'MM1305080', 'RzXgkFLN', '64840aad2ac23c2c2f7685f6882ed3a9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:36', NULL, '200513977'),
(1160, '1305087', 'MB1305087', 'xGlL1EtF', 'f036489e11891d6cf587a83d46c526a3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:38', NULL, '200513977'),
(1161, '1404014', 'EC1404014', '9j8I9rT7', '1e3081c45b68e358e5f3f773983eba39', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:39', NULL, '200513977'),
(1162, '1406119', 'SM1406119', 'Xbu0qGdi', '18a9c2596080890612f712cdfd5a41ba', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:41', NULL, '200513977'),
(1163, '1504040', 'RC1504040', 'E7ZbMxu9', '072326bcd393098fbcf2c204239be369', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:42', NULL, '200513977'),
(1164, '1505117', 'MD1505117', 'jPsWOqdS', 'e2865286a10b97e6108e8ba2c3ff7013', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:44', NULL, '200513977'),
(1165, '1505145', 'RB1505145', 'h1SNRX9t', '01e3dce3634fe104022f4cef1d90c874', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:45', NULL, '200513977'),
(1166, '1506002', 'JL1506002', 'Bnfx4PX4', 'fa231600d6ea2b0f01190029721cfe6f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:46', NULL, '200513977'),
(1167, '1506004', 'JM1506004', 'cUckH0SO', '3c9513a9546db120a2421088ec7352aa', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:48', NULL, '200513977'),
(1168, '1506166', 'GQ1506166', 'xBuunjYJ', '4679bc5f8fae8b53a2a3e30dd3afb56d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:49', NULL, '200513977'),
(1169, '1506312', 'LA1506312', 'ujZQ78YF', 'afcdc6e8e5d5b4a2311f4512aa4a5a2f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:51', NULL, '200513977'),
(1170, '1506314', 'JD1506314', 'xT925uFL', '87749d1eb5f07355041a008bb95b02af', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:52', NULL, '200513977'),
(1171, '1507147', 'RL1507147', 'Z3Oy5DD9', '66239e1ee2adb9e673c721ef9d2c86ae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:53', NULL, '200513977'),
(1172, '1507192', 'RR1507192', 'hdhsj04H', '41c586df12a224a4189c2b8b1c8eb829', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:55', NULL, '200513977'),
(1173, '1508080', 'ML1508080', 'Z3prBmPJ', 'dc597300615589140f2ef4c11637b497', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:56', NULL, '200513977'),
(1174, '312001', 'RB312001', 'AFNcO9sC', '58462e26a050ddd0f717ebd44fa39414', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:58', NULL, '200513977'),
(1175, '402003', 'RT402003', 'Y6ybnyZZ', 'ca80b0f84bb2e243feecc3bd0837983e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:33:59', NULL, '200513977'),
(1176, '402033', 'AN402033', '8nbiHIlK', '274502f13f1477bf7d8f6e56d18a82f8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:00', NULL, '200513977'),
(1177, '403010', 'JM403010', 'sAEHztIi', 'be19dbb31484c0e6c8564ebc59574012', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:02', NULL, '200513977'),
(1178, '502017', 'VD502017', '8agi8JXk', '0a5d8713e1291a179d5e1e7544341e21', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:03', NULL, '200513977'),
(1179, '503017', 'GH503017', 'DbD4zjDF', '72f65d1b13958e134d1ed2a1da2da191', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:05', NULL, '200513977'),
(1180, '601003', 'DL601003', 'bk1X9bak', 'ad02de75ea1ab5a3465850fef7837557', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:06', NULL, '200513977'),
(1181, '608016', 'BC608016', '9JDbIO5r', '44c4b97141b5db84407169fe4fa23b88', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:08', NULL, '200513977'),
(1182, '609019', 'MB609019', 'Znb0lKc9', 'dcee87ee235727a3c5ed1a0ba4f1f2c6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:09', NULL, '200513977'),
(1183, '611041', 'WP611041', 'WSLy73P3', 'd7d8221ea63de031af5f6929ac564be4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:10', NULL, '200513977'),
(1184, '706008', 'FT706008', '3M8TJCmq', '95fecac38d6495d1ffa4cbd985172f07', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:12', NULL, '200513977'),
(1185, '707018', 'LC707018', 'MRR0gdTL', '2ae4b167c27f477ae116b86237c07be0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:13', NULL, '200513977'),
(1186, '708061', 'PT708061', 'uKBykaOM', '02a966645ff2835e85357e13d7f60b9b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:14', NULL, '200513977'),
(1187, '709022', 'JF709022', 'khcsZkgs', '4a956384494c8d59a19f3f522d90f583', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:16', NULL, '200513977'),
(1188, '710013', 'LF710013', 'OXn64Jiw', '396a742bebcae3bf0b950f5bce2f244e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:17', NULL, '200513977'),
(1189, '711116', 'PM711116', 'AOhKX2Dm', 'a1cc88f891b2d2d5259d0f146726b407', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:19', NULL, '200513977'),
(1190, '803024', 'MB803024', 'xg8FppTq', 'bed2dd11177348535c9319902e423495', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:20', NULL, '200513977'),
(1191, '805107', 'MN805107', 'N0CbEgaL', '7dbf5ec868e172067ddaa982dc5af45f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:22', NULL, '200513977'),
(1192, '807048', 'ML807048', '4y73dKNy', 'a6d0a64167a607d2214834f215ff300d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:23', NULL, '200513977'),
(1193, '1004022', 'RP1004022', 'eWnwY6fP', 'b8771d96992d0a28a51234ff15942d80', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:24', NULL, '200513977'),
(1194, '1011018', 'RD1011018', 'onSyqu2P', '577dbd1de6877aefea2ae0dd2401b396', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:26', NULL, '200513977'),
(1195, '1011053', 'JA1011053', 'P6fx4qfi', '8d5362f11c5354ede0079944fc42ea65', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:27', NULL, '200513977'),
(1196, '1101048', 'RA1101048', 'TO9GOod0', '98ce92df2602347e064fafd1d22cddca', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:29', NULL, '200513977'),
(1197, '1102003', 'JQ1102003', '037T3Mef', '1397cb8db152fb0edb9702e1cb0d69ee', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:30', NULL, '200513977'),
(1198, '1103060', 'JS1103060', 'Y5qWJSic', 'ce219a3df10b1658eb28237a9aa6056e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:32', NULL, '200513977'),
(1199, '1104012', 'GM1104012', 'mcOQJXJS', 'e6609b81dde6aff2efaf31401faf9d3e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:33', NULL, '200513977'),
(1200, '1110038', 'MB1110038', 'UqRuUSnQ', '8670c2745d8bf56346755099a4e53e44', 'Sample', 1, 1, 1, 1, 1, 1, 1, 1, 1, '2015-09-24 12:34:34', NULL, '200513977'),
(1201, '1204121', 'LC1204121', 'ywXZoSxd', 'c84d42e481d5d12864fd0709a0b9e5dc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:36', NULL, '200513977'),
(1202, '1211158', 'AT1211158', 'APS2bxjg', 'ae6a165a8435fac81c9779b0647a3f25', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:37', NULL, '200513977'),
(1203, '1307025', 'JL1307025', 'SmyZAnlS', 'd7b12af864df9859cdfb5665c35631e7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:39', NULL, '200513977'),
(1204, '1310098', 'AB1310098', 'cF4r9WDx', '3f3c47f64a2e77eb813a0a916b4480d3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:40', NULL, '200513977'),
(1205, '1403131', 'RC1403131', '9bRnLMDQ', 'f657bfcd8831aeee251a12d88ae5a3e3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:42', NULL, '200513977'),
(1206, '1404004', 'MR1404004', '4AOA5Uad', '035d68be1006be4acbe9489c1ac0c9da', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:43', NULL, '200513977'),
(1207, '1404043', 'MS1404043', 'le9h7Pj9', 'f10f92267db228be7549b7607c9d605e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:45', NULL, '200513977'),
(1208, '1404115', 'ET1404115', 'kc81ALQ4', 'f60860781c8e209e6c27da93a8b80c2b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:46', NULL, '200513977'),
(1209, '1405009', 'KG1405009', 'tnkakjuH', '10a88ba4919d655b1d757303fb12f969', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:48', NULL, '200513977'),
(1210, '1405010', 'RS1405010', 'LOlMj0rN', '757b3026d8727a7b35ae708a196702cb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:49', NULL, '200513977'),
(1211, '1405012', 'ZG1405012', 'fQPBiTk0', '42278899827ae3e38005dc19516f75ce', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:51', NULL, '200513977'),
(1212, '1405091', 'RZ1405091', 'rfS9P5lS', '4589c4fe2b68fd3ff3605035d7906731', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:52', NULL, '200513977'),
(1213, '1405093', 'MS1405093', 'NYiXQPBT', '0ba8b9e70a641543be71324b48445bd0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:53', NULL, '200513977'),
(1214, '1407001', 'EF1407001', 'yw3ldffe', 'f4187305722d2416f454ecd42d7c7e27', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:55', NULL, '200513977'),
(1215, '1407002', 'HM1407002', 'xQkfzGCi', 'dda5f1b6ed6356ce52c4690345ad7884', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:56', NULL, '200513977'),
(1216, '1407026', 'DR1407026', '7zGjY8YY', 'a8fd826e6085cecda463f4f0c87a080d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:58', NULL, '200513977'),
(1217, '1407042', 'HF1407042', 'MgfOu9T1', '3dbbfc320943acae74529eaa1eac35f3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:34:59', NULL, '200513977'),
(1218, '1408002', 'AB1408002', 'K71uCDad', '79d4e2ac40cf8f98fa184fe6c3ae39bc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:01', NULL, '200513977'),
(1219, '1408003', 'JA1408003', 'FiR6ULUo', '781e04910e745068bbc7d057512531d1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:02', NULL, '200513977'),
(1220, '1408006', 'HV1408006', 'pklwAC4M', '950846508c065314ec2685abb63194c8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:03', NULL, '200513977'),
(1221, '1408067', 'NM1408067', 'G8am3ERa', 'd5ae80a13b28f6f0751708adfb3582e1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:05', NULL, '200513977'),
(1222, '1408102', 'RM1408102', 'bCQYRDhZ', '92bdfdcd63363ce5d23df47bcea676f9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:06', NULL, '200513977'),
(1223, '1409104', 'MD1409104', 'DN8tOfq7', 'f59ce380199e065627e583827689ee04', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:07', NULL, '200513977'),
(1224, '1409107', 'JA1409107', 'YifcNJti', '2881e69719e03ba3dbfab7e47e2f4e75', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:09', NULL, '200513977'),
(1225, '1409110', 'AG1409110', 'cUuGF8tU', 'ae8e94470012d1bf502d4846b6d8e1a7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:10', NULL, '200513977'),
(1226, '1409111', 'MV1409111', 'esEYOcdf', '15b24ac1d5e722bf2ab0a07f11ebee6d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:12', NULL, '200513977'),
(1227, '1409113', 'LT1409113', 'SrnfeIRO', '20353d4351e400d2dff9e9df03273f4f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:13', NULL, '200513977'),
(1228, '1409114', 'AD1409114', 'QFdjbfz0', '6c16dbdd5a1131513e185c8455caea13', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:14', NULL, '200513977'),
(1229, '1411149', 'SG1411149', 'zsdm7Zbn', '79d110c6c0d0cc995dd779425dc02984', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:16', NULL, '200513977'),
(1230, '1412007', 'SD1412007', 'JE0dMO2d', 'fa6c2d1a2cc4b42b5c77827034c13f97', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:17', NULL, '200513977'),
(1231, '1412109', 'HM1412109', 'oQjKDgsj', 'b0b2b547e676c3cccbb304fa59ec1100', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:19', NULL, '200513977'),
(1232, '1501079', 'AV1501079', 'EaBmPlaO', 'a097e46902d3607901859bfb93455925', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:20', NULL, '200513977'),
(1233, '1502060', 'HG1502060', 'u6MuA1Z9', 'ddb4b77f518b04c0c3e45edd2c7855f0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:21', NULL, '200513977'),
(1234, '1504003', 'AR1504003', '0DZWErlD', 'b19ac7571adc5a1141f8bcb7fbb3fd93', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:23', NULL, '200513977'),
(1235, '1505119', 'MV1505119', 'E49NYuX0', '0c57c4afb67ced56a19017c6f12335af', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:24', NULL, '200513977'),
(1236, '1506136', 'AS1506136', 'tZeimKBf', '090ac37542e99042a9a8b05d18ca5888', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:26', NULL, '200513977'),
(1237, '711033', 'JB711033', 'cGbGNAjj', '5fb9c99ea8ea469e212817f184d41ca9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:27', NULL, '200513977'),
(1238, '1203169', 'RN1203169', 'UmePJzfH', 'da4d14ccce39bc0c1f7eb37f08ecd144', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:29', NULL, '200513977'),
(1239, '1206103', 'RF1206103', 'CICMJuwG', '693690fa6f51a8fa186b1ea06481e5d8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:30', NULL, '200513977'),
(1240, '1208002', 'AD1208002', 'WSsBQGJg', 'e7da521fd61222140ee5e12a7f4457dd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:32', NULL, '200513977'),
(1241, '1212030', 'AA1212030', 'oyS2Zyat', '28ee3e64998e24f140579d2e478fc8a0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:33', NULL, '200513977'),
(1242, '1306082', 'EM1306082', 'NlN8S8j9', 'a4a04ac51ebd710541ac178503303fa2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:34', NULL, '200513977'),
(1243, '1403089', 'RN1403089', 'JTUuTRSI', '19d4bca3a0490585bded6ee7922508d2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:36', NULL, '200513977'),
(1244, '1406018', 'MN1406018', '539dSRTj', 'ffa0f64d3107974d4e207d20578059da', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:37', NULL, '200513977'),
(1245, '1504099', 'DM1504099', 'jyP6Qd0I', '7381fabcb5e5e3a68e7b8ef799b49995', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:38', NULL, '200513977'),
(1246, '1504100', 'JQ1504100', 'uwhzrCFb', '44d8949997da1f63def0b511279fb457', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:40', NULL, '200513977'),
(1247, '1505030', 'AG1505030', '0bLkTHiW', 'aa272b23121957575e70c514198bb29c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:41', NULL, '200513977'),
(1248, '1505118', 'RM1505118', 'ydBDzwXn', '7229659e2b9f0fad11144ece857081f6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:43', NULL, '200513977'),
(1249, '1506003', 'GC1506003', 'F2x1CsMD', '6ef325cc3b7ca9e7fcd57d24c9678004', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:44', NULL, '200513977'),
(1250, '1508002', 'RD1508002', 'lmOkhZSx', '14fb748ba9aad529e5228c076a78cf75', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:46', NULL, '200513977'),
(1251, '1509002', 'JC1509002', 'RGp6KArH', '74d8b19ec032157cbb5e44edb3bd9557', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:47', NULL, '200513977'),
(1252, '1509068', 'JA1509068', 'oBHNtldW', 'd80bed3608cd97ac0088e0c4929826cd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:48', NULL, '200513977'),
(1253, '1509069', 'BV1509069', 'FPUm1DZk', '075bb48f4533cbb96469bbc558b769fb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:50', NULL, '200513977'),
(1254, '1411155', 'JD1411155', 'dQX9HI3o', 'a1463298c9266a456af4fed72d2bb3a8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:52', NULL, '200513977'),
(1255, '1003', 'RP1003', 'jZyaRBzP', '202919090beb65a9ae6f9fa3dae1fdf6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:54', NULL, '200513977'),
(1256, '3002', 'NG3002', 'eWLdKByW', 'fbd6b1daf9311985ae110a229d57c71a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:55', NULL, '200513977'),
(1257, '107001', 'EA107001', '3htdfyAn', '6466759da9bb1ed911a1904ceecda428', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:57', NULL, '200513977'),
(1258, '710050', 'JB710050', 'rqMgzLLw', 'b3242b8a803731d2fa8a8fa7795dbe9e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:35:58', NULL, '200513977'),
(1259, '803035', 'AA803035', 'ObolgmmH', '8bc416627e866b98752595b4e0d219dc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:00', NULL, '200513977'),
(1260, '907016', 'RC907016', 'aoFPWYjQ', 'c3d46b86aca05612eb3cc1c5493f03c6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:02', NULL, '200513977'),
(1261, '1103042', 'LV1103042', '1fbc3z2T', '762054e4959685bfa38913d2898fa8c4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:03', NULL, '200513977'),
(1262, '1104027', 'AB1104027', 'Q6WmdC87', '5b96181b5e5b9d3afc2c4d0e7b787225', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:04', NULL, '200513977'),
(1263, '1105123', 'RV1105123', 'jRG1oY58', '2a5213d4eebe10421ba465fde11c9408', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:06', NULL, '200513977'),
(1264, '1110023', 'RM1110023', 'LKI68FjF', '142c0fa37364fa6a7ad4ab2dc0e8021d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:08', NULL, '200513977'),
(1265, '1112027', 'ML1112027', 'i3unCwis', '11bf3a78584588f80743b08169a8712b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:09', NULL, '200513977'),
(1266, '1201019', 'HP1201019', 'NjsHueKS', '1d162aa698aca0299ca9b35dbba4adba', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:11', NULL, '200513977'),
(1267, '1201021', 'NT1201021', 'i9D0e0pt', 'bdcdc830bd55d8bfded46f8cea37d432', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:12', NULL, '200513977'),
(1268, '1201022', 'PS1201022', '9bL2XqWP', 'a1c9ae401baa10e69b22a0c7b0182095', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:13', NULL, '200513977'),
(1269, '1201035', 'SS1201035', '7znmHLRb', '9c04711ed4c9fe5279d73728589750b7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:15', NULL, '200513977'),
(1270, '1201100', 'MS1201100', '820XK5H4', '49074eca13590466b29da1528b01bdda', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:16', NULL, '200513977'),
(1271, '1201126', 'CM1201126', 'KBXDeyAA', 'ea04255b66657531e0131629a8fab076', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:18', NULL, '200513977'),
(1272, '1202096', 'VB1202096', 'Tra3ttZk', '28cdf4dd0eff457160df5c62574a38a0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:19', NULL, '200513977'),
(1273, '1202132', 'GF1202132', 'a6ift9HD', '7745fe9d323968a76e640670412cf292', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:21', NULL, '200513977'),
(1274, '1203102', 'JI1203102', 'bJCmP8zm', '29d7ceb1e9ede851842b742d04974e38', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:22', NULL, '200513977'),
(1275, '1203125', 'RR1203125', '6jRMmdjN', 'fbc7706f30e2ac8b14b28b82e01e64ef', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:24', NULL, '200513977'),
(1276, '1203174', 'DV1203174', '6IL9jxOC', '90ca2b180c9821df3075fd339ecb6799', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:25', NULL, '200513977'),
(1277, '1204165', 'JT1204165', 'HxfDI0gU', '0cae6d361e66ad6ff1c4e75890ed024f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:27', NULL, '200513977'),
(1278, '1204179', 'RE1204179', 'NoUE4Qr1', '738374e631e983bdfa6f242e91840417', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:28', NULL, '200513977'),
(1279, '1206048', 'BC1206048', 'lT5M75PW', 'df7b27dc78a8a753b5a66b525eb4dfae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:30', NULL, '200513977'),
(1280, '1211002', 'JS1211002', 'lKTNyMhF', 'efaffc47021922173ef672fc39878b7c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:31', NULL, '200513977'),
(1281, '1302141', 'RA1302141', 'pbGBBaxD', '85d2e8ae44369558c727285f3d655bcf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:33', NULL, '200513977'),
(1282, '1305089', 'AV1305089', 'ltZ2jdYI', 'e3cc71d915c1edb054ca9dc4b3cac69c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:34', NULL, '200513977'),
(1283, '1308001', 'VU1308001', 'XrPtqLQm', '272a5ded61d5951f0ffa2f16ea9e1d80', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:36', NULL, '200513977'),
(1284, '1308136', 'RA1308136', 'KKpYLZJX', 'd1a2dbaf9f102d6d9cc3f7caf916195f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:37', NULL, '200513977'),
(1285, '1310078', 'PL1310078', 'xTyJQ0ZR', 'ee4758690efe5c580b1acdc7d90cf3cd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:39', NULL, '200513977'),
(1286, '1311067', 'JQ1311067', 'DXodTGbH', 'ea5f14407cf02443f6eb0efbcff2e9ea', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:40', NULL, '200513977'),
(1287, '1311146', 'RG1311146', 'SyPdNkAW', '46e8176ecf22394de554031ae55dc7d8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:42', NULL, '200513977'),
(1288, '1403096', 'LF1403096', 'TqF74k0r', 'e317651a139765d9845759b236abcda9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:43', NULL, '200513977'),
(1289, '1403098', 'JH1403098', 'L1l8Hkyd', '3a0db3de6f9fa45929a3df187646c449', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:45', NULL, '200513977'),
(1290, '1403099', 'EE1403099', '1giqGdHG', 'c29ceda1f117d8fc6e50973b32512d7b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:46', NULL, '200513977'),
(1291, '1403100', 'JY1403100', 'ChhOboKY', '78b033df68b39061bbe819b480d837ac', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:48', NULL, '200513977'),
(1292, '1403201', 'OT1403201', 'R1tc3ypB', '85aae5bfcf7deea164c28f233732abd8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:49', NULL, '200513977'),
(1293, '1405128', 'FB1405128', 'lo2pRwJM', '1ad643a50ecbd04d59d8e87e8d77e23b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:51', NULL, '200513977'),
(1294, '1405217', 'MS1405217', 'lpeo1iCS', 'aa0008aad2bbba2b53524fc59aa7395f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:52', NULL, '200513977'),
(1295, '1405220', 'MC1405220', '6gWE32zk', 'a6cc93113e2339c7c6258600aaf2bb62', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:54', NULL, '200513977'),
(1296, '1405226', 'RF1405226', 'gdcINRQo', 'd46487bc40902d144f720c14ffb938db', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:55', NULL, '200513977'),
(1297, '1407031', 'RC1407031', 'Zn5Eyou9', 'f0ad120021eda72b65e08abfedbaf5d2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:57', NULL, '200513977'),
(1298, '1407033', 'EC1407033', 'ZRkkITx1', '7b2645f7072280a5284dd3542e60cc12', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:36:58', NULL, '200513977'),
(1299, '1407034', 'RP1407034', 'PA0zmRKl', '66fbdbd40cb7c958e642566a1aaa63c4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:00', NULL, '200513977'),
(1300, '1407040', 'CM1407040', 'cgGDU0Di', '14accc9aac88ca12c7c7c67be6e997f7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:01', NULL, '200513977'),
(1301, '1407250', 'JA1407250', 'UOrqtjio', '797532296238a1ddb373597c105b1988', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:03', NULL, '200513977'),
(1302, '1410122', 'JR1410122', 'Jz9X9twi', 'd1402c8b3a007f337cc9b4f76d885f28', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:04', NULL, '200513977'),
(1303, '1410129', 'JE1410129', 'KPaSBYxR', 'ec8be0a268de8da287c1bbfdcb60ab99', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:05', NULL, '200513977'),
(1304, '1410132', 'FL1410132', 'eSfnu9Bg', '7bff08fed355beea83093f8552fc52fb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:07', NULL, '200513977'),
(1305, '1411104', 'EM1411104', 'P2CRS779', 'e2a593ef3361b4a9019be0971eba5f5d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:08', NULL, '200513977'),
(1306, '1411106', 'JP1411106', 'X1Ot91br', '298ad35750898c4e758b17ab640e331b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:09', NULL, '200513977'),
(1307, '1411115', 'RL1411115', 'l6GrIUDf', '8eccb66502a59fb1c5e7abbfc616489b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:11', NULL, '200513977'),
(1308, '1411125', 'NM1411125', 'tgLZ737N', '3050d04bf3e95526e7fb59f75e9c2c8e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:12', NULL, '200513977'),
(1309, '1411126', 'CE1411126', 'YRxxs8pt', '331b7a091d6fa7a67d6cd40501375df9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:14', NULL, '200513977'),
(1310, '1411133', 'HB1411133', 'u9TC3YCI', '2ccef4d91f66db6da281fcaf8d7a5952', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:15', NULL, '200513977'),
(1311, '1411276', 'JV1411276', 'AZgCcQgj', 'ab8b9da15be6d321515a1066b21515ae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:17', NULL, '200513977'),
(1312, '1411277', 'LF1411277', 'x9ARTW1P', 'df1edb7c2a5b8bb4be6a8bd84bee6d71', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:18', NULL, '200513977'),
(1313, '1411278', 'RV1411278', '2HHxsea0', 'bb2c67deabd16af16d75de08cd50d144', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:20', NULL, '200513977'),
(1314, '1411284', 'DT1411284', 'HswCIjqL', '197edd2744252ddd4c192ea0cbd20ca1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:21', NULL, '200513977'),
(1315, '1412012', 'QT1412012', 'UA8e0j8p', '795d2721c3b2f86919a6c51b5eab118d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:23', NULL, '200513977'),
(1316, '1502193', 'JC1502193', 'tLw0ptDQ', 'd623186d4bc471dd3a6f37a66f53d86c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:24', NULL, '200513977'),
(1317, '1502194', 'AL1502194', 'gk4szJQM', '3e0d9bc70eeadcd4624e9aee79e7a94c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:26', NULL, '200513977'),
(1318, '1502196', 'IM1502196', 'pUf9twHR', '02fa31da7ac86a34773543b9cd520309', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:27', NULL, '200513977'),
(1319, '1503110', 'AJ1503110', 'ktZDBqso', '5147e774e6223d1630828b2dcaac7de3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:29', NULL, '200513977'),
(1320, '1503118', 'RO1503118', 'UNWSLWOO', 'd6ef00f72a5c797b8d1b75be8df4942e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:30', NULL, '200513977'),
(1321, '1503132', 'JV1503132', 'm85q2toq', 'b845187aa3c08c07ae024c303363d2dc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:31', NULL, '200513977'),
(1322, '1503135', 'RA1503135', 'ZZbtcx1H', 'be3550447a3cc36cd9a622d363b6f2a7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:33', NULL, '200513977'),
(1323, '1503145', 'WM1503145', 'iL3FYN0C', 'cf4145c2c053dde7c9b47772eb8c048f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:34', NULL, '200513977'),
(1324, '1503149', 'MA1503149', 'WuFsy1Ud', 'a6e1ceb9349a43f69c824586c50c9b27', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:36', NULL, '200513977'),
(1325, '1503151', 'JL1503151', 'yWL0yQj3', '448cd65fa20f4ab6ca7a1818f6c28a10', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:37', NULL, '200513977'),
(1326, '1503220', 'KL1503220', 'eCUjbQsw', '8970a0bbe938e72c1ed6f29bbd2dfebc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:38', NULL, '200513977'),
(1327, '1503228', 'VM1503228', 'L34FztUT', '63c98ffaad54d71fc3d01a6dc766dadc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:40', NULL, '200513977'),
(1328, '1503233', 'NM1503233', '8z4dQXL0', '3ce1e0162e396c0148b2378f15d59c69', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:41', NULL, '200513977'),
(1329, '1503243', 'MD1503243', 'y6xhkEzc', '6682fb93f5fabe275c88cc54f8ae4b93', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:43', NULL, '200513977'),
(1330, '1503252', 'EB1503252', 'xiyQjqG1', 'd1596ee0cff59bfa331be23b1b9cf99b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:44', NULL, '200513977'),
(1331, '1503257', 'RH1503257', 'epeum7YA', '38e1849341b7308ade84463ade5b2801', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:46', NULL, '200513977'),
(1332, '1503258', 'JN1503258', 'CdY6fqID', '049ca40944e3f235699964d9aaba466f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:47', NULL, '200513977'),
(1333, '1503260', 'FJ1503260', 'AroR8eB1', '87a5225439f187d6bfc8c6ba34e6cb69', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:49', NULL, '200513977'),
(1334, '1503261', 'SA1503261', 'HZGPUKIo', 'c7a00a1bf35cba84322caa97735a393c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:50', NULL, '200513977'),
(1335, '1503262', 'JM1503262', 'DrgtPKmx', '1a91a8f3ee2bc71930e1761b5c56c495', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:51', NULL, '200513977'),
(1336, '1503265', 'RL1503265', 'XbIBSHC6', 'a35ecf99a94830a74ebb1eca02865422', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:53', NULL, '200513977'),
(1337, '1503266', 'GB1503266', 'mkMQ9so3', '274ea7aaad925b7927dac65d5e06e5f3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:54', NULL, '200513977'),
(1338, '1503274', 'LL1503274', '6ansPLbq', 'b7f752c7d79afd55e6365de8678ae0f5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:56', NULL, '200513977'),
(1339, '1503277', 'JD1503277', 'sgntX1O5', '0c182deb1575053a55aa24e3440fb29e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:57', NULL, '200513977'),
(1340, '1503280', 'PP1503280', 'fG9FisNQ', '3a7f8e5d6d41e20f6f50f13881c71ff5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:37:58', NULL, '200513977'),
(1341, '1505101', 'JC1505101', '6BHO5Eqq', 'c6fc33a2fdfad605bae96ff9bc737ff6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:00', NULL, '200513977'),
(1342, '1505102', 'JD1505102', 'zMhspYah', 'ed5765dd7442e70e250f46b05a9eaf16', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:01', NULL, '200513977'),
(1343, '1505103', 'JI1505103', 'GrkOaZy4', '0c591d28068a11e6e4d9faf4c197b35c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:03', NULL, '200513977'),
(1344, '1505162', 'AR1505162', '5qDMDW6R', 'c48b11c847dfa0ad244bffa0351a4f2e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:04', NULL, '200513977'),
(1345, '1505163', 'DA1505163', 'xABRcFGK', 'cf5fe589a0a47aba39895260b9b00e91', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:05', NULL, '200513977'),
(1346, '1505164', 'JB1505164', 'rkq4uhgI', '1ec8b6257ead3265d7302fc7c9aac690', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:07', NULL, '200513977'),
(1347, '1505165', 'JP1505165', 'K6imrfhQ', 'e5cf4cb23b04037aca2503ab82a77a03', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:08', NULL, '200513977'),
(1348, '1505166', 'HY1505166', 'AE0UqQTD', '047fb9f9b8a7be056c5715a4b12fcd5d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:10', NULL, '200513977'),
(1349, '1505167', 'MM1505167', 'R9rBaQA5', 'e049207c22efae66eb09ea3fc727252c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:11', NULL, '200513977'),
(1350, '1505168', 'JM1505168', 'ALsYqdAU', 'a28a65d23c466c56cb43c557747cb3b1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:13', NULL, '200513977'),
(1351, '1505169', 'RG1505169', 'EM1qhUef', '943ca63b69374ca1cec011b2068ea27f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:14', NULL, '200513977'),
(1352, '1505170', 'RB1505170', 'j3AxPp05', '98806de659a2291aa002fd81228d95e8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:15', NULL, '200513977'),
(1353, '1505171', 'AL1505171', 'PCH6e181', '6f823c50991a14c86bdc0ceebb3c1a83', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:17', NULL, '200513977'),
(1354, '1505173', 'AA1505173', 'jzqo4JHC', 'ace1f63559a02c6d5779e552d5df83d0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:18', NULL, '200513977'),
(1355, '1505174', 'KA1505174', 'QCHSDz7D', '03cd41d4049208b38a7b14cf4dae4db8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:20', NULL, '200513977'),
(1356, '1505175', 'FD1505175', 'nQeBpmLH', '3d48da0e62064939182fa2b843ec0633', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:21', NULL, '200513977'),
(1357, '1505176', 'JP1505176', 'DQmY6yMG', 'cde56752cace60716532176e6be7de52', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:23', NULL, '200513977'),
(1358, '1505179', 'JA1505179', 'tGZ5JhH2', 'eafaccd69ca2f2efcd8895d4d3655bc9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:24', NULL, '200513977'),
(1359, '1505180', 'HB1505180', 'ic44Yj9P', '00d62eb2f301226d391f62356bc1ba65', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:26', NULL, '200513977'),
(1360, '1505181', 'DD1505181', 'nMiAgT3s', 'd570658050bf6bc444f21a82d7745121', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:27', NULL, '200513977'),
(1361, '1505182', 'NM1505182', 'ZbRh1hwD', '793b759d4a3801dcfa487ac4af0d3cb8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:28', NULL, '200513977'),
(1362, '1505183', 'GM1505183', 'rBaehQcS', 'a67efb1988b997d1a79a24f70068048e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:30', NULL, '200513977'),
(1363, '1505184', 'AG1505184', 'uCkOfJ4c', '05449687e168f7f34007c64fb9b150fd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:31', NULL, '200513977'),
(1364, '1505185', 'RR1505185', 'o3CrYKaP', '2c795e9212e35de714fecdd4befc15f7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:33', NULL, '200513977'),
(1365, '1505186', 'EM1505186', '8rdFFxMZ', 'ac9a1dcbda47ebdf503dac782440b1f4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:34', NULL, '200513977'),
(1366, '1505187', 'JT1505187', '3gAA4Thn', '28648d6567753ec7d72217166a140529', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:35', NULL, '200513977'),
(1367, '1505189', 'JP1505189', 'nretXU4z', '2cca2dc2cd3bd917121c6388e48bbd15', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:37', NULL, '200513977'),
(1368, '1505190', 'RP1505190', '2nxGD7ZM', '0e6ba68de596be4abd85f44ae1e48776', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:38', NULL, '200513977'),
(1369, '1505191', 'AP1505191', 'L0Qchbzk', 'c56ac8b4b14ee2c9d95c36c1d4017475', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:40', NULL, '200513977'),
(1370, '1505192', 'JM1505192', 'CbkjToyg', 'b5d1c4511a0f49ee57831ced82f4a893', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:41', NULL, '200513977'),
(1371, '1505193', 'AP1505193', 'rOYS3tSh', '5c5c7e4c9ac3d79b3d630081952f1b77', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:43', NULL, '200513977'),
(1372, '1505194', 'RA1505194', 'nZTeRJQM', 'd56d4006fc4adda2177daa9160274952', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:44', NULL, '200513977'),
(1373, '1505195', 'MB1505195', '7HJsAykL', 'fb1ea259522a3051a81305d0aa0e13ca', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:45', NULL, '200513977'),
(1374, '1505197', 'EG1505197', '0i1b9P5J', '08b89d72a95822445fe821cffe3cfca7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:47', NULL, '200513977'),
(1375, '1505198', 'TA1505198', 'Eigo0tJc', '2b67365e50f78183f225b15c01612fd7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:48', NULL, '200513977'),
(1376, '1505200', 'JR1505200', '1A3BJPAM', '9017cb9026d8d2606d97c415d014b8b1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:50', NULL, '200513977'),
(1377, '1505206', 'AM1505206', 'UQmzNt4R', '4b19a05f03285b262a5869d5749e2764', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:51', NULL, '200513977'),
(1378, '1505208', 'PA1505208', 'y5JnF8tw', '0d7eddb75fa5078b631f14a1424382e7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:52', NULL, '200513977'),
(1379, '1505209', 'ES1505209', 'sZlRLyjE', 'a0ccce5aefe1002592e51ff5e7fb8acf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:54', NULL, '200513977'),
(1380, '1506056', 'RL1506056', '4pPHQ6xg', '92ce7026c4ba59baabb3ecdb26df352b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:56', NULL, '200513977'),
(1381, '1506155', 'ML1506155', 'lkH5DkcK', '5de116ea0625db959785df494a4ee028', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:38:58', NULL, '200513977'),
(1382, '1506157', 'MM1506157', '7u3ELNPP', 'dd5cead6720e68f3ac5c7f6b8aa5d174', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:00', NULL, '200513977'),
(1383, '1506158', 'EV1506158', 'QsUCPLRt', '8ee83351b45f251e5f2f6faf4ef78e7d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:01', NULL, '200513977'),
(1384, '1506159', 'SD1506159', '6aB3hMzd', 'f97360044d8eeb802b0e82def4863b3e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:02', NULL, '200513977'),
(1385, '1506160', 'DP1506160', 'atc8iWJZ', '873ff782d723851df6d83aba6af0caa8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:04', NULL, '200513977'),
(1386, '1506162', 'DP1506162', '115cjNKo', '8ccf9b5fadcd4175c8aa1640aa805860', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:05', NULL, '200513977'),
(1387, '1506163', 'AN1506163', 'hQdtaI72', '4eb4c5b62223dec468da07f262308e86', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:07', NULL, '200513977'),
(1388, '1506189', 'RG1506189', 'd5aBEA14', '57f5a960b3d6bbf234fa356773afd49c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:08', NULL, '200513977'),
(1389, '1506241', 'JD1506241', 'A8PiyD7G', '6c0322a859a3a1f0a91d326b4190883b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:09', NULL, '200513977'),
(1390, '1507084', 'AD1507084', '7DpKFAip', 'e0d1d727c618f0c9e471efcc675347d5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:11', NULL, '200513977'),
(1391, '1507087', 'KA1507087', 'IJWPewg9', '68327a23df4149177777c27e55bd292f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:12', NULL, '200513977'),
(1392, '1507088', 'AB1507088', '0RUO3JHO', '8371ca8862b0ae1fa8c04dab9a17a5f6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:14', NULL, '200513977'),
(1393, '1507090', 'DM1507090', 'iD5Gyhkr', '0e94a2355bc4f7c31197acc75b31a9c1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:15', NULL, '200513977'),
(1394, '1507091', 'RR1507091', 'KL8uD86D', '1f086f8a8701815e1e558d679b715b09', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:17', NULL, '200513977'),
(1395, '1507092', 'JP1507092', 'Ebju0J2b', 'b58b3c5926bb7b0c5ef9d270053019ff', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:18', NULL, '200513977'),
(1396, '1507153', 'RR1507153', 'zKEG4W0W', '23a8ed677ce143a41977e1cc429416ca', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:20', NULL, '200513977'),
(1397, '1507251', 'RA1507251', 'yOTQ4dPl', '8d2fad7b340324653699d4e48c931ee8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:21', NULL, '200513977'),
(1398, '1508008', 'GP1508008', 'LqQcAWef', '2acd01c0121f363b78087a418c8c875c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:22', NULL, '200513977'),
(1399, '1508009', 'JE1508009', '1O4PXsEZ', 'c986aab6a9ec13470b024df41bb7558f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:24', NULL, '200513977'),
(1400, '1508010', 'AM1508010', 'uqKEMpJl', '61385afbfa492341ca4528e93ddd3932', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:25', NULL, '200513977'),
(1401, '1508129', 'KB1508129', '33LWihBZ', '654a4db8545df607bf668b780214263e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:26', NULL, '200513977'),
(1402, '1508130', 'JN1508130', 'w5F3kUiZ', '76d00f312d684fbf745589ea17c60563', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:28', NULL, '200513977'),
(1403, '1508131', 'FB1508131', 'jJpG1jOR', '23b3eab5df0094d89e6995a4cc93575a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:29', NULL, '200513977'),
(1404, '1508132', 'LS1508132', 'S92GNTgA', 'b6155ce78537c80d4019ab4cd69cf16e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:31', NULL, '200513977'),
(1405, '1508133', 'AG1508133', '1aEtYO7s', '104840e1d31720618622d119c8387414', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:32', NULL, '200513977'),
(1406, '1508134', 'MK1508134', 'OG8Lgzn4', '08cb1f6568139974a5f41ca05bc677f3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:33', NULL, '200513977'),
(1407, '1508135', 'VB1508135', 'uu5mHnLH', 'b9ef37fe9dbd545755fe749eebf72fd6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:35', NULL, '200513977'),
(1408, '1508136', 'LS1508136', 'kCQgdBqz', '84acd60edc0c58956ef0413b9114aa72', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:36', NULL, '200513977'),
(1409, '1508137', 'RO1508137', 'au1DhFjm', '915de4f9405f0f902dfa6d48b6be3215', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:38', NULL, '200513977'),
(1410, '1509001', 'MG1509001', '3HoAcR4d', '07cf24f3b1bf6a5b1c295d426e7e12bc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:39', NULL, '200513977'),
(1411, '1509108', 'CD1509108', '3egKYedO', 'b97a166da484ec15d5d8d4e2fc3e9226', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:40', NULL, '200513977'),
(1412, '1509137', 'ES1509137', 'U34mCeyN', '859a3316506afe29c9722a8a8d71a44d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:42', NULL, '200513977'),
(1413, '9709001', 'RV9709001', 'ITPYZcGM', '2de850f7cf3379b0fb8b7dad14d881ea', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:43', NULL, '200513977'),
(1414, '9710001', 'RN9710001', '161dtgtD', '9a4d37b63e568f83a9f9270da5405673', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:45', NULL, '200513977'),
(1415, '9903001', 'AC9903001', 'UwHag0n4', '09da329b3af876abe6d9ca8f120ab2e7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:46', NULL, '200513977'),
(1416, '307001', 'EP307001', 'faZr71wd', 'ad7a4e1dc5917663a32fe794a0079149', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:48', NULL, '200513977'),
(1417, '707088', 'TG707088', 'nIzygdck', '70693509bf0986a0b273a8ee70c88748', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:49', NULL, '200513977'),
(1418, '1102124', 'AF1102124', '6l5RXKb3', '021d8e1c09467fed7d9a29613dbc568e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:51', NULL, '200513977'),
(1419, '1104098', 'CE1104098', 'ArBrAf0N', '5afe7c68a4fd492c6fe3502c3ee392c4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:52', NULL, '200513977'),
(1420, '1301054', 'SC1301054', 'pctthGCj', '2c66773b4973f60b27ba3f3fdb9935a9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:54', NULL, '200513977'),
(1421, '1304115', 'DS1304115', 'oMb1Q6k5', 'f2fcc54be8027aeb005ff5f0c3d43cb0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:56', NULL, '200513977'),
(1422, '1304127', 'AO1304127', '7rdgYLhG', '7076db15ae6d4fff03e775372dc5825e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:57', NULL, '200513977'),
(1423, '1307150', 'EM1307150', '0DGZn5fQ', '752113b3c0022e6a5797fcca7b92069d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:39:58', NULL, '200513977'),
(1424, '1309130', 'LV1309130', '0PTWdqJ3', 'b42c24a152c9a1183c3a84b2f7557f71', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:00', NULL, '200513977'),
(1425, '1403083', 'MA1403083', 'tsaqZF1D', '1d167b229fa1822785bf410170e486c3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:01', NULL, '200513977'),
(1426, '1403084', 'DD1403084', 'qPuElinf', '49fddf540d82bfc4984ae96f3eacd1f9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:02', NULL, '200513977'),
(1427, '1403109', 'JC1403109', 'F8bzD6Sy', '00ab2d186f45d02b6b093821b9c3fc32', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:04', NULL, '200513977'),
(1428, '1403128', 'CT1403128', '7k4Mw0cS', 'c02863db293ce55315e523b080492142', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:05', NULL, '200513977'),
(1429, '1406122', 'JM1406122', 'QAsplgL4', '56ea04cc9fa5f5184587ee95d7d6aaa7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:07', NULL, '200513977'),
(1430, '1409175', 'RI1409175', 'RswHsOzZ', 'ab785e517835d1583781a0fb8f1c5319', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:08', NULL, '200513977'),
(1431, '1411083', 'NV1411083', 'xzMAOn6L', '0e185c66dec198a9428812829ad24a6d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:10', NULL, '200513977'),
(1432, '1501024', 'JS1501024', 'knkthARN', '978644b3f92ea916afa84edf2780b36f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:11', NULL, '200513977'),
(1433, '1502006', 'RL1502006', 'ctA2bKw1', 'dbbeb891f081054f745e3dc979021e32', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:12', NULL, '200513977'),
(1434, '1503104', 'IR1503104', 'MQmF9ipA', 'e91ffe167a0d6dc80bab945e61ce94a9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:14', NULL, '200513977'),
(1435, '1503111', 'JD1503111', '41KPRxDO', '27d2e249776e8ec40408efbd41019cae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:15', NULL, '200513977'),
(1436, '1504098', 'NS1504098', 'Lcx06W4g', '3f6230ff464ff3191cff8664d04d7e7f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:16', NULL, '200513977'),
(1437, '1505031', 'JS1505031', 'L1qSdsLx', '0cdf4a6baf2fa1031f698523f9a2e38d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:18', NULL, '200513977'),
(1438, '1506165', 'DM1506165', '1WDfKb3g', 'd15d5fcf2f42f16d2dde1de81491efa2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:19', NULL, '200513977'),
(1439, '1508070', 'CR1508070', 'e6LHhDHA', '437119e821f7d57a34627d9dd4acb871', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:21', NULL, '200513977'),
(1440, '1509163', 'AR1509163', '1x2O4eEe', '243a3f2c7f4e754d75d67caf6c6842f7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:22', NULL, '200513977'),
(1441, '1509164', 'CM1509164', 'hwx5Eere', '022a8e8237a8d28d969d37d232cc55b9', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:23', NULL, '200513977'),
(1442, '805053', 'RP805053', 'TbqF6s2L', '9ddc5c996a49a51c609e8fa7a921ac24', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:25', NULL, '200513977'),
(1443, '911019', 'AZ911019', 'uR402i0m', '65d1068e03cd1ec11344612288e53fcb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:26', NULL, '200513977'),
(1444, '1201007', 'IV1201007', '9zETLeXc', '171aaf67611568b91cddf92f1fd59751', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:28', NULL, '200513977'),
(1445, '1205008', 'MM1205008', 'XQPccq3c', 'c0df34e17ada8a4bbd582cdb5df9109f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:29', NULL, '200513977'),
(1446, '1208001', 'EC1208001', 'izlEyBti', '6ff1a5936e98f557282b93b97f42680c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:31', NULL, '200513977'),
(1447, '1303102', 'EE1303102', 'RXl5zBM3', 'e71a8fe2cdb2f2b54b62c5996dcd8c13', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:32', NULL, '200513977'),
(1448, '1304130', 'MM1304130', 'L3LIi7O6', '121c7a31d013e66789f61bb36923928c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:34', NULL, '200513977'),
(1449, '1311011', 'MC1311011', 'tkNl5Sy2', '4d67216e393f3ca62bbaa76bcda96c42', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:35', NULL, '200513977'),
(1450, '1312107', 'MT1312107', 'yzspXzZA', '4e4d24613219c461032ddf9e0025adaa', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:37', NULL, '200513977'),
(1451, '1410030', 'KL1410030', 'LqRNR9JW', 'bbcdd8a30302e24f040ce83b417eee50', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:38', NULL, '200513977'),
(1452, '1410170', 'JL1410170', 'ZbbfgECq', '72f37e16328ae6d7a3308021975e93ed', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:40', NULL, '200513977'),
(1453, '1504126', 'MR1504126', '7Pp62UpP', '9b6159d4318a4e9f36c8fa6320dbe8b3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:41', NULL, '200513977'),
(1454, '1507133', 'JB1507133', 'u8oGDODO', '0cc2c5362142167d5168b6bb202af45d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:42', NULL, '200513977');
INSERT INTO `mmpi_user_access` (`id`, `employee_id`, `username`, `password`, `security_key`, `validated`, `task`, `request`, `admin`, `user_mgt`, `cluster_mgt`, `flow_mgt`, `settings`, `group_mgt`, `reports`, `date_created`, `date_updated`, `creator_id`) VALUES
(1455, '1507193', 'CM1507193', 'aTjup26T', 'd4c449bdb3535c75c85ab7b343103141', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:44', NULL, '200513977'),
(1456, '1509177', 'EA1509177', 'mGgWL099', '606d878ef413d32c04fe07898596a6aa', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:45', NULL, '200513977'),
(1457, '603018', 'BQ603018', 'yF8zgpBb', 'e8e3cafab3e043fe434503e4332a623e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:47', NULL, '200513977'),
(1458, '608064', 'FE608064', 'tKdFrk2K', 'c788ca86c762b5b43f58e4f1aa68a0c4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:49', NULL, '200513977'),
(1459, '708075', 'EC708075', 'zrU19QBh', 'b5adb6f92826a3efce63cb88b8a5b9a0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:50', NULL, '200513977'),
(1460, '804067', 'MC804067', '2MQKmWMI', '11fa23421554e22db820ef2cc226d076', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:52', NULL, '200513977'),
(1461, '808061', 'ED808061', '1HUluGzb', '59f9c47cee9c63f031681a26833f287d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:53', NULL, '200513977'),
(1462, '1304122', 'KC1304122', 'knx4JBfA', 'f4bca648b58e8b16d6639ed69428b58c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:54', NULL, '200513977'),
(1463, '1411255', 'JC1411255', 'UGnGyFqU', '5d3923ddc15e9e21bb945f64a93f1487', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:56', NULL, '200513977'),
(1464, '1503081', 'EP1503081', 'ybhbaDm7', '7c4ff57464307fde80e85825e9d0b78b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:57', NULL, '200513977'),
(1465, '1503108', 'DP1503108', 'ehfwXBjp', '82c7ee3b3bd6d6daa513d51f43368aad', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:40:59', NULL, '200513977'),
(1466, '1506057', 'RR1506057', 'puq9lyU3', 'c41b366a409b2ce8178b0cfc4630532b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:00', NULL, '200513977'),
(1467, '1507196', 'MC1507196', 'GGXmB7j9', '2ced28cf5f4eb56f540955626ddb0bd5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:02', NULL, '200513977'),
(1468, '1401009', 'FV1401009', '27pcMPfO', '7d2c5899473acf13b830c27494668d4e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:04', NULL, '200513977'),
(1469, '1502007', 'RQ1502007', 'pe6FkthZ', '59555bc669baa113f64587c051b46a71', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:06', NULL, '200513977'),
(1470, '9110001', 'WG9110001', 'A4uACG5l', '0a2712e2f3194da9665afb6416be469f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:07', NULL, '200513977'),
(1471, '9110002', 'JL9110002', 'P4koKgtP', 'e40237c9ac4160dca8833c0f556fb60b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:09', NULL, '200513977'),
(1472, '9301001', 'DS9301001', '4iAfiKfM', '6109062f1a5c930cb7823918297aa65b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:10', NULL, '200513977'),
(1473, '9401002', 'JJ9401002', 'hUWTSbrt', '18051d3df0c6943ff9c505a9c6cddae5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:11', NULL, '200513977'),
(1474, '1410203', 'LM1410203', '8yQCnWmc', '1a983a394f33d8376ec43415e2b98b31', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:13', NULL, '200513977'),
(1475, '1411234', 'KC1411234', 'QSJCZJEi', 'aa0b5790fd7fb07ce56b0001bdd569e6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:15', NULL, '200513977'),
(1476, '1412041', 'AS1412041', 'KYDji6Is', '501469e03e615b981c2461f739505f1d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:16', NULL, '200513977'),
(1477, '1505053', 'CF1505053', '3aIPh3i0', '49497bcea65d121da5031c217e27b6f0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:18', NULL, '200513977'),
(1478, '1506033', 'AC1506033', 'YU4Y1ORQ', 'bb6199da05574e89d2b50cf01e09849a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:19', NULL, '200513977'),
(1479, '1506064', 'RV1506064', 'GLSnKA3n', '75ad377e552ad1f5a28666141a79fbd7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:21', NULL, '200513977'),
(1480, '1506140', 'RB1506140', 'GNTRS6wR', '12f3361115067b558febf145f488347c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:22', NULL, '200513977'),
(1481, '1506287', 'AF1506287', 'rjUwMUM8', '583370252e227115234d7c2d3f28f64e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:24', NULL, '200513977'),
(1482, '1507051', 'CO1507051', 'nEePkfaZ', '8c54e30b33139d5a54d6e58b8ee1beac', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:25', NULL, '200513977'),
(1483, '1507119', 'JY1507119', 'OM0x2qms', '0d53d61b4eb27e08acf7ab4b4f18d920', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:26', NULL, '200513977'),
(1484, '1509050', 'MZ1509050', 'W2fREtYp', 'af107514e3a639e5c40953f2abbfff5c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:28', NULL, '200513977'),
(1485, '204002', 'LE204002', 'Gewz68mY', '8b690aebe573f2fd845f05aea8803e98', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:29', NULL, '200513977'),
(1486, '707089', 'PC707089', 'fIXYGKmP', '4413d5d58cbe9428d1ba54ccdd80f84c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:31', NULL, '200513977'),
(1487, '1107053', 'SA1107053', 'dyOoilPl', 'a4b40e5cad08140230f052bb251ccf14', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:32', NULL, '200513977'),
(1488, '1301048', 'RA1301048', '7o0NGsW7', 'f386c5aa339fcb96e4a21af9429c4937', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:34', NULL, '200513977'),
(1489, '1306107', 'JB1306107', 'tfY5rx27', '255f879e58bc87d462e1e56140ab4687', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:36', NULL, '200513977'),
(1490, '1407065', 'SG1407065', 'TzkyUf48', '6e0c41623a4e4fdc344f7087b17118bc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:37', NULL, '200513977'),
(1491, '1505109', 'AG1505109', 'k9wPxYbo', '581ce0136a34bb0fc43264732a045245', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:39', NULL, '200513977'),
(1492, '1205260', 'RS1205260', 'j60BAJKq', '83a687e3dc78d8e8a96a635f5a8261ea', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:40', NULL, '200513977'),
(1493, '1403111', 'JL1403111', '4ceExtfE', 'a2f2e45eccb9d175302fa44eedba58f3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:42', NULL, '200513977'),
(1494, '1506244', 'KC1506244', 'bbWjnfk4', 'aa1a98c2662e7989a309c03d626ff582', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:43', NULL, '200513977'),
(1495, '9001', 'CV9001', 'abQaakDr', '42ff70f5ef1ccdf90e13e1793a9c25e2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:45', NULL, '200513977'),
(1496, '108002', 'RM108002', '8RCLqyfj', 'e9c51d73a228e7c6ecaeac2cbe7e2374', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:46', NULL, '200513977'),
(1497, '108004', 'QC108004', '8rOmBOYb', '63451d3ce416f2ff607710b2cae368cf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:48', NULL, '200513977'),
(1498, '204001', 'MG204001', 'q1SuUGMn', 'd8809c8d343be7a05d385d66eaf1ae5c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:49', NULL, '200513977'),
(1499, '305002', 'LL305002', 'aJ2hc5SR', '1646f0495483a5027e6e203df61d8a91', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:51', NULL, '200513977'),
(1500, '401010', 'RB401010', 'FUFgQCNf', 'eb1f23fdb4fa3b5cbb60a6aadfa40154', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:52', NULL, '200513977'),
(1501, '401011', 'RE401011', 'WPEOkRGN', '1ffafca4241c1722bc1efccdfe0593b5', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:54', NULL, '200513977'),
(1502, '401023', 'JN401023', 'w4KNQ2ui', 'ba051edc9032b91e5c6f7e1b3b449ff2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:55', NULL, '200513977'),
(1503, '405004', 'MP405004', 'ioLt5has', '33eb56edc52a25b41c9a5ac5320fe880', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:57', NULL, '200513977'),
(1504, '409008', 'MC409008', 'uYuyT8db', '65fb277a4a58acfcbf8e0637ef458dae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:41:58', NULL, '200513977'),
(1505, '411003', 'GF411003', '9Gd6Zuk6', '0554202f796ee62cc63b02289ce7308b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:00', NULL, '200513977'),
(1506, '509019', 'QB509019', 'KMEr3h5w', '7e6016e6ab41b268b2d12fc898105f7f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:01', NULL, '200513977'),
(1507, '705010', 'JK705010', '3ghDNxbB', '4de2d16acad805c26e2bcb1081d20c7e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:03', NULL, '200513977'),
(1508, '712010', 'DC712010', 'JdZCcQRC', '4c91e0844871f679bf1e0dbe2c34e45d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:04', NULL, '200513977'),
(1509, '802054', 'AP802054', '2QtOf2pa', '22e62555ef40dc272a0e2064ed607b54', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:05', NULL, '200513977'),
(1510, '907021', 'JM907021', '0999QTDg', '89fac87f0107f61a84cdeefd91fc3a08', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:07', NULL, '200513977'),
(1511, '1007098', 'NM1007098', 'mXh4NWb7', '04ecb0583bca340e9a35a0ceec15cc01', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:08', NULL, '200513977'),
(1512, '1101105', 'LR1101105', 'XppTPYAt', '2c81667d4aee3997bdf5c618dd1a5516', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:10', NULL, '200513977'),
(1513, '1104029', 'JS1104029', 'iGU3mjSB', '51749b76851dd248833317fc512a05d2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:11', NULL, '200513977'),
(1514, '1104030', 'PC1104030', 'ACm85PJg', 'a26333600d3b6c9744e777f761a115d3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:12', NULL, '200513977'),
(1515, '1105087', 'JM1105087', 'Jw1C55Rl', '29300830b422ece877d5c75a6cfd3d24', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:14', NULL, '200513977'),
(1516, '1107017', 'BG1107017', 'fLGnncHh', '17784087b2ef8553e7be23a72eb9f638', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:15', NULL, '200513977'),
(1517, '1111073', 'JI1111073', '81tp2Ict', '7e02da421422315de916bc1ef277abbd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:17', NULL, '200513977'),
(1518, '1112084', 'JP1112084', 'n9S77ytJ', '1ac949eed17ea46e70f825b75c71d07c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:18', NULL, '200513977'),
(1519, '1206201', 'LP1206201', '7AbF53ra', 'c950c465d38b4c5303685ac56062d061', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:20', NULL, '200513977'),
(1520, '1211024', 'JI1211024', 'e07xYM6Y', 'cbf8cbf43a1ca69a50b3ea92a5770448', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:21', NULL, '200513977'),
(1521, '1211155', 'GV1211155', 'icFYt3lp', '656f15566b7138b84af9affcbc0086c7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:22', NULL, '200513977'),
(1522, '1301027', 'VF1301027', 'HeNjL3sn', 'df761993a73533540cb432f571a2efda', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:24', NULL, '200513977'),
(1523, '1302184', 'BN1302184', 'Ze01MrES', '5f1a39d6a5fc6941964b391953bcab24', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:25', NULL, '200513977'),
(1524, '1305081', 'JA1305081', 'Zjxkaq51', 'e781c369efef452db1aaa48c7e520784', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:27', NULL, '200513977'),
(1525, '1307026', 'AB1307026', 'Z5qkqFNw', '4e2e50fb2e6b91e183691d40a8d47799', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:28', NULL, '200513977'),
(1526, '1307079', 'RB1307079', 'oPIroLMq', '70b3a0e909f2689f0695f2c506049c61', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:30', NULL, '200513977'),
(1527, '1309012', 'MB1309012', 'i3c2FnHm', 'b84d2c563d3f112a86f53561a02c77d2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:31', NULL, '200513977'),
(1528, '1310023', 'BR1310023', '6qje4lun', '7cf1cb7fac504366aafc5017d424c566', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:32', NULL, '200513977'),
(1529, '1310051', 'WB1310051', 'U0eB6pwk', 'e43129f9e0cbef1c9b03e809422e8de7', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:34', NULL, '200513977'),
(1530, '1311049', 'SD1311049', 'PpHJQG2B', '46016f29ec9dde6de002f7d0fa747c30', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:35', NULL, '200513977'),
(1531, '1403127', 'IN1403127', 'x9ZzFpjW', '8ca3046009dd70869156c2a953c1c0fc', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:36', NULL, '200513977'),
(1532, '1405172', 'MB1405172', 'GAidhgIy', '187b177aec43e07ed50f6fe25bf38a10', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:38', NULL, '200513977'),
(1533, '1409119', 'ML1409119', 'jN3ldEX0', '0695e2e870edc9a801310a59a41764f2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:39', NULL, '200513977'),
(1534, '1502125', 'AV1502125', 'PXmTshi8', 'c3590c3289c98065d47f6cd79f29a20f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:41', NULL, '200513977'),
(1535, '1502126', 'FT1502126', 'E74WiFHs', '4bd8a32ded8608991a573df59c53d9a6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:42', NULL, '200513977'),
(1536, '1504041', 'PP1504041', 'juNJSozt', '459bf28954e4cf302f3b5fc5e8acf70a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:43', NULL, '200513977'),
(1537, '1504042', 'MV1504042', 'TOlGYb66', '2fdc0d3f1030e6c51b07844d92dd3b95', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:45', NULL, '200513977'),
(1538, '1504097', 'GE1504097', 'JTO95wbH', '3051ad050b03d82664ff8ee3721349fb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:46', NULL, '200513977'),
(1539, '1507052', 'DM1507052', '82H0tIBg', '4b50090e2fd9b880809deb437cad6bb1', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:48', NULL, '200513977'),
(1540, '8602001', 'PS8602001', '5mHLssnD', '44773a7adf10f3b598652388f7fbe9f3', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:49', NULL, '200513977'),
(1541, '9305001', 'FM9305001', 'TU6b6ooH', '99d95658acfd9109125240b909ca20b6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:50', NULL, '200513977'),
(1542, '9811001', 'LO9811001', 'ofCd3QLz', 'c7c6f309731cd6da71f84daa856f64ec', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:52', NULL, '200513977'),
(1543, '1407004', 'ZR1407004', 'Xuz7JFcd', '7cee0778934794e30789256b2a0a2db8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:53', NULL, '200513977'),
(1544, '1505144', 'RO1505144', 'BUjN9H10', 'd78646843861141ee7b37c30b969fcda', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:55', NULL, '200513977'),
(1545, '1509006', 'HM1509006', '1pMYO6Qq', 'ce5d428a03b35de156e34e8d0679b260', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:56', NULL, '200513977'),
(1546, '205002', 'MC205002', 'arSidgFp', '4e751c96b026e7455f877e4fdd4a33ff', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:58', NULL, '200513977'),
(1547, '1405017', 'KT1405017', 'yCT6Z6eC', '2f7b868b4b9b2511299f598dabe52d09', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:42:59', NULL, '200513977'),
(1548, '1502113', 'GD1502113', 'XnuLwNO2', '2ef3561c5c4f806e0e191541bd528121', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:01', NULL, '200513977'),
(1549, '105001', 'MP105001', 'sbkCQFUm', '99cb118ebd71617679320875881ac537', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:02', NULL, '200513977'),
(1550, '506006', 'MG506006', 'Ssc9UeXu', '48971d1bdb4b4ad9b972ed10d5f33abf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:04', NULL, '200513977'),
(1551, '1103180', 'DM1103180', 'UZqiJ8Wl', '6f8e6157af0cbb9da4b74fb76558651f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:05', NULL, '200513977'),
(1552, '1104046', 'MD1104046', 'JsB9DZDH', 'd84929caac888a08f00d25f9090c72b8', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:07', NULL, '200513977'),
(1553, '1204080', 'LS1204080', 'ZxfepUy6', 'e263cbebdefd990e7d137444b3658f4a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:08', NULL, '200513977'),
(1554, '1207193', 'JP1207193', 'SNFXZHeu', '9444eb02e1ac43d9b4fa31fbc4e64e40', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:09', NULL, '200513977'),
(1555, '1305079', 'MA1305079', 'uGmAUk0K', '211262ffb6cc77f0b83b25e4b59c6bae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:11', NULL, '200513977'),
(1556, '1307107', 'MA1307107', '2nrW8ZMt', 'efb46b48f0601812724655db6bddba0f', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:12', NULL, '200513977'),
(1557, '1311048', 'SD1311048', 'MkunddeK', '0605c795b355ab33f912c124fd83e35e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:13', NULL, '200513977'),
(1558, '1405003', 'CZ1405003', '0nRrNaBF', '11180bf4f780140322f4f5cf182d1106', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:15', NULL, '200513977'),
(1559, '1405065', 'SR1405065', 'iipg2NFE', '2ecd1524a7436121fa4052d9d5186c63', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:16', NULL, '200513977'),
(1560, '1408048', 'CA1408048', 'rRoz518k', '7e1c0fd0e85366b676342e2f382a4b65', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:17', NULL, '200513977'),
(1561, '1410031', 'SN1410031', 'zWxcunUR', 'c27ccef4a0a020587636ae364d657093', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:19', NULL, '200513977'),
(1562, '1501094', 'SB1501094', 'BHTQazEH', '3b36df179ebe975db006642f654f5d6c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:20', NULL, '200513977'),
(1563, '1503105', 'ER1503105', 'LXNd18Do', '5fcf9a0aa4d3db0d4796c6b45b8da85e', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:22', NULL, '200513977'),
(1564, '1505029', 'HN1505029', '48i25N1J', '6b6e5585b9a3156f0000f114ce6f1516', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:23', NULL, '200513977'),
(1565, '1505032', 'EG1505032', 'A9lfYM6A', '0df955053d2206a722cb5b15e00ed056', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:24', NULL, '200513977'),
(1566, '1505104', 'ED1505104', 'wImRrosP', '46045af1b039b390abe4eaebcd64e224', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:26', NULL, '200513977'),
(1567, '1505120', 'EG1505120', 'ScgLnq8P', '8e9ab654ad195a04441a04fd2a012a23', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:27', NULL, '200513977'),
(1568, '1506001', 'MV1506001', 'uTtMeyhD', 'ffa7dadfb959b1192299f48f04d62341', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:28', NULL, '200513977'),
(1569, '1506243', 'IB1506243', 'xlnDPfJs', '312589360da54a1b0012bb16bfc7fa65', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:30', NULL, '200513977'),
(1570, '1506313', 'MB1506313', '6TlJuCDL', '7fba33477e0f4ceb9ec957cac4680e8c', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:31', NULL, '200513977'),
(1571, '1507195', 'PA1507195', '6a0K4hRj', 'ce8c73aca50537a006bb7132d7197907', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:33', NULL, '200513977'),
(1572, '1508128', 'GD1508128', 'OnNO8Yhx', '2c622e3579e24f4bcd55d0ab200d06ac', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:34', NULL, '200513977'),
(1573, '1509110', 'MR1509110', 'DlCWO8gC', 'e595c7cf6098e8551217a2f555ccb487', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:35', NULL, '200513977'),
(1574, '9005001', 'MM9005001', 'bdr9DGhJ', 'e18b800a7e44febba703388f50719538', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:37', NULL, '200513977'),
(1575, '201001', 'RG201001', 'nqIxxL5G', '5997cab039dfff2035b6be3872741e09', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:38', NULL, '200513977'),
(1576, '402008', 'BQ402008', 'DiAzQiPn', 'b9fcd65b63db02e33bf52db41f807751', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:40', NULL, '200513977'),
(1577, '407002', 'ME407002', 'Bxwg9gHE', '724f21c3dcc05e5ffe95780dd887c365', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:41', NULL, '200513977'),
(1578, '412011', 'JA412011', '89LlIAYr', '5e4a8c8efb73c3ee8f6185b53b99048b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:43', NULL, '200513977'),
(1579, '507011', 'ME507011', 'd6FWMutg', '891907ec5eebd3e8257030cf96694cd6', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:44', NULL, '200513977'),
(1580, '607028', 'JB607028', '2RG10giE', 'cff9fffc58c953592994a3a707b2e9ea', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:46', NULL, '200513977'),
(1581, '804005', 'JP804005', 'FI4dx0uD', '820d17d648086a668a46bd4398edc6bf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:47', NULL, '200513977'),
(1582, '805042', 'CC805042', 'KFS35MO8', '10223dac3bf3f8032fa4f3e3133168ca', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:48', NULL, '200513977'),
(1583, '809039', 'RA809039', 'zLyjfAhU', '3ba30513b2b919e43887860a1e249c1b', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:50', NULL, '200513977'),
(1584, '910036', 'EA910036', 'BNsEG4xi', 'b451a4ef1a14ed21f49e8d5ffd9dd800', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:51', NULL, '200513977'),
(1585, '1109071', 'RO1109071', 'yfIRcaCa', '9e1bc45624599ee0875fc4e9c64a4512', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:53', NULL, '200513977'),
(1586, '1110031', 'CR1110031', 'qdSsAfnu', 'b57838e4a9dcbb25542de2bbdea625ce', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:54', NULL, '200513977'),
(1587, '1201124', 'RC1201124', 'TuBwUzdZ', '986c4e877da09d9b0c1cd1b75c922487', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:55', NULL, '200513977'),
(1588, '1202137', 'NG1202137', 'rkJosqRO', 'ff309a3f8edaf6c7524a192f95e10887', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:57', NULL, '200513977'),
(1589, '1203014', 'AL1203014', 'rYrOS8xl', '326a2aa296e29f31691058f2d26e44d0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:43:58', NULL, '200513977'),
(1590, '1205187', 'MP1205187', 'YMnjzqNL', '68340aa19cd6bc496ed51375a6dd5201', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:00', NULL, '200513977'),
(1591, '1205259', 'RM1205259', 'bUuSgAti', '1a597078e63ba892fb6a58cd859ecb22', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:01', NULL, '200513977'),
(1592, '1205272', 'DI1205272', '9bQMsz89', '0a54d554f673d6cbd3e994550b794ebf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:03', NULL, '200513977'),
(1593, '1206050', 'KT1206050', '6eIbLJdL', '830a334efd30868c2107cf7923f881f4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:04', NULL, '200513977'),
(1594, '1304055', 'FD1304055', 'KPtPQkDF', 'd6655501345a433fa4e13dedb7a00897', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:06', NULL, '200513977'),
(1595, '1306077', 'RT1306077', 'S7uffdOe', '56555bc3ffa80e020901ac1ff65168fe', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:07', NULL, '200513977'),
(1596, '1310115', 'RL1310115', 'PyGJgcHM', '3775af139c0e35210f54a55973704961', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:09', NULL, '200513977'),
(1597, '1401028', 'HR1401028', 'IRuwAi7x', 'e6d00abc2d5a18925d09d0076ab1c803', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:10', NULL, '200513977'),
(1598, '1402074', 'RL1402074', 'AlJHOn0o', '9310592dd28a7c69dfab1e288e02d597', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:11', NULL, '200513977'),
(1599, '1402212', 'MT1402212', 'ZFjYkzpB', '64d43f7ff9aa1c9cc46396f0ed59aec4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:13', NULL, '200513977'),
(1600, '1403077', 'AV1403077', '3bxIYynG', 'da5a829d63a626b6767c0f0543eb9e3d', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:14', NULL, '200513977'),
(1601, '1403112', 'RT1403112', '2ehXzHed', 'dffb4498b60e94e1824dfc868cecdbfd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:16', NULL, '200513977'),
(1602, '1403200', 'EB1403200', 'PLgbhAtH', 'ab565fa9945e81419d484365458226ae', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:17', NULL, '200513977'),
(1603, '1403203', 'AV1403203', 'WGaSud2K', 'c85bdef31bb458bacada793309fe05cd', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:18', NULL, '200513977'),
(1604, '1408073', 'PD1408073', 'UggI9lyi', '9a541c8071525f035080677aea534e81', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:20', NULL, '200513977'),
(1605, '1408089', 'RP1408089', 'ea03of1U', 'e3046ad832f6360a6f97c4bf07a59140', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:21', NULL, '200513977'),
(1606, '1411283', 'JC1411283', 'l4FYAoaM', '200985785780309b9bbe80168baf5dd4', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:23', NULL, '200513977'),
(1607, '1412177', 'CG1412177', 'HKKkdfQx', '037ae5f797ee3147a2aa097bea62915a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:24', NULL, '200513977'),
(1608, '1412178', 'RR1412178', '2xedUM2D', '8d6435098f293d8fbc551d31f5bc3305', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:26', NULL, '200513977'),
(1609, '1503107', 'GO1503107', '418WlNqe', 'd1a7c45768dc4aa2600fbb60230e6bf2', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:27', NULL, '200513977'),
(1610, '1503178', 'AE1503178', 'sK2gBMEl', 'ebad44aa03e453abc6e5ede9fcae1a85', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:28', NULL, '200513977'),
(1611, '1503244', 'EM1503244', 'MXjaqhCh', 'e39a59e5a98d05e2411cef8f4a79691a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:30', NULL, '200513977'),
(1612, '1503276', 'SF1503276', 'sdf9RCa8', '1e4c83d56780bf89e281302b882d1f29', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:31', NULL, '200513977'),
(1613, '1505066', 'DB1505066', 'KCFb6HYg', 'f110a84a085cff3c6371ab9be18eafaf', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:32', NULL, '200513977'),
(1614, '1505067', 'BP1505067', '1JWPDd2E', '67f8bb51485ae635fbb2d10b3247c5ea', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:34', NULL, '200513977'),
(1615, '1505207', 'WR1505207', '7UyIEowR', 'a66776e9af96c6754e830d8443e57377', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:35', NULL, '200513977'),
(1616, '1506339', 'GR1506339', 'Q1H161OL', 'beac5f349c355bdca5cc83eff947a44a', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:36', NULL, '200513977'),
(1617, '1507253', 'LR1507253', 'Zj3SSNzB', 'd40a4471f5a7c68dfe3766462f84bd45', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:38', NULL, '200513977'),
(1618, '7903001', 'TF7903001', 'lxuQzEUm', '03d693cf3b99573141cf81e2aaacbe63', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:39', NULL, '200513977'),
(1619, '8903001', 'AS8903001', 'bHHPNcjB', 'b87b188f32b61316cb6e2155d5b72d84', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:41', NULL, '200513977'),
(1620, '9206001', 'AD9206001', 'eaoKbJj1', 'a3af1b6f830c0ba2f207c473ddffdf25', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:42', NULL, '200513977'),
(1621, '9604001', 'GE9604001', '0YgDoqIk', 'dcf373c595ed509c14c338524aee66f0', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-24 12:44:44', NULL, '200513977'),
(1622, '696969', 'JA696969', 'FeQfds91', '810be069e12ca1c6064befdb67b1ef25', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-25 06:17:14', NULL, '200513977'),
(1623, '686868', 'SA686868', 'gx9JDNUR', 'c97d64b0eb4c3cdafabb6f4a04280f1b', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-25 06:54:00', NULL, '200513977'),
(1624, '676767', 'JA676767', '1sBpgdm4', '2ea3f5a30f5e684755bee344f7b7adab', 'Sample', 1, 1, 0, 0, 0, 0, 0, 1, 1, '2015-09-27 08:58:34', NULL, '200513977'),
(1625, '656565', 'JA656565', 'Eepr0dOM', '951dc107183ed89f485e089384f50d9a', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-27 09:31:23', NULL, '200513977'),
(1626, '646464', 'JA646464', '5Ohme7Py', 'cc3ab7c32786b13ee9e619c90362b6cf', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-27 09:34:00', NULL, '200513977'),
(1627, '636363', 'JA636363', 'YkOdtznm', 'b67491628b9105bd5f276ae6ef3b0e96', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-27 09:35:16', NULL, '200513977'),
(1629, 'JOMAR', 'JOMAR', 'JOMAR', 'JOMAR', 'JOMAR', 1, 1, 1, 1, 1, 1, 1, 1, 1, '2015-09-28 09:02:13', NULL, 'JOMAR'),
(1630, 'PANCHO', 'PANCHO', 'PANCHO', 'PANCHO', 'PANCHO', 1, 1, 1, 1, 1, 1, 1, 1, 1, '2015-09-28 09:03:06', NULL, 'PANCHO'),
(1631, 'JAMES', 'JAMES', 'JAMES', 'JAMES', 'JAMES', 1, 1, 1, 1, 0, 0, 0, 0, 0, '2015-09-28 09:05:37', NULL, 'JAMES'),
(1632, 'JAM', 'JAM', 'JAM', 'JAM', 'JAM', 1, 1, 1, 1, 1, 1, 1, 1, 1, '2015-09-28 09:07:03', NULL, 'JAM'),
(1634, '999999', 'SA999999', 'NqW8cnWt', '979e39f3eab8401dd7e3626dadb968fb', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-29 13:35:53', NULL, '200513977'),
(1635, '109991231', 'MM109991231', 'YXBEUxhL', '2b04b4981667d05d9e66e381056a0153', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-29 21:47:32', NULL, '200513977'),
(1636, '2423234234', 'BK2423234234', 'q4NZjqMN', 'da2f67a416d5c8d7a3dda447e3242e63', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-29 22:01:53', NULL, '200513977'),
(1637, '41234234234234', 'BK41234234234234', 'bFJboB6q', '32aefee26faf67a2191996e2b3e0488e', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-29 22:06:36', NULL, '200513977'),
(1638, '100114098', 'JB100114098', 'TtHdJdFS', '3450bf57c5495a70eed24c0ed436770b', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-29 22:13:39', NULL, '200513977'),
(1639, '123456', 'MF123456', 'zrJpfsmy', 'f7bfda429d3e16c3acd7f6b67875ab8c', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-30 06:58:02', NULL, '200513977'),
(1640, '201213804', 'MY201213804', 'DBxdUb77', '17638437378f7b602a00de11b80b761f', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-30 14:39:33', NULL, '200513977'),
(1641, '200', 'zz200', 'ntyj3A88', '94cc814c977027dd43f0bacd695e22f4', 'Sample', 1, 1, 1, 1, 1, 1, 0, 1, 1, '2015-09-30 15:01:23', NULL, '200513977'),
(1642, '324234234', 'ZZ324234234', 'bIdMdkit', '19ff5c1b1bbbfcff8a168ee686e875fb', 'Sample', 1, 1, 0, 0, 1, 0, 0, 0, 0, '2015-09-30 15:20:17', NULL, '200513977');

-- --------------------------------------------------------

--
-- Table structure for table `ref_access_type`
--

CREATE TABLE IF NOT EXISTS `ref_access_type` (
  `access_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`access_type_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ref_access_type`
--

INSERT INTO `ref_access_type` (`access_type_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `status_id`) VALUES
(2, 'Sample', 'Sample', '2015-09-23 04:06:50', '0000-00-00 00:00:00', 'Gene', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_category`
--

CREATE TABLE IF NOT EXISTS `ref_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ref_category`
--

INSERT INTO `ref_category` (`category_id`, `name`, `code`) VALUES
(1, 'Human Resources', 'HR'),
(2, 'Purchasing Related Request', 'PR');

-- --------------------------------------------------------

--
-- Table structure for table `ref_data_type`
--

CREATE TABLE IF NOT EXISTS `ref_data_type` (
  `data_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `reference` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`data_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ref_data_type`
--

INSERT INTO `ref_data_type` (`data_type_id`, `name`, `reference`) VALUES
(1, 'Text', NULL),
(2, 'Text Area', NULL),
(3, 'Dropdown', NULL),
(4, 'Date', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_department`
--

CREATE TABLE IF NOT EXISTS `ref_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  `code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`department_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `ref_department`
--

INSERT INTO `ref_department` (`department_id`, `name`, `description`, `date_created`, `created_by`, `status_id`, `code`) VALUES
(1, 'Human Resource', 'Human Resource', '2015-09-23 04:45:48', 'Gene', 1, 'HR'),
(2, 'Accounting', 'Accounting', '2015-09-24 09:55:04', 'Gene', 1, 'AC'),
(3, 'Corporate Services Department', 'Corporate Services Department', '2015-09-24 09:55:04', 'Gene', 1, 'CS'),
(4, 'Treasury Receivables Operations Support', 'Treasury Receivables Operations Support', '2015-09-24 09:55:40', 'Gene', 1, 'TR'),
(72, '3 Cents Marketing', '3 Cents Marketing', '2015-09-24 12:33:02', 'Gene', 1, 'CM'),
(73, 'Administrative', 'Administrative', '2015-09-24 12:33:57', 'Gene', 1, 'AD'),
(74, 'Audit', 'Audit', '2015-09-24 12:35:26', 'Gene', 1, 'AU'),
(75, 'Bayswater Realty and Development Corporation', 'Bayswater Realty and Development Corporation', '2015-09-24 12:35:51', 'Gene', 1, 'BR'),
(76, 'Carmona', 'Carmona', '2015-09-24 12:35:52', 'Gene', 1, 'CA'),
(77, 'Corporate Services', 'Corporate Services', '2015-09-24 12:39:47', 'Gene', 1, 'CS'),
(78, 'Information Technology', 'Information Technology', '2015-09-24 12:40:46', 'Gene', 1, 'IT'),
(79, 'Logistics', 'Logistics', '2015-09-24 12:41:03', 'Gene', 1, 'LO'),
(80, 'Maintenance', 'Maintenance', '2015-09-24 12:41:04', 'Gene', 1, 'MN'),
(81, 'MMPI (Violago)', 'MMPI (Violago)', '2015-09-24 12:41:12', 'Gene', 1, 'VI'),
(82, 'Operations', 'Operations', '2015-09-24 12:41:28', 'Gene', 1, 'OP'),
(83, 'Remedial Accounts Management and Legal', 'Remedial Accounts Management and Legal', '2015-09-24 12:41:39', 'Gene', 1, 'RA'),
(84, 'Sales and Marketing', 'Sales and Marketing', '2015-09-24 12:41:42', 'Gene', 1, 'SM'),
(85, 'Spare Parts', 'Spare Parts', '2015-09-24 12:41:44', 'Gene', 1, 'SP'),
(86, 'Training and Development', 'Training and Development', '2015-09-24 12:42:52', 'Gene', 1, 'TD'),
(87, 'Treasury (Payables)', 'Treasury (Payables)', '2015-09-24 12:42:57', 'Gene', 1, 'TP'),
(88, 'Treasury (Receivables)', 'Treasury (Receivables)', '2015-09-24 12:43:01', 'Gene', 1, 'TR'),
(89, 'Warehouse Carmona', 'Warehouse Carmona', '2015-09-24 12:43:37', 'Gene', 1, 'WC');

-- --------------------------------------------------------

--
-- Table structure for table `ref_designation`
--

CREATE TABLE IF NOT EXISTS `ref_designation` (
  `designation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`designation_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=398 ;

--
-- Dumping data for table `ref_designation`
--

INSERT INTO `ref_designation` (`designation_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `status_id`) VALUES
(1, 'Chairman', 'Chairman', '2015-09-24 09:56:15', '0000-00-00 00:00:00', 'Gene', 1),
(2, 'Executive Officer', 'Executive Officer', '2015-09-24 09:56:39', '0000-00-00 00:00:00', 'Gene', 1),
(3, 'Operations Manager', 'Operations Manager', '2015-09-24 09:56:39', '0000-00-00 00:00:00', 'Gene', 1),
(4, 'Department Manager', 'Department Manager', '2015-09-24 09:57:20', '0000-00-00 00:00:00', 'Gene', 1),
(5, 'Regional Manager', 'Regional Manager', '2015-09-24 09:57:20', '0000-00-00 00:00:00', 'Gene', 1),
(8, 'Area Manager', 'Area Manager', '2015-09-24 09:58:36', '0000-00-00 00:00:00', 'Gene', 1),
(9, 'Branch Manager', 'Branch Manager', '2015-09-24 09:58:36', '0000-00-00 00:00:00', 'Gene', 1),
(10, 'Credit Supervisor', 'Credit Supervisor', '2015-09-24 09:59:10', '0000-00-00 00:00:00', 'Gene', 1),
(11, 'Cashier', 'Cashier', '2015-09-24 09:59:10', '0000-00-00 00:00:00', '', 1),
(12, 'Account Counsellor', 'Account Counsellor', '2015-09-24 09:59:54', '0000-00-00 00:00:00', 'Gene', 1),
(13, 'Marketing Assistant', 'Marketing Assistant', '2015-09-24 09:59:54', '0000-00-00 00:00:00', 'Gene', 1),
(14, 'Branch Mechanic', 'Branch Mechanic', '2015-09-24 10:00:24', '0000-00-00 00:00:00', 'Gene', 1),
(15, 'Team Leader', 'Team Leader', '2015-09-24 10:00:24', '0000-00-00 00:00:00', 'Gene', 1),
(16, 'Staff', 'Staff', '2015-09-24 10:00:54', '0000-00-00 00:00:00', 'Gene', 1),
(17, 'Specialist', 'Specialist', '2015-09-24 10:00:54', '0000-00-00 00:00:00', 'Gene', 1),
(18, 'Associate', 'Associate', '2015-09-24 10:01:15', '0000-00-00 00:00:00', 'Gene', 1),
(19, 'Analyst', 'Analyst', '2015-09-24 10:01:15', '0000-00-00 00:00:00', 'Gene', 1),
(20, 'Agent', 'Agent', '2015-09-24 10:01:26', '0000-00-00 00:00:00', 'Gene', 1),
(310, 'Driver', 'Driver', '2015-09-24 12:33:04', '2015-09-24 00:00:00', 'Gene', 1),
(311, 'Driver/Helper', 'Driver/Helper', '2015-09-24 12:33:06', '2015-09-24 00:00:00', 'Gene', 1),
(312, 'Liaison Officer', 'Liaison Officer', '2015-09-24 12:33:09', '2015-09-24 00:00:00', 'Gene', 1),
(313, 'Warehouse Supervisor', 'Warehouse Supervisor', '2015-09-24 12:33:12', '2015-09-24 00:00:00', 'Gene', 1),
(314, 'Technician', 'Technician', '2015-09-24 12:33:15', '2015-09-24 00:00:00', 'Gene', 1),
(315, 'Repo Management Officer', 'Repo Management Officer', '2015-09-24 12:34:02', '2015-09-24 00:00:00', 'Gene', 1),
(316, 'Sales and Marketing Manager', 'Sales and Marketing Manager', '2015-09-24 12:34:41', '2015-09-24 00:00:00', 'Gene', 1),
(317, 'Credit and Collection Manager', 'Credit and Collection Manager', '2015-09-24 12:34:48', '2015-09-24 00:00:00', 'Gene', 1),
(318, 'Operation Manager', 'Operation Manager', '2015-09-24 12:34:55', '2015-09-24 00:00:00', 'Gene', 1),
(319, 'Auditor', 'Auditor', '2015-09-24 12:35:27', '2015-09-24 00:00:00', 'Gene', 1),
(320, 'Bookkeeper', 'Bookkeeper', '2015-09-24 12:35:51', '2015-09-24 00:00:00', 'Gene', 1),
(321, 'Quality Control - Staff', 'Quality Control - Staff', '2015-09-24 12:35:53', '2015-09-24 00:00:00', 'Gene', 1),
(322, 'CKD Staff', 'CKD Staff', '2015-09-24 12:35:55', '2015-09-24 00:00:00', 'Gene', 1),
(323, 'Acting Leadman', 'Acting Leadman', '2015-09-24 12:35:56', '2015-09-24 00:00:00', 'Gene', 1),
(324, 'Maintenance - Leadman', 'Maintenance - Leadman', '2015-09-24 12:35:59', '2015-09-24 00:00:00', 'Gene', 1),
(325, 'Main Line - Assembler', 'Main Line - Assembler', '2015-09-24 12:36:01', '2015-09-24 00:00:00', 'Gene', 1),
(326, 'Sub Assy Line Staff', 'Sub Assy Line Staff', '2015-09-24 12:36:02', '2015-09-24 00:00:00', 'Gene', 1),
(327, 'Sub-Assy Line Staff', 'Sub-Assy Line Staff', '2015-09-24 12:36:05', '2015-09-24 00:00:00', 'Gene', 1),
(328, 'Leadman - Stencil Section', 'Leadman - Stencil Section', '2015-09-24 12:36:07', '2015-09-24 00:00:00', 'Gene', 1),
(329, 'CKD', 'CKD', '2015-09-24 12:36:10', '2015-09-24 00:00:00', 'Gene', 1),
(330, 'Endurance Testing - Staff', 'Endurance Testing - Staff', '2015-09-24 12:36:20', '2015-09-24 00:00:00', 'Gene', 1),
(331, 'Sub Assy Frame Staff', 'Sub Assy Frame Staff', '2015-09-24 12:36:23', '2015-09-24 00:00:00', 'Gene', 1),
(332, 'Main Line - Leadman', 'Main Line - Leadman', '2015-09-24 12:36:24', '2015-09-24 00:00:00', 'Gene', 1),
(333, 'Assembly Team Lead', 'Assembly Team Lead', '2015-09-24 12:36:26', '2015-09-24 00:00:00', 'Gene', 1),
(334, 'Reworks - Staff', 'Reworks - Staff', '2015-09-24 12:36:30', '2015-09-24 00:00:00', 'Gene', 1),
(335, 'Welder', 'Welder', '2015-09-24 12:36:32', '2015-09-24 00:00:00', 'Gene', 1),
(336, 'Maintenance Staff', 'Maintenance Staff', '2015-09-24 12:36:33', '2015-09-24 00:00:00', 'Gene', 1),
(337, 'Production Manager', 'Production Manager', '2015-09-24 12:36:35', '2015-09-24 00:00:00', 'Gene', 1),
(338, 'Assembler', 'Assembler', '2015-09-24 12:36:38', '2015-09-24 00:00:00', 'Gene', 1),
(339, 'Sub-Assy Frame', 'Sub-Assy Frame', '2015-09-24 12:36:47', '2015-09-24 00:00:00', 'Gene', 1),
(340, 'Electrician', 'Electrician', '2015-09-24 12:36:49', '2015-09-24 00:00:00', 'Gene', 1),
(341, 'Production Staff', 'Production Staff', '2015-09-24 12:36:55', '2015-09-24 00:00:00', 'Gene', 1),
(342, 'Prodution Supply Team Lead', 'Prodution Supply Team Lead', '2015-09-24 12:37:10', '2015-09-24 00:00:00', 'Gene', 1),
(343, 'Tool Keeper', 'Tool Keeper', '2015-09-24 12:37:22', '2015-09-24 00:00:00', 'Gene', 1),
(344, 'MSD Unloader', 'MSD Unloader', '2015-09-24 12:37:38', '2015-09-24 00:00:00', 'Gene', 1),
(345, 'MSD Staff', 'MSD Staff', '2015-09-24 12:37:45', '2015-09-24 00:00:00', 'Gene', 1),
(346, 'Stencil', 'Stencil', '2015-09-24 12:37:48', '2015-09-24 00:00:00', 'Gene', 1),
(347, 'Sub-Assy', 'Sub-Assy', '2015-09-24 12:38:02', '2015-09-24 00:00:00', 'Gene', 1),
(348, 'Assembler / Main Line', 'Assembler / Main Line', '2015-09-24 12:38:07', '2015-09-24 00:00:00', 'Gene', 1),
(349, 'Sub Assy Stencil', 'Sub Assy Stencil', '2015-09-24 12:38:12', '2015-09-24 00:00:00', 'Gene', 1),
(350, 'Sub-Assy Engine', 'Sub-Assy Engine', '2015-09-24 12:38:20', '2015-09-24 00:00:00', 'Gene', 1),
(351, 'Assembler / Engine', 'Assembler / Engine', '2015-09-24 12:38:49', '2015-09-24 00:00:00', 'Gene', 1),
(352, 'Sub - Frame 1', 'Sub - Frame 1', '2015-09-24 12:38:57', '2015-09-24 00:00:00', 'Gene', 1),
(353, 'Production Supply', 'Production Supply', '2015-09-24 12:39:13', '2015-09-24 00:00:00', 'Gene', 1),
(354, 'Administrative Plant Manager', 'Administrative Plant Manager', '2015-09-24 12:39:38', '2015-09-24 00:00:00', 'Gene', 1),
(355, 'CKD Leadman', 'CKD Leadman', '2015-09-24 12:39:45', '2015-09-24 00:00:00', 'Gene', 1),
(356, 'LTO Supervisor (Budget)', 'LTO Supervisor (Budget)', '2015-09-24 12:39:47', '2015-09-24 00:00:00', 'Gene', 1),
(357, 'LTO Supervisor (ORCR)', 'LTO Supervisor (ORCR)', '2015-09-24 12:39:49', '2015-09-24 00:00:00', 'Gene', 1),
(358, 'Manager', 'Manager', '2015-09-24 12:39:50', '2015-09-24 00:00:00', 'Gene', 1),
(359, 'Documentation Supervisor', 'Documentation Supervisor', '2015-09-24 12:39:55', '2015-09-24 00:00:00', 'Gene', 1),
(360, 'HR Associate', 'HR Associate', '2015-09-24 12:40:24', '2015-09-24 00:00:00', 'Gene', 1),
(361, 'Talent Management Services Supervisor', 'Talent Management Services Supervisor', '2015-09-24 12:40:27', '2015-09-24 00:00:00', 'Gene', 1),
(362, 'HR Manager', 'HR Manager', '2015-09-24 12:40:36', '2015-09-24 00:00:00', 'Gene', 1),
(363, 'Sr. Programmer', 'Sr. Programmer', '2015-09-24 12:40:46', '2015-09-24 00:00:00', 'Gene', 1),
(364, 'Jr. Programmer', 'Jr. Programmer', '2015-09-24 12:40:49', '2015-09-24 00:00:00', 'Gene', 1),
(365, 'IT Manager', 'IT Manager', '2015-09-24 12:40:55', '2015-09-24 00:00:00', 'Gene', 1),
(366, 'IT Technician', 'IT Technician', '2015-09-24 12:40:57', '2015-09-24 00:00:00', 'Gene', 1),
(367, 'Sr. Network Administrator', 'Sr. Network Administrator', '2015-09-24 12:41:00', '2015-09-24 00:00:00', 'Gene', 1),
(368, 'Desktop Support', 'Desktop Support', '2015-09-24 12:41:01', '2015-09-24 00:00:00', 'Gene', 1),
(369, 'Logistics Head', 'Logistics Head', '2015-09-24 12:41:03', '2015-09-24 00:00:00', 'Gene', 1),
(370, 'Company Mechanic', 'Company Mechanic', '2015-09-24 12:41:05', '2015-09-24 00:00:00', 'Gene', 1),
(371, 'Maintenance Supervisor', 'Maintenance Supervisor', '2015-09-24 12:41:06', '2015-09-24 00:00:00', 'Gene', 1),
(372, 'Account Counselor', 'Account Counselor', '2015-09-24 12:41:14', '2015-09-24 00:00:00', 'Gene', 1),
(373, 'Mechanic', 'Mechanic', '2015-09-24 12:41:17', '2015-09-24 00:00:00', 'Gene', 1),
(374, 'Assistant Cashier', 'Assistant Cashier', '2015-09-24 12:41:18', '2015-09-24 00:00:00', 'Gene', 1),
(375, 'Operations Supervisor', 'Operations Supervisor', '2015-09-24 12:41:29', '2015-09-24 00:00:00', 'Gene', 1),
(376, 'Messenger', 'Messenger', '2015-09-24 12:41:36', '2015-09-24 00:00:00', 'Gene', 1),
(377, 'Supervisor', 'Supervisor', '2015-09-24 12:41:41', '2015-09-24 00:00:00', 'Gene', 1),
(378, 'Spareparts Credit and Collection Supervisor', 'Spareparts Credit and Collection Supervisor', '2015-09-24 12:41:44', '2015-09-24 00:00:00', 'Gene', 1),
(379, 'Sales Staff', 'Sales Staff', '2015-09-24 12:41:46', '2015-09-24 00:00:00', 'Gene', 1),
(380, 'Spare Parts Cashier', 'Spare Parts Cashier', '2015-09-24 12:41:47', '2015-09-24 00:00:00', 'Gene', 1),
(381, 'Sales Coordinator', 'Sales Coordinator', '2015-09-24 12:41:50', '2015-09-24 00:00:00', 'Gene', 1),
(382, 'Area Coordinator', 'Area Coordinator', '2015-09-24 12:41:53', '2015-09-24 00:00:00', 'Gene', 1),
(383, 'Parts Controller', 'Parts Controller', '2015-09-24 12:41:57', '2015-09-24 00:00:00', 'Gene', 1),
(384, 'Partsman', 'Partsman', '2015-09-24 12:41:59', '2015-09-24 00:00:00', 'Gene', 1),
(385, 'Spare Parts Sales and Collections Manager', 'Spare Parts Sales and Collections Manager', '2015-09-24 12:42:00', '2015-09-24 00:00:00', 'Gene', 1),
(386, 'Sales and Collection Coordinator', 'Sales and Collection Coordinator', '2015-09-24 12:42:03', '2015-09-24 00:00:00', 'Gene', 1),
(387, 'Encoder', 'Encoder', '2015-09-24 12:42:22', '2015-09-24 00:00:00', 'Gene', 1),
(388, 'Warehouseman', 'Warehouseman', '2015-09-24 12:42:29', '2015-09-24 00:00:00', 'Gene', 1),
(389, 'Stockman', 'Stockman', '2015-09-24 12:42:50', '2015-09-24 00:00:00', 'Gene', 1),
(390, 'Training Specialist', 'Training Specialist', '2015-09-24 12:42:53', '2015-09-24 00:00:00', 'Gene', 1),
(391, 'Training Coordinator', 'Training Coordinator', '2015-09-24 12:42:54', '2015-09-24 00:00:00', 'Gene', 1),
(392, 'Helper', 'Helper', '2015-09-24 12:43:39', '2015-09-24 00:00:00', 'Gene', 1),
(393, 'Warehouse Encoder', 'Warehouse Encoder', '2015-09-24 12:43:40', '2015-09-24 00:00:00', 'Gene', 1),
(394, 'Trucking', 'Trucking', '2015-09-24 12:44:03', '2015-09-24 00:00:00', 'Gene', 1),
(395, 'Warehouse - Driver', 'Warehouse - Driver', '2015-09-24 12:44:06', '2015-09-24 00:00:00', 'Gene', 1),
(396, 'Warehouse Staff', 'Warehouse Staff', '2015-09-24 12:44:37', '2015-09-24 00:00:00', 'Gene', 1),
(397, 'Warehouse Assistant/Delivery Helper', 'Warehouse Assistant/Delivery Helper', '2015-09-24 12:44:43', '2015-09-24 00:00:00', 'Gene', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_dropdown`
--

CREATE TABLE IF NOT EXISTS `ref_dropdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `item` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ref_dropdown_table_fields1_idx` (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ref_dropdown`
--

INSERT INTO `ref_dropdown` (`id`, `field_id`, `item`) VALUES
(1, 10, 'Economy'),
(2, 10, 'Business'),
(3, 17, 'No Check-in Baggage'),
(4, 17, 'Up to 15kg'),
(5, 17, 'Up to 20kg'),
(6, 17, 'Up to 25kg');

-- --------------------------------------------------------

--
-- Table structure for table `ref_group`
--

CREATE TABLE IF NOT EXISTS `ref_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ref_location`
--

CREATE TABLE IF NOT EXISTS `ref_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `ref_location`
--

INSERT INTO `ref_location` (`location_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `status_id`) VALUES
(23, 'HEAD OFFICE', 'HEAD OFFICE', '2015-09-24 12:33:01', '2015-09-24 00:00:00', 'Gene', 1),
(24, 'WAREHOUSE', 'WAREHOUSE', '2015-09-24 12:35:52', '2015-09-24 00:00:00', 'Gene', 1),
(25, 'BRANCH', 'BRANCH', '2015-09-24 12:41:12', '2015-09-24 00:00:00', 'Gene', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_position`
--

CREATE TABLE IF NOT EXISTS `ref_position` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`position_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ref_position`
--

INSERT INTO `ref_position` (`position_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `status_id`) VALUES
(1, 'Sample Position', 'Sample Position', '2015-09-23 06:34:28', '0000-00-00 00:00:00', 'Gene', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_priority`
--

CREATE TABLE IF NOT EXISTS `ref_priority` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`priority_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ref_status`
--

CREATE TABLE IF NOT EXISTS `ref_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ref_status`
--

INSERT INTO `ref_status` (`status_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `type`) VALUES
(1, 'Active', 'Active', '2015-09-23 04:06:10', '0000-00-00 00:00:00', 'Gene', 'Sample');

-- --------------------------------------------------------

--
-- Table structure for table `ref_units`
--

CREATE TABLE IF NOT EXISTS `ref_units` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`unit_id`),
  KEY `fk_ref_designation_ref_status_idx` (`status_id`),
  KEY `fk_ref_units_ref_department1_idx` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ref_units`
--

INSERT INTO `ref_units` (`unit_id`, `department_id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `status_id`) VALUES
(1, 1, 'Recruitment', 'Recruitment', '2015-09-24 10:07:42', '0000-00-00 00:00:00', 'Gene', 1),
(2, 1, 'Payroll', 'Payroll', '2015-09-24 10:07:42', '0000-00-00 00:00:00', 'Gene', 1),
(3, 1, 'Training', 'Training', '2015-09-24 10:08:09', '0000-00-00 00:00:00', 'Gene', 1),
(4, 3, 'Registration', 'Registration', '2015-09-24 10:08:46', '0000-00-00 00:00:00', 'Gene', 1),
(5, 3, 'Documentations', 'Documentations', '2015-09-24 10:08:46', '0000-00-00 00:00:00', 'Gene', 1),
(6, 3, 'OR/CR', 'OR/CR', '2015-09-24 10:09:12', '0000-00-00 00:00:00', 'Gene', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_workflow`
--

CREATE TABLE IF NOT EXISTS `ref_workflow` (
  `workflow_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `creator_id` varchar(45) NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `tat` decimal(18,2) DEFAULT NULL,
  `ref_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`workflow_id`),
  KEY `fk_ref_workflow_ref_category1_idx` (`category_id`),
  KEY `fk_ref_workflow_ref_status1_idx` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `ref_workflow`
--

INSERT INTO `ref_workflow` (`workflow_id`, `description`, `name`, `category_id`, `status`, `creator_id`, `date_created`, `date_updated`, `tat`, `ref_no`) VALUES
(1, 'Laptop Request', 'Laptop Delivery', 1, 1, '200513977', '2015-09-24 23:29:20', NULL, 2.50, 'HR201509242329'),
(2, 'Request For Plane Ticket', 'Request For Plane Ticket', 2, 1, '', '2015-09-27 09:40:30', NULL, 4.00, 'PR201509270940'),
(10, 'Sick Leave', 'Sick Leave', 1, 1, '104002', '2015-09-30 09:11:18', NULL, 4.00, 'HR20150930051107'),
(11, 'sd', 'Sampple', 1, 1, '104002', '2015-09-30 11:55:44', NULL, 3.00, 'HR20150930075533'),
(12, 'Sick Leave', 'Sick Leave', 1, 1, '104002', '2015-09-30 12:27:05', NULL, 8.00, 'HR20150930082653'),
(13, 'Vacation Leave', 'Vacation Leave', 1, 1, '104002', '2015-09-30 13:51:59', NULL, 9.00, 'HR20150930095148'),
(14, 'Sample Lang', 'Sample Lang', 2, 1, '104002', '2015-09-30 14:21:25', NULL, 9.00, 'PR20150930102114'),
(15, 'Sample', 'Sample', 2, 1, '104002', '2015-09-30 14:25:36', NULL, 6.00, 'PR20150930102523');

-- --------------------------------------------------------

--
-- Table structure for table `request_activities`
--

CREATE TABLE IF NOT EXISTS `request_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `approver_id` varchar(45) DEFAULT NULL,
  `tat` decimal(10,2) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `date_request` datetime DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `comments` longtext,
  `date_started` datetime DEFAULT NULL,
  `date_ended` datetime DEFAULT NULL,
  `activity_name` varchar(45) DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_request_activities_employe_request1_idx` (`request_id`),
  KEY `fk_request_activities_approval_status1_idx` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `request_activities`
--

INSERT INTO `request_activities` (`id`, `request_id`, `approver_id`, `tat`, `sequence`, `date_request`, `date_created`, `date_updated`, `status`, `comments`, `date_started`, `date_ended`, `activity_name`, `date_approved`) VALUES
(13, 19, '1011053', 0.50, 1, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Search Flight/Travel Agency', NULL),
(14, 19, '1104046', 0.50, 2, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Provide Recommendation', NULL),
(15, 19, '1102124', 0.50, 3, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Assess and Approve Recommended Flight', NULL),
(16, 19, '1102003', 0.50, 4, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Process Payment', NULL),
(17, 19, '1004022', 0.50, 5, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Provide Flight Details', NULL),
(18, 19, '108004', 0.50, 6, NULL, '2015-09-27 11:47:04', NULL, 1, NULL, NULL, NULL, 'Acknowledge Completion of Request', NULL),
(19, 20, '1011053', 0.50, 1, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Search Flight/Travel Agency', NULL),
(20, 20, '1104046', 0.50, 2, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Provide Recommendation', NULL),
(21, 20, '1102124', 0.50, 3, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Assess and Approve Recommended Flight', NULL),
(22, 20, '1102003', 0.50, 4, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Process Payment', NULL),
(23, 20, '1004022', 0.50, 5, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Provide Flight Details', NULL),
(24, 20, '108004', 0.50, 6, '2015-09-28 12:52:09', '2015-09-28 04:52:17', NULL, 1, NULL, NULL, NULL, 'Acknowledge Completion of Request', NULL),
(25, 22, '1011053', 0.50, 1, '2015-09-29 08:40:59', '2015-09-29 00:41:07', NULL, 2, NULL, '2015-09-29 11:29:35', NULL, 'Search Flight/Travel Agency', NULL),
(26, 22, '1104046', 0.50, 2, '2015-09-29 08:40:59', '2015-09-29 00:41:08', NULL, 2, NULL, '2015-09-29 11:32:38', NULL, 'Provide Recommendation', NULL),
(27, 22, '1102124', 0.50, 3, '2015-09-29 08:40:59', '2015-09-29 00:41:08', NULL, 2, NULL, '2015-09-29 11:33:36', NULL, 'Assess and Approve Recommended Flight', NULL),
(28, 22, '1102003', 0.50, 4, '2015-09-29 08:40:59', '2015-09-29 00:41:08', NULL, 2, NULL, '2015-09-29 11:35:07', NULL, 'Process Payment', NULL),
(29, 22, '1004022', 0.50, 5, '2015-09-29 08:40:59', '2015-09-29 00:41:08', NULL, 2, NULL, '2015-09-29 11:38:04', NULL, 'Provide Flight Details', NULL),
(30, 22, '108004', 0.50, 6, '2015-09-29 08:40:59', '2015-09-29 00:41:08', NULL, 2, NULL, '2015-09-29 11:41:59', NULL, 'Acknowledge Completion of Request', NULL),
(31, 24, '1011053', 0.50, 1, '2015-09-29 11:45:34', '2015-09-29 03:45:43', NULL, 1, NULL, NULL, NULL, 'Search Flight/Travel Agency', NULL),
(32, 24, '1104046', 0.50, 2, '2015-09-29 11:45:34', '2015-09-29 03:45:43', NULL, 1, NULL, NULL, NULL, 'Provide Recommendation', NULL),
(33, 24, '1102124', 0.50, 3, '2015-09-29 11:45:34', '2015-09-29 03:45:43', NULL, 1, NULL, NULL, NULL, 'Assess and Approve Recommended Flight', NULL),
(34, 24, '1102003', 0.50, 4, '2015-09-29 11:45:34', '2015-09-29 03:45:44', NULL, 1, NULL, NULL, NULL, 'Process Payment', NULL),
(35, 24, '1004022', 0.50, 5, '2015-09-29 11:45:34', '2015-09-29 03:45:44', NULL, 1, NULL, NULL, NULL, 'Provide Flight Details', NULL),
(36, 24, '108004', 0.50, 6, '2015-09-29 11:45:34', '2015-09-29 03:45:44', NULL, 1, NULL, NULL, NULL, 'Acknowledge Completion of Request', NULL),
(37, 33, '636363', 1.00, 1, '2015-09-30 17:18:46', '2015-09-30 09:19:02', NULL, 1, NULL, NULL, NULL, 'Sample', NULL),
(38, 34, '1011053', 0.50, 1, '2015-09-30 20:19:29', '2015-09-30 12:19:45', NULL, 1, NULL, NULL, NULL, 'Search Flight/Travel Agency', NULL),
(39, 34, '1104046', 0.50, 2, '2015-09-30 20:19:29', '2015-09-30 12:19:46', NULL, 1, NULL, NULL, NULL, 'Provide Recommendation', NULL),
(40, 34, '1102124', 0.50, 3, '2015-09-30 20:19:29', '2015-09-30 12:19:46', NULL, 1, NULL, NULL, NULL, 'Assess and Approve Recommended Flight', NULL),
(41, 34, '1102003', 0.50, 4, '2015-09-30 20:19:29', '2015-09-30 12:19:46', NULL, 1, NULL, NULL, NULL, 'Process Payment', NULL),
(42, 34, '1004022', 0.50, 5, '2015-09-30 20:19:29', '2015-09-30 12:19:46', NULL, 1, NULL, NULL, NULL, 'Provide Flight Details', NULL),
(43, 34, '108004', 0.50, 6, '2015-09-30 20:19:29', '2015-09-30 12:19:46', NULL, 1, NULL, NULL, NULL, 'Acknowledge Completion of Request', NULL),
(44, 35, '2423234234', 3.00, 1, '2015-09-30 22:26:08', '2015-09-30 14:26:30', NULL, 1, NULL, NULL, NULL, 'Sample', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_approval`
--

CREATE TABLE IF NOT EXISTS `request_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `approver_id` varchar(45) NOT NULL,
  `tat` decimal(18,2) NOT NULL,
  `sequence` int(11) NOT NULL,
  `date_request` datetime DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `comments` longtext,
  `date_started` datetime DEFAULT NULL,
  `date_approved` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_request_approval_employe_request1_idx` (`request_id`),
  KEY `fk_request_approval_mmpi_table_of_organization1_idx` (`approver_id`),
  KEY `fk_request_approval_approval_status1_idx` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `request_approval`
--

INSERT INTO `request_approval` (`id`, `request_id`, `approver_id`, `tat`, `sequence`, `date_request`, `date_created`, `date_updated`, `status`, `comments`, `date_started`, `date_approved`) VALUES
(76, 34, '1110038', 0.50, 1, '2015-09-30 20:19:29', '2015-09-30 12:19:44', NULL, 1, NULL, '2015-09-30 20:19:29', NULL),
(77, 34, '1006055', 0.50, 2, '2015-09-30 20:19:29', '2015-09-30 12:19:45', NULL, 1, NULL, NULL, NULL),
(78, 35, '123456', 3.00, 1, '2015-09-30 22:26:08', '2015-09-30 14:26:27', NULL, 1, NULL, '2015-09-30 22:26:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_fields`
--

CREATE TABLE IF NOT EXISTS `request_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `value` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_request_fields_employe_request1_idx` (`request_id`),
  KEY `fk_request_fields_table_fields1_idx` (`field_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=183 ;

--
-- Dumping data for table `request_fields`
--

INSERT INTO `request_fields` (`id`, `field_id`, `request_id`, `value`, `date_created`) VALUES
(166, 5, 34, 'S', '2015-09-30 12:19:47'),
(167, 6, 34, 's', '2015-09-30 12:19:47'),
(168, 8, 34, '3', '2015-09-30 12:19:48'),
(169, 9, 34, '3', '2015-09-30 12:19:48'),
(170, 11, 34, '3', '2015-09-30 12:19:48'),
(171, 12, 34, '3', '2015-09-30 12:19:48'),
(172, 13, 34, '3', '2015-09-30 12:19:49'),
(173, 14, 34, '3', '2015-09-30 12:19:49'),
(174, 15, 34, '3', '2015-09-30 12:19:49'),
(175, 18, 34, '3', '2015-09-30 12:19:49'),
(176, 19, 34, '3', '2015-09-30 12:19:49'),
(177, 4, 34, '2015-09-01', '2015-09-30 12:19:50'),
(178, 7, 34, '2015-09-02', '2015-09-30 12:19:50'),
(179, 16, 34, '2015-09-23', '2015-09-30 12:19:50'),
(180, 10, 34, 'Economy', '2015-09-30 12:19:50'),
(181, 17, 34, 'Up to 15kg', '2015-09-30 12:19:51'),
(182, 32, 35, 'Try', '2015-09-30 14:26:31');

-- --------------------------------------------------------

--
-- Table structure for table `request_status`
--

CREATE TABLE IF NOT EXISTS `request_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `request_status`
--

INSERT INTO `request_status` (`id`, `name`, `created_by`, `date_created`) VALUES
(1, 'In-Progress', NULL, '2015-09-25 06:09:07'),
(2, 'Completed', NULL, '2015-09-25 06:09:07'),
(3, 'Rejected', 'Gene', '2015-09-28 05:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `table_fields`
--

CREATE TABLE IF NOT EXISTS `table_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `required` bit(1) DEFAULT NULL,
  `data_type_id` int(11) NOT NULL,
  `table_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`field_id`),
  KEY `fk_table_fields_workflow_table1_idx` (`table_id`),
  KEY `fk_table_fields_ref_data_type1_idx` (`data_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `table_fields`
--

INSERT INTO `table_fields` (`field_id`, `label`, `required`, `data_type_id`, `table_id`) VALUES
(1, 'First Name', b'1', 1, 1),
(2, 'Last Name', b'1', 1, 1),
(3, 'Delivery Address', b'0', 1, 1),
(4, 'Date Required', b'1', 4, 2),
(5, 'Origin', b'1', 1, 2),
(6, 'Designation', b'1', 1, 2),
(7, 'Date of Departure', b'1', 4, 2),
(8, 'Preferred Time of Departure', b'1', 1, 2),
(9, 'Preferred Airline', b'1', 1, 2),
(10, 'Flight Class', b'1', 3, 2),
(11, 'Name of Passenger', b'1', 1, 2),
(12, 'Designation', b'1', 1, 2),
(13, 'Rank', b'1', 1, 2),
(14, 'Mobile Number', b'1', 1, 2),
(15, 'Nationality', b'1', 1, 2),
(16, 'Date of Birth', b'1', 4, 2),
(17, 'Check in Baggage', b'1', 3, 2),
(18, 'Justification', b'1', 1, 2),
(19, 'Notes', b'0', 1, 2),
(27, 'Reason', b'1', 2, 7),
(28, 'Start Date', b'1', 4, 7),
(29, 'Reason', b'1', 3, 8),
(30, 'Sample', b'1', 1, 9),
(31, 'Try', b'1', 2, 9),
(32, 'Sample', b'1', 1, 10),
(33, 'Sample', b'1', 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` varchar(45) DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_log_mmpi_table_of_organization1_idx` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `date_login`, `employee_id`, `session_id`) VALUES
(1, '2015-09-29 02:32:47', '1110038', '4omp34qsbfu062f8q4sjrsu9v0'),
(2, '2015-09-29 02:34:51', '1110038', '4omp34qsbfu062f8q4sjrsu9v0'),
(3, '2015-09-29 02:35:14', '1110038', '4omp34qsbfu062f8q4sjrsu9v0'),
(4, '2015-09-29 02:35:30', '1110038', '4omp34qsbfu062f8q4sjrsu9v0'),
(5, '2015-09-29 02:35:44', '1110038', '4omp34qsbfu062f8q4sjrsu9v0'),
(6, '2015-09-29 02:45:54', '104002', '4omp34qsbfu062f8q4sjrsu9v0'),
(7, '2015-09-29 03:29:00', '104002', '4omp34qsbfu062f8q4sjrsu9v0'),
(8, '2015-09-29 03:29:35', '1006055', 'ehmd29oj1g65vr56ff6bsqr293'),
(9, '2015-09-29 03:32:32', '1011053', 'ehmd29oj1g65vr56ff6bsqr293'),
(10, '2015-09-29 03:33:38', '1104046', 'ehmd29oj1g65vr56ff6bsqr293'),
(11, '2015-09-29 03:35:09', '1102124', 'ehmd29oj1g65vr56ff6bsqr293'),
(12, '2015-09-29 03:36:13', '1102003', 'ehmd29oj1g65vr56ff6bsqr293'),
(13, '2015-09-29 03:39:37', '108004', 'ehmd29oj1g65vr56ff6bsqr293'),
(14, '2015-09-29 03:42:01', '1004022', 'ehmd29oj1g65vr56ff6bsqr293'),
(15, '2015-09-29 03:43:25', '108004', 'ehmd29oj1g65vr56ff6bsqr293'),
(16, '2015-09-29 04:34:17', '104002', 'ohn0bqlj1ji6775pvea70gqv86'),
(17, '2015-09-29 07:18:22', '104002', 'vb5trhk3429q7tmhlrvir20b01'),
(18, '2015-09-29 08:24:07', '104002', '1q635dh3rvlavgcprtq0qtf407'),
(19, '2015-09-29 08:31:25', '104002', 's2fhl52rfk8o9f5dkot6gdule4'),
(20, '2015-09-29 08:42:02', '104002', '8r0c1l7fh03egt7e53fv0er485'),
(21, '2015-09-29 11:45:26', '104002', 'ragd6c11njn6o5l447l26pfl85'),
(22, '2015-09-29 12:03:04', '104002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(23, '0000-00-00 00:00:00', '104002', '17l7k7ur7ge48jmlvqftpl7r07'),
(24, '2015-09-29 12:25:57', '676767', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(25, '2015-09-29 12:26:58', '104002', 'kub8dbq9lf9hhsc87g97soco12'),
(26, '2015-09-29 12:27:24', '676767', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(27, '2015-09-29 12:29:49', '104002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(28, '2015-09-29 12:45:44', '104002', '7ju192coi7blldeob2voq030t3'),
(29, '2015-09-29 12:46:22', '104002', 'nnkig9ubuv495ta1frsp80ilk6'),
(30, '2015-09-29 12:49:13', '104002', 'oqot5c4cvjqriljvp9fbu8las4'),
(31, '2015-09-29 12:51:18', '104002', '2d4iflhilvl16lio4i18od4ko0'),
(32, '2015-09-29 12:52:56', '104002', 'doinm6caf1u3pm3airqkttuip3'),
(33, '2015-09-29 13:01:45', '104002', 'g2et7c4jj2m31ujuadjsbge1k6'),
(34, '2015-09-29 13:09:48', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(35, '0000-00-00 00:00:00', '104002', 'bjgc978fm4mnklfg3bthdhnmn1'),
(36, '2015-09-29 13:22:33', '104002', 'nnkig9ubuv495ta1frsp80ilk6'),
(37, '2015-09-29 13:23:50', '8805001', 'i033jgn03j4mb066q2gvv9loe6'),
(38, '2015-09-29 13:24:36', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(39, '2015-09-29 13:25:28', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(40, '2015-09-29 13:26:16', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(41, '2015-09-29 13:27:18', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(42, '2015-09-29 13:28:17', '807002', 'i033jgn03j4mb066q2gvv9loe6'),
(43, '2015-09-29 13:29:11', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(44, '2015-09-29 13:30:24', '104002', 'nnkig9ubuv495ta1frsp80ilk6'),
(45, '2015-09-29 13:32:26', '104002', 'nnkig9ubuv495ta1frsp80ilk6'),
(46, '2015-09-29 13:48:25', '999999', 'i033jgn03j4mb066q2gvv9loe6'),
(47, '2015-09-29 13:48:51', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(48, '2015-09-29 13:53:26', '104002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(49, '2015-09-29 13:54:28', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(50, '2015-09-29 13:57:49', '807002', 'i033jgn03j4mb066q2gvv9loe6'),
(51, '2015-09-29 14:00:28', '104002', 'nnkig9ubuv495ta1frsp80ilk6'),
(52, '2015-09-29 14:02:00', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(53, '2015-09-29 14:09:14', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(54, '2015-09-29 14:19:43', '104002', 'os1clc6524s19uc8gnqocfmmq6'),
(55, '2015-09-29 14:22:22', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(56, '2015-09-29 14:24:35', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(57, '2015-09-29 14:26:06', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(58, '2015-09-29 14:27:58', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(59, '2015-09-29 14:30:24', '104002', 'os1clc6524s19uc8gnqocfmmq6'),
(60, '2015-09-29 14:30:46', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(61, '2015-09-29 14:39:10', '104002', 'os1clc6524s19uc8gnqocfmmq6'),
(62, '2015-09-29 14:43:17', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(63, '2015-09-29 14:43:42', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(64, '2015-09-29 14:44:36', '104002', 'i033jgn03j4mb066q2gvv9loe6'),
(65, '2015-09-29 14:45:26', '807002', 'i033jgn03j4mb066q2gvv9loe6'),
(66, '2015-09-29 14:47:29', '807002', 'lj9nk6l9f9vsr6cpu6dtj8hcu0'),
(67, '2015-09-29 21:35:45', '104002', '3eccjefl8npgpjg4ck9j420lg3'),
(68, '2015-09-29 21:44:09', '104002', '3eccjefl8npgpjg4ck9j420lg3'),
(69, '2015-09-29 21:59:36', '104002', '3eccjefl8npgpjg4ck9j420lg3'),
(70, '0000-00-00 00:00:00', '104002', 'cg08jv2cp6qmmslsjvlo1d7f76'),
(71, '0000-00-00 00:00:00', '104002', 'iqiln4itmd4n3tbndeqqumtru1'),
(72, '0000-00-00 00:00:00', '104002', '0u41icgg1ee335ohgr8av0d5p7'),
(73, '2015-09-30 01:38:13', '104002', 'oeaq6um3m545tb9uka7o33gjd4'),
(74, '0000-00-00 00:00:00', '104002', '3f4o1qmtmdnnimhb6q6vc6jgf2'),
(75, '2015-09-30 03:59:23', '104002', 'oeaq6um3m545tb9uka7o33gjd4'),
(76, '0000-00-00 00:00:00', '104002', 'l7pfon1ci26328lsjaa9p8vru2'),
(77, '2015-09-30 06:37:00', '104002', 'qpvk32lkm84llq4e9he1rcocu3'),
(78, '2015-09-30 06:56:55', '807002', '2b2vn36uudldrr2g9qm98ih0f5'),
(79, '0000-00-00 00:00:00', '104002', 'rjot76ifr1otk0f173uupf46h5'),
(80, '2015-09-30 10:30:21', '104002', 'dekht4amthithvh6bu7q0bmp82'),
(81, '2015-09-30 12:33:31', '104002', 'dekht4amthithvh6bu7q0bmp82'),
(82, '2015-09-30 12:34:33', '104002', 'k9uafnl5u3cmmv277kht7i7vf6'),
(83, '2015-09-30 13:01:28', '104002', 'oeaq6um3m545tb9uka7o33gjd4'),
(84, '2015-09-30 13:02:59', '104002', 'h1pif2a0qckbt5orgr3f293d40'),
(85, '2015-09-30 13:04:06', '104002', 'h1pif2a0qckbt5orgr3f293d40'),
(86, '0000-00-00 00:00:00', '104002', '5gqreb3abpokjldo521tibugj7'),
(87, '2015-09-30 13:22:23', '104002', 'dekht4amthithvh6bu7q0bmp82'),
(88, '2015-09-30 13:57:14', '104002', '8j23hl2nsi2iqj2jp60mqttea3'),
(89, '2015-09-30 16:25:24', '104002', 'eamfv2rvfek94r9a0nim9u3vi0'),
(90, '2015-09-30 16:28:22', '807002', 'eamfv2rvfek94r9a0nim9u3vi0'),
(91, '2015-09-30 17:30:26', '807002', 'v1v1nk58r3b2jmheple9lhgbe3'),
(92, '2015-09-30 17:31:46', '104002', 'v1v1nk58r3b2jmheple9lhgbe3');

-- --------------------------------------------------------

--
-- Table structure for table `workflow_activity`
--

CREATE TABLE IF NOT EXISTS `workflow_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) DEFAULT NULL,
  `activity_name` varchar(45) DEFAULT NULL,
  `tat` decimal(10,2) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `employee_id` varchar(45) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `fk_workflow_activity_ref_workflow1_idx` (`workflow_id`),
  KEY `fk_workflow_activity_mmpi_table_of_organization1_idx` (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `workflow_activity`
--

INSERT INTO `workflow_activity` (`activity_id`, `workflow_id`, `activity_name`, `tat`, `sequence`, `employee_id`, `group_id`) VALUES
(1, 2, 'Search Flight/Travel Agency', 0.50, 1, '1011053', NULL),
(2, 2, 'Provide Recommendation', 0.50, 2, '1104046', NULL),
(3, 2, 'Assess and Approve Recommended Flight', 0.50, 3, '1102124', NULL),
(4, 2, 'Process Payment', 0.50, 4, '1102003', NULL),
(5, 2, 'Provide Flight Details', 0.50, 5, '1004022', NULL),
(6, 2, 'Acknowledge Completion of Request', 0.50, 6, '108004', NULL),
(9, 12, 'Sample', 3.00, 1, '109991231', NULL),
(10, 13, 'Merry Go Round', 3.00, 1, '2423234234', NULL),
(11, 14, 'Sample Lang', 3.00, 1, '2423234234', NULL),
(12, 15, 'Sample', 3.00, 1, '2423234234', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `workflow_approver`
--

CREATE TABLE IF NOT EXISTS `workflow_approver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approver_id` varchar(45) DEFAULT NULL,
  `tat` float DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  `assigned_position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workflow_approver_mmpi_table_of_organization1_idx` (`approver_id`),
  KEY `fk_workflow_approver_ref_workflow1_idx` (`workflow_id`),
  KEY `fk_workflow_approver_ref_designation1_idx` (`assigned_position`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `workflow_approver`
--

INSERT INTO `workflow_approver` (`id`, `approver_id`, `tat`, `sequence`, `workflow_id`, `assigned_position`) VALUES
(1, '1112084', 0.5, 1, 1, NULL),
(2, '1007098', 2, 2, 1, NULL),
(3, '1110038', 0.5, 1, 2, NULL),
(4, '1006055', 0.5, 2, 2, NULL),
(15, '636363', 3, 1, 12, NULL),
(16, '100114098', 2, 2, 12, NULL),
(17, '123456', 3, 1, 13, NULL),
(18, '100114098', 3, 2, 13, NULL),
(19, '123456', 3, 1, 14, NULL),
(20, '696969', 3, 2, 14, NULL),
(21, '123456', 3, 1, 15, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `workflow_table`
--

CREATE TABLE IF NOT EXISTS `workflow_table` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`table_id`),
  KEY `fk_workflow_table_ref_workflow1_idx` (`workflow_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `workflow_table`
--

INSERT INTO `workflow_table` (`table_id`, `workflow_id`) VALUES
(1, 1),
(2, 2),
(7, 12),
(8, 13),
(9, 14),
(10, 15);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employe_request`
--
ALTER TABLE `employe_request`
  ADD CONSTRAINT `fk_employe_request_mmpi_table_of_organization1` FOREIGN KEY (`requestor_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employe_request_ref_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `ref_workflow` (`workflow_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employe_request_request_status1` FOREIGN KEY (`status`) REFERENCES `request_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mmpi_group_matrix`
--
ALTER TABLE `mmpi_group_matrix`
  ADD CONSTRAINT `fk_ref_group_has_mmpi_table_of_organization_mmpi_table_of_org1` FOREIGN KEY (`employee_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ref_group_has_mmpi_table_of_organization_ref_group1` FOREIGN KEY (`group_id`) REFERENCES `ref_group` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mmpi_table_of_organization`
--
ALTER TABLE `mmpi_table_of_organization`
  ADD CONSTRAINT `fk_mmpi_table_of_organization_ref_department1` FOREIGN KEY (`department_id`) REFERENCES `ref_department` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mmpi_table_of_organization_ref_designation1` FOREIGN KEY (`designation_id`) REFERENCES `ref_designation` (`designation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mmpi_table_of_organization_ref_location1` FOREIGN KEY (`location_id`) REFERENCES `ref_location` (`location_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mmpi_table_of_organization_ref_position1` FOREIGN KEY (`position_id`) REFERENCES `ref_position` (`position_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mmpi_table_of_organization_ref_status1` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `mmpi_user_access`
--
ALTER TABLE `mmpi_user_access`
  ADD CONSTRAINT `fk_mmpi_user_access_mmpi_table_of_organization1` FOREIGN KEY (`employee_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_access_type`
--
ALTER TABLE `ref_access_type`
  ADD CONSTRAINT `fk_ref_designation_ref_status100` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_department`
--
ALTER TABLE `ref_department`
  ADD CONSTRAINT `fk_ref_designation_ref_status10` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_designation`
--
ALTER TABLE `ref_designation`
  ADD CONSTRAINT `fk_ref_designation_ref_status` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_dropdown`
--
ALTER TABLE `ref_dropdown`
  ADD CONSTRAINT `fk_ref_dropdown_table_fields1` FOREIGN KEY (`field_id`) REFERENCES `table_fields` (`field_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_group`
--
ALTER TABLE `ref_group`
  ADD CONSTRAINT `fk_ref_designation_ref_status101` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_location`
--
ALTER TABLE `ref_location`
  ADD CONSTRAINT `fk_ref_designation_ref_status0` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_position`
--
ALTER TABLE `ref_position`
  ADD CONSTRAINT `fk_ref_designation_ref_status01` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_priority`
--
ALTER TABLE `ref_priority`
  ADD CONSTRAINT `fk_ref_designation_ref_status1` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_units`
--
ALTER TABLE `ref_units`
  ADD CONSTRAINT `fk_ref_designation_ref_status102` FOREIGN KEY (`status_id`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ref_units_ref_department1` FOREIGN KEY (`department_id`) REFERENCES `ref_department` (`department_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ref_workflow`
--
ALTER TABLE `ref_workflow`
  ADD CONSTRAINT `fk_ref_workflow_ref_category1` FOREIGN KEY (`category_id`) REFERENCES `ref_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ref_workflow_ref_status1` FOREIGN KEY (`status`) REFERENCES `ref_status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_activities`
--
ALTER TABLE `request_activities`
  ADD CONSTRAINT `fk_request_activities_approval_status1` FOREIGN KEY (`status`) REFERENCES `approval_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_activities_employe_request1` FOREIGN KEY (`request_id`) REFERENCES `employe_request` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_approval`
--
ALTER TABLE `request_approval`
  ADD CONSTRAINT `fk_request_approval_approval_status1` FOREIGN KEY (`status`) REFERENCES `approval_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_approval_employe_request1` FOREIGN KEY (`request_id`) REFERENCES `employe_request` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_approval_mmpi_table_of_organization1` FOREIGN KEY (`approver_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_fields`
--
ALTER TABLE `request_fields`
  ADD CONSTRAINT `fk_request_fields_employe_request1` FOREIGN KEY (`request_id`) REFERENCES `employe_request` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_fields_table_fields1` FOREIGN KEY (`field_id`) REFERENCES `table_fields` (`field_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `table_fields`
--
ALTER TABLE `table_fields`
  ADD CONSTRAINT `fk_table_fields_ref_data_type1` FOREIGN KEY (`data_type_id`) REFERENCES `ref_data_type` (`data_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_table_fields_workflow_table1` FOREIGN KEY (`table_id`) REFERENCES `workflow_table` (`table_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_log`
--
ALTER TABLE `user_log`
  ADD CONSTRAINT `fk_user_log_mmpi_table_of_organization1` FOREIGN KEY (`employee_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `workflow_activity`
--
ALTER TABLE `workflow_activity`
  ADD CONSTRAINT `fk_workflow_activity_mmpi_table_of_organization1` FOREIGN KEY (`employee_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_workflow_activity_ref_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `ref_workflow` (`workflow_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `workflow_approver`
--
ALTER TABLE `workflow_approver`
  ADD CONSTRAINT `fk_workflow_approver_ref_designation1` FOREIGN KEY (`assigned_position`) REFERENCES `ref_designation` (`designation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_workflow_approver_mmpi_table_of_organization1` FOREIGN KEY (`approver_id`) REFERENCES `mmpi_table_of_organization` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_workflow_approver_ref_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `ref_workflow` (`workflow_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `workflow_table`
--
ALTER TABLE `workflow_table`
  ADD CONSTRAINT `fk_workflow_table_ref_workflow1` FOREIGN KEY (`workflow_id`) REFERENCES `ref_workflow` (`workflow_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
