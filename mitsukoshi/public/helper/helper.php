<?php

class Helper
{
	

	public function subtractDate($date1,$date2){
     	 $datediff = strtotime($date1) - strtotime($date2);
         $rf = RefHoliday::find("date(holiday_date) BETWEEN date('$date2') and date('$date1')");
        
         $holiday_diff = sizeof($rf );
     	 $days = round($datediff/(60*60*24),2) - $holiday_diff;
         // $days = $holiday_diff;
         if($days <0)
         {
             $days = 0;
         }
     	 if($date1 == null || $date2 == null ){
     	 	return 0;
     	 }
    	 return round($days,2);
	}
	public function generateId(){
		$stamp = date("Ymdhis");
		return $stamp;
	}
	public function generateToken()
    {
        return md5(rand(9999,1)."-".time());
    }
    
    public function _generateRandomDateTime($start_date, $end_date)
	{
	    // Convert to timetamps
	    $min = strtotime($start_date);
	    $max = strtotime($end_date);

	    // Generate random number using above bounds
	    $val = rand($min, $max);

	    // Convert back to desired date format
	    return date('Y-m-d H:i', $val);
	
	}

	public function convertToDate($date,$format){
		if($date == null) return '';
		$date = new DateTime($date,new DateTimeZone("Asia/Hong_Kong"));
		return $date->format($format);
	}
    public function _addHours($date,$hours){
        $now = new DateTime($date,new DateTimeZone("Asia/Hong_Kong")); //current date/time
        $now->add(new DateInterval("PT{$hours}H"));
        $new_time = $now->format('M d, Y h:i A');
        return $new_time;
    }
	public function _paginate($data,$page,$limit){

		$paginator =  new PaginatorModel(
				array(
					"data"=>$data,
					"limit"=>$limit,
					"page"=>$page
				)
		);
		$page = $paginator->getPaginate();
		return $page->items;
	}
    public function _paginateArray($data,$page,$limit){

        $paginator =  new PaginatorArray(
                array(
                    "data"=>$data,
                    "limit"=>$limit,
                    "page"=>$page
                )
        );
        $page = $paginator->getPaginate();
        return $page->items;
    }

	public function _getLastPage($data,$limit){
		
		return ceil(sizeof($data) / $limit);	
	}
	public function _echoJson($status,$message,$data = []){
		$result = array(
				"status"=>$status,
				"message"=>$message,
				"data"=>$data,
                "messages"=>$message
		);
		echo json_encode($result);
		die;
	}

	public function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}
       
        public function mailNotifUserRegistration($username,$password,$email) {
            $to = $email;
            $subject = 'Workflow Management System';
            $message = "You may login to the Workflow Management System using the credentials below:\n\n" .
                "Username: $username \n".
                "Password: $password";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
        }
        public function mailNotifResetPassword($username,$password,$email) {
            $to = $email;
            $subject = 'Workflow Management System';
            $message = "You may login to the Workflow Management System using your temporary password below:\n\n" .
                "Username: $username \n".
                "Password: $password";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
        }        
        public function mailNotifApprover($email) {
            $to = $email;
            $subject = 'Workflow Management System';
            $message = "A pending task needs your approval";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
        }
        
        public function mailNotifActivity($email) {
            $to = $email;
            $subject = 'Workflow Management System';
            $message = "A pending activity needs your action";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
        }
        
		public function mailRequestComplete($email, $request_id) {
			$to = $email;
            $subject = 'Workflow Management System';
            $message = "Your request with ID ". $request_id . " has been completed.";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
		}
		
		public function mailRequestRejected($email, $request_id) {
			$to = $email;
            $subject = 'Workflow Management System';
            $message = "Your request with ID " . $request_id . " was rejected";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
		}
        public function mailNotifCreateWorkflow($email,$workflow_id) {
            $to = $email;
            $subject = 'Workflow Management System';
            $message = "Workflow $workflow_id has been created.";

            $headers = "From: wfs@mitsukoshimotors.com\r\nReply-To: wfs@mitsukoshimotors.com";
            $mail_sent = @mail($to, $subject, $message, $headers);
        }            
}

?>