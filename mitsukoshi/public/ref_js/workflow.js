var RefWorkflow = {	
	getByDepartmentId : function(department_id){
		return $.ajax({
			type:'get',
			url: URL + 'refs/listworkflowbydepartment/' + department_id,
			dataType: 'json'
		});
	},
	getByWorkflowId : function(workflow_id){
		return $.ajax({
			type:'get',
			url: URL + 'refs/listworkflowbyid/' + workflow_id,
			dataType: 'json'
		});
	}
};