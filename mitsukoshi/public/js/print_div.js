function printContent(div){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(div).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}
function printContentLandscape(div){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(div).innerHTML;
	document.body.innerHTML = printcontent;      
	window.print();
	document.body.innerHTML = restorepage;
}
function saveAsPdf(div) {
  
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(div).innerHTML;
    document.body.innerHTML = printcontent;
    var doc = new jsPDF();          
    var elementHandler = {
      '#ignorePDF': function (element, renderer) {
        return true;
      }
    };
    var source = window.document.getElementsByTagName("body")[0];

    doc.fromHTML(
        printcontent,
        15,
        15,
        {
          'width': 180,'elementHandlers': elementHandler
        });
        doc.output("dataurlnewwindow");
}
