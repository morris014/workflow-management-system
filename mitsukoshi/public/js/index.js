document.onmousedown=disableclick;
status="Right Click Disabled";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}

$(document).ready(function() {
      $('#btnCancelUser').addClass('hidden');
    $('a#edituser').on('click', function() {
        $(this).addClass('hidden');
        $('.form-control').removeAttr('disabled');
        $('input[type="checkbox"]').removeAttr('disabled');
        $('h6#user-label').text('EDIT USER DETAILS OF');
        $('#saveuser').removeClass('hidden');
        $('#btnCancelUser').removeClass('hidden');
        $('#btnDeleteUser').addClass('hidden');
        $('#edit_user_title').text('Configure Permissions');
    });
  });