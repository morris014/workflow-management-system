// var $s = jQuery.noConflict();
 $(function() {
    $( "#datepickers" ).datepicker({
      showOn: "button",
      buttonText: "&#9660;"
    });

  });


  $(document).ready(function() {
    var availableTags = [

    ];
      $( "#main-search" ).autocomplete({
      source: availableTags
    }).autocomplete("widget").addClass("main-search fixed-ul");
  });

function saveholiday(my_data){
	return $.ajax({
		url : URL + 'settings/add',
		dataType: 'json',
		data: my_data,
		type: 'post'
	});
} 

function deleteholiday(my_data){
	return $.ajax({
		url : URL + 'settings/delete',
		dataType: 'json',
		data: my_data,
		type: 'post'
	});
} 

$(document).ready(function(){
    
        
	$(document).on('click','#btnSaveHoliday',function(e){
		e.preventDefault();
		if((document.getElementById("holiday_name").value.length === 0) || (document.getElementById("datepickers").value.length === 0)){
			alert('Please enter both a name and a date for the holiday');
			return;
		}
                
			var holiday_data = {};
			holiday_data.holiday_name = $('#holiday_name').val();
                        holiday_data.holiday_date = $('#datepickers').val();

			saveholiday(holiday_data).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'admin/settings',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
    			alert(jqXHR.responseText);
			});  
	});
        
	$(document).on('click','.btn-remove-holiday',function(e){
		e.preventDefault();

			var holiday_data = {};
			holiday_data.holiday_name = $(this).attr("name");
                        holiday_data.hid = $(this).attr("hid");

			deleteholiday(holiday_data).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'admin/settings',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
    			alert(jqXHR.responseText);
			});                  
	});

});

