
function showSuccessModal(message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#successModal').modal(options);
	$('#successMessage').html(message);
	$('#successModal #btnOk').click(function(){
		$('#successModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}
function showDeleteUserModal(employee_id,message,redirect_url,time){
        
	var options = {
		"show": true
	};
       
	$('#deleteModal').modal(options);
	$('#deleteMessage').html(message);
        $('#btnDiscardUser').attr('employee-id',employee_id);  
	$('#deleteModal #btnDel').click(function(){
		$('#deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}

function showResetUserModal(employee_id,message,redirect_url,time){
        
	var options = {
		"show": true
	};
       
	$('#resetModal').modal(options);
	$('#resetMessage').html(message);
        $('#btnResetUser').attr('employee-id',employee_id);  
	$('#resetModal #btnDel').click(function(){
		$('#deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}
function showDeleteGroupModal(group_id,message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#deleteGroupModal').modal(options);
	$('#deleteMessage').html(message);
        $('#btnDiscardGroup').attr('group-id',group_id);  
	$('#deleteModal #btnDel').click(function(){
		$('#deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}
function showDeleteModal(message,title,redirect_url,time){
	var options = {
		"show": true
	};
	$('.deleteModal').modal(options);
	$('#deleteMessage').html(message);
	$('#title').html(title);
	$('#successModal #btnDel').click(function(){
		$('.deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}

function showFollowupModal(message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#followupModal').modal(options);
	$('#followupMessage').html(message);
	$('#followupModal #btnOk').click(function(){
		$('#followupModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}	

function showCancelModal(request_id,message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#cancelModal').modal(options);
	$('#cancelMessage').html(message);
        $('#btnCancelOk').attr('request-id',request_id);  
	/*$('#cancelModal #btnOk').click(function(){
		$('#cancelModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});*/
	$('#cancelModal #btnCancel').click(function(){
		$('#cancelModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}

function showDiscardModal(message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#discardModal').modal(options);
	$('#discardMessage').html(message);
	$('#discardModal #btnOk').click(function(){
		$('#discardModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}

function showChangeWorkflow(workflow_id,message,redirect_url,time){
	var options = {
		"show": true
	};
	$('#activateworkflow').modal(options);
	$('#deleteMessage').html(message);
        $('#btnActivateWorkflow').attr('workflow-id',workflow_id);  
        $('#btnDiscardWorkflow').attr('workflow-id',workflow_id); 
	/*$('#deleteModal #btnDel').click(function(){
		$('#deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});*/

}