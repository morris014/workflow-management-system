$(function(){
	$(document).on('click','.btnSubmit',function(){
		var employee_id = $(this).attr('employee_id');
		$('#employee_id').val(employee_id);
		
                $('#frmEdit').submit();
                //$('#alternateSubmitEdit').click();
	});

});	

$(function(){
	$(document).on('keypress','.user-search',function(){
            if(event.which === 13){
                var user_search_key = $('.user-search').val();
                $('#user_search_key').val(user_search_key);
                $('#frmUserSearch').submit();
                //alert(user_search_key);
            }
	});
});	
$(function(){
	$(document).on('click','.input-group-btn-user-search',function(){
            var user_search_key = $('.user-search').val();
            $('#user_search_key').val(user_search_key);
            $('#frmUserSearch').submit();
            //alert(user_search_key);
	});
        
 	$('.btnDeleteUserClass').click(function(e){
		var first_name = $(this).attr('first-name');
                var last_name = $(this).attr('last-name');
                var employee_id = $(this).attr('employee-id');
                
		showDeleteUserModal(employee_id,"<strong>Deleting User "+first_name+" "+last_name+"</strong><br>"+"This action can't be reversed.",URL + 'admin/userManagement/editUser',1);
	});       
        
 	$('#btnDiscardUser').click(function(){
		var employee_id = $(this).attr('employee-id');
		var params = {};
		params.employee_id = employee_id;
		//showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
		$.ajax({
			url: URL + 'admin/userManagement/deleteUser',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
			console.log(result);
			if(result.status == 0){
			
				alert(result.message);
			}else if(result.status == 1){
				
				console.log("Deleted");
				window.location.href = URL+'admin/userManagement';
			}
		});
	});          
        
        
        $('.btnResetPasswordClass').click(function(e){
		var first_name = $(this).attr('first-name');
                var last_name = $(this).attr('last-name');
                var employee_id = $(this).attr('employee-id');
                
		showResetUserModal(employee_id,"<strong>Reset Passwor for  User "+first_name+" "+last_name+"</strong><br>"+"This action can't be reversed.",URL + 'admin/userManagement/editUser',1);
	});  
        
 	$('#btnResetUser').click(function(){
		var employee_id = $(this).attr('employee-id');
		var params = {};
		params.employee_id = employee_id;
		//showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
		$.ajax({
			url: URL + 'admin/userManagement/resetUser',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
			console.log(result);
			if(result.status == 0){
			
				alert(result.message);
			}else if(result.status == 1){
				
				console.log("Deleted");
				window.location.href = URL+'admin/userManagement';
			}
		});
	});           
        
});	
