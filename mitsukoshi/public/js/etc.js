$(".menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

/* center modal */
function centerModals($element) {
    var $modals;
    if ($element.length) {
        $modals = $element;
    } else {
    $modals = $('.modal-flow:visible');
    }
    $modals.each( function(i) {
        var $clone = $(this).clone().css('display', 'block').appendTo('body');
        var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
        top = top > 0 ? top : 0;
        $clone.remove();
        $(this).find('.modal-content').css("margin-top", top);
    });
}
$('.modal-flow').on('show.bs.modal', function(e) {
    centerModals($(this));
});

$(window).on('resize', centerModals);

$(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
    
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
    
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
    
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
    
            // Replace image with new SVG
            $img.replaceWith($svg);
    
        }, 'xml');
    
    });
});

$(".collapse").on('shown.bs.collapse', function(){
    $('span', $(this).siblings('.panel-heading')).html('<small>Collapse</small><i class="fa  fa-caret-up" style="color:red;margin-left:5px;"></i');
});

$(".collapse").on('hide.bs.collapse', function(){
    $('span', $(this).siblings('.panel-heading')).html('<small>Expand</small><i class="fa  fa-caret-down" style="color:red;margin-left:5px;"></i');
});
$(".collapse").on('hidden.bs.collapse', function(){
    $('span', $(this).siblings('.panel-heading')).html('<small>Expand</small><i class="fa  fa-caret-down" style="color:red;margin-left:5px;"></i');
});