function savegroup(my_data){
	return $.ajax({
		url : URL + 'group/add',
		dataType: 'json',
		data: my_data,
		type: 'post'
	});
}

function fillAutocomplete(txtBox,source){
		
		$(txtBox ).autocomplete({
		  minLength: 0,
	    source: function(request, response) {
		    var results = $.ui.autocomplete.filter(source.data, request.term);

	    response(results.slice(0, 4));
		},
		change:function(){
			
		},
	    select: function(event,ui){
	    		var member_id = ui.item.employee_id;
	      		$(txtBox).attr('member_id', member_id);
	      		$(txtBox).attr('designation', ui.item.designation);
	      		$(txtBox).attr('email', ui.item.email);		
	      		$(txtBox).attr('fullname', ui.item.fullname);		
	      }
	    }).autocomplete("instance")._renderItem = function(ul,item){
	    	$(ul).addClass('main-search');
			 return $("<li/>")
	            .append(item.label)
	            .appendTo(ul);
	    };  		
		
}

$(function(){
	$(document).ready(function(){
		$('#btnAddMember').click(function(){
			var template = $('#tplAddMember').html();	
			$(template).appendTo('#addMemberContainer').hide().fadeIn().find('#members').each(function(){
				fillAutocomplete(this,autocompletesource);
			});
		});

		$('#btnSave').click(function(){
			if($('#frmGroup').geneValidateForm() > 0){
				return;
			}

			if($('.fields-members').length <= 0){
				alert('Add members please?');
				return;
			}

			var group_data = {};
			group_data.group_name = $('#group_name').val();

			var members = [];

			$('.fields-members').each(function(){
				var member_data = {
					member_id : $(this).attr('member_id')
				};
				members.push(member_data);
			});
			group_data.members = members;

			savegroup(group_data).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status == 1){
					
					showSuccessModal(result.message,URL + 'admin/groupManagement',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
    			alert(jqXHR.responseText);
			});  
		});

	});
        
        var existingMembers;
	$(document).on('click','.btn-add-member',function(){

		if($(this).parent().geneValidateFormByAttributes('member_id') > 0){
			return;
		}

		var parent = $(this).parent().find('#members');
		var data = {
			member_id : $(parent).attr('member_id'),
			designation : $(parent).attr('designation'),
			fullname  : $(parent).attr('fullname')
		};

                existingMembers = $(".fields-members").map(function(){return $(this).attr("member_id");}).get();
                
                if ($.inArray(data.member_id, existingMembers) > -1)
                {
                    alert('The Member you are trying to add is already a part of this group.');
                    $('#members').val('');
                    return;
                }

		var template = $('#tplMember').html();
		var html = Mustache.to_html(template,data);
		$(html).appendTo('#membersContainer').hide().fadeIn();
		$(this).parent().parent().remove();

	});

	$(document).on('click','.btn-remove-member',function(){
		$(this).parent().parent().remove();
	});

});