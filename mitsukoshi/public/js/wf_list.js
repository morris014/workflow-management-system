$(function(){
	$(document).on('keypress','.wf-search',function(){
            if(event.which === 13){
                var wf_search_key = $('.wf-search').val();
                $('#wf_search_key').val(wf_search_key);
                $('#frmWorkflowSearch').submit();
                //alert(user_search_key);
            }
	});
});	
$(function(){
	$(document).on('click','.input-group-btn-wf-search',function(){
            var wf_search_key = $('.wf-search').val();
            $('#wf_search_key').val(wf_search_key);
            $('#frmWorkflowSearch').submit();
            //alert(user_search_key);
	});

    $(document).on('click','.btn-edit-workflow',function(){

        var workflow_id = $(this).attr('workflow_id');
        if(workflow_id > 0){
            $('#workflow_id').val(workflow_id);
            $('#frmEditWorkflow').submit();
        }
    });
    
 	$('.classChangeWorkflow').click(function(){
		var workflow_id = $(this).attr('workflow-id');
                
		showChangeWorkflow(workflow_id,"",URL + 'admin/userManagement/editUser',1);
	});   
 	$('#btnDiscardWorkflow').click(function(){
		var workflow_id = $(this).attr('workflow-id');
		var params = {};
		params.workflow_id = workflow_id;
                //alert(workflow_id);
		//showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
		$.ajax({
			url: URL + 'admin/workflowManagement/suspendWorkflow',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
			console.log(result);
			if(result.status == 0){
			
				alert(result.message);
			}else if(result.status == 1){
				
				console.log("Deleted");
				window.location.href = URL+'admin';
			}
		});
	});    
  	$('#btnActivateWorkflow').click(function(){
		var workflow_id = $(this).attr('workflow-id');
		var params = {};
		params.workflow_id = workflow_id;

		//showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
		$.ajax({
			url: URL + 'admin/workflowManagement/activateWorkflow',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
			console.log(result);
			if(result.status == 0){
			
				alert(result.message);
			}else if(result.status == 1){
				
				console.log("Deleted");
				window.location.href = URL+'admin';
			}
		});
	});    
        
        
        
 	$('#btn-sort-workflow-date').click(function(){
		//var workflow_id = $(this).attr('workflow-id');
                //window.location.href = URL+'admin/index';
                  $('#frmWorkflowSearchDate').submit();
		//showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
		/*$.ajax({
			url: URL + 'admin/index',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
                    
		});*/
	});          
});	
