
  
$('#frmCreateUser').on('keyup keypress', function(e) {
  var code = e.keyCode || e.which;
  if (code == 13) { 
    e.preventDefault();
    return false;
  }
});


$('#frmEditUser').on('keyup keypress', function(e) {
  var code = e.keyCode || e.which;
  if (code == 13) { 
    e.preventDefault();
    return false;
  }
});
$(function(){
	$('#my_full_name').html('');

	function CreateUser(serialize_data){
		return $.ajax({
			 url : URL + 'Mmpitableoforganization/register',
			 data: serialize_data,
			 type: 'post',
			 dataType: 'json',
			 error: function (jqXHR, textStatus, errorThrown) {
	            if (jqXHR.status == 500) {
	                    alert('Internal error: ' + jqXHR.responseText);
	            } else {
	                    alert('Unexpected error.');
	            }
    		 }
		});
	}
	function EditUser(serialize_data){
            
		return $.ajax({
			 url : URL + 'Mmpitableoforganization/edit',
			 data: serialize_data,
			 type: 'post',
			 dataType: 'json',
			 error: function (jqXHR, textStatus, errorThrown) {
	            if (jqXHR.status == 500) {
	                    alert('Internal error: ' + jqXHR.responseText);
	            } else {
	                    alert('Unexpected error.');
	            }
    		 }
		});
	}
	$(document).ready(function(){

		$('#pnlAdminPermission').addClass('hidden');

		$('#administrator').change(function(){
			if(this.checked){
				$('#pnlAdminPermission').removeClass('hidden');				
			}else{
				$('#pnlAdminPermission').addClass('hidden');				
			}
		});
		$('#tasks').change(function(){
			$('#request').prop('checked',this.checked);
		});
		$('#request').change(function(){
			$('#tasks').prop('checked',this.checked);
		});
		$('#frmEditUser').submit(function(e){
                    
			e.preventDefault();
			var total_error = $(this).geneValidateForm();
  			
			if(total_error > 0){
				return;
			}
                 
			var data = {};
			$(this).find('input[type=hidden]').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('input[type=email]').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('input[type=text]').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('select').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('input[type=checkbox]').each(function(i,val){
				if($(this).is(':checked')){
					data[val.name] = 1;	
				}else{
					data[val.name] = 0;	
				}
				
			});
                  
			EditUser(data).done(function(result){
				if(result.status == 0){
					alert(result.messages);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'admin/userManagement',1);


				}
			}).fail(function (jqXHR, textStatus, error) {
    			alert(jqXHR.responseText);
			});
		});
		$('#frmCreateUser').submit(function(e){
			e.preventDefault();
			


			var total_error = $(this).geneValidateForm();
			
			if(total_error > 0){
				return;
			}

			var data = {};

			$(this).find('input[type=email]').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('input[type=text]').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('select').each(function(i,val){
				data[val.name] = val.value;
			});
			$(this).find('input[type=checkbox]').each(function(i,val){
				if($(this).is(':checked')){
					data[val.name] = 1;	
				}else{
					data[val.name] = 0;	
				}
				
			});

			CreateUser(data).done(function(result){
				if(result.status == 0){
					alert(result.messages);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'admin/userManagement',1);


				}
			}).fail(function (jqXHR, textStatus, error) {
    			alert(jqXHR.responseText);
			});
		});

		$("#lname").bind("change paste keyup", function() {
			var fullname  = [];
			var lname = $('#lname').val();
			var fname = $('#fname').val();
			if(!lname.isEmpty()) fullname.push(lname);
			if(!fname.isEmpty()) fullname.push(fname);
			
   			$('#my_full_name').html(fullname.join(', ').toUpperCase()); 
		});
		$("#fname").bind("change paste keyup", function() {
   			var fullname  = [];
			var lname = $('#lname').val();
			var fname = $('#fname').val();
			if(!lname.isEmpty()) fullname.push(lname);
			if(!fname.isEmpty()) fullname.push(fname);
   			$('#my_full_name').html(fullname.join(', ').toUpperCase()); 	
		});
	});
});