$(function(){
	function approverEmail(data){		
		return $.ajax({
			url: URL + 'task/followup',
			data: data,
			type: 'post',
			dataType: 'json'
		});
	}

	$(document).ready(function(){
		$('#btnFollowup').click(function(){
                    var lastemail = $(this).attr('data-email');
                    console.log(lastemail);
//                    approverEmail(lastemail).done(function(){
//                        showFollowupModal("Last pending user is notified via e-mail.",URL + 'request',1);
//                    });
                $.post(URL + 'task/followup', {email_data: lastemail}, function (returned_data) {
                    console.log(returned_data); //or do whatever you like with the variable
                });
            
                showFollowupModal("Last pending user is notified via e-mail.",URL + 'request',1);

		});
		$('#btnCancel').click(function(){
                    var request_id = $(this).attr('request-id');
                    
                    showCancelModal(request_id,"This action can't be reversed.",URL + 'request',1);
		});   
                $('#btnCancelOk').click(function(){
                    
                        var request_id = $(this).attr('request-id');
                       
                        var params = {};
                        params.request_id = request_id;
                        //showDeleteModal('This action cant be reversed','Deleting User', URL + 'admin',1);
                        $.ajax({
                                url: URL + 'admin/requestManagement/deleteRequest',
                                data: params,
                                type: 'post',
                                dataType: 'json'
                        }).done(function(result){
                                console.log(result);
                                //alert(result.message);
                                if(result.status == 0){
                                        alert(result.message);
                                }else if(result.status == 1){

                                        console.log("Deleted");
                                        window.location.href = URL+'request';
                                }
                        });
                });                  
	});
});
