function showDeleteModal(message,title,redirect_url,time){
	var options = {
		"show": true
	};
	$('.deleteModal').modal(options);
	$('#deleteMessage').html(message);
	$('#title').html(title);
	$('#successModal #btnDel').click(function(){
		$('.deleteModal').modal('hide');
		setTimeout(function(){
			window.location.replace(redirect_url);
		}, time * 1000);
	});

}
$(function(){
	$(document).on('keypress','.group-search',function(){
            if(event.which === 13){
                var group_search_key = $('.group-search').val();
                $('#group_search_key').val(group_search_key);
                $('#frmGroupSearch').submit();
                //alert(user_search_key);
            }
	});
});	
$(function(){
	$(document).on('click','.input-group-btn-group-search',function(){
            var group_search_key = $('.group-search').val();
            $('#group_search_key').val(group_search_key);
            $('#frmGroupSearch').submit();
            //alert(user_search_key);
	});
        
 	$('#btnDeleteGroup').click(function(){
                var group_id = $(this).attr('group-id');
                var group_name = $(this).attr('group-name');
		showDeleteGroupModal(group_id,"<strong>Deleting Group "+group_name+"</strong><br>"+"This action can't be reversed.",URL + 'admin/groupManagement/editUser',1);
	}); 
        
	$('#btnDiscardGroup').click(function(){
		var group_id = $(this).attr('group-id');
		console.log(group_id);
		var params = {};
		params.group_id = group_id;
		showDeleteModal('This action cant be reversed','Deleting Group', URL + 'admin',1);
		$.ajax({
			url: URL + 'admin/groupManagement/deleteGroup',
			data: params,
			type: 'post',
			dataType: 'json'
		}).done(function(result){
			console.log(result);
			if(result.status == 0){
			
				alert(result.message);
			}else if(result.status == 1){
				//showSuccessModal(result.message,URL + 'admin/groupManagement',1);
				console.log("Deleted");
				window.location.href = URL+'admin/groupManagement';
			}
		});
	});

});	
$(function(){
	$(document).on('click','.btnSubmitGroup',function(){
	
                $('#frmEditGroup').submit();
                //$('#alternateSubmitEdit').click();
	});

});	