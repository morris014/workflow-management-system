var current_index;

$(document).ready(function() {
    $('#btnDisc').click(function () {
        showDiscardModal("Are you sure you want to discard the changes for this workflow?", URL + 'admin', 1);
    });
    $('#btnDiscardWorkflowFinal').click(function () {
        setTimeout(function () {
            window.location.replace(URL + 'admin');
        }, 1 * 1000);
    });
    if($('#workflow_name').val()!="")
    {
       $('#btnNext').removeClass('hidden'); 
    }
});

$('#workflow_name').keypress(function() {

    $('#btnNext').removeClass('hidden');
    $(this).focus();
});


$(document).on('change','.fields_list',function(){
	var data_type_id = this.value;

	if(data_type_id == 	3){
		var template = $('#tplAddList').html();
		$(this).parent().find('#dropdownlist').append(template);
		$(".add-list").select2({
		    placeholder: 'Hit Enter to add list',
    		tags: true
  		});
	}else{
		$(this).parent().find('#dropdownlist').html('');
	}
});
function save(my_data){
	return $.ajax({
		url : URL + 'RefWorkflow/save',
		dataType: 'json',
		data: my_data,
		type: 'post'
	});
}

	
function saveWorkflow(){
	console.log($('#workflow_ids').val());
    
        //alert(priority);
        var priority = null;
        
        if ($('#priority').is(":checked"))
        {
             priority = 1;
        }
        else
        {
            priority = 0;
        }
        
        var advisory = null;
        
        if ($('#advisory').is(":checked"))
        {
             advisory = 1;
        }
        else
        {
            advisory = 0;
        }        
        
        var department = $('input:checkbox:checked.department').map(function () {
            return this.value;
        }).get(); 
      

	var step_1_error = $('#step_1').geneValidateForm();
	if(step_1_error > 0){
		$('#collapseOne').collapse('show');
		return;
	}

	var ref_workflow = {
		name: $('#workflow_name').val(),
		description: $('#workflow_description').val(),
		category_id: $('#category_id').val(),
		category_name: $('#category_id option:selected').text(),
		workflow_id : $('#workflow_ids').val(),
                priority: priority,
                advisory:advisory,
                department_id : $('#department_id').val(),
                department_access:department,
	};
	
	var ref_workflow = {
		name: $('#workflow_name').val(),
		description: $('#workflow_description').val(),
		category_id: $('#category_id').val(),
		category_name: $('#category_id option:selected').text(),
		department_id: $('#department_id').val(),
		department_name: $('#department_id option:selected').text(),
		workflow_id : $('#workflow_ids').val(),
                priority: priority,
                advisory:advisory,
                department_access:department,
	};
	
	
	var fields = [];
	$('.my_fields').each(function(){
		var field = {
			required : $(this).attr('is-required'),
			data_type_id: $(this).attr('data-type-id'),
			label : $(this).attr('label'),
			items : $(this).attr('items')

		};
		fields.push(field);
	});

	ref_workflow.fields = fields;

	var approvers = [];
	var approver_sequence = 0 ;
	$('.my_approvers').each(function(){
		approver_sequence += 1;
		var approver = {
			approver_id : $(this).attr('approver_id'),
			designation : $(this).attr('designation'),
			email : $(this).attr('email'),
			fullname : $(this).attr('fullname'),
			sequence : approver_sequence,
			tat : $(this).attr('tat'),

		};
		approvers.push(approver);
	});

	ref_workflow.approvers = approvers;	


	var activities = [];
	var activitiy_sequence = 0 ;
	$('.my_activities').each(function(){
		activitiy_sequence += 1;
		var activity = {
			approver_id : $(this).attr('approver_id'),
			sequence : activitiy_sequence,
			designation : $(this).attr('designation'),
			email : $(this).attr('email'),
			fullname : $(this).attr('fullname'),
			activity_name : $(this).attr('activity_name'),
			tat : $(this).attr('tat'),
		};
		activities.push(activity);
	});

	ref_workflow.activities = activities;	


	
	return ref_workflow;

}

function viewWorkflowDetails(workflow_details){
		$('#WORKFLOW_NAME').html(workflow_details.name);
        $('#REQUEST_TYPE').html(workflow_details.name);
		$('#WORKFLOW_CATEGORY').html(workflow_details.department_name);
		$('#WORKFLOW_DESCRIPTION').html(workflow_details.description);
		var fields = workflow_details.fields;
		var approvers = workflow_details.approvers;
		var activities = workflow_details.activities;
	
		var fields_html = '';
		for (var i = 0; i < fields.length; i++) {
			var data = fields[i];
			if(data.required){
				data.is_required = true;	
			}
		
			var template = '';
			switch(data.data_type_id){
				case "1":
					 template= $('#pblshText').html();
				break;
				case "2":
					 template= $('#pblshTextArea').html();
				break;
				case "3":
					 var items = data.items;
					 var options_html = '';
					var options_array = items.split(',');
					if(options_array.length > 0){
						for (var x = 0; x < options_array.length; x++) {
							options_html += '<option>'+options_array[x]+'</option>';
						}
					}else{
						options_html = options_array;
					} 
					 data.options_html = options_html;
					 template= $('#pblshDropDown').html();
				break;
				case "4":
					 template= $('#pblshDate').html();
				break;
			}
			
			
			var html = Mustache.to_html(template,data);
			fields_html += html;
		};
		$('#WORKFLOW_FIELDS').html('');
		$(fields_html).appendTo('#WORKFLOW_FIELDS');

		var task_html = '<tr><td style="padding-left:30px !important;">Initiated</td><td>Initiator of Request</td><td></td><td></td></tr>';
		
		for (var i = 0; i < approvers.length; i++) {
			var data = approvers[i];
			var template = $('#pblshApprover').html();
			var html = Mustache.to_html(template,data);
			task_html += html;
		};
                var tmp_data = activities[activities.length - 1];
               
		for (var i = 0; i < activities.length; i++) {
                    //if(tmp_data['activity_name'] != "Acknowledge Request")
                    //{
			var data = activities[i];
			var template = $('#pblshActivity').html();
                        //alert(data['tat']);JAMES
			var html = Mustache.to_html(template,data);
			task_html += html;
                    //}
		};
		$('#taskContainer').html('');
		task_html += '<tr><td style="padding-left:30px !important;">Acknowledge Request</td><td>Initiator of Request</td><td></td><td></td></tr>';
		$(task_html).appendTo('#taskContainer');
                //alert("--------------------");


		$('#editWorkflow').click(function(){
			$('#create_workflow').removeClass('hidden');
			$('#publish_workflow').addClass('hidden');
			$('#collapseOne').collapse('show');
                        $('#btnNext').click(function(){
				current_index = -1;
				WORKFLOW_DETAILS = saveWorkflow();
				console.log(WORKFLOW_DETAILS);
				viewWorkflowDetails(WORKFLOW_DETAILS);

				$('#step-circle-lists-5').addClass('active');
				$('span#collapse4').removeClass('hidden');
				$('#create_workflow').addClass('hidden');
				$('#publish_workflow').removeClass('hidden');
                                var ischecked_access = $('#accessibility').attr('ischecked');
				if(ischecked_access == "checked")
                                {
                                     $('#pnlAdminPermission').removeClass('hidden');
                                }                     
                        });                        
		});	
		$('#discardWorkflow').click(function(){
			window.location.replace(URL + 'admin');
		});	
}
$('#publishWorkflow').click( function () {

    workflow_details = saveWorkflow();

    save(workflow_details).done(function (result) {
        if (result.status == 0) {
            alert(result.message);
        } else if (result.status == 1) {
            showSuccessModal(result.message, URL + 'admin', 1);
        }
    }).fail(function (jqXHR, textStatus, error) {
        alert(jqXHR.responseText);
    });
});

function autocompleteFollowing(txtBox){
	$(txtBox).prop('disabled',true);

	$(txtBox).prop('disabled',false);
	$(txtBox ).autocomplete({
	  minLength: 0,
    source: function(request, response) {
	    var results = $.ui.autocomplete.filter(search_list.data, request.term);
            
    response(results.slice(0, 10));
	},
	change:function(){
		
	},
    select: function(event,ui){
    		var approver_id = ui.item.employee_id;
      		$(txtBox).attr('approver_id', approver_id);
      		$(txtBox).attr('designation', ui.item.designation);
      		$(txtBox).attr('email', ui.item.email);		
      		$(txtBox).attr('fullname', ui.item.fullname);		
      }
    }).autocomplete("instance")._renderItem = function(ul,item){
    	$(ul).addClass('main-search');
		 return $("<li/>")
            .append(item.label)
            .appendTo(ul);
    };  		
	
    

}


$(function(){
	var current_index = 0;
	var collapse_array = ['#collapseOne','#collapseTwo','#collapseThree','#collapseFour','#collapseFive'];

	var button_array = [
					['',''],
					['#btnAddAnotherField',''],
					['#btnAddApprover','#btnSkip'],
					['#btnAddActivity',''],
					['#btnDone',''],
				];
	$('#collapseOne').on('show.bs.collapse', function () {
	  	 current_index = 0;
	  	$('#btnAddAnotherField').addClass('hidden');
		$('#btnAddApprover').addClass('hidden');
		$('#btnAddActivity').addClass('hidden');
		$('#btnSkip').addClass('hidden');
		$('#btnDone').addClass('hidden');

		//$('#collapseTwo').collapse('hide');
		//$('#collapseThree').collapse('hide');
		//$('#collapseFour').collapse('hide');
		//$('#collapseFive').collapse('hide');
                

	});
	$('#collapseTwo').on('show.bs.collapse', function () {
	  	 current_index = 1;
		$('#btnAddAnotherField').removeClass('hidden');
		$('#btnAddApprover').addClass('hidden');
		$('#btnAddActivity').addClass('hidden');
		$('#btnSkip').addClass('hidden');
		$('#btnDone').addClass('hidden');

		//$('#collapseOne').collapse('hide');
		//$('#collapseThree').collapse('hide');
		//$('#collapseFour').collapse('hide');
		//$('#collapseFive').collapse('hide');

		$('#step-circle-lists-2').addClass('active');
		$('span#collapse1').removeClass('hidden');

	});
	$('#collapseThree').on('show.bs.collapse', function () {
	  	current_index = 2;
//		$('#btnAddAnotherField').addClass('hidden');
		$('#btnAddApprover').removeClass('hidden');
		$('#btnAddActivity').addClass('hidden');
		$('#btnSkip').removeClass('hidden');
		$('#btnDone').addClass('hidden');

		//$('#collapseTwo').collapse('hide');
		//$('#collapseOne').collapse('hide');
		//$('#collapseFour').collapse('hide');
		//$('#collapseFive').collapse('hide');

		$('#step-circle-lists-3').addClass('active');
		$('span#collapse2').removeClass('hidden');
	});
	$('#collapseFour').on('show.bs.collapse', function () {
	  	current_index = 3;
//		$('#btnAddAnotherField').addClass('hidden');
//		$('#btnAddApprover').addClass('hidden');
		$('#btnAddActivity').removeClass('hidden');
		$('#btnSkip').removeClass('hidden');
		$('#btnDone').addClass('hidden');

		//$('#collapseTwo').collapse('hide');
		//$('#collapseThree').collapse('hide');
		//$('#collapseOne').collapse('hide');
		//$('#collapseFive').collapse('hide');

		$('#step-circle-lists-4').addClass('active');
		$('span#collapse3').removeClass('hidden');

	});
        
//jpb

        
        $('.btn-edit-toggle').click(function () {
            var $this = $(this);
            $this.toggleClass('btn-edit-toggle');
            if ($this.hasClass('btn-edit-toggle')) {
                $this.text('Edit');
            } else {
                $this.text('Save');
            }
        });    
        
	$(document).on('click','#btnEditWorkflowDetails',function(){

            
            if ($(this).hasClass('btn-edit-toggle')){

                $('.workflow-det:not(:selected)').attr('disabled', true);               
            } else {

                $('.workflow-det:not(:selected)').attr('disabled', false);             
            }
                    
	});
        
      
        
        
	$('#btnNext').click(function(){
            var total_error = 0;
            //if(current_index == 0)
            //{
                if($("#workflow_name").val() == "")
                {
                        var err_message = '<p id="errMsgWFname" class="text-danger">This field is required.</p>';
                        
                        var foundin = $('#workflow_name_div:contains("This field is required.")');
                        if(foundin.length <= 0) { 
                             $("#workflow_name_div").append(err_message);  
                             total_error = total_error +1;
                         }
                        
                        total_error = total_error +1;
                }
                else
                {
                    $("#errMsgWFname").remove();  
                }
                if($.trim($("#frmFieldsContainer").html())==''  && current_index == 1)
                {
                        var err_message = '<p id="errMsgWFDesign" class="text-danger" style="padding-left:30px;"><br/>This field is required.</p>';
                        
                        var foundin = $('#frmFieldsContainer:contains("This field is required.")');
                        if(foundin.length <= 0) { 
                             $("#frmFieldsContainer").append(err_message);  
                             total_error = total_error +1;
                        }
                        
                        total_error = total_error +1;
                }
                else
                {
                    $("#errMsgWFDesign").remove();  
                }
               
                if($.trim($(".my_activities").html())==''  && current_index == 3)
                {
                        var err_message = '<p id="errMsgWFAct" class="text-danger" style="padding-left:30px;"><br/>This field is required.</p>';
     
                        var foundin = $('#activityPrompt:contains("This field is required.")');
                        if(foundin.length <= 0) { 
                             $("#activityPrompt").append(err_message);  
                             total_error = total_error +1;
                        }
                        
                        total_error = total_error +1;
                }
                else
                {
                    $("#errMsgWFAct").remove();  
                }                
                
            //}
			if(current_index == 3 && total_error == 0){
				current_index = -1;
				WORKFLOW_DETAILS = saveWorkflow();
				console.log(WORKFLOW_DETAILS);
				viewWorkflowDetails(WORKFLOW_DETAILS);

				$('#step-circle-lists-5').addClass('active');
				$('span#collapse4').removeClass('hidden');
				$('#create_workflow').addClass('hidden');
				$('#publish_workflow').removeClass('hidden');
                                var ischecked_access = $('#accessibility').attr('ischecked');
				if(ischecked_access == "checked")
                                {
                                     $('#pnlAdminPermission').removeClass('hidden');
                                }
			}	
                                //$( document ).ready(function() {
                                
                           
                    if(total_error == 0)
                    {
			//$(collapse_array[current_index]).collapse('hide');	
			$(collapse_array[current_index + 1]).parent().removeClass('hidden');	
			$(collapse_array[current_index +1]).collapse('show');
                        
                        $('.workflow-det:not(:selected)').attr('disabled', true);
                        $('#btnEditWorkflowDetails').removeClass('hidden');
                        $('#WORKFLOW_FIELDS :input').attr('disabled', true);  
                    }
	});	
	$('#btnSkip').click(function(){

			if(current_index == 3){
				current_index = -1;
				WORKFLOW_DETAILS = saveWorkflow();
				console.log(WORKFLOW_DETAILS);
				viewWorkflowDetails(WORKFLOW_DETAILS);

				$('#step-circle-lists-5').addClass('active');
				$('span#collapse4').removeClass('hidden');
				$('#create_workflow').addClass('hidden');
				$('#publish_workflow').removeClass('hidden');
				
			}	
//			$(collapse_array[current_index]).collapse('hide');	
			$(collapse_array[current_index + 1]).parent().removeClass('hidden');	
			$(collapse_array[current_index +1]).collapse('show');	
	});

	$(document).ready(function(){	
		var WORKFLOW_DETAILS  = null;
		$('.approverS').each(function(){
			autocompleteFollowing(this);
		});
		
		$('#btnAddAnotherField').click(function(){ 
			var template = $('#tplFields').html();
			$(template).appendTo('#tblFieldsContainer').hide().fadeIn();
                        $("#errMsgWFDesign").remove();
		});
	});	   
	$(document).on('click','.btnDeleteField',function(){
		$(this).parent().parent().fadeOut(function(){
			$(this).remove();
		});
	});
	$(document).on('click','.btnDeleteFields',function(){
		$(this).parent().parent().parent().fadeOut(function(){
			$(this).remove();
		});
	});
	$(document).on('click','#btnDel',function(){
            $('#frmDiscard').submit();
	});        
	$(document).on('click','.btnSaveField',function(){
                $("#errMsgWFDesign").remove(); 
		var parent = $(this).parent().parent();
		var errors = $(parent).geneValidateFields();
		if(errors > 0) return;
		var data_type_id = $(this).parent().parent().find('#dropDataType').val();
		var is_required = $(this).parent().parent().find('#chckRequired').is(':checked');
		var label = $(this).parent().parent().find('#lblField').val()
		var options = '';
		var options_html = '';
		var items = '';
		if(data_type_id == "3"){
			options += $(this).parent().parent().find('#add-list').val();
			items = options;
			var options_array = options.split(',');
			for (var i = 0; i < options_array.length; i++) {
				options_html += '<option value="'+options_array[i]+'">'+options_array[i]+'</option>';
			}
		}
		
		var data = {};	
		if(is_required){
			data.is_required = is_required;	
		}
		data.options = options_html;
		data.required = is_required;
		data.label = label;
		data.data_type_id = data_type_id; 
		data.items = options;

		var template = '';


		switch(data_type_id){
			case "1":
				template = $('#fldText').html();                               
			break;

			case "2":
				template = $('#fldTextArea').html();
			break;
			case "3":
				template = $('#fldDropdown').html();
			break;
			case "4":
				template = $('#fldDate').html();
			break;

		}

	    
		var html = Mustache.to_html(template,data)
		$(html).appendTo('#frmFieldsContainer').hide().fadeIn();
		$( ".datepicker" ).datepicker({
	      showOn: "button",
	      buttonText: "&#9660;"
	    });

		$(this).parent().parent().remove();
                $('.my_fields:not(:selected)').attr('disabled', true);
                $('.ui-datepicker-trigger').addClass('hidden');
	});


	$(document).on('click','.btnDeleteFldApprover',function(){
		$(this).parent().parent().parent().remove();

	});
	$(document).on('click','.btnSaveApprover',function(){
			var parent = $(this).parent().parent();
			var errors = $(parent).geneValidateFields();
                        var groups_id = $("#approverSGroups").val();
                        var groups_name =  $("#approverSGroups option:selected" ).text();
                        if($("#approverSs").val() != "")
                        {
                            errors = 0;
                        }
                       
                        if($("#approverSs").is(':hidden') == true)
                        {
                            errors = 0;
                            if(groups_id.isEmpty()){
                                errors = 1;
                            }
                           
                        }                        
			if(errors > 0) return;
                        var data= null;
                        if($("#approverSs").is(':hidden') == true)
                        {
                            data = {
                                    approver_id : "A-"+groups_id,
                                    email : "",
                                    designation :groups_name,
                                    tat : $(parent).find('#tat').val(),
                                    fullname: groups_name,
                            };
                        }
                        else
                        {
                            data = {
                                    approver_id : $(parent).find('#approverSs').attr('approver_id'),
                                    email : $(parent).find('#approverSs').attr('email'),
                                    designation : $(parent).find('#approverSs').attr('designation'),
                                    tat : $(parent).find('#tat').val(),
                                    fullname: $(parent).find('#approverSs').attr('fullname'),
                            };
                            if (data.approver_id === '{{approver_id}}'){alert('The name you entered is not a recognized user of the system. Please contact your system administrator.'); return;}
                        }
			
			var template = $('#fldApprovers').html();
			var html = Mustache.to_html(template,data);
			$(html).appendTo('#tblApproversContainer').hide().fadeIn();
			$(parent).remove();
	});

	$(document).on('click','#btnAddApprover',function(){

		var template = $('#fldAddApprovers').html();
		$(template).appendTo('#tblAddApproversContainer').hide().fadeIn().find('.approverS').each(function(){
			autocompleteFollowing(this);
			
			spin_days($(this).parent().parent().find('#tat'));
			
			
		});
                $("#approverSGroups").hide();
	});
	$(document).on('click','#btnAddActivity',function(){
		var template = $('#fldAddActivities').html();
		$(template).appendTo('#tblAddActivitesContainer').hide().fadeIn().find('.approverS').each(function(){
			autocompleteFollowing(this);

			spin_days($(this).parent().parent().find('#tat'));
		});
               $(".approverSGroup").hide();
               $("#errMsgWFAct").remove();  
	});

	$(document).on('click','.btnDeleteApprover',function(){
		$(this).parent().parent().remove();
	});
	
	$(document).on('click','.btnSaveActivity',function(){
			var parent = $(this).parent().parent();
			var errors = $(parent).geneValidateFields();
                        var groups_id = $(parent).find(".approverSGroup").val();
                        var groups_name =  $(parent).find(".approverSGroup option:selected" ).text();
                        var approver_id = $(parent).find('.approverS').attr('approver_id');
                        console.log (groups_id, groups_name, approver_id);
                        
                        if($(parent).find(".approverS").val() != "")
                        {
                            errors = 0;
                        }
                        if($(parent).find("#activity_name").val() == "")
                        {
                            errors = 1;
                        }                          
                        if($(parent).find(".approverS").is(':hidden') == true)
                        {
                            errors = 0;
                            if(groups_id.isEmpty()){
                                errors = 1;
                            }
                           
                        }
			if(errors > 0) return;
                        
                        
                        var data = null;
                        
			if($(parent).find(".approverS").is(':hidden') == true)
                        {
                            data = {
                                    approver_id : "TEAM-"+groups_id,
                                    email : 'TEAM',
                                    designation : '',
                                    tat : $(parent).find('#tat').val(),
                                    fullname: groups_name,
                                    activity_name: $(parent).find('#activity_name').val(),
                            };
                        }
                        else
                        {
                             data = {
                                    approver_id : $(parent).find('.approverS').attr('approver_id'),
                                    email : $(parent).find('.approverS').attr('email'),
                                    designation : $(parent).find('.approverS').attr('designation'),
                                    tat : $(parent).find('#tat').val(),
                                    fullname: $(parent).find('.approverS').attr('fullname'),
                                    activity_name: $(parent).find('#activity_name').val(),
                            };
                            if (data.approver_id === '{{approver_id}}'){alert('The name you entered is not a recognized user of the system. Please contact your system administrator.'); return;}                            
                        }
			
			var template = $('#fldActivities').html();
			var html = Mustache.to_html(template,data);
			$(html).appendTo('#tblActivitiesContainer').hide().fadeIn();
			$(parent).remove();
	});
                $(".assign-person").attr("style", "color:#444");
                $(".assign-group").attr("style", "color:#9a9a9a");
              
	$(document).on('click','.assign-group',function(){
		$(this).closest(".assign-to-panel").find(".approverS").hide().fadeOut();
                $(this).closest(".assign-to-panel").find(".approverSGroup").fadeIn();
                $(this).closest(".assign-to-panel").find(".assign-group").attr("style", "color:#444");
                $(this).closest(".assign-to-panel").find(".assign-person").attr("style", "color:#9a9a9a");
	});	
	$(document).on('click','.assign-person',function(){
		$(this).closest(".assign-to-panel").find(".approverSGroup").hide().fadeOut();
                $(this).closest(".assign-to-panel").find(".approverS").fadeIn();
                $(this).closest(".assign-to-panel").find(".assign-person").attr("style", "color:#444");
                $(this).closest(".assign-to-panel").find(".assign-group").attr("style", "color:#9a9a9a");
	});        
        $('#pnlAdminPermission').addClass('hidden');
        
        $('#accessibility').change(function () {
            if (this.checked) {
                $('#pnlAdminPermission').removeClass('hidden');
            } else {
                $('#pnlAdminPermission').addClass('hidden');
            }
        });

    
//        $("#assign-persons").attr("style", "color:#444");
//        $("#assign-groups").attr("style", "color:##9a9a9a");
//    
//	$(document).on('click','#assign-groups',function(){
//		$("#approverSs").hide().fadeOut();
//                $("#approverSGroups").fadeIn();
//                $("#assign-groups").attr("style", "color:#444");
//                $("#assign-persons").attr("style", "color:#9a9a9a");
//	});	
//	$(document).on('click','#assign-persons',function(){
//		$("#approverSGroups").hide().fadeOut();
//                $("#approverSs").fadeIn();
//                $("#assign-persons").attr("style", "color:#444");
//                $("#assign-groups").attr("style", "color:#9a9a9a");
//	}); 
	
});



