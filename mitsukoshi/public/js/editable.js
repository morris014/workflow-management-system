$(function(){
	$(document).ready(function(){
		 $(document).on('click','.edit-btn',function(){

		 	  $(this).removeClass('edit-btn');
		 	  $(this).parent().parent().find('input').removeAttr('disabled').focus();
		 	  $(this).parent().parent().find('select').removeAttr('disabled').focus();
		 	  $(this).parent().parent().find('#errMsg').remove();
		      $(this).parent().parent().parent().parent().addClass('active');
		      $(this).addClass('save-btn').html('Save');
		 });
		 $(document).on('click','.save-btn',function(){
		 	  $(this).removeClass('save-btn');

		 	  $(this).parent().parent().find('input').attr('disabled',true);
		 	  $(this).parent().parent().find('select').attr('disabled',true);
		 	  $(this).parent().parent().find('#errMsg').remove();
		      $(this).parent().parent().parent().parent().removeClass('active');
		      $(this).addClass('edit-btn').html('Edit');
		 });
	});
});