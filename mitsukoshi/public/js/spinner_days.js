
function spin_days(container){
    function spin (val){
    $(container).spinner({
        min: 0,
        max:99,
        step:val
    });
    }

    $(document).ready(function() {
    $.widget( "ui.spinner", $.ui.spinner, {
    _format: function(value) { return value; },
    _parse: function(value) { return parseFloat(value); }
    });

    $(container).val(0.25);
    spin(0.25);

    $('.ui-spinner-button.ui-spinner-up').on('click', function() {
      var get_num = $(container).val();
      //var get_num = num_step.substr(0, num_step.indexOf(" "));

        if(get_num<1){
          
          spin(.25);
        }
        if(get_num>=1){
          spin(1);
        }
    });

      $('.ui-spinner-button.ui-spinner-down').on('click', function() {
        var get_num = $(container).val();
     //   var get_num = num_step.substr(0, num_step.indexOf(" "));
          if(get_num>1){
            spin(1);
          }
          if (get_num<=1){
            
            spin(.25);
          }
      });
    });

}