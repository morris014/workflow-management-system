$(function(){

	function initiateRequest(workflow_id,fields){
		var params = {};
		params.workflow_id = workflow_id;
		params.fields = fields;
		params.comments = $('#txtComments').val();
		return $.ajax({
			url : URL + 'request/initiaterequest',
			type:'post',
			data: params,
			dataType: 'json',
			async: true
		});
	}
	function uploadFiles(data){
		
		return $.ajax({
			url: URL + 'task/upload?files',
			data: data,
			type: 'post',
			dataType: 'json',
			processData: false, // Don't process the files
        	contentType: false,
        	async: false
		});
	}        
	function uploadFile(request_id,my_files){
		var params = {};
		params.request_id = request_id;
		params.my_files = my_files;
		return $.ajax({
			url: URL + 'request/upload',
			data: params,
			type: 'post',
			dataType: 'json'
		});
	}
	function initTables(tables){
		var tables_html = '';
		$(tables).each(function(i,val){
			var table_id = val.table_id;
			var fields = val.fields;
			var fields_html = '';
			$(fields).each(function(){
				var data_type_id = this.data_type_id;
				var template = null;
				switch(data_type_id){
					case "1":
					case 1:
						template = $('#tplText').html();
						
					break;
					case 2:
					case "2":
						template = $('#tplTextArea').html();
						
					break;
					case 3:
					case "3":
						template = $('#tplDropdown').html();
						
					break;
					case 4:
					case "4":
						template = $('#tplDate').html();
						
					break;
				}
				if(this.required > 0){
					this.is_required = true;
				}
				fields_html += Mustache.to_html(template,this);
			});
			
			var tplTable = $('#tplTable').html();
			var data = {}; 
			data.fields = fields_html;
			tables_html += Mustache.to_html(tplTable,data);
		});
		$('#pnlTables').html(tables_html);

	}
	function initWorkFlowDetails(workflow_id,name,category,description){
		$('#txtWorkflowId').html(workflow_id);
		$('#txtWorkflowName').html(name);
		$('#txtWorkflowCategory').html(category);
		$('#txtWorkflowDescription').html(description);
	}
	function clearWorkflowDetails(){
		$('#txtWorkflowId').html('');
		$('#txtWorkflowName').html('');
		$('#txtWorkflowCategory').html('');
		$('#txtWorkflowDescription').html('');
		
	}
	function clearTables(){
		$('#pnlTables').html('');
	}
	function hidePanels(){
		$('#pnlComments').addClass('collapse');
		$('#pnlRequestDetails').addClass('collapse');
		$('#pnlFields').addClass('collapse');
	}
	function showPanels(){
		$('#pnlComments').removeClass('collapse');
		$('#pnlRequestDetails').removeClass('collapse');
		$('#pnlFields').removeClass('collapse');	
	}
	$(document).ready(function(){
		clearWorkflowDetails();
		hidePanels();

		$('#cboDepartment').change(function(){
			$('#cboWorkflows').html('');
			hidePanels();
			var department_id = this.value;
			if(department_id > 0){
				$('#cboWorkflows').attr("disabled","disabled");
				RefWorkflow.getByDepartmentId(department_id).done(function(results){
					var options = '<option value="0"></option>';
                                        
                                            $(results).each(function(i,val){
                                                
                                                if(val.status != 3)
                                                {
                                                    options += '<option value = "' + val.workflow_id + '">' + val.name + '</option>';
                                                }
                                            });
					$('#cboWorkflows').html(options);
					$('#cboWorkflows').removeAttr('disabled');
				});
				  
			}
		});

		

	});

	$(document).on('change','#cboWorkflows',function(){
		clearWorkflowDetails();			
		clearTables();
		var workflow_id = this.value;
		if(workflow_id > 0){
			$('#loader').removeClass('hidden');
			RefWorkflow.getByWorkflowId(workflow_id).done(function(results){
				initWorkFlowDetails(results.details.workflow_id,results.details.name,results.details.department,results.details.description);

				initTables(results.tables);

				showPanels();

				$('#loader').addClass('hidden');
			}).fail(function (jqXHR, textStatus, error) {
        		alert(jqXHR.responseText);
    		});

		}else{
			hidePanels();
		}


	});
        
        var files;
        $('#file-select-initiate').on('change', prepareUpload);
        function prepareUpload(event)
        {
            files = event.target.files;
        }
        $('#initiate-request').click(function () {
            
          
        });
	$(document).on('submit','#frmInitiateRequest',function(e){
		e.preventDefault();
		var workflow_id = $('#cboWorkflows').val();
		var fields = [];
		var err = 0;
		$('.tableFields').each(function(){
			err = $(this).geneValidateForm();
			if(err > 0){
				return;
			}
			$(this).find('input[type=text]').each(function(){
				var field_data = {};
				field_data.value = this.value;
				field_data.field_id = $(this).attr('field-id');
				fields.push(field_data);
			});
			$(this).find('textarea').each(function(){
				var field_data = {};
				field_data.value = this.value;
				field_data.field_id = $(this).attr('field-id');
				fields.push(field_data);
			});
			$(this).find('input[type=date]').each(function(){
				var field_data = {};
				field_data.value = this.value;
				field_data.field_id = $(this).attr('field-id');
				fields.push(field_data);
			});

			$(this).find('select').each(function(){
				var field_data = {};
				field_data.value = $(this).find(':selected').text();
				field_data.field_id = $(this).attr('field-id');
				fields.push(field_data);
			});

		});
	
		if(err > 0) return; 
		initiateRequest(workflow_id,fields).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status > 1){
                                        //alert(result.status);
                                        
                                        var request_id = result.status;
                                        var data = new FormData();
                                        
                                        var total_files = 0;
                                        var file_name = '';
                                        var my_files = {};       
                                        if (files != null) {
                                            
                                            $.each(files, function (key, value) {
                                            total_files += 1;
                                                data.append(key, value);
                                        });
                                        }   
                                        var is_uploaded = true;
                                        
                                        if (total_files > 0) {
                                            uploadFiles(data).done(function (result) {
                                                if (result.status == 0) {
                                                    console.log(result);
                                                    alert('Uploading Failed');
                                                    is_uploaded = false;
                                                    return;
                                                }
                                                
                                                my_files.name = result.data[0].name;
                                                my_files.path = result.data[0].path;
                                            });
                                        }
                                        
                                        if (is_uploaded == false) {
                                            return;
                                        }
                                        uploadFile(request_id, my_files).done(function (result) {
                                            if (result.status == 0) {

                                                alert("->>>"+result.message);
                                            } else if (result.status == 1) {
                                                //showSuccessModal(result.message, URL + 'task', 1);
                                            }
                                        }).fail(function (jqXHR, textStatus, error) {
                                            //alert(jqXHR.responseText);
                                        });
                                    
                                        
                                        
                                        
                                        
                                        
                                        showSuccessModal(result.message,URL + 'request?status=in-progress&id=' + result.data.request_id,1);
				}
		}).fail(function (jqXHR, textStatus, error) {
        	alert(jqXHR.responseText);
    	});
	});

});