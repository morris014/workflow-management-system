$(function(){

	function fillAutocomplete(txtBox,source){
		
		$(txtBox ).autocomplete({
		  minLength: 0,
	    source: function(request, response) {
		    var results = $.ui.autocomplete.filter(source.data, request.term);

	    response(results.slice(0, 4));
		},
		change:function(){
			
		},
	    select: function(event,ui){
	    		var approver_id = ui.item.employee_id;
	      		$(txtBox).attr('approver_id', approver_id);
	      		$(txtBox).attr('designation', ui.item.designation);
	      		$(txtBox).attr('email', ui.item.email);		
	      		$(txtBox).attr('fullname', ui.item.fullname);		
	      }
	    }).autocomplete("instance")._renderItem = function(ul,item){
	    	$(ul).addClass('main-search');
			 return $("<li/>")
	            .append(item.label)
	            .appendTo(ul);
	    };  		
		
	}
	function uploadFiles(data){
		
		return $.ajax({
			url: URL + 'task/upload?files',
			data: data,
			type: 'post',
			dataType: 'json',
			processData: false, // Don't process the files
        	contentType: false,
        	async: false
		});
	}
	function returnRequest(request_id,comments){
		var params = {};
		params.request_id = request_id;
		params.comments = comments;
		return $.ajax({
			url: URL + 'task/returnrequest',
			data: params,
			type: 'post',
			dataType: 'json'
		});
	}

	function approve(request_id,comments,my_files){
		var params = {};
		params.request_id = request_id;
		params.comments = comments;
		params.my_files = my_files;
		return $.ajax({
			url: URL + 'task/approve',
			data: params,
			type: 'post',
			dataType: 'json'
		});
	}
	
	function reject(request_id,comments,my_files){
		var params = {};
		params.request_id = request_id;
		params.comments = comments;
		params.my_files = my_files;
		
		return $.ajax({
			url: URL + 'task/reject',
			data: params,
			type: 'post',
			dataType: 'json'
		});
	}

	function reassign(request_id,comments,re_assign_id){
		var params = {};
		params.request_id = request_id;
		params.comments = comments;
		params.re_assign_id = re_assign_id;
		return $.ajax({
			url: URL + 'task/reassign',
			data: params,
			type: 'post',
			dataType: 'json'
		});
	}

	$(document).ready(function(){
		var files;

		$('#file-select').on('change', prepareUpload);
		function prepareUpload(event)
		{
		  files = event.target.files;
		}
		if (autocompletesource) fillAutocomplete($('#approver'),autocompletesource);

		$('#btnApprove').click(function(){
                        var comment = $("#txtComment").val();
                        if(comment.isEmpty())
                        {
                            $("#txtComment").removeClass('gene_required');
                        }                        
			var data = new FormData();
			var total_files = 0;
			var file_name = '';
			var my_files = {};
			if(files != null){
				$.each(files, function(key, value){
			    	total_files += 1;
			        data.append(key, value);
		    	});
			
			}
			
			var request_id = $('#request_id').val();
			var comments = $('#txtComment').val();

			if($('#commentsContainer').geneValidateForm() > 0){

				return;
			}
			var is_uploaded = true;

			if(total_files > 0){
				uploadFiles(data).done(function(result){
					if(result.status == 0){
						console.log(result);
						alert('Uploading Failed');
						is_uploaded = false;
						return;
					}
			    	my_files.name = result.data[0].name;
			    	my_files.path = result.data[0].path;
			    }); 
		    }
		    if(is_uploaded == false){
		    	return;
		    }
			approve(request_id,comments,my_files).done(function(result){
				if(result.status == 0){

					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'task',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
        		alert(jqXHR.responseText);
    		});
		});
		
		$('#btnReject').click(function(){
			var data = new FormData();
			var total_files = 0;
			var file_name = '';
			var my_files = {};
			if(files != null){
				$.each(files, function(key, value){
			    	total_files += 1;
			        data.append(key, value);
		    	});
			
			}
			
			var request_id = $('#request_id').val();
			var comments = $('#txtComment').val();

			if($('#commentsContainer').geneValidateForm() > 0){

				return;
			}
			var is_uploaded = true;

			if(total_files > 0){
				uploadFiles(data).done(function(result){
					if(result.status == 0){
						console.log(result);
						alert('Uploading Failed');
						is_uploaded = false;
						return;
					}
			    	my_files.name = result.data[0].name;
			    	my_files.path = result.data[0].path;
			    }); 
		    }
		    if(is_uploaded == false){
		    	return;
		    }
			
			reject(request_id,comments,my_files).done(function(result){
				if(result.status == 0){

					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'task',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
        		alert(jqXHR.responseText);
    		});
		});

		$('#btnReassign').click(function(){
			var request_id = $('#request_id').val();
			var re_assign_id = $('#approver').attr('approver_id');
			var comments = $('#txtComment').val();

			if($('#approverContainer').geneValidateFormByAttributes('approver_id') > 0){

				return;
			}			

			reassign(request_id,comments,re_assign_id).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'task',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
        		alert(jqXHR.responseText);
    		});
		});

		$('#btnReturn').click(function(){
			var request_id = $('#request_id').val();
			var comments = $('#txtComment').val();

			if($('#commentsContainer').geneValidateForm() > 0){

				return;
			}
			
			returnRequest(request_id,comments).done(function(result){
				if(result.status == 0){
					alert(result.message);
				}else if(result.status == 1){
					showSuccessModal(result.message,URL + 'task',1);
				}
			}).fail(function (jqXHR, textStatus, error) {
        		alert(jqXHR.responseText);
    		});
		});
	});
	$('.dropdownSort').on('click',function(e){
		e.preventDefault();
		console.log('Ariel');
	});
});