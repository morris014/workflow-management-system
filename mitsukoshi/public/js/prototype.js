function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
String.prototype.isEmpty = function() { // <-- removed the argument
   return !this.replace(/^\s+/g, '').length; // boolean (`true` if field is empty)
};

function isEmailExisting(my_data){
  
	return $.ajax({
		url : URL + 'Mmpitableoforganization/isEmailExisting',
		dataType: 'json',
		data: my_data,
		type: 'post'
	});
}
$.fn.geneValidateFormByAttributes = function(attr){
	var total_error = 0;
	$(this).find('.gene_required').each(function(){
		$(this).parent().find('#errMsg').remove();
		$(this).parent().removeClass('has-error');
		var value = $(this).attr(attr);
		if(!value || value == 0 || value == null){
			total_error += 1;
			$(this).parent().addClass('has-error');	
			var err_message = '<p id="errMsg" class="text-danger">This field is required.</p>';
			$(this).parent().append(err_message);
		}
	});
	return total_error;
}
$.fn.geneValidateForm = function(){
	var total_error = 0;

	$(this).find('.gene_required').each(function(){
		$(this).parent().find('#errMsg').remove();
		$(this).parent().removeClass('has-error');
		var value = $(this).val();
  
		if(value.isEmpty()){
			total_error += 1;
			$(this).parent().addClass('has-error');	
			var err_message = '<p id="errMsg" class="text-danger" style="font-size:12px">This field is required. </p>';
			$(this).parent().append(err_message);
		}
	});
                    var employee_id =$("#employee_id").val();
                    //alert(employee_id);
        var email = $("#email").val();
        if(typeof email != 'undefined')
        {
            if(email.isEmpty() == false)
            {
                var str = email.split("@");
                if(str[1] != "mitsukoshimotors.com" )
                //if(str[1] != "rebar.ph" )
                {
                    var err_message = '<p id="errMsg"  style="font-size:12px" class="text-danger">Invalid Email Address </p>';
                    var foundin = $('#email_div:contains("Invalid Email Address")');
                    if(foundin.length <= 0)
                    {
                        $("#email_div").append(err_message);   
                    }
                    total_error += 1;   



                }

                    var params = {
                        email : email,
                        employee_id : employee_id
                    };
                    //params.email = email;            
                    var emailExist = isEmailExisting(params).done(function (result) {
                        //alert("Status:"+result.status);
                        if (result.status == 1) {

                            //err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger">This email address already exists.</p>';
                            //var foundin = $('#email_div:contains("This email address already exists.")');
                           // if(foundin.length <= 0)
                            //{
                           //    $("#email_div").append(err_message); 
                            //}

                            total_error += 1;
                            return 1;
                        }     
                        else
                        {
                            return 0;
                        }
                                }).fail(function (jqXHR, textStatus, error) {
                         alert(jqXHR.responseText);
                     });

                   


            }
        }
        
        var lname = $("#lname").val();
        if(lname!=null)
        {
            if(lname.length > 45)
            {
                var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger"><strong>Last Name has a maximum of 45 characters.</strong> </p>';
                $("#lname_div").append(err_message);   
                total_error += 1;
            }
        }
 
        var fname = $("#fname").val();
        if(fname!=null)
        {
            if(fname.length > 45)
            {
                var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger"><strong>First Name has a maximum of 45 characters.</strong> </p>';
                $("#fname_div").append(err_message);   
                total_error += 1;
            }
        }    
        
        var mname = $("#mname").val();
        if(mname!=null)
        {
            if(mname.length > 45)
            {
                var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger"><strong>Middle Name has a maximum of 45 characters.</strong> </p>';
                $("#mname_div").append(err_message);   
                total_error += 1;
            }
        }   
        
        var group_name = $("#group_name").val();
        if(group_name!=null)
        {
            if(group_name.length > 45)
            {
                var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger"><strong>Group Name has a maximum of 45 characters.</strong> </p>';
                $("#group_name_div").append(err_message);   
                total_error += 1;
            }
        }            
        
        if(($("#tasks").prop('checked') == false) && 
             ($("#request").prop('checked') == false) &&
             ($("#administrator").prop('checked') == false) &&
             ($("#flow_mgt").prop('checked') == false) &&
             ($("#user_mgt").prop('checked') == false) &&
             ($("#group_mgt").prop('checked') == false) &&
             ($("#settings").prop('checked') == false) &&
             ($("#reports").prop('checked') == false))
        {
            //do something
            var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger">Please configure permissions. </p>';
            var foundin = $('#permission-checkbox:contains("Please configure permissions.")');
                if(foundin.length <= 0)
                {
                    $("#permission-checkbox").append(err_message);   
                }           
            total_error += 1;        
        }   
        
        if($("#administrator").prop('checked') == true )
        {
            if(($("#flow_mgt").prop('checked') == false) &&
             ($("#user_mgt").prop('checked') == false) &&
             ($("#group_mgt").prop('checked') == false) &&
             ($("#settings").prop('checked') == false))
            {
                var err_message = '<p id="errMsg"  style="font-size:12px"  class="text-danger">Please select administrator access.</p>';
                var foundin = $('#permission-checkbox:contains("Please select administrator access.")');
                if(foundin.length <= 0)
                {
                    $("#permission-checkbox").append(err_message);   
                }           
                total_error += 1;                
            }
            
        }
        //alert(total_error);
	return total_error;
}

$.fn.geneValidateRequestForm = function(){
	var total_error = 0;
	$(this).find('.gene_required').each(function(){
		var value = $(this).val();
		if(value.isEmpty()){
			total_error += 1;
			$(this).parent().addClass('has-error');	
			var err_message = '<p id="errMsg" class="text-danger">This field is required. </p>';
			alert(err_message);
		}
	});
	return total_error;
}

$.fn.geneValidateFields = function(){
	var total_error = 0;
	$(this).find('.gene_required').each(function(){
		$(this).parent().removeClass('has-error');
		var value = $(this).val();
		if(value.isEmpty()){
			total_error += 1;
			$(this).parent().addClass('has-error');	
		}
	});
	return total_error;
}