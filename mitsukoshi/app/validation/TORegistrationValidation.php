<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class TORegistrationValidation extends Validation
{
    public function initialize()
    {
        $this->add(
            'firs_name',
            new PresenceOf(
                array(
                    'message' => 'First name is required'
                )
            )
        );

        $this->add(
            'last_name',
            new PresenceOf(
                array(
                    'message' => 'Last name is required'
                )
            )
        );


        $this->add(
            'email',
            new PresenceOf(
                array(
                    'message' => 'The e-mail is required'
                )
            )
        );

        $this->add(
            'email',
            new Email(
                array(
                    'message' => 'The e-mail is not valid'
                )
            )
        );


        $this->add(
            'contact_num',
            new PresenceOf(
                array(
                    'message' => 'Contact no is required'
                )
            )
        );


        $this->add(
            'location_id',
            new PresenceOf(
                array(
                    'message' => 'Location is required'
                )
            )
        );
        $this->add(
            'designation_id',
            new PresenceOf(
                array(
                    'message' => 'Designation is required'
                )
            )
        );
        $this->add(
            'department_id',
            new PresenceOf(
                array(
                    'message' => 'Department is required'
                )
            )
        );
        $this->add(
            'immediate_superior_id',
            new PresenceOf(
                array(
                    'message' => 'Immediate superior is required'
                )
            )
        );
    }
}