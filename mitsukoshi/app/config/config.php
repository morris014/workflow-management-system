<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$env = 'default';

if(@$_SERVER['HTTP_HOST'] == 'localhost')
{
    $env = 'localhost';

}
else
{
    $env = 'devops';
    //$http_origin = $_SERVER['HTTP_ORIGIN'];
    //header("Access-Control-Allow-Origin: $http_origin");
}   


//add base uri environment here
$baseuri = array();

$baseuri['localhost'] = 'http://localhost/workflow-management-system/mitsukoshi/';
$baseuri['devops'] = 'http://107.6.113.222:8086/';


$database['localhost'] = array(
        'adapter'     => 'Mysql',
        'host'        => '127.0.0.1',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'mitsukoshi_db',
        'charset'     => 'utf8',
    );
$database['rebar'] = array(
    'adapter'     => 'Mysql',
    'host'        => '128.199.80.20',
    'username'    => 'root',
    'password'    => 'redhorsebeer',
    'dbname'      => 'mitsukoshi_db',
    'charset'     => 'utf8',
); 

$database['devops'] = array(
        'adapter'     => 'Mysql',
        'host'        => '107.6.113.222',
        'username'    => 'aws',
        'password'    => 'aws1234',
        'dbname'      => 'mitsukoshi_db',
        'charset'     => 'utf8',
        'port' => '6033'
);


//set constant for base uri and image uri
define("BASE_URI",$baseuri[$env]);

return new \Phalcon\Config(array(
    'database' => $database[$env],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'validationDir'     => APP_PATH . '/app/validation/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => $baseuri[$env],
    )
));
