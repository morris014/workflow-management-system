<?php

$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
	$config->application->controllersDir,
	$config->application->modelsDir,
	$config->application->pluginsDir,
	$config->application->validationDir
));

$loader->registerClasses(
    array(
        "Helper"         => "helper/helper.php",
        'Fpdf' => "vendor/fpdf/fpdf.php"
    )
);


$loader->register();
