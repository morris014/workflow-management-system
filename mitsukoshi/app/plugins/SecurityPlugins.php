<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as Assets;


class SecurityPlugins extends Plugin
{
	
	public function beforeDispatch(Event $event , Dispatcher $dispatcher)
	{
	 	$controller_name = $dispatcher->getControllerName();
		$action_name = $dispatcher->getActionName();
		$this->view->setVar('controller_name',$controller_name);
		$this->view->setVar('action_name',$action_name);
		

		
		DEFINE('FIRST_NAME',($this->session->get('firs_name') != null) ? $this->session->get('firs_name') : '');
		DEFINE('LAST_NAME',($this->session->get('last_name') != null) ? $this->session->get('last_name') : '');
		DEFINE('EMAIL',($this->session->get('email') != null) ? $this->session->get('email') : '');
		DEFINE('TASK',($this->session->get('task') != null) ? $this->session->get('task') : 0);
		DEFINE('REQUEST',($this->session->get('request') != null) ? $this->session->get('request') : 0);
		DEFINE('ADMIN',($this->session->get('admin') != null) ? $this->session->get('admin') : 0);
		DEFINE('USER_MGT',($this->session->get('user_mgt') != null) ? $this->session->get('user_mgt') : 0);
		DEFINE('CLUSTER_MGT',($this->session->get('cluster_mgt') != null) ? $this->session->get('cluster_mgt') : 0);
		DEFINE('FLOW_MGT',($this->session->get('flow_mgt') != null) ? $this->session->get('flow_mgt') : 0);
		DEFINE('SETTINGS',($this->session->get('settings') != null) ? $this->session->get('settings') : 0);
		DEFINE('GROUP_MGT',($this->session->get('group_mgt') != null) ? $this->session->get('group_mgt') : 0);
		DEFINE('REPORTS',($this->session->get('reports') != null) ? $this->session->get('reports') : 0);
		DEFINE('EMPLOYEE_ID',($this->session->get('employee_id') != null) ? $this->session->get('employee_id') : '');
		DEFINE('USERNAME',($this->session->get('username') != null) ? $this->session->get('username') : '');
		DEFINE('DESIGNATION',($this->session->get('designation') != null) ? $this->session->get('designation') : '');
		DEFINE('DEPT_CODE',($this->session->get('dept_code') != null) ? $this->session->get('dept_code') : '');
		DEFINE('DEPT_ID',($this->session->get('department_id') != null) ? $this->session->get('department_id') : '');

	}


}