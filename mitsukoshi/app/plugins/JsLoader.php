<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as Assets;


class JsLoader extends Plugin
{	

	public function jspdf()
	{
		
		$this->addJs('global_footer_js','vendor/jspdf/jspdf.js');
                $this->addJs('global_footer_js','vendor/jspdf/plugins/cell.js');
		$this->addJs('global_footer_js','vendor/jspdf/plugins/standard_fonts_metrics.js');
		$this->addJs('global_footer_js','vendor/jspdf/plugins/split_text_to_size.js');
		$this->addJs('global_footer_js','https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js');
                $this->addJs('global_footer_js','vendor/jspdf/plugins/addhtml.js');
		$this->addJs('global_footer_js','vendor/jspdf/plugins/addimage.js');	
                //$this->addJs('global_footer_js','vendor/jspdf/plugins/filesaver.js');	
	}
	public function globalJS()
	{
		$this->addJs('global_footer_js',"js/jquery.min.js");
		$this->addJs('global_footer_js',"js/jquery-migrate.js");
		$this->addJs('global_footer_js','js/jquery-ui.js');		
		$this->addJs('global_footer_js',"js/bootstrap.min.js");
		$this->addJs('global_footer_js',"js/print_div.js");	
		$this->addJs('global_footer_js',"js/etc.js");		
		$this->addJs('global_footer_js',"js/editable.js");
		$this->addJs('global_footer_js',"js/prototype.js");
		$this->addJs('global_footer_js',"js/modal_function.js");
		$this->addJs('global_footer_js',"js/select2.min.js");
		$this->addJs('global_footer_js',"js/bootstrap-filestyle.min.js");
		$this->addJs('global_footer_js',"js/spinner_days.js");
                $this->addJs('global_footer_js',"js/index.js");
		$this->mustache_api();
	}

	public function mustache_api()
	{
		$this->addJs('global_header_js','vendor/Mustache/mustache.js-master/mustache.js');
	}
	public function outputGlobalHeaderJs($collection = "global_header_js")
	{

	}

	public function outputGlobalFooterJs($collection = 'global_footer_js')
	{
				
	}



	public function addJs($collection,$path,$local=true)
	{
		$scripts = $this->assets->collection($collection);
		$scripts->addJs($path,$local);

	}

	public function beforeDispatch(Event $event , Dispatcher $dispatcher)
	{
		$controller_name = $dispatcher->getControllerName();
		$action_name = $dispatcher->getActionName();
		$params = $dispatcher->getParams();


		$this->globalJS();


		switch($controller_name)
		{
			case "admin":
                                $this->addJs('global_footer_js','js/wf_list.js');    
				$this->addJs('global_footer_js','js/searchautocomplete.js');
				switch($action_name)
				{
					case "userManagement":
						$this->addJs('global_footer_js','js/add_user.js');
						$this->addJs('global_footer_js','js/user_list.js');
                                                
                                                if(sizeof($params) == 0){
                                                    $this->addJs('global_footer_js',"vendor/datatable.js");
                                                }
					break;
					case "create_workflow":
						$this->addJs('global_footer_js','js/create_workflow.js');
					break;

					case "groupManagement":
						$this->addJs('global_footer_js','js/group_list.js');
                                                
						if(sizeof($params)){
							switch($params[0]){
								case 'editGroup':
									$this->addJs('global_footer_js','js/edit_group.js');
								break;
								case 'createGroup':
									$this->addJs('global_footer_js','js/create_group.js');
								break;
                                                                default:
                                                                    $this->addJs('global_footer_js',"vendor/datatable.js");
                                                                break;
                                                                
							}
						}
                                                        if(sizeof($params) == 0){
                                                            $this->addJs('global_footer_js',"vendor/datatable.js");
                                                        }
					break;
					case 'reports':
						$this->jspdf();
                                                $this->addJs('global_footer_js',"js/reports.js");  
						$this->addJs('global_footer_js',"vendor/datatable.js");   
                                                 
					break;
					case "settings":
						$this->addJs('global_footer_js','js/settings.js');
					break;  
                                        default:
                                            $this->addJs('global_footer_js', "vendor/datatable.js");
                                        break;
                }
                                //if($action_name=="")
                                //{
                                  
                                //}
			break;

			case "request":
			$this->addJs('global_footer_js','js/searchautocomplete.js');
                        $this->addJs('global_footer_js','js/request_list.js');
                        $this->addJs('global_footer_js','js/request.js');
				switch($action_name)
				{
					case "initiate":
						$this->addJs('global_footer_js','ref_js/workflow.js');
						$this->addJs('global_footer_js','js/generate_controls.js');
						$this->addJs('global_footer_js','js/initiate.js');
					break;
				
				}
			break;
			case "task":
			$this->addJs('global_footer_js','js/searchautocomplete.js');
				$this->addJs('global_footer_js','js/task.js');
                                $this->addJs('global_footer_js','js/task_list.js');
			break;
		}

	}
}
