<?php
use Phalcon\Mvc\Model\Validator\Uniqueness;

class MmpiUserAccess extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $employee_id;

    /**
     *
     * @var string
     */
    public $username;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $security_key;

    /**
     *
     * @var string
     */
    public $validated;

    /**
     *
     * @var integer
     */
    public $task;

    /**
     *
     * @var integer
     */
    public $request;

    /**
     *
     * @var integer
     */
    public $admin;

    /**
     *
     * @var integer
     */
    public $user_mgt;

    /**
     *
     * @var integer
     */
    public $cluster_mgt;

    /**
     *
     * @var integer
     */
    public $flow_mgt;

    /**
     *
     * @var integer
     */
    public $settings;
    /**
     *
     * @var integer
     */
    public $group_mgt;
    /**
     *
     * @var integer
     */
    public $reports;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $creator_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('employee_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mmpi_user_access';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiUserAccess[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiUserAccess
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    /**
     * Author : Gene
     *
    **/
    public static function register($data){
   
        $record = new MmpiUserAccess();
        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"messages"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"messages"=>"Saved Succesfully","data"=>$record);
    }
    /**
     * Author : Gene
     *
    **/
    public function beforeValidation()
    {
        $this->validate(new Uniqueness(
            array(
                "field"   => "employee_id",
                "message" => "Employee is already registered"
            )
        ));
       

        return !$this->validationHasFailed();
    }
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
        $this->date_updated = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;   
    }
}
