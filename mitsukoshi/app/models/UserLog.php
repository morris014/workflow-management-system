<?php

class UserLog extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $date_login;

    /**
     *
     * @var string
     */
    public $employee_id;

    /**
     *
     * @var string
     */
    public $session_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('employee_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLog[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }
      public function beforeValidationOnCreate()
    {
        $this->date_login = CURR_DATETIME;
    }
    
    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLog
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_log';
    }

    public static function registerLog($employee_id,$session_id){
      
    }
}
