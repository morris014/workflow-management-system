<?php

class WorkflowTable extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $table_id;

    /**
     *
     * @var integer
     */
    public $workflow_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('table_id', 'TableFields', 'table_id', array('alias' => 'TableFields'));
        $this->belongsTo('workflow_id', 'RefWorkflow', 'workflow_id', array('alias' => 'RefWorkflow'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'workflow_table';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowTable[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowTable
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
