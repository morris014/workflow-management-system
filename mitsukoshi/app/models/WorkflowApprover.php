<?php

class WorkflowApprover extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $approver_id;

    /**
     *
     * @var double
     */
    public $tat;

    /**
     *
     * @var integer
     */
    public $sequence;

    /**
     *
     * @var integer
     */
    public $workflow_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('workflow_id', 'RefWorkflow', 'workflow_id', array('alias' => 'RefWorkflow'));
        $this->belongsTo('approver_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'workflow_approver';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowApprover[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowApprover
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getDetails($workflow_id)
    {
        $phql = "SELECT wa.tat,wa.approver_id,
                 rd.name as designation,tos.firs_name,tos.last_name,tos.email,tos.employee_id
                 FROM WorkflowApprover wa 
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = wa.approver_id
                 INNER JOIN RefDesignation rd ON tos.designation_id = rd.designation_id
                 WHERE wa.workflow_id = ?1
                 ORDER BY wa.sequence ASC
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        return $data;
    }
}
