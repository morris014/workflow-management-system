<?php

class RefDepartment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $department_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var integer
     */
    public $status_id;

    /**
     *
     * @var string
     */
    public $code;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('department_id', 'MmpiTableOfOrganization', 'department_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->hasMany('department_id', 'MmpiWorkflows', 'department_id', array('alias' => 'MmpiWorkflows'));
        $this->hasMany('department_id', 'RefWorkflow', 'department_id', array('alias' => 'RefWorkflow'));
        $this->belongsTo('status_id', 'RefStatus', 'status_id', array('alias' => 'RefStatus'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_department';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDepartment[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDepartment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
