<?php

use Phalcon\Mvc\Model\Validator\Email as Email;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class MmpiTableOfOrganization extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var string
     */
    public $employee_id;

    /**
     *
     * @var string
     */
    public $firs_name;

    /**
     *
     * @var string
     */
    public $middle_name;

    /**
     *
     * @var string
     */
    public $last_name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $contact_num;

    /**
     *
     * @var integer
     */
    public $location_id;

    /**
     *
     * @var integer
     */
    public $designation_id;

    /**
     *
     * @var integer
     */
    public $department_id;

    /**
     *
     * @var integer
     */
    public $status_id;

    /**
     *
     * @var string
     */
    public $immediate_superior_id;

    /**
     *
     * @var integer
     */
    public $position_id;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $date_created;
    

    /**
     *
     * @var string
     */
    public $request_id;    

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('employee_id', 'MmpiGroupMatrix', 'employee_id', array('alias' => 'MmpiGroupMatrix'));
        $this->hasMany('employee_id', 'MmpiTableOfOrganization', 'immediate_superior_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->hasMany('employee_id', 'MmpiUserAccess', 'employee_id', array('alias' => 'MmpiUserAccess'));
        $this->hasMany('employee_id', 'MmpiUserAccess', 'creator_id', array('alias' => 'MmpiUserAccess'));
        $this->hasMany('employee_id', 'MmpiWorkflowApprovers', 'employee_id', array('alias' => 'MmpiWorkflowApprovers'));
        $this->hasMany('employee_id', 'MmpiWorkflows', 'requestor_id', array('alias' => 'MmpiWorkflows'));
        $this->hasMany('employee_id', 'MmpiWorkflows', 'creator_id', array('alias' => 'MmpiWorkflows'));
        $this->belongsTo('location_id', 'RefLocation', 'location_id', array('alias' => 'RefLocation'));
        $this->belongsTo('designation_id', 'RefDesignation', 'designation_id', array('alias' => 'RefDesignation'));
        $this->belongsTo('department_id', 'RefDepartment', 'department_id', array('alias' => 'RefDepartment'));
        $this->belongsTo('status_id', 'RefStatus', 'status_id', array('alias' => 'RefStatus'));
    }

    /**
     * Allows to register records
     * Author: Gene
     * @param mixed $parameters
     * @return array("status"=>0,"message"=>$err_msg,"data"=>[]);
     */
    public static function register($data){
        $validation = new TORegistrationValidation();
        // 1. VALIDATE DATA
        $messages = $validation->validate($data);
        if (count($messages)) {
            $err_msg = "";
            foreach ($messages as $message) {
                $err_msg .= $message.'<br>';
            }
            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }

        // 2. CREATE INSTANCE
        $record = new MmpiTableOfOrganization();
        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"message"=>"Saved Succesfully","data"=>$record);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiTableOfOrganization[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiTableOfOrganization
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Author : Gene
     *
    **/
    
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
        $this->date_updated = CURR_DATETIME;

        $this->validate(new Uniqueness(
            array(
                "field"   => "employee_id",
                "message" => "Employee is already registered"
            )
        ));
       

        return !$this->validationHasFailed();
    }
    
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
    public function getList($searchKey)
    {
        if($searchKey == "")
        {
            $phql = "SELECT 
                     to.date_updated,to.employee_id,to.last_name,to.firs_name,to.email,
                     rds.name as designation,
                     rd.name as department,
                     rl.name as location
                     FROM MmpiTableOfOrganization to
                     LEFT JOIN RefDesignation rds ON rds.designation_id = to.designation_id
                     LEFT JOIN RefDepartment rd ON rd.department_id = to.department_id
                     LEFT JOIN RefLocation rl ON rl.location_id = to.location_id
                     where (to.employee_id <> 0 and to.status_id <> 3)
                     ORDER BY to.date_updated ASC
                    ";
        }
        else
        {
            $phql = "SELECT 
                     to.date_updated,to.employee_id,to.last_name,to.firs_name,to.email,
                     rds.name as designation,
                     rd.name as department,
                     rl.name as location,
                     CONCAT(lower(firs_name), ' ', lower(last_name)) as whole_name 
                     FROM MmpiTableOfOrganization to
                     LEFT JOIN RefDesignation rds ON rds.designation_id = to.designation_id
                     LEFT JOIN RefDepartment rd ON rd.department_id = to.department_id
                     LEFT JOIN RefLocation rl ON rl.location_id = to.location_id
                     where (lower(to.employee_id) like lower('%$searchKey%') or 
                            lower(to.last_name) like lower('%$searchKey%') or
                            lower(to.firs_name) like lower('%$searchKey%') or
                            lower(to.email) like lower('%$searchKey%') or    
                            lower(rds.name) like lower('%$searchKey%') or
                            lower(rd.name) like lower('%$searchKey%') or
                            lower(rl.name )like lower('%$searchKey%')  or
			    CONCAT(lower(firs_name), ' ', lower(last_name))  like lower('%$searchKey%') )   
                                and (to.employee_id <> 0 and to.status_id <> 3)
                     ORDER BY to.date_updated ASC
                    ";            
        }
        $data = $this->modelsManager->executeQuery($phql);   
        return $data;
    }

    
    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mmpi_table_of_organization';
    }

}
