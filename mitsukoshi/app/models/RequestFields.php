<?php

class RequestFields extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $field_id;

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var string
     */
    public $value;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('request_id', 'EmployeRequest', 'request_id', array('alias' => 'EmployeRequest'));
        $this->belongsTo('field_id', 'TableFields', 'field_id', array('alias' => 'TableFields'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'request_fields';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestFields[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestFields
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    public static function register($data){
        $record = new RequestFields();
        $helper = new Helper();
        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"message"=>"Saved Succesfully","data"=>$record);
    }
  
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
}
