<?php

class RefGroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $group_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var integer
     */
    public $status_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('group_id', 'MmpiGroupMatrix', 'group_id', array('alias' => 'MmpiGroupMatrix'));
        $this->belongsTo('status_id', 'RefStatus', 'status_id', array('alias' => 'RefStatus'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_group';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefGroup[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefGroup
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeValidationOnCreate(){
        $this->date_created = CURR_DATETIME;
        $this->date_updated = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate(){
        $this->date_updated = CURR_DATETIME;
    }
    public function getList($searchKey){
        if($searchKey == "")
        {
			$phql  = "SELECT 
                      rg.group_id,COUNT(m.group_id) as total_members,rg.name,rg.date_updated 
                      FROM RefGroup rg  
                      LEFT JOIN Members m ON m.group_id = rg.group_id
					  WHERE rg.status_id = 1
                      GROUP BY rg.group_id
                      ORDER BY rg.date_updated ASC
                     ";
        }
        else
        {
             $phql  = "SELECT 
                      rg.group_id,COUNT(m.group_id) as total_members,rg.name,rg.date_updated 
                      FROM RefGroup rg  
                      LEFT JOIN Members m ON m.group_id = rg.group_id 
                      LEFT JOIN MmpiTableOfOrganization tog ON tog.employee_id = m.member_id
                      where (lower(rg.name) like lower('%$searchKey%') or
                            lower(tog.employee_id) like lower('%$searchKey%') or 
                 	    lower(tog.last_name) like lower('%$searchKey%') or
                            lower(tog.firs_name) like lower('%$searchKey%'))    
						AND rg.status_id = 1
                      GROUP BY rg.group_id
                      ORDER BY rg.date_updated DESC
                     ";           
        }
        $data = $this->modelsManager->executeQuery($phql);
        return $data;
    }

    public function getDetails($group_id){
        $phql  = "SELECT rg.group_id,m.member_id,
                  rg.name,rg.date_updated,rg.date_created,
                  to.firs_name,to.last_name,
                  rd.name as designation
                  FROM RefGroup rg  
                  LEFT JOIN Members m ON m.group_id = rg.group_id 
                  INNER JOIN MmpiTableOfOrganization to ON to.employee_id = m.member_id
                  INNER JOIN RefDesignation rd ON rd.designation_id = to.designation_id
                  WHERE rg.group_id = ?1 AND rg.status_id = 1
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$group_id));
        return $data;
    }
}
