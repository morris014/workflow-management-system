<?php

class RefStatus extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $status_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('status_id', 'MmpiTableOfOrganization', 'status_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->hasMany('status_id', 'MmpiWorkflowActivities', 'status_id', array('alias' => 'MmpiWorkflowActivities'));
        $this->hasMany('status_id', 'RefAccessType', 'status_id', array('alias' => 'RefAccessType'));
        $this->hasMany('status_id', 'RefDataType', 'status_id', array('alias' => 'RefDataType'));
        $this->hasMany('status_id', 'RefDepartment', 'status_id', array('alias' => 'RefDepartment'));
        $this->hasMany('status_id', 'RefDesignation', 'status_id', array('alias' => 'RefDesignation'));
        $this->hasMany('status_id', 'RefDropdown', 'status_id', array('alias' => 'RefDropdown'));
        $this->hasMany('status_id', 'RefGroup', 'status_id', array('alias' => 'RefGroup'));
        $this->hasMany('status_id', 'RefLocation', 'status_id', array('alias' => 'RefLocation'));
        $this->hasMany('status_id', 'RefPriority', 'status_id', array('alias' => 'RefPriority'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_status';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefStatus[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefStatus
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
