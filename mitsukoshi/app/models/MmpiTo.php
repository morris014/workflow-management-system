<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class MmpiTo extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var int
     */
    public $designation_id;

    /**
     *
     * @var int
     */
    public $immediate_superior;

    /**
     *
     * @var int
     */
    public $dep_manager;

    /**
     *
     * @var int
     */
    public $executive_officer;

    
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
     
    }

    /**
     * Allows to register records
     * Author: Gene
     * @param mixed $parameters
     * @return array("status"=>0,"message"=>$err_msg,"data"=>[]);
  

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiTableOfOrganization[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MmpiTableOfOrganization
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Author : Gene
     *
    **/
    
    
    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mmpi_to';
    }

}
