<?php

class RequestActivities extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var string
     */
    public $approver_id;

    /**
     *
     * @var double
     */
    public $tat;

    /**
     *
     * @var integer
     */
    public $sequence;

    /**
     *
     * @var string
     */
    public $date_reequest;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $comments;

    /**
     *
     * @var string
     */
    public $date_started;

    /**
     *
     * @var string
     */
    public $date_ended;

    /**
     *
     * @var string
     */
    public $activity_name;

    /**
     * Initialize method for model.
     */
      public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
    public function initialize()
    {
        $this->belongsTo('status', 'ApprovalStatus', 'id', array('alias' => 'ApprovalStatus'));
        $this->belongsTo('request_id', 'EmployeRequest', 'request_id', array('alias' => 'EmployeRequest'));
        $this->belongsTo('approver_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'request_activities';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestActivities[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestActivities
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    
    public static function register($data)
    {
        $record = new RequestActivities();
        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"message"=>"Saved Succesfully","data"=>$record);
    }
    public function getRequestActivitiesForComplete($employee_id,$request_id,$member){
        
        $phql = "SELECT * FROM RequestActivities ra where date_started is not null AND date_approved is null AND (approver_id = ?1 $member) AND request_id = ?2 AND (status = 1 OR status = 2)";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$employee_id,2=>$request_id));
        return $data;   
    }
    public function getRequestActivitiesDetails($request_id){
        
        $phql = "SELECT rg.name as groupname,  ra.approver_id,ra.date_updated,ra.filename,ra.path,ra.activity_name,ra.date_approved,ar.name as status_name,ra.date_started,ra.tat,ra.status,
                        tos.firs_name,tos.last_name,tos.email,
                        rd.name as designation,ra.comments
                 FROM RequestActivities ra
                 LEFT JOIN MmpiTableOfOrganization tos ON tos.employee_id = ra.approver_id
                 LEFT JOIN ApprovalStatus ar ON ar.id = ra.status
                 LEFT JOIN RefDesignation rd ON rd.designation_id = tos.designation_id
                 LEFT JOIN RefGroup rg ON rg.group_id = SUBSTR(ra.approver_id,6,length(ra.approver_id))
                 WHERE ra.request_id = ?1
                 ORDER BY ra.sequence ASC
                 
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        return $data;   
    }

    public function getStartedActivities($request_id){
        
        $phql = "SELECT ra.approver_id ,rg.name as groupname, ra.filename,ra.path,ra.activity_name,ra.date_approved,ar.name as status_name,
                        ra.date_started,ra.tat,ra.status,to.firs_name,to.last_name,to.email,
                        rd.name as designation,ra.comments
                 FROM RequestActivities ra
                 LEFT JOIN MmpiTableOfOrganization to ON to.employee_id = ra.approver_id
                 LEFT JOIN ApprovalStatus ar ON ar.id = ra.status
                 LEFT JOIN RefDesignation rd ON rd.designation_id = to.designation_id
                 LEFT JOIN RefGroup rg ON rg.group_id = SUBSTR(ra.approver_id,6,length(ra.approver_id))
                 WHERE ra.request_id = ?1 AND (ra.status = 1 or ra.status = 2) AND ra.date_started is not null
                 ORDER BY ra.sequence ASC
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        return $data;   
    }
}
