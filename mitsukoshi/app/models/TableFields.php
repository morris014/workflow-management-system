<?php

class TableFields extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $field_id;

    /**
     *
     * @var string
     */
    public $label;

    /**
     *
     * @var string
     */
    public $required;

    /**
     *
     * @var integer
     */
    public $data_type_id;

    /**
     *
     * @var integer
     */
    public $table_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('data_type_id', 'RefDataType', 'data_type_id', array('alias' => 'RefDataType'));
        $this->belongsTo('table_id', 'WorkflowTable', 'table_id', array('alias' => 'WorkflowTable'));
        $this->hasMany('field_id', 'RefDropdown', 'field_id', array('alias' => 'RefDropdown'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return TableFields[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return TableFields
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'table_fields';
    }

}
