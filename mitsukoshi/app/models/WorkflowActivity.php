<?php

class WorkflowActivity extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $activity_id;

    /**
     *
     * @var integer
     */
    public $workflow_id;

    /**
     *
     * @var string
     */
    public $activity_name;

    /**
     *
     * @var double
     */
    public $tat;

    /**
     *
     * @var integer
     */
    public $sequence;

    /**
     *
     * @var string
     */
    public $employee_id;

    /**
     *
     * @var integer
     */
    public $group_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('employee_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->belongsTo('workflow_id', 'RefWorkflow', 'workflow_id', array('alias' => 'RefWorkflow'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'workflow_activity';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowActivity[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WorkflowActivity
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    public function getDetails($workflow_id)
    {
        $phql = "SELECT wa.activity_id,wa.tat,wa.employee_id,wa.activity_name,
                 rd.name as designation,tos.firs_name,tos.last_name,tos.email
                 FROM WorkflowActivity wa 
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = wa.employee_id
                 INNER JOIN RefDesignation rd ON tos.designation_id = rd.designation_id
                 WHERE wa.workflow_id = ?1
                 ORDER BY wa.sequence ASC
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        return $data;
    }
    public function getDetailsWorkflow($workflow_id)
    {
        $phql = "SELECT rg.name as groupname,wa.group_id,wa.activity_id,wa.tat,wa.employee_id,wa.activity_name,
                 rd.name as designation,tos.firs_name,tos.last_name,tos.email
                 FROM WorkflowActivity wa 
                 LEFT JOIN MmpiTableOfOrganization tos ON tos.employee_id = wa.employee_id
                 LEFT JOIN RefDesignation rd ON tos.designation_id = rd.designation_id
                 LEFT JOIN RefGroup rg ON rg.group_id = wa.group_id
                 WHERE wa.workflow_id = ?1
                 ORDER BY wa.sequence ASC
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        return $data;
    }
}
