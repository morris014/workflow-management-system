<?php

class RefWorkflow extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $workflow_id;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $category_id;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $creator_id;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;
    
     public $department_id;

    /**
     * Initialize method for model.
     */
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
    public function initialize()
    {
        $this->hasMany('workflow_id', 'EmployeRequest', 'workflow_id', array('alias' => 'EmployeRequest'));
        $this->hasMany('workflow_id', 'WorkflowApprover', 'workflow_id', array('alias' => 'WorkflowApprover'));
        $this->hasMany('workflow_id', 'WorkflowActivity', 'workflow_id', array('alias' => 'WorkflowActivity'));
        $this->hasMany('workflow_id', 'WorkflowTable', 'workflow_id', array('alias' => 'WorkflowTable'));
        $this->belongsTo('status', 'RefStatus', 'status_id', array('alias' => 'RefStatus'));
        $this->belongsTo('category_id', 'RefCategory', 'category_id', array('alias' => 'RefCategory'));
        $this->belongsTo('department_id', 'RefDepartment', 'department_id', array('alias' => 'RefDepartment'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_workflow';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefWorkflow[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefWorkflow
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getByDepartment($department_id,$user_dep){
        
        $phql = "SELECT 
                 rf.workflow_id,rf.name,rf.description,rf.category_id,rc.name as category,rf.status 
                 FROM RefWorkflow rf
                 INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 WHERE rf.department_id = ?1 and ((department_access like '%$user_dep%' or department_access like '%,$user_dep' or department_access like '$user_dep,%' or department_access like '%,$user_dep,%') or department_access is null)
                 ";
        //var_dump($phql);die();
       $data = $this->modelsManager->executeQuery($phql,array(1=>$department_id));
       return $data;          
    }
    public function getByDepartmentSuper($department_id){
        
        $phql = "SELECT 
                 rf.workflow_id,rf.name,rf.description,rf.category_id,rc.name as category,rf.status 
                 FROM RefWorkflow rf
                 INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 WHERE rf.department_id = ?1
                 ";
        //var_dump($phql);die();
       $data = $this->modelsManager->executeQuery($phql,array(1=>$department_id));
       return $data;          
    }    
    public function getByWorkflowId($workflow_id){
        
        $phql = "SELECT 
                 rf.workflow_id,rf.name,rf.description,rf.category_id,rc.name as category
                 FROM RefWorkflow rf
                  INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 WHERE rf.workflow_id = ?1
                 ";
       $data = $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
       return $data;          
    }

    public function getWorkflow($searchKey)
    {
        if($searchKey == "")
        {
            $phql = "SELECT 
                        SUM(if(er.status = 2,1,0)) AS completed,
                        SUM(if(er.status = 1,1,0)) AS in_progress,
                        SUM(if(er.status = 3,1,0)) AS rejected,
                        rf.workflow_id,
                        rf.date_created,rf.ref_no,rf.name,rc.name as category,rf.status FROM 
                     RefWorkflow rf
                     LEFT JOIN  EmployeRequest er ON er.workflow_id = rf.workflow_id
                     INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                     
                     GROUP BY rf.workflow_id
                     ";
        }
        else
        {
             $phql = "SELECT SUM(if(er.status = 2,1,0)) AS completed,SUM(if(er.status = 1,1,0)) AS in_progress,SUM(if(er.status = 3,1,0)) AS rejected,rf.date_created,rf.ref_no,rf.name,rc.name as category,rf.status FROM 
                      RefWorkflow rf
                     LEFT JOIN  EmployeRequest er ON er.workflow_id = rf.workflow_id
                     INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                     where rf.status <> 3 and ((lower(rf.ref_no) like lower('%$searchKey%') or
                            lower(rf.name) like lower('%$searchKey%') or
                            lower(rc.name) like lower('%$searchKey%')))
                     GROUP BY rf.workflow_id
                     ";           
        }
        $data = $this->modelsManager->executeQuery($phql);
        return $data;  

    }

    public function deleteRelated($workflow_id){
        $phql = 'DELETE FROM WorkflowActivity where workflow_id = ?1';
        $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        $phql = 'DELETE FROM WorkflowApprover where workflow_id = ?1';
        $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        $tables = WorkflowTable::find(array('conditions'=>'workflow_id = ?1','bind'=>array(1=>$workflow_id)));
        foreach ($tables as $x) {

            foreach ($x->TableFields as $y) {
                if($y->data_type_id == 3){
                    foreach($y->RefDropdown as $t){
                        $t->delete();
                    }
                }
                $y->delete();
            }
            $x->delete();
        }
        $this->modelsManager->executeQuery($phql,array(1=>$workflow_id));
        


    }
}
