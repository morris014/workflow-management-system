<?php

class RefPosition extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $position_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var integer
     */
    public $status_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('position_id', 'MmpiTableOfOrganization', 'position_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->belongsTo('status_id', 'RefStatus', 'status_id', array('alias' => 'RefStatus'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_position';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefPosition[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefPosition
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
