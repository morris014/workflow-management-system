<?php

class RequestApproval extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var string
     */
    public $approver_id;

    /**
     *
     * @var double
     */
    public $tat;

    /**
     *
     * @var integer
     */
    public $sequence;

    /**
     *
     * @var string
     */
    public $date_request;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
    public function initialize()
    {
        $this->belongsTo('status', 'ApprovalStatus', 'id', array('alias' => 'ApprovalStatus'));
        $this->belongsTo('request_id', 'EmployeRequest', 'request_id', array('alias' => 'EmployeRequest'));
        $this->belongsTo('approver_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'request_approval';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestApproval[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RequestApproval
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function register($data)
    {
        $record = new RequestApproval();
        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"message"=>"Saved Succesfully","data"=>$record);
    }

    public function getApproval($employee_id,$overdue = '',$searchKey,$sort=0,$member_search_approval,$member_search){
        $sortString='';
		switch($sort)
		{
			case '0': $sortString='er.date_request DESC';
				break;
			case '1': $sortString='rf.priority, er.date_request DESC';
				break;
			case '2': $sortString='er.ref_no, er.date_request DESC';
				break;
			case '3': $sortString='rf.category_id, er.date_request DESC';
				break;
			case '4': $sortString='rf.name, er.date_request DESC';
				break;
			case '5': $sortString='TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400 DESC';
				break;
			 
		}
		

        if($overdue == 'overdue'){
            $conditions_ra.= ' AND ((ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) -'
                    . '(getHolidayCount(ra.date_started)) )'
                    . '> ra.tat)';
            $conditions_raa.= ' AND ((ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),raa.date_started))/86400,2) -'
                    . '(getHolidayCount(raa.date_started)) )'
                    . '> raa.tat)';
        }
        if($overdue == 'in-progress'){
            $conditions_ra.= ' AND ((ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) - '
                    .'(getHolidayCount(ra.date_started)) )'
                    . '<= ra.tat) ';
            $conditions_raa.= ' AND ((ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),raa.date_started))/86400,2) - '
                    . '(getHolidayCount(raa.date_started)) )'
                    . '<= raa.tat)';
        }        

        if($searchKey != "")
        {
            $conditions_ra = '';
            $conditions_raa = '';
        }
        $phql = "SELECT er.workflow_id, ra.id as approval_id,raa.id as activity_id,er.request_id,er.ref_no,rf.name,er.date_request,er.status,
                ra.date_started as app_date_start,raa.date_started as act_date_start,raa.activity_name,
                ra.status as approval_status,raa.status as act_status FROM EmployeRequest er
                 LEFT JOIN RefWorkflow rf ON rf.workflow_id = er.workflow_id
                 LEFT JOIN RequestApproval ra ON ra.request_id = er.request_id
                 LEFT JOIN RequestActivities raa ON raa.request_id = er.request_id
                 LEFT JOIN RefCategory rc ON rc.category_id = rf.category_id
                 WHERE ((lower(er.ref_no) like lower('%$searchKey%') or lower(rf.name) like lower('%$searchKey%')  or lower(rc.name) like lower('%$searchKey%')) AND (ra.approver_id = ?1 $member_search_approval) AND ra.date_started is not null AND ra.status = 1 $conditions_ra) OR ((raa.approver_id = ?1 $member_search) AND raa.date_started is not null AND raa.status = 1 $conditions_raa)
                     
                 GROUP BY er.request_id
                 ORDER BY $sortString
                 ";

        $data = $this->modelsManager->executeQuery($phql,array(1=>$employee_id));
        
        return $data;
    }

    public function getRequestApprovalDetails($request_id){
        $phql = "SELECT ra.filename,ra.path,ra.date_approved,ar.name as status_name,ra.date_started,ra.tat,ra.status,
                        tos.firs_name,tos.last_name,tos.email,ra.comments,rd.name as designation
                 FROM RequestApproval ra
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = ra.approver_id
                 INNER JOIN ApprovalStatus ar ON ar.id = ra.status
                 INNER JOIN RefDesignation rd ON rd.designation_id = tos.designation_id
                 WHERE ra.request_id = ?1
                 ORDER BY ra.sequence ASC
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        return $data;   
    }

    public function getStartedApproval($request_id)
    {
        
        $phql = "SELECT ra.filename,ra.path,ra.date_approved,ar.name as status_name,
                        ra.date_started,ra.tat,ra.status,tos.firs_name,tos.last_name,tos.email,ra.comments,
                        rd.name as designation
                 FROM RequestApproval ra
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = ra.approver_id
                 INNER JOIN ApprovalStatus ar ON ar.id = ra.status
                 INNER JOIN RefDesignation rd ON rd.designation_id = tos.designation_id
                 WHERE ra.request_id = ?1 AND (ra.status= 1 or ra.status = 2) AND ra.date_started is not null
                 ORDER BY ra.sequence ASC
                 
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        return $data;
    }

}
