<?php

class RefHoliday extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $hid;

    /**
     *
     * @var string
     */
    public $holiday_name;
    
        /**
     *
     * @var string
     */
    public $holiday_date;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('hid', 'RefWorkflow', 'hid', array('alias' => 'RefWorkflow'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_holiday';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefHoliday[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefHoliday
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
