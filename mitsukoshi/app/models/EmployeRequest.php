<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class EmployeRequest extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var string
     */
    public $requestor_id;

    /**
     *
     * @var integer
     */
    public $workflow_id;

    /**
     *
     * @var string
     */
    public $date_request;

    /**
     *
     * @var string
     */
    public $date_created;

    /**
     *
     * @var string
     */
    public $date_updated;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $ref_no;

    /**
     *
     * @var string
     */
    public $comments;

    /**
     * Initialize method for model.
     */
    public function beforeValidationOnCreate()
    {
        $this->date_created = CURR_DATETIME;
    }
    public function beforeValidationOnUpdate()
    {
        $this->date_updated = CURR_DATETIME;
    }
    public function initialize()
    {
        $this->hasMany('request_id', 'RequestApproval', 'request_id', array('alias' => 'RequestApproval'));
        $this->hasMany('request_id', 'RequestActivities', 'request_id', array('alias' => 'RequestActivities'));
        $this->hasMany('request_id', 'RequestFields', 'request_id', array('alias' => 'RequestFields'));
        $this->belongsTo('requestor_id', 'MmpiTableOfOrganization', 'employee_id', array('alias' => 'MmpiTableOfOrganization'));
        $this->belongsTo('workflow_id', 'RefWorkflow', 'workflow_id', array('alias' => 'RefWorkflow'));
        $this->belongsTo('status', 'RequestStatus', 'id', array('alias' => 'RequestStatus'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return EmployeRequest[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return EmployeRequest
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getRequest($searchKey,$userId)
    {
        $phql ="SELECT er.request_id,er.requestor_id,er.workflow_id,er.total_completed,er.total_tasks,er.tat,
                       mto.email,mto.firs_name,mto.last_name,er.ref_no,er.date_request,
                       rd.name as designation,rw.name as request_type,rc.name as category,
                       rs.name as status_name,er.status
                FROM EmployeRequest er
                INNER JOIN RefWorkflow rw ON rw.workflow_id = er.workflow_id
                INNER JOIN RefDepartment rc ON rc.department_id = rw.department_id
                INNER JOIN MmpiTableOfOrganization mto ON mto.employee_id = er.requestor_id
                INNER JOIN RequestStatus rs ON rs.id = er.status
                INNER JOIN RefDesignation rd ON rd.designation_id = mto.designation_id 
                WHERE ( lower(er.ref_no) like lower('%$searchKey%') or
                        lower(rw.name) like lower('%$searchKey%')  or
                        lower(rc.name) like lower('%$searchKey%')
                        ) and mto.employee_id = '$userId'
               ";
        $data = $this->modelsManager->executeQuery($phql);
        return $data;  

    }
    
    public static function register($data){
        $record = new EmployeRequest();
        $helper = new Helper();

        if (!$record->save($data)){
            $err_msg = "";
            foreach ($record->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        $x  = EmployeRequest::findFirstByRequestId($record->request_id);
        $x->ref_no =  DEPT_CODE.'-'.$helper->generateId().$record->request_id;
        if (!$x->save()){
            $err_msg = "";
            foreach ($x->getMessages() as $value) {
                $err_msg .= $value."<br>";
            }

            return array("status"=>0,"message"=>$err_msg,"data"=>[]);
        }
        return array("status"=>1,"message"=>"Saved Succesfully","data"=>$record);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'employe_request';
    }

    public function getRequestDetails($request_id, $sort=0)
    {
		$sortString='';
		switch($sort)
		{
			case '0': $sortString='er.date_request DESC';
				break;
			case '1': $sortString='rw.priority DESC, er.date_request DESC';
				break;
			case '2': $sortString='er.ref_no, er.date_request DESC';
				break;
			case '3': $sortString='rw.category_id, er.date_request DESC';
				break;
			case '4': $sortString='rw.name, er.date_request DESC';
				break;
			case '5': $sortString='TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400';
				break;
			 
		}
   
        $phql ="SELECT er.workflow_id, er.total_completed,er.total_tasks,er.tat,
                       mto.email,mto.firs_name,mto.last_name,er.ref_no,er.date_request,
                       rd.name as designation,rw.name as request_type,rc.name as category,
                       rs.name as status_name,er.status,er.request_id
                FROM EmployeRequest er
                INNER JOIN RefWorkflow rw ON rw.workflow_id = er.workflow_id
                INNER JOIN RefDepartment rc ON rc.department_id = rw.department_id
                INNER JOIN MmpiTableOfOrganization mto ON mto.employee_id = er.requestor_id
                INNER JOIN RequestStatus rs ON rs.id = er.status
                INNER JOIN RefDesignation rd ON rd.designation_id = mto.designation_id 
                WHERE er.request_id = ?1
                ORDER BY $sortString
				LIMIT 1
               ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        return (sizeof($data) > 0) ? $data[0] : [];
    }
	
	public function getRequestDetailsRequest($conditions='', $sort=0)
    {
		$sortString='';
		switch($sort)
		{
			case '0': $sortString='er.date_request DESC';
				break;
			case '1': $sortString='rw.priority DESC, er.date_request DESC';
				break;
			case '2': $sortString='er.ref_no DESC, er.date_request ';
				break;
			case '3': $sortString='rw.category_id, er.date_request DESC';
				break;
			case '4': $sortString='rw.name, er.date_request DESC';
				break;
			case '5': $sortString='TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400';
				break;
			 
		}
   
        $phql ="SELECT er.total_completed,er.total_tasks,er.tat,
                       mto.email,mto.firs_name,mto.last_name,er.ref_no,er.date_request,
                       rd.name as designation,rw.name as request_type,rc.name as category,
                       rs.name as status_name,er.status,er.request_id
                FROM EmployeRequest er
                INNER JOIN RefWorkflow rw ON rw.workflow_id = er.workflow_id
                INNER JOIN RefDepartment rc ON rc.department_id = rw.department_id
                INNER JOIN MmpiTableOfOrganization mto ON mto.employee_id = er.requestor_id
                INNER JOIN RequestStatus rs ON rs.id = er.status
                INNER JOIN RefDesignation rd ON rd.designation_id = mto.designation_id 
                WHERE $conditions
                ORDER BY $sortString
               ";
		//var_dump($phql); die;
        $data = $this->modelsManager->executeQuery($phql,array(1=>$request_id));
        
        return $data;
    }

    // for testing var_dump(EmployeRequest::getRawReports()[0]->total_overdue);die;
    public function getRawReports(){
        $sql = "SELECT 
                SUM(
                IF(((IF(DATE_FORMAT(date_request,'%Y-%m-%d 17:00:00') > date_request,time_to_sec(TIMEDIFF(DATE_FORMAT(date_request,'%Y-%m-%d 17:00:00'),date_request))/3600,9) +IF(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') > NOW(),time_to_sec(TIMEDIFF(NOW(),DATE_FORMAT(date_request,'%Y-%m-%d 08:00:00')))/3600,9)
                +(DATEDIFF(now(),date_request) - 1) * 9)/9)>tat AND er.status = 1 ,1,0)) as total_overdue,
                SUM(if(er.status = 2,1,0)) AS completed,
                SUM(IF(((IF(DATE_FORMAT(date_request,'%Y-%m-%d 17:00:00') > date_request,time_to_sec(TIMEDIFF(DATE_FORMAT(date_request,'%Y-%m-%d 17:00:00'),date_request))/3600,9) +IF(DATE_FORMAT(NOW(),'%Y-%m-%d 17:00:00') > NOW(),time_to_sec(TIMEDIFF(NOW(),DATE_FORMAT(date_request,'%Y-%m-%d 08:00:00')))/3600,9)
                +(DATEDIFF(now(),date_request) - 1) * 9)/9)<=tat AND er.status = 1 ,1,0)) as in_progress,
                SUM(if(er.status = 3,1,0)) AS rejected,
                SUM(if(er.status = 1,1,0)) as total,
                SUM(if(er.date_ended is null, 0, DATEDIFF(er.date_ended,er.date_request))) as completion
                FROM employe_request er";
        $result = new EmployeRequest();

        // Execute the query
        return new Resultset(null, $result, $result->getReadConnection()->query($sql));
    }

    public function getReportsByTask($requestor_id,$member){
 // A raw SQL statement

        $sql   = "SELECT sum(r.total) as total,sum(r.completed) as completed ,sum(r.in_progress) as in_progress,sum(r.rejected) as rejected,sum(r.overdue) as overdue FROM (

        SELECT SUM(if(ra.status = 1,1,0)) as total,
                                SUM(if(ra.status = 2,1,0)) AS completed,
                                SUM(if(ra.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) - 
                                
                                (getHolidayCount(ra.date_started)))
                                <= ra.tat,1,0)) AS in_progress,
                                SUM(if(ra.status = 3,1,0)) AS rejected,
                                SUM(if(ra.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) -
                                (getHolidayCount(ra.date_started)))
                                > ra.tat,1,0)) as overdue,
                                er.request_id,er.ref_no,rf.name,er.date_request,er.status 
                                FROM employe_request er
                         LEFT JOIN ref_workflow rf ON rf.workflow_id = er.workflow_id
                         LEFT JOIN request_approval ra ON ra.request_id = er.request_id
                         WHERE ((ra.approver_id = '$requestor_id' $member)  AND ra.date_started is not null AND ra.status = 1)
                   UNION
                   SELECT SUM(if(ra.status = 1,1,0)) as total,
                                SUM(if(ra.status = 2,1,0)) AS completed,
                                SUM(if(ra.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) - 
                                (getHolidayCount(ra.date_started)))
                                <= ra.tat,1,0)) AS in_progress,
                                SUM(if(ra.status = 3,1,0)) AS rejected,
                                SUM(if(ra.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),ra.date_started))/86400,2) -
                                (getHolidayCount(ra.date_started)))                                
                                > ra.tat,1,0)) as overdue,
                                er.request_id,er.ref_no,rf.name,er.date_request,er.status 
                                FROM employe_request er
                         LEFT JOIN ref_workflow rf ON rf.workflow_id = er.workflow_id
                         LEFT JOIN request_activities ra ON ra.request_id = er.request_id
                         WHERE ((ra.approver_id = '$requestor_id' $member)  AND ra.date_started is not null AND ra.status = 1)) as r
        WHERE r.total is not null and r.completed is not null and r.rejected is not null";

        // Base model
        $robot = new EmployeRequest();

        // Execute the query
        return new Resultset(null, $robot, $robot->getReadConnection()->query($sql));
    }
    public function getReportsByRequestorId($requestor_id){
        
        $phql = "SELECT SUM(if(er.status = 1,1,0)) as total,
                        SUM(if(er.status = 2,1,0)) AS completed,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2)  -
                        (getHolidayCount(er.date_request)) )
                        <= er.tat+1,1,0)) AS in_progress,
                        SUM(if(er.status = 3,1,0)) AS rejected,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - 
                        (getHolidayCount(er.date_request)) )
                         > er.tat+1,1,0)) as overdue,
                        SUM(if(er.date_ended is null, 0, DATEDIFF(er.date_ended,er.date_request))) as completion
                 FROM EmployeRequest er WHERE er.requestor_id = ?1 
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$requestor_id));
        return $data;  
    }
    public function getInitiator()
    {
        $phql = "SELECT tos.employee_id,CONCAT(tos.firs_name,' ',tos.last_name) as fullname 
                 FROM MmpiTableOfOrganization tos
                 INNER JOIN EmployeRequest er on tos.employee_id = er.requestor_id
                 GROUP BY tos.employee_id";
        $data = $this->modelsManager->executeQuery($phql);
        return $data;           
    }
    public function getReportsDetails($from = '',$to ='',$status = 0,$initiator_id = "0",$department_id = 0,$workflow_id = 0)
    {   
        $conditions = '';
        $binds = array();
        if($department_id == 0 && $from == '' && $to == '' && status == 0 && $initiator_id=="0" && $department_id==0 && $workflow_id==0)
        {
            //$conditions.=' WHERE date(er.date_request)  >= DATE(NOW()) - INTERVAL 7 DAY';   
        }        
        if($from != ''){
           
            $from = $this->helper->convertToDate($from,'Y-m-d');
            $conditions.=($conditions == '') ? " WHERE date(er.date_request) >= '$from'" : "AND date(er.date_request) >= '$from' ";
            $binds[1] = $from;
        }

        if($to != ''){
            $to = $this->helper->convertToDate($to,'Y-m-d');
            $conditions.=($conditions == '') ? " WHERE date(er.date_request) <= '$to'" : "AND date(er.date_request) <= '$to' ";
            $binds[2] = $to;
        }

        if($status != 0){
            if($status == 4){
                $conditions.=($conditions == '') ?  " WHERE er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - "
                        .'(getHolidayCount(er.date_request)) )'
                        . "> er.tat+1" : 
                    
                    
                    " AND (er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - "
                        .'(getHolidayCount(er.date_request)) )'
                        . "> er.tat+1";   
            }else{
                $conditions.=($conditions == '') ? " WHERE er.status = '$status'" : " AND er.status = '$status' ";
                $binds[3] = $status;    
            }         
        }
        if($initiator_id != "0"){
            $conditions.=($conditions == '') ? " WHERE er.requestor_id = '$initiator_id'" : " AND er.requestor_id = '$initiator_id'";
            $binds[4] = $initiator_id; 
        }

        if($department_id != 0){
            $conditions.=($conditions == '') ? " WHERE rf.department_id = $department_id" : " AND rf.department_id = $department_id ";
            $binds[5] = $department_id; 
        }
        
        if($workflow_id != 0){
            $conditions.=($conditions == '') ? " WHERE  rf.workflow_id = $workflow_id" : " AND  rf.workflow_id = $workflow_id ";
            $binds[6] = $workflow_id; 
        }
        
        $phql = "SELECT rf.date_created,rf.ref_no,rf.name as workflow,rc.name as category,
                        er.date_request,tos.firs_name,tos.last_name ,er.ref_no,er.tat,
                        rs.name as status_name,er.status,er.request_id
                 FROM EmployeRequest er
                 LEFT JOIN  RefWorkflow rf ON rf.workflow_id = er.workflow_id
                 INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = er.requestor_id
                 INNER JOIN RequestStatus rs ON rs.id = er.status
                 $conditions
                 GROUP BY er.request_id
                 ORDER BY er.date_request DESC
                 ";
     
        $data = $this->modelsManager->executeQuery($phql,$binds);
       //var_dump($phql);die();
        return $data;  
    }  
    public function getReports($from = '',$to ='',$status = 0,$initiator_id = "0",$department_id = 0,$workflow_id = 0){
        $conditions = '';
        $binds = array();

        if($department_id == 0 && $from == '' && $to == '' && status == 0 && $initiator_id=="0" && $department_id==0 && $workflow_id==0)
        {
            //$conditions.=' WHERE date(er.date_request)  >= DATE(NOW()) - INTERVAL 7 DAY';   
        }
        
        //if($department_id > 0)
        //{
        //    $conditions.=' WHERE rc.department_id = ?1';
        //    $binds[1] = $department_id;
        //}
        
        if($from != ''){
           
            $from = $this->helper->convertToDate($from,'Y-m-d');
            $conditions.=($conditions == '') ? " WHERE date(er.date_request) >= '$from'" : "AND date(er.date_request) >= '$from' ";
            $binds[2] = $from;
        }

        if($to != ''){
            $to = $this->helper->convertToDate($to,'Y-m-d');
            $conditions.=($conditions == '') ? " WHERE date(er.date_request) <= '$to'" : "AND date(er.date_request) <= '$to' ";
            $binds[3] = $to;
        }

        if($status != 0){
            if($status == 4){
                $conditions.=($conditions == '') ?  "WHERE er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) -"
                        .'(getHolidayCount(er.date_request)) )'
                        . "> er.tat+1" 
                        
                        
                        : "AND (er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - "
                        .'(getHolidayCount(er.date_request)) )'                   
                        . "> er.tat+1) ";   
            }else{
                $conditions.=($conditions == '') ? " WHERE er.status = $status" : " AND er.status = $status ";
                $binds[4] = $status;    
            }         
        }
        if($initiator_id != "0"){
            
            $conditions.=($conditions == '') ? " WHERE er.requestor_id = '$initiator_id'" : " AND er.requestor_id = '$initiator_id'";
            $binds[5] = $initiator_id; 
            //var_dump($conditions);die();
        }

        if($department_id != 0){
            $conditions.=($conditions == '') ? " WHERE rf.department_id = '$department_id'" : " AND rf.department_id = '$department_id' ";
            $binds[6] = $department_id; 
        }

        if($workflow_id != 0){
            $conditions.=($conditions == '') ? " WHERE rf.workflow_id = '$workflow_id'" : " AND rf.workflow_id = '$workflow_id' ";
            $binds[7] = $workflow_id; 
        }
        
        $phql = "SELECT SUM(1) as total,
                        SUM(if(er.status = 2,1,0)) AS completed,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - (getHolidayCount(er.date_request)) )
                        <= er.tat+1,1,0)) AS in_progress,
                        SUM(if(er.status = 3,1,0)) AS rejected,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2)  - (getHolidayCount(er.date_request)) )
                        > er.tat+1,1,0)) as overdue,
                        SUM(if(er.date_ended is null, 0, DATEDIFF(er.date_ended,er.date_request))) as completion
                 FROM EmployeRequest er 
                 LEFT JOIN  RefWorkflow rf ON rf.workflow_id = er.workflow_id
                 INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 $conditions
                 ";
       
        $data = $this->modelsManager->executeQuery($phql,$binds);
        return $data;  
    }


    public function getReportsByEmployeeId($employee_id){
        $phql = "SELECT SUM(if(er.status = 2,1,0)) AS completed,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - (getHolidayCount(er.date_request)) )
                        <= er.tat+1,1,0)) AS in_progress,
                        SUM(if(er.status = 3,1,0)) AS rejected,
                        SUM(if(er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2)  - (getHolidayCount(er.date_request)) )
                        > er.tat+1,1,0)) as overdue
                 FROM EmployeRequest er
                 LEFT JOIN  RefWorkflow rf ON rf.workflow_id = er.workflow_id
                 INNER JOIN RefDepartment rc ON rc.department_id = rf.department_id
                 INNER JOIN MmpiTableOfOrganization tos ON tos.employee_id = er.requestor_id
                 INNER JOIN RequestStatus rs ON rs.id = er.status
                 WHERE er.requestor_id = ?1
                 GROUP BY er.request_id
                 ";
        $data = $this->modelsManager->executeQuery($phql,array(1=>$employee_id));
        return $data;  
    }
}
