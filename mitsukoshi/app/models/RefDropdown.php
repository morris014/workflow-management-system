<?php

class RefDropdown extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $field_id;

    /**
     *
     * @var string
     */
    public $item;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('field_id', 'TableFields', 'field_id', array('alias' => 'TableFields'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_dropdown';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDropdown[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDropdown
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
