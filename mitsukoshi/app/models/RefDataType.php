<?php

class RefDataType extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $data_type_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $reference;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('data_type_id', 'TableFields', 'data_type_id', array('alias' => 'TableFields'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'ref_data_type';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDataType[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RefDataType
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
