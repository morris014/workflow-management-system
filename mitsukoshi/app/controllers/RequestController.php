<?php

class RequestController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                //echo BASE_URI."/admin";die();
                return $this->response->redirect(BASE_URI."/admin");
                // then redirect to your login page
        }   
        else {
            $user = MmpiUserAccess::findFirst("employee_id='".EMPLOYEE_ID."'");
           
            if($user->request != 1)
            {
                return $this->response->redirect(BASE_URI);
            }     
        }
                
    }   
    public function sortBySubkey(&$array, $subkey, $sortType = SORT_ASC) {
        foreach ($array as $subarray) {
            $keys[] = $subarray[$subkey];
        }
        array_multisort($keys, $sortType, $array);
    }
    public function indexAction($status = '')
   	{   
        
        if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                //echo BASE_URI."/admin";die();
                return $this->response->redirect(BASE_URI."/admin");
                // then redirect to your login page
        }    else {
            $user = MmpiUserAccess::findFirst("employee_id='".EMPLOYEE_ID."'");
           
            if($user->request != 1)
            {
                return $this->response->redirect(BASE_URI);
            }     
        }        
        $searchKey = $this->request->getPost('request_search_key');
        
        $status = isset($_GET['status']) ? $_GET['status'] : 'overdue';
		$sort = isset($_GET['sort']) ? $_GET['sort'] : '0';
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $conditions = 'er.requestor_id = '.EMPLOYEE_ID;
        switch ($status) {
            case 'in-progress':
                $conditions .= ' AND er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - '
                    . '(getHolidayCount(er.date_request)))'
                    . ' <= er.tat+1';
                $this->session->set('search_request_key',"");
                break;
            case 'overdue':
                $conditions .= ' AND er.status = 1 AND (ROUND(TIME_TO_SEC(TIMEDIFF(NOW(),er.date_request))/86400,2) - '
                    . '(getHolidayCount(er.date_request)))'
                    . ' > er.tat+1';
                $this->session->set('search_request_key',"");
                break;  
            case 'all':
                $conditions .= ' AND er.status = 1';
                $this->session->set('search_request_key',"");
                break;  
            case 'search':
                $conditions .= '';
                //$searchKeySession = $this->session->set('search_request_key',$searchKey);
                if($searchKey == "")
                {
                    $searchKeySession = $this->session->get('search_request_key');
                }
                else
                {
                    $this->session->set('search_request_key',$searchKey); 
                    $searchKeySession = $this->session->get('search_request_key');  
                }
                    
                //if($searchKeySession == "")
                //{
                //   $this->session->set('search_request_key',$searchKey); 
                //}
                $searchKey = $this->session->get('search_request_key');
                
                break;  
            default:
                # code...
                break;
        }
        
		/*added by james*/
        if($searchKey != "")
        {
            $requests = EmployeRequest::getRequest($searchKey,$this->session->get('employee_id')); 
        }
        else
        {
            $requests = EmployeRequest::getRequestDetailsRequest($conditions, $sort);						  
        }
        /*added by james*/
        


        $request_details = array();
        if(sizeof($requests) > 0){
            if($id){
                $request_details = EmployeRequest::getRequestDetails($id);    
            }else{
                $request_details = EmployeRequest::getRequestDetails($requests[0]->request_id);    
            }
            
        }
        

        $this->view->setMainView('index');
        $this->view->setVar('req_search_key',$searchKey);
        $this->view->setVar('page_content','request/index');
        $this->view->setVar('requests',$requests);
        $this->view->setVar('request_details',$request_details);
        $this->view->setVar('status',($status == null) ? 'overdue' : $status);

        $this->view->setVar('request_id',(sizeof($request_details) > 0) ? $request_details->request_id : 0);
		$this->view->setVar('sort',$sort);
    }

    public function initiateAction()
    {
    	$categories = RefCategory::find();
        $departments = RefDepartment::find();
        $this->view->categories = $categories;
        $this->view->departments = $departments;
        $this->view->setMainView('index');
        $this->view->setVar('page_content','request/initiate');
	}

    public function initiaterequestAction()
    {

        /* GET FROM POST*/
        $data = $this->request->getPost();
        $request_fields = $data['fields'];
        $workflow_id = $data['workflow_id'];

        /* FIND WORKFLOW IF NOT EXISTING RETURN FALSE */
        $workflow = RefWorkflow::findFirstByWorkflowId($workflow_id);
        if(!$workflow){
            $this->helper->_echoJson(0,'Could not find the workflow');
            exit();
        }

        /* GET RELATED DATA FROM WORKFLOW TEMPLATE */
        $request_approver = $workflow->getWorkflowApprover(array("order"=>"sequence ASC"));
        $request_activities = $workflow->getWorkflowActivity(array("order"=>"sequence ASC"));

        /* BEGIN TRANSACTION */
        $this->db->begin();

        
        /* INITIALIZE REQUEST DATA */ 
        $request_data = array(
            'requestor_id' => EMPLOYEE_ID,
            'workflow_id' => $workflow_id,
            'date_request' => CURR_DATETIME,
            'status' => 1,
            'comments' => $data['comments'],
            'tat'=>$workflow->tat,
            'total_tasks' => sizeof($request_approver) + sizeof($request_activities),
            
        );

        /*  SAVE REQUEST */
        $result = EmployeRequest::register($request_data);
        if(!$result['status']){
            $this->db->rollback();
            echo json_encode($result);
            $this->view->disable();
            exit();
        }

        /*  GET REQUEST ID */
        $request_id = $result['data']->request_id;
        $request_data['request_id'] = $request_id;
    

        /* SAVE REQUEST APPROVER */
        $counter = 0; // USE TO SET START DATE ONLY FOR THE FIRST APPROVAL
        if(sizeof($request_approver) > 0){
            foreach ($request_approver as $approver) {
                /* INNITIALIZE APPROVER DATA */

                if($approver->approver_mgt_line_id != NULL)
                {
                    if($approver->approver_mgt_line_id == "1")
                    {
                        $to_main = MmpiTableOfOrganization::findFirst("employee_id ='".EMPLOYEE_ID."'");
                        $to = MmpiTo::findFirst("email='".$to_main->email."'");
                        
                        $to_main = MmpiTableOfOrganization::findFirst("email ='".$to->immediate_superior."'");
                        $approver_id = $to_main->employee_id;
                    }
                    else if($approver->approver_mgt_line_id == "2")
                    {
                        $to_main = MmpiTableOfOrganization::findFirst("employee_id ='".EMPLOYEE_ID."'");
                        $to = MmpiTo::findFirst("email='".$to_main->email."'");
                        
                        $to_main = MmpiTableOfOrganization::findFirst("email ='".$to->dep_manager."'");
                        $approver_id = $to_main->employee_id;                      
                    }
                    else
                    {
                        $to_main = MmpiTableOfOrganization::findFirst("employee_id ='".EMPLOYEE_ID."'");
                        $to = MmpiTo::findFirst("email='".$to_main->email."'");
                        
                        $to_main = MmpiTableOfOrganization::findFirst("email ='".$to->executive_officer."'");
                        $approver_id = $to_main->employee_id;                           
                    }
                    if($approver_id == "")
                    {
                        $to_main = MmpiTableOfOrganization::findFirst("designation_id = 2");
                         $approver_id = $to_main->employee_id;                        
                    }            
                    //$approver_id = "20151009030934";
                     $approver_data = array(
                        'approver_id' => $approver_id,
                        'tat' => $approver->tat,
                        'sequence' => $approver->sequence,
                        'request_id' => $request_id,
                        'status' => 1,
                        'date_request' => CURR_DATETIME,
                        'date_started' => ($counter == 0) ? CURR_DATETIME : null
                    );  
                     
                }
                else
                {
                    $approver_data = array(
                        'approver_id' => $approver->approver_id,
                        'tat' => $approver->tat,
                        'sequence' => $approver->sequence,
                        'request_id' => $request_id,
                        'status' => 1,
                        'date_request' => CURR_DATETIME,
                        'date_started' => ($counter == 0) ? CURR_DATETIME : null
                    );
                }

                $counter+=1;

                $result = RequestApproval::register($approver_data);
                if(!$result['status']){
                    $this->db->rollback();
                    echo json_encode($result);
                    $this->view->disable();
                    exit();
                }
            }
        }

        /* SAVE REQUEST ACTIVITY */
        if(sizeof($request_activities) > 0){
             foreach ($request_activities as $activity) {
                 
                /* INNITIALIZE ACTIVITY DATA */
                if ($activity->activity_name != "Acknowledge Request") {
                    if($activity->employee_id == NULL)
                    {
					$activity_data = array(
						'approver_id' => "TEAM-".$activity->group_id,
						'tat' => $activity->tat,
						'sequence' => $activity->sequence,
						'request_id' => $request_id,
						'status' => 1,
						'date_request' => CURR_DATETIME,
						'date_started' => ($counter == 0) ? CURR_DATETIME : null,
						'activity_name' => $activity->activity_name
					);
                    }
                    else{
					$activity_data = array(
						'approver_id' => $activity->employee_id,
						'tat' => $activity->tat,
						'sequence' => $activity->sequence,
						'request_id' => $request_id,
						'status' => 1,
						'date_request' => CURR_DATETIME,
						'date_started' => ($counter == 0) ? CURR_DATETIME : null,
						'activity_name' => $activity->activity_name
					);                        
                    }

                } 
                else {
					$activity_data = array(
						'approver_id' => EMPLOYEE_ID,
						'tat' => $activity->tat,
						'sequence' => $activity->sequence,
						'request_id' => $request_id,
						'status' => 1,
						'date_request' => CURR_DATETIME,
						'date_started' => ($counter == 0) ? CURR_DATETIME : null,
						'activity_name' => $activity->activity_name
					);
				}
                $counter+=1;

                $result = RequestActivities::register($activity_data);
                if(!$result['status']){
                    $this->db->rollback();
                    echo json_encode($result);
                    $this->view->disable();
                    exit();
                }
            }
        }


        /* SAVE FIELDS VALUES */
        foreach ($request_fields as $fields) {
            $fields_details = TableFields::findFirstByFieldId($fields['field_id']);
            $field_data = array(
                'field_id' => $fields['field_id'],
                'value' => $fields['value'],
                'request_id' => $request_id,
                'required' => $fields_details->required,
                'data_type_id' => $fields_details->data_type_id,
                'label' => $fields_details->label,
            );

            $result = RequestFields::register($field_data);
            if(!$result['status']){
                $this->db->rollback();
                echo json_encode($result);
                $this->view->disable();
                exit();
            }
        }
        
 

        /* COMMIT TRANSACTION IF EVERYTHING GOES WELL */
        $this->db->commit();
        $this->helper->_echoJson($request_id,'<strong>'.$workflow->name.'</strong> has been submited',$request_data);
    }
    public function uploadAction()
    {
        $request_id = $this->request->getPost('request_id');
        $files = $this->request->getPost('my_files');
        $filename = isset($files['name']) ? $files['name'] : null;
        $path = isset($files['path']) ? $files['path'] : null;
        

     	$request_details = EmployeRequest::findFirstByRequestId($request_id);
    	if(!$request_details){
    		$this->_echoJson(0,'Could not found the request');
    		exit();
    	}       
        
        $this->db->begin();
     		$request_details->filename  = $filename;
                $request_details->path  = $path;
    		if(!$request_details->save()){
    			$this->db->rollback();
    			$err_msg = '';
    			foreach ($request_details->getMessages() as $value) {
    				$err_msg.=$value.'<br>';
    			}
				$this->helper->_echoJson(0,$err_msg,$request_id);
				exit();
    		}
    		$this->db->commit();       
        $this->db->commit();
        $this->helper->_echoJson(1,'<strong>Request </strong> has been approved',$request_id);
    }
  
}

