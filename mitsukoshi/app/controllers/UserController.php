<?php

class UserController extends ControllerBase
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {
    	
    	$this->view->setVar('page_content','user/index');
    }
  	
  	public function geneAction($email){
  		$phql = "SELECT ua.username,ua.password 
				FROM MmpiUserAccess ua
				LEFT JOIN MmpiTableOfOrganization tos ON tos.employee_id = ua.employee_id
				where tos.email = ?1";
		$data = $this->modelsManager->executeQuery($phql,array(1=>$email));

		foreach ($data as $value) {
			echo $value->username."<br>";
			echo $value->password;
		}
  		$this->view->disable();
  	}
}

