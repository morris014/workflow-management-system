<?php

class RefsController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {

    }
    public function getSource()
    {
        $results = array();
        $data = MmpiTableOfOrganization::getList();
        $result =array();
        if(sizeof($data) > 0){
            
            foreach ($data as $value) {
                $list = array();
                $list['label'] = $value->firs_name.' '.$value->last_name.', '.$value->designation;
                $list['href'] = '';
                $list['employee_id'] = $value->employee_id;
                $list['email'] = $value->email;
                $list['designation'] = $value->designation;
                $list['fullname'] = $value->firs_name.' '.$value->last_name;
                $result[] = $list;
            }
        }
        $results['data'] = $result;
        return json_encode($results);
    }
    public function listemployeeAction()
    {
        $results = array();
        $data = MmpiTableOfOrganization::getList();
        $result =array();
        if(sizeof($data) > 0){
            
            foreach ($data as $value) {
                $list = array();
                $list['label'] = $value->firs_name.' '.$value->last_name.', '.$value->designation;
                $list['href'] = '';
                $list['employee_id'] = $value->employee_id;
                $list['email'] = $value->email;
                $list['designation'] = $value->designation;
                $list['fullname'] = $value->firs_name.' '.$value->last_name;
                $result[] = $list;
            }
        }
        $results['data'] = $result;
        echo json_encode($results);
        $this->view->disable();   
    }
    public function listworkflowbydepartmentAction($department_id)
    {
    	$results = array();
        $user = MmpiTableOfOrganization::findFirst("employee_id='".EMPLOYEE_ID."'");
        if($user->position_id == 2)//super user
        {
            $data = RefWorkflow::getByDepartmentSuper($department_id);
        }
        else
        {
            $data = RefWorkflow::getByDepartment($department_id,$user->department_id);
        }
    	if(sizeof($data) > 0){
	    	foreach ($data as $value) {
	    		$results[] = array(
	    			'workflow_id' => $value->workflow_id,
	    			'description' => $value->description,
	    			'name' => $value->name,
	    			'department_id' => $value->department_id,
                                'status' => $value->status,
	    		);
	    	}
    	}
    	echo json_encode($results);
    	$this->view->disable();
    }

    public function listworkflowbyidAction($workflow_id)
    {
        $results = array();
        $data = RefWorkflow::findFirst($workflow_id);
        if($data){
            $results['details'] = array(
                'workflow_id' => $data->workflow_id,
                'description' => $data->description,
                'name' => $data->name,
                'department_id' => $data->department_id,
                'department' => $data->RefDepartment->name,
            );

            
            foreach ($data->WorkflowTable as $value) {
                $table = array(
                    'table_id' => $value->table_id,
                );
                foreach ($value->TableFields as $field) {
                    $items = '';
                    if($field->data_type_id == 3){
                        foreach ($field->RefDropdown as $itm) {
                            $items.='<option value="'.$itm->item.'">'.$itm->item.'</option>';
                        }
                    }
                    $table['fields'][] = array(
                        'field_id' => $field->field_id,
                        'label' => $field->label,
                        'required' => $field->required,
                        'data_type_id' => $field->data_type_id,
                        'items' => $items
                    );
                    //echo $field->field_id."-".$field->label."-".$field->required."-";die();
                }
                $results['tables'][] = $table;
            }


        }
        echo json_encode($results);
        $this->view->disable();
    }
}

