<?php

class ExceluploaderController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {
    	$data = [
					  [104002,"Lito","Agao","Estabaya","lito.estabaya@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","3 Cents Marketing","Richmond Ngan","---"],
					  [807002,"Mark Anthony","Abaño","Rolle","markanthony.rolle@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver/Helper","3 Cents Marketing","Richmond Ngan","---"],
					  [8802001,"Irenia","Calas","Tanoja","irenia.tanoja@mitsukoshimotors.com",7223333,"HEAD OFFICE","Marketing Assistant","3 Cents Marketing","Richmond Ngan","---"],
					  [8805001,"Tirso","Agao","Perol","tirso.perol@mitsukoshimotors.com",7223333,"HEAD OFFICE","Liaison Officer","3 Cents Marketing","Richmond Ngan","---"],
					  [8902001,"Imelda","Gomez","Bardon","imelda.bardon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Cashier","3 Cents Marketing","Richmond Ngan","---"],
					  [8909001,"Nelia","Quibral","Cortel","nelia.cortel@mitsukoshimotors.com",7223333,"HEAD OFFICE","Warehouse Supervisor","3 Cents Marketing","Richmond Ngan","---"],
					  [9404001,"Arnold","Napitan","Cillano","arnold.cillano@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver/Helper","3 Cents Marketing","Richmond Ngan","---"],
					  [9406002,"Joel","Co","Del Rosario","joel.delrosario@mitsukoshimotors.com",7223333,"HEAD OFFICE","Technician","3 Cents Marketing","Richmond Ngan","---"],
					  [9801001,"Ronald","Madelo","Legaspina","ronald.legaspina@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","3 Cents Marketing","Richmond Ngan","---"],
					  [9906001,"Julieto","Demetita","Panolino","julieto.panolino@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","3 Cents Marketing","Richmond Ngan","---"],
					  [508006,"Joel","Ulang","Rizalte","joel.rizalte@mitsukoshimotors.com",7223333,"HEAD OFFICE","Team Leader","Accounting","Richmond Ngan","---"],
					  [509007,"Noel","Bermas","Buates","noel.buates@mitsukoshimotors.com",7223333,"HEAD OFFICE","Team Leader","Accounting","Richmond Ngan","---"],
					  [604016,"Rizell","Talosig","Segovia","rizell.segovia@mitsukoshimotors.com",7223333,"HEAD OFFICE","Team Leader","Accounting","Richmond Ngan","---"],
					  [1006055,"Junelyn","Reyes","Garcia","junelyn.garcia@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1204036,"Maria Ellaine","Paguio","Santos","mariaellaine.santos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1209089,"Mary Rose","Taganahan","Alforte","maryrose.alforte@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1210052,"Randy","Arellano","Villarete","randy.villarete@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1211154,"Mark Ishmael","Tarlit","Guilas","markishmael.guilas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1301056,"Brandon","Tubay","Garcia","brandon.garcia@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1302140,"Joan","Manuevo","Flores","joan.flores@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1305068,"Rosechell","Madarang","Mendoza","rosechell.mendoza@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1305080,"Ma. Marla","De Chavez","Miña","mamarla.mina@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1305087,"Melanie","Pabrua","Beso","melanie.beso@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1404014,"Eden","Copla","Cortiz","eden.cortiz@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1406119,"Stephanie Joy","Palaruan","Mantala","stephaniejoy.mantala@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1504040,"Rochelle","Cuales","Cabiltes","rochelle.cabiltes@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1505117,"Melanie","Binibini","Domingo","melanie.domingo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1505145,"Ron Braylle","Basister","Borromeo","ronbraylle.borromeo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1506002,"John Philip","Clorado","Lapiguira","johnphilip.lapiguira@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1506004,"Julius","Tamayo","Montalban","julius.montalban@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1506166,"Ge-ann","Buenaventura","Quiboal","ge-ann.quiboal@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1506312,"Lovely","Bartolome","Antonio","lovely.antonio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1506314,"Jio Carlo","Balboa","De Leon","jiocarlo.deleon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1507147,"Roda","Padilla","Lamonte","roda.lamonte@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1507192,"Rolie","Yabis","Ranjo","rolie.ranjo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [1508080,"Maria Gracia","Jazmin","Luna","mariagracia.luna@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Accounting","Richmond Ngan","---"],
					  [312001,"Ramon","Cula","Besana","ramon.besana@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [402003,"Romeo","Ramos","Turla","romeo.turla@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [402033,"Adonis","Miranda","Nievarez","adonis.nievarez@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [403010,"Jenny","Calleja","Marasigan","jenny.marasigan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [502017,"Vic","Valdez","De Vera","vic.devera@mitsukoshimotors.com",7223333,"HEAD OFFICE","Repo Management Officer","Administrative","Richmond Ngan","---"],
					  [503017,"Gilbert","Cabatingan","Horner","gilbert.horner@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [601003,"Dexter","L.","Lorca","dexter.lorca@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [608016,"Bunny","Altuna","Castillo","bunny.castillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [609019,"Michael","Acapuyan","Baculo","michael.baculo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [611041,"Walter","Pascua","Parada","walter.parada@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [706008,"Froilan","Taguiam","Taguinod","froilan.taguinod@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [707018,"Leonil","Teope","Campollo","leonil.campollo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [708061,"Primrose","Tagatac","Tabaniag","primrose.tabaniag@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [709022,"Julius","Reyes","Flores","julius.flores@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [710013,"Larry","Raipen","Francisco","larry.francisco@mitsukoshimotors.com",7223333,"HEAD OFFICE","Repo Management Officer","Administrative","Richmond Ngan","---"],
					  [711116,"Paul","Bunagan","Maraggun","paul.maraggun@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [803024,"Mark Anthony","Gotingco","Baloran","markanthony.baloran@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [805107,"Ma. Luz","Abay-abay","Nahial","maluz.nahial@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [807048,"Mark Ezekiel","Destreza","Lolos","markezekiel.lolos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1004022,"Rommel","Marzan","Patricio","rommel.patricio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1011018,"Robert","Del Rosario","Domingo","robert.domingo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1011053,"Johan","Guintu","Alejandro","johan.alejandro@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1101048,"Raymond","Muyargas","Alimurong","raymond.alimurong@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1102003,"Juan","Revilla","Quimio Jr.","juan.quimiojr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1103060,"Junior","Catindoy","Sibolboro","junior.sibolboro@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1104012,"Gerald","Dilan","Marzan","gerald.marzan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1110038,"Michael","Ramos","Bautista","michael.bautista@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1204121,"Limuel","Alejandria","Compuesto","limuel.compuesto@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1211158,"Adrian","Cuenca","Tabucon","adrian.tabucon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1307025,"Jone","Siaotong","Labrador","jone.labrador@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1310098,"Angelo","Bordeos","Buelo","angelo.buelo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1403131,"Ronnie","Pimentel","Cubol","ronnie.cubol@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales and Marketing Manager","Administrative","Richmond Ngan","---"],
					  [1404004,"Mario Vincent","Avila","Reyes Jr.","mariovincent.reyesjr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales and Marketing Manager","Administrative","Richmond Ngan","---"],
					  [1404043,"Mark","Francisco","Sangangbayan","mark.sangangbayan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales and Marketing Manager","Administrative","Richmond Ngan","---"],
					  [1404115,"Edward","Saria","Tibayan","edward.tibayan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1405009,"Kris Robert","Umagat","Gonzales","krisrobert.gonzales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1405010,"Randy","Gonzalo","Segundo","randy.segundo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Credit and Collection Manager","Administrative","Richmond Ngan","---"],
					  [1405012,"Zalde","Apao","Galas","zalde.galas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1405091,"Ronald","David","Zapanta","ronald.zapanta@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales and Marketing Manager","Administrative","Richmond Ngan","---"],
					  [1405093,"Mario","Ganiban","Simon","mario.simon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1407001,"Elgin","Pelicano","Fulgencio","elgin.fulgencio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Regional Manager","Administrative","Richmond Ngan","---"],
					  [1407002,"Harry","Santos","Militar","harry.militar@mitsukoshimotors.com",7223333,"HEAD OFFICE","Operation Manager","Administrative","Richmond Ngan","---"],
					  [1407026,"Dennis","Villamin","Reyes","dennis.reyes@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1407042,"Henry","Endraca","Felices","henry.felices@mitsukoshimotors.com",7223333,"HEAD OFFICE","Operation Manager","Administrative","Richmond Ngan","---"],
					  [1408002,"Alfonso","Macinas","Balane Jr.","alfonso.balanejr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1408003,"Jandilbert","Camile","Arca","jandilbert.arca@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1408006,"Hernando","Santos","Vega Jr.","hernando.vegajr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1408067,"Neil","Ramones","Montilla","neil.montilla@mitsukoshimotors.com",7223333,"HEAD OFFICE","Regional Manager","Administrative","Richmond Ngan","---"],
					  [1408102,"Ryan","De Guzman","Maningas","ryan.maningas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1409104,"Melvin Roy","Garrote","Dablo","melvinroy.dablo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1409107,"Jhomar","Momo","Alinsunorin","jhomar.alinsunorin@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1409110,"Anthony","Hangad","Gomez","anthony.gomez@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1409111,"Marvin","B.","Vales","marvin.vales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Regional Manager","Administrative","Richmond Ngan","---"],
					  [1409113,"Leonilo","A.","Tano","leonilo.tano@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1409114,"Alger","M.","Dabalos","alger.dabalos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1411149,"Simplicio","Romulo","Garay Jr.","simplicio.garayjr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1412007,"Salne","Naret","Dahuya","salne.dahuya@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1412109,"Herald","Rosario","Magleo","herald.magleo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1501079,"Antonio","Abas","Vargas","antonio.vargas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1502060,"Hubert","Manalo","Geronimo","hubert.geronimo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Credit and Collection Manager","Administrative","Richmond Ngan","---"],
					  [1504003,"Anthony","Carandang","Reyes","anthony.reyes@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1505119,"Mark Irvin","Cruz","Veneracion","markirvin.veneracion@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [1506136,"Alberto","Gemeniano","Setias Jr.","alberto.setiasjr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Manager","Administrative","Richmond Ngan","---"],
					  [711033,"Jerome","Ancajas","Bayron","jerome.bayron@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1203169,"Ruel","Gan","Niro","ruel.niro@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1206103,"Ricardo","Bermudez","Franco Jr.","ricardo.francojr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1208002,"Aldrin Bradly","Dugay","Doctor","aldrinbradly.doctor@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1212030,"Alvin","Anastacio","Albis","alvin.albis@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1306082,"Edmond Ralph","Conanan","Morales","edmondralph.morales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1403089,"Richard","Pid","Nabua","richard.nabua@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1406018,"Mendel Gregor","A.","Nicolas","mendelgregor.nicolas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1504099,"Don Abraham","Aquino","Manubay","donabraham.manubay@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1504100,"John Vincent","Salamat","Quitlong","johnvincent.quitlong@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1505030,"Algen","Langcamon","Gantiao","algen.gantiao@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1505118,"Rex","Catibog","Matala","rex.matala@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1506003,"Gerald","Palir","Capanas","gerald.capanas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1508002,"Rynell Troy","Lofamia","Dizon","rynelltroy.dizon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1509002,"Joseph Rizmel","Ignacio","Clemente","josephrizmel.clemente@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1509068,"Jhoed","Delos Reyes","Almogela","jhoed.almogela@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1509069,"Bernard","Pastor","Villamor","bernard.villamor@mitsukoshimotors.com",7223333,"HEAD OFFICE","Auditor","Audit","Richmond Ngan","---"],
					  [1411155,"Jingky","Avila","Denosta","jingky.denosta@mitsukoshimotors.com",7223333,"HEAD OFFICE","Bookkeeper","Bayswater Realty and Development Corporation","Richmond Ngan","---"],
					  [1003,"Rodel","Lee","Peñaredondo","rodel.peñaredondo@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [3002,"Nelson","Cubelo","Gepulla","nelson.gepulla@mitsukoshimotors.com",7223333,"WAREHOUSE","CKD Staff","Carmona","Victoriano Unay","---"],
					  [107001,"Edgar","Salon","Arillaga","edgar.arillaga@mitsukoshimotors.com",7223333,"WAREHOUSE","Acting Leadman","Carmona","Victoriano Unay","---"],
					  [710050,"Jesus","---","Besinga","jesus.besinga@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Carmona","Victoriano Unay","---"],
					  [803035,"Alberto","Franco","Alferez","alberto.alferez@mitsukoshimotors.com",7223333,"WAREHOUSE","Maintenance - Leadman","Carmona","Victoriano Unay","---"],
					  [907016,"Rolando","P.","Cataluña","rolando.cataluña@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1103042,"Laureano","Silagpo","Valdez Jr.","laureano.valdezjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1104027,"Antony","Guadalupe","Bonifacio","antony.bonifacio@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [1105123,"Roger","Bonifacio","Villaruel","roger.villaruel@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub-Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1110023,"Reynaldo","Curang","Morales","reynaldo.morales@mitsukoshimotors.com",7223333,"WAREHOUSE","Leadman - Stencil Section","Carmona","Victoriano Unay","---"],
					  [1112027,"Marcial","Turbanada","Lobos Jr.","marcial.lobosjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1201019,"Homer","Flaviano","Panganiban","homer.panganiban@mitsukoshimotors.com",7223333,"WAREHOUSE","CKD","Carmona","Victoriano Unay","---"],
					  [1201021,"Norwin","Nungay","Tingzon","norwin.tingzon@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1201022,"Paulito","Rosales","Salingsing Jr","paulito.salingsingjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1201035,"Samson","Rabie","Soliven","samson.soliven@mitsukoshimotors.com",7223333,"WAREHOUSE","Acting Leadman","Carmona","Victoriano Unay","---"],
					  [1201100,"Melvin","Lucelo","Samoranos","melvin.samoranos@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1201126,"Cristian","Perez","Morales","cristian.morales@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [1202096,"Von Amir","Sabino","Benitez","vonamir.benitez@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1202132,"Gerardo","Doque","Felipe","gerardo.felipe@mitsukoshimotors.com",7223333,"WAREHOUSE","Endurance Testing - Staff","Carmona","Victoriano Unay","---"],
					  [1203102,"Jay","Escoltor","Iligan","jay.iligan@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1203125,"Reynaldo","Feria","Rivera","reynaldo.rivera@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Frame Staff","Carmona","Victoriano Unay","---"],
					  [1203174,"Daniel","Nuñeza","Viñas","daniel.viñas@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Leadman","Carmona","Victoriano Unay","---"],
					  [1204165,"Jose Rogie","Surio","Tan","joserogie.tan@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembly Team Lead","Carmona","Victoriano Unay","---"],
					  [1204179,"Rachel Ian","Pascual","Erasmo","rachelian.erasmo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1206048,"Benjamin","Gara","Cuyag Jr.","benjamin.cuyagjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1211002,"Julius Kim","Villaruel","Sabino","juliuskim.sabino@mitsukoshimotors.com",7223333,"WAREHOUSE","Reworks - Staff","Carmona","Victoriano Unay","---"],
					  [1302141,"Romeo","Ortiz","Amparo","romeo.amparo@mitsukoshimotors.com",7223333,"WAREHOUSE","Welder","Carmona","Victoriano Unay","---"],
					  [1305089,"Adelo","Cabillian","Valdemar","adelo.valdemar@mitsukoshimotors.com",7223333,"WAREHOUSE","Maintenance Staff","Carmona","Victoriano Unay","---"],
					  [1308001,"Victoriano","Altiz","Unay","victoriano.unay@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Manager","Carmona","Richmond Ngan","---"],
					  [1308136,"Rodel","Aquino","Ancheta","rodel.ancheta@mitsukoshimotors.com",7223333,"WAREHOUSE","Welder","Carmona","Victoriano Unay","---"],
					  [1310078,"Paul","Bala","Lamsen","paul.lamsen@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1311067,"Jerry","Idoy","Quirante","jerry.quirante@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1311146,"Rocel","Dela Vega","Guadalupe","rocel.guadalupe@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [1403096,"Leonardo","Moje","Florendo Jr.","leonardo.florendojr@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1403098,"Jestoni","Villacencio","Hugo","jestoni.hugo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1403099,"Eduardo","Navales","Esquejo","eduardo.esquejo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1403100,"Jayson","Magno","Yongco","jayson.yongco@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub-Assy Frame","Carmona","Victoriano Unay","---"],
					  [1403201,"Othelo","B.","Teope","othelo.teope@mitsukoshimotors.com",7223333,"WAREHOUSE","Electrician","Carmona","Victoriano Unay","---"],
					  [1405128,"Florante","Sabino","Berido","florante.berido@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1405217,"Mico","Umali","Saligumba","mico.saligumba@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Frame Staff","Carmona","Victoriano Unay","---"],
					  [1405220,"Mervin","Santillar","Cortez","mervin.cortez@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1405226,"Ramon","Amores","Federipe","ramon.federipe@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1407031,"Rodolfo","Cubol","Colina","rodolfo.colina@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1407033,"Edwin","Pacificar","Cubol","edwin.cubol@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1407034,"Ralph Alvin","Roquiño","Panuncio","ralphalvin.panuncio@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1407040,"Carfil","Aranda","Memije","carfil.memije@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1407250,"Jervie","Alcanse","Abdon","jervie.abdon@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1410122,"John Michael","Rapsing","Rosales","johnmichael.rosales@mitsukoshimotors.com",7223333,"WAREHOUSE","Reworks - Staff","Carmona","Victoriano Unay","---"],
					  [1410129,"Jayrald","Restaba","Emprese","jayrald.emprese@mitsukoshimotors.com",7223333,"WAREHOUSE","CKD","Carmona","Victoriano Unay","---"],
					  [1410132,"Felipe","Molos","Leonor","felipe.leonor@mitsukoshimotors.com",7223333,"WAREHOUSE","Reworks - Staff","Carmona","Victoriano Unay","---"],
					  [1411104,"Eljay","Villanueva","Millare","eljay.millare@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411106,"Jonathan","Bonifacio","Paler","jonathan.paler@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411115,"Rey","-","Lopez","rey.lopez@mitsukoshimotors.com",7223333,"WAREHOUSE","Prodution Supply Team Lead","Carmona","Victoriano Unay","---"],
					  [1411125,"Nestor","Galingan","Mangubat Jr.","nestor.mangubatjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411126,"Cyrill","Mangalino","Elago","cyrill.elago@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411133,"Harold","Lumapag","Bernaldez","harold.bernaldez@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411276,"Jerry","Agravante","Villanueva","jerry.villanueva@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411277,"Louie","Villanueva","Fabon","louie.fabon@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411278,"Rodel","Eleserio","Villaganas","rodel.villaganas@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1411284,"Dennis","Lambo","Tan","dennis.tan@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1412012,"Quinn Ojie","-","Tocmoc","quinnojie.tocmoc@mitsukoshimotors.com",7223333,"WAREHOUSE","Tool Keeper","Carmona","Victoriano Unay","---"],
					  [1502193,"Joel","Alfonso","Cetra","joel.cetra@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1502194,"Arthur Gerald","Loja","Longares","arthurgerald.longares@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1502196,"Isabelo","Salisi","Masaclao","isabelo.masaclao@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Staff","Carmona","Victoriano Unay","---"],
					  [1503110,"Andres","Drilon","Japona Jr.","andres.japonajr@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1503118,"Richard","Madayag","Orongan","richard.orongan@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1503132,"Jefferson","Famoso","Vasquez","jefferson.vasquez@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503135,"Raymart","Gutierrez","Amarillo","raymart.amarillo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503145,"Walter","Orbeta","Micosa","walter.micosa@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503149,"Manolo","Claveria","An Jr.","manolo.anjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503151,"Jessie","Lucas","Lubasan","jessie.lubasan@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503220,"Kheyvee","Bilog","Lanting","kheyvee.lanting@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Unloader","Carmona","Victoriano Unay","---"],
					  [1503228,"Vencent","Orlanda","Montiadora","vencent.montiadora@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503233,"Niño Ulyses","Tamayo","Magnase","niñoulyses.magnase@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503243,"Mikko Robert","Romano","Denum","mikkorobert.denum@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503252,"Esmael","Dela Cruz","Banaag","esmael.banaag@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1503257,"Reynaldo","Marasigan","Honra Jr.","reynaldo.honrajr@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1503258,"Jobet","Sandoval","Natividad","jobet.natividad@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503260,"Francis Jay","Santos","Jadion","francisjay.jadion@mitsukoshimotors.com",7223333,"WAREHOUSE","Stencil","Carmona","Victoriano Unay","---"],
					  [1503261,"Sherwin","Barangas","Añonuevo","sherwin.añonuevo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503262,"Joshua","Tabor","Magadon","joshua.magadon@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1503265,"Ronnel","Aquias","Lontoc","ronnel.lontoc@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1503266,"Gabriel","-","Bulac","gabriel.bulac@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1503274,"Leo","Servano","Lucendo","leo.lucendo@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503277,"Joemar","Balboa","Datoon","joemar.datoon@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1503280,"Patrick","Servito","Prado","patrick.prado@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1505101,"John Brylle","Capistrano","Cabrera","johnbrylle.cabrera@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505102,"Jayson","De Lima","Dimanarig","jayson.dimanarig@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505103,"Jecky","Patalita","Inso","jecky.inso@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub-Assy","Carmona","Victoriano Unay","---"],
					  [1505162,"Ar-Jay","Laveña","Regonaos","ar-jay.regonaos@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505163,"Dennis Rod","Panganiban","Alcantara","dennisrod.alcantara@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505164,"Jofrey","Costin","Bugarin","jofrey.bugarin@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505165,"Jay Paul","Castor","Padernal","jaypaul.padernal@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler / Main Line","Carmona","Victoriano Unay","---"],
					  [1505166,"Henry","Pilo","Yu","henry.yu@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505167,"Mark Joseph","Balares","Macalalad","markjoseph.macalalad@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505168,"John Carlo","Galanida","Masanga","johncarlo.masanga@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Stencil","Carmona","Victoriano Unay","---"],
					  [1505169,"Roy","De Guzman","Garcia","roy.garcia@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505170,"Ronnie","Beniza","Balawang","ronnie.balawang@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505171,"Aldrich Allen","Mesa","Lagana","aldrichallen.lagana@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505173,"Alvin","Segaya","Alay-ay","alvin.alay-ay@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505174,"Kederson Keith","Magno","Adrales","kedersonkeith.adrales@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505175,"Fernando","Oliver","Dichoso Jr.","fernando.dichosojr@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub-Assy Engine","Carmona","Victoriano Unay","---"],
					  [1505176,"Jay-Ar","Orubia","Pakingan","jay-ar.pakingan@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505179,"Jeffrey","Joson","Aranda","jeffrey.aranda@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Staff","Carmona","Victoriano Unay","---"],
					  [1505180,"Havib","Creer","Bilog","havib.bilog@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505181,"Donnie","Palencia","De Lima","donnie.delima@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505182,"Nestor","Bueno","Mañibo","nestor.mañibo@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505183,"Gerardo","Perez","Macalua Jr.","gerardo.macaluajr@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505184,"Arnold","Lunasco","Geneblazo","arnold.geneblazo@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505185,"Rhoiel","Reyes","Ranido","rhoiel.ranido@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505186,"Eugene","Formento","Merilles","eugene.merilles@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505187,"Jomar","Magnanao","Tamor","jomar.tamor@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505189,"James","Cagadoc","Pagulayan","james.pagulayan@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505190,"Renz Allen","De Jesus","Perdonio","renzallen.perdonio@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505191,"Arjohn","De Jesus","Perez","arjohn.perez@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505192,"Jhasfer","Micosa","Mercado","jhasfer.mercado@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505193,"Angelito","Gutierrez","Pintor","angelito.pintor@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505194,"Raymund","Anaño","Adaptante","raymund.adaptante@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505195,"Mark Aldrin","De Ramos","Bautista","markaldrin.bautista@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505197,"Erwin","Pangilinan","Galura","erwin.galura@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505198,"Terence","De Laryarte","Andres","terence.andres@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1505200,"Joel","Umandap","Ramos","joel.ramos@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler / Engine","Carmona","Victoriano Unay","---"],
					  [1505206,"Alvin John","Aranda","Mapalad","alvinjohn.mapalad@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1505208,"Paulo","Fullente","Agao","paulo.agao@mitsukoshimotors.com",7223333,"WAREHOUSE","Stencil","Carmona","Victoriano Unay","---"],
					  [1505209,"Ernie","Catalan","Serquillos","ernie.serquillos@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [1506056,"Rosalito","Rosales","Loyola","rosalito.loyola@mitsukoshimotors.com",7223333,"WAREHOUSE","Stencil","Carmona","Victoriano Unay","---"],
					  [1506155,"Marijune","Verana","Lucero","marijune.lucero@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub - Frame 1","Carmona","Victoriano Unay","---"],
					  [1506157,"Michael John","Torrion","Makinano","michaeljohn.makinano@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506158,"Ermelito","Enrina","Villanueva","ermelito.villanueva@mitsukoshimotors.com",7223333,"WAREHOUSE","Quality Control - Staff","Carmona","Victoriano Unay","---"],
					  [1506159,"Shaun Lucky","Ergino","De Castro","shaunlucky.decastro@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506160,"Diomher","Copino","Peñero","diomher.peñero@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506162,"Daryl","Serra","Penson","daryl.penson@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506163,"Adrian","Sulatorio","Naynes","adrian.naynes@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506189,"Ronnel","Gudao","Gulane","ronnel.gulane@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1506241,"Jhefry","Buscas","David","jhefry.david@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1507084,"Albert","Pajuay","Denulan","albert.denulan@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507087,"Kenneth James","Mangandi","Albofuera","kennethjames.albofuera@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507088,"Alvin","Ocmer","Boco","alvin.boco@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Supply","Carmona","Victoriano Unay","---"],
					  [1507090,"Dexzon","Suganob","Monares","dexzon.monares@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507091,"Reynan","Mercado","Ramos","reynan.ramos@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507092,"John Cris","Dioneda","Parilla","johncris.parilla@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507153,"Reynaldo","Cenizal","Recido","reynaldo.recido@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1507251,"Romel","Santiago","Arce","romel.arce@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1508008,"Gabriel","Cullo","Pilit","gabriel.pilit@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1508009,"Jhon-rey","Restaba","Emprese","jhon-rey.emprese@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1508010,"Angelo","Restaba","Manabat","angelo.manabat@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1508129,"Kim","Corsiga","Buitizon","kim.buitizon@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Supply","Carmona","Victoriano Unay","---"],
					  [1508130,"Jessie","Tenorio","Nacario","jessie.nacario@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [1508131,"Filmark","Sobremonte","Bautista","filmark.bautista@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub - Frame 1","Carmona","Victoriano Unay","---"],
					  [1508132,"Levi","Mates","Sale","levi.sale@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1508133,"Amante","Solayao","Garcia Jr.","amante.garciajr@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1508134,"Mark Anthony","Baldisimo","Kalahatian","markanthony.kalahatian@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Unloader","Carmona","Victoriano Unay","---"],
					  [1508135,"Virgilio","Parec","Bagasol Jr.","virgilio.bagasoljr@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1508136,"Larry","Rosales","Salamoding","larry.salamoding@mitsukoshimotors.com",7223333,"WAREHOUSE","Sub Assy Line Staff","Carmona","Victoriano Unay","---"],
					  [1508137,"Rudy Jay","Ascano","Obena","rudyjay.obena@mitsukoshimotors.com",7223333,"WAREHOUSE","Assembler","Carmona","Victoriano Unay","---"],
					  [1509001,"Mariecor Syrah","Hernandez","Genciagan","mariecorsyrah.genciagan@mitsukoshimotors.com",7223333,"WAREHOUSE","Administrative Plant Manager","Carmona","Victoriano Unay","---"],
					  [1509108,"Christian","Tagara","Dela Cruz","christian.delacruz@mitsukoshimotors.com",7223333,"WAREHOUSE","Production Supply","Carmona","Victoriano Unay","---"],
					  [1509137,"Edfra","Rotaquio","Soltones","edfra.soltones@mitsukoshimotors.com",7223333,"WAREHOUSE","MSD Unloader","Carmona","Victoriano Unay","---"],
					  [9709001,"Rowen","Bonifacio","Villaruel","rowen.villaruel@mitsukoshimotors.com",7223333,"WAREHOUSE","Acting Leadman","Carmona","Victoriano Unay","---"],
					  [9710001,"Ronick","Bocaling","Nacasi","ronick.nacasi@mitsukoshimotors.com",7223333,"WAREHOUSE","Main Line - Assembler","Carmona","Victoriano Unay","---"],
					  [9903001,"Alexander","Formalejo","Castillo Jr.","alexander.castillojr@mitsukoshimotors.com",7223333,"WAREHOUSE","CKD Leadman","Carmona","Victoriano Unay","---"],
					  [307001,"Ernalyn","Manipon","Paras","ernalyn.paras@mitsukoshimotors.com",7223333,"HEAD OFFICE","LTO Supervisor (Budget)","Corporate Services","Richmond Ngan","---"],
					  [707088,"Theresa","Tan","Geronimo","theresa.geronimo@mitsukoshimotors.com",7223333,"HEAD OFFICE","LTO Supervisor (ORCR)","Corporate Services","Richmond Ngan","---"],
					  [1102124,"Agnes","Verdejo","Flores","agnes.flores@mitsukoshimotors.com",7223333,"HEAD OFFICE","Manager","Corporate Services","Richmond Ngan","---"],
					  [1104098,"Cecille","Isiang","Estanio","cecille.estanio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1301054,"Sheila","Dela Cruz","Calusayan","sheila.calusayan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1304115,"Donald","Navarro","Soliman","donald.soliman@mitsukoshimotors.com",7223333,"HEAD OFFICE","Documentation Supervisor","Corporate Services","Richmond Ngan","---"],
					  [1304127,"Annabel","Tayoto","Oclares","annabel.oclares@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1307150,"Elmer","Pellero","Masgong II","elmer.masgongii@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1309130,"Lady Lyka","Reyes","Villalobos","ladylyka.villalobos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1403083,"Ma. Luisa","Lutap","Acido","maluisa.acido@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1403084,"Diana Jessica","Zubiri","De Lara","dianajessica.delara@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1403109,"Jonalyn","Nuguit","Cepillo","jonalyn.cepillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1403128,"Cherrylyn","Antolin","Tandoy","cherrylyn.tandoy@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1406122,"Joana","Marin","Mendoza","joana.mendoza@mitsukoshimotors.com",7223333,"HEAD OFFICE","Documentation Supervisor","Corporate Services","Richmond Ngan","---"],
					  [1409175,"Rachel","Salado","Ibrahim","rachel.ibrahim@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1411083,"Nessa","Huab","Villa","nessa.villa@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1501024,"Judy Ann","Valle","Salamat","judyann.salamat@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1502006,"Rosell Ann","Santos","Liwanag","rosellann.liwanag@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1503104,"Isabel Francheska","Espiña","Reyes","isabelfrancheska.reyes@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1503111,"Jerome","Austria","De Guzman","jerome.deguzman@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1504098,"Nhel Patrick","Abarro","Sacramento","nhelpatrick.sacramento@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1505031,"John Paul","Talosig","Sol","johnpaul.sol@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1506165,"Daisa","Pacaldo","Mainit","daisa.mainit@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1508070,"Christofer John","Lemu","Rivero","christoferjohn.rivero@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1509163,"Antonette","Rios","Rafol","antonette.rafol@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [1509164,"Cecile","Malay","Maaba","cecile.maaba@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Corporate Services","Richmond Ngan","---"],
					  [805053,"Romelyn","Ordillano","Paez","romelyn.paez@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [911019,"Aina","Benghilo","Zamora","aina.zamora@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1201007,"Ileen","Buenaventura","Villanueva","ileen.villanueva@mitsukoshimotors.com",7223333,"HEAD OFFICE","Talent Management Services Supervisor","Human Resource","Richmond Ngan","---"],
					  [1205008,"Mary Anjenette","Magayam","Maaba","maryanjenette.maaba@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1208001,"Elsie","Viesca","Castillo","elsie.castillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1303102,"Estefanie","Utlang","Enquig","estefanie.enquig@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1304130,"Ma. Kharla","Opon","Mendoza","makharla.mendoza@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1311011,"Maria Reina","Cabahug","Coronel","mariareina.coronel@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1312107,"Mark","Dela Cruz","Tenorio","mark.tenorio@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Manager","Human Resource","Richmond Ngan","---"],
					  [1410030,"Kimberly","Rances","Leal","kimberly.leal@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1410170,"Jan Erika","Bontia","Lopez","janerika.lopez@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1504126,"Marinel","Galupo","Rivera","marinel.rivera@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1507133,"Jerine","Mandap","Basco","jerine.basco@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1507193,"Charissa","Lugo","Miras","charissa.miras@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [1509177,"Ellen Gay","Dela Cruz","Alipio","ellengay.alipio@mitsukoshimotors.com",7223333,"HEAD OFFICE","HR Associate","Human Resource","Richmond Ngan","---"],
					  [603018,"Benjamin","Villeno","Querimit","benjamin.querimit@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sr. Programmer","Information Technology","Richmond Ngan","---"],
					  [608064,"Fermin","Felipe","Escueta","fermin.escueta@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Information Technology","Richmond Ngan","---"],
					  [708075,"Eduardo","Famisaran","Calusayan Jr.","eduardo.calusayanjr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Jr. Programmer","Information Technology","Richmond Ngan","---"],
					  [804067,"Matt","Cadion","Concepcion","matt.concepcion@mitsukoshimotors.com",7223333,"HEAD OFFICE","Technician","Information Technology","Richmond Ngan","---"],
					  [808061,"Enriqueto","Mejica","Dula Jr.","enriqueto.dulajr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Technician","Information Technology","Richmond Ngan","---"],
					  [1304122,"King Ramces","Tolentino","Calderon","kingramces.calderon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Information Technology","Richmond Ngan","---"],
					  [1411255,"Jonathan","Quiambao","Castillo","jonathan.castillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","IT Manager","Information Technology","Richmond Ngan","---"],
					  [1503081,"Edison","Coke","Pizarra","edison.pizarra@mitsukoshimotors.com",7223333,"HEAD OFFICE","IT Technician","Information Technology","Richmond Ngan","---"],
					  [1503108,"Dante","Danganan","Pangan","dante.pangan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sr. Programmer","Information Technology","Richmond Ngan","---"],
					  [1506057,"Ryan","Gonzales","Rosaldo","ryan.rosaldo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sr. Network Administrator","Information Technology","Richmond Ngan","---"],
					  [1507196,"Mikko","Sibal","Concepcion","mikko.concepcion@mitsukoshimotors.com",7223333,"HEAD OFFICE","Desktop Support","Information Technology","Richmond Ngan","---"],
					  [1401009,"Francisco","Tavas","Veroya Jr.","francisco.veroyajr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Logistics Head","Logistics","Richmond Ngan","---"],
					  [1502007,"Rosal","Barcebal","Quiñones","rosal.quiñones@mitsukoshimotors.com",7223333,"HEAD OFFICE","Company Mechanic","Maintenance","Richmond Ngan","---"],
					  [9110001,"Wilfredo","Albino","Geronimo","wilfredo.geronimo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Maintenance Supervisor","Maintenance","Richmond Ngan","---"],
					  [9110002,"Joseph","Las Piñas","Labayo","joseph.labayo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Welder","Maintenance","Richmond Ngan","---"],
					  [9301001,"Dominador","Narredo","Suliva","dominador.suliva@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Maintenance","Richmond Ngan","---"],
					  [9401002,"Jessie","Dela Gracia","Juanico","jessie.juanico@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Maintenance","Richmond Ngan","---"],
					  [1410203,"Leonardo","Garcia","Manuel","leonardo.manuel@mitsukoshimotors.com",7223333,"BRANCH","Marketing Assistant","MMPI (Violago)","Quintos Villoso","---"],
					  [1411234,"Kenneth","De Jesus","Carreon","kenneth.carreon@mitsukoshimotors.com",7223333,"BRANCH","Account Counselor","MMPI (Violago)","Quintos Villoso","---"],
					  [1412041,"Al Jonn","Abaigar","Santos","aljonn.santos@mitsukoshimotors.com",7223333,"BRANCH","Account Counselor","MMPI (Violago)","Quintos Villoso","---"],
					  [1505053,"Cris John","Palad","Felix","crisjohn.felix@mitsukoshimotors.com",7223333,"BRANCH","Mechanic","MMPI (Violago)","Quintos Villoso","---"],
					  [1506033,"Angela Mae","Jacobo","Curioso","angelamae.curioso@mitsukoshimotors.com",7223333,"BRANCH","Assistant Cashier","MMPI (Violago)","Quintos Villoso","---"],
					  [1506064,"Ruel","Quintos","Villoso","ruel.villoso@mitsukoshimotors.com",7223333,"BRANCH","Branch Manager","MMPI (Violago)","Richmond Ngan","---"],
					  [1506140,"Raymund Angelo","-","Buaya","raymundangelo.buaya@mitsukoshimotors.com",7223333,"BRANCH","Account Counselor","MMPI (Violago)","Quintos Villoso","---"],
					  [1506287,"Anthony","Abesamis","Felipe","anthony.felipe@mitsukoshimotors.com",7223333,"BRANCH","Account Counselor","MMPI (Violago)","Quintos Villoso","---"],
					  [1507051,"Chat Rea","Cabaltea","Orpilla","chatrea.orpilla@mitsukoshimotors.com",7223333,"BRANCH","Assistant Cashier","MMPI (Violago)","Quintos Villoso","---"],
					  [1507119,"Joy","Carado","Ygrubay","joy.ygrubay@mitsukoshimotors.com",7223333,"BRANCH","Marketing Assistant","MMPI (Violago)","Quintos Villoso","---"],
					  [1509050,"Michael","Impas","Zabat","michael.zabat@mitsukoshimotors.com",7223333,"BRANCH","Mechanic","MMPI (Violago)","Quintos Villoso","---"],
					  [204002,"Luzviminda","Roxas","Endozo","luzviminda.endozo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Operations Supervisor","Operations","Richmond Ngan","---"],
					  [707089,"Precy","Cabaccan","Cammayo","precy.cammayo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Operations","Richmond Ngan","---"],
					  [1107053,"Sunshine","Calara","Aldave","sunshine.aldave@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Operations","Richmond Ngan","---"],
					  [1301048,"Rubelyn","Dalaodao","Andaya","rubelyn.andaya@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Operations","Richmond Ngan","---"],
					  [1306107,"Jeremie","Ceballo","Bongat","jeremie.bongat@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Operations","Richmond Ngan","---"],
					  [1407065,"Santiago","Sarmiento","Gonzales","santiago.gonzales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Messenger","Operations","Richmond Ngan","---"],
					  [1505109,"Arianne Mae","Millena","Garay","ariannemae.garay@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Operations","Richmond Ngan","---"],
					  [1205260,"Rogevin","Galarrita","Salurio","rogevin.salurio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Associate","Remedial Accounts Management and Legal","Richmond Ngan","---"],
					  [1403111,"Jenar","Lucero","Luzon","jenar.luzon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Supervisor","Remedial Accounts Management and Legal","Richmond Ngan","---"],
					  [1506244,"Karren Mae","Guadayo","Cruz","karrenmae.cruz@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Sales and Marketing","Richmond Ngan","---"],
					  [9001,"Carmen","Santillan","Vicencio","carmen.vicencio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Spareparts Credit and Collection Supervisor","Spare Parts","Richmond Ngan","---"],
					  [108002,"Rolando","Atencio","Morales","rolando.morales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales Staff","Spare Parts","Richmond Ngan","---"],
					  [108004,"Queenie","Alberca","Castillo","queenie.castillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Spare Parts Cashier","Spare Parts","Richmond Ngan","---"],
					  [204001,"Michael","Duropan","Ganitano","michael.ganitano@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [305002,"Lino","Socorro","Lobos","lino.lobos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales Coordinator","Spare Parts","Richmond Ngan","---"],
					  [401010,"Rhona","Santos","Balino","rhona.balino@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [401011,"Rafael","Ganitano","Esperanza","rafael.esperanza@mitsukoshimotors.com",7223333,"HEAD OFFICE","Area Coordinator","Spare Parts","Richmond Ngan","---"],
					  [401023,"Jose","Eugenio","Nobleza","jose.nobleza@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [405004,"Mark Anthony","Dajao","Panolino","markanthony.panolino@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [409008,"Ma. Luisa","Caillo","Cleofe","maluisa.cleofe@mitsukoshimotors.com",7223333,"HEAD OFFICE","Parts Controller","Spare Parts","Richmond Ngan","---"],
					  [411003,"Gerald","Duque","Felipe","gerald.felipe@mitsukoshimotors.com",7223333,"HEAD OFFICE","Partsman","Spare Parts","Richmond Ngan","---"],
					  [509019,"Quintin","Sosing","Balanquit","quintin.balanquit@mitsukoshimotors.com",7223333,"HEAD OFFICE","Spare Parts Sales and Collections Manager","Spare Parts","Richmond Ngan","---"],
					  [705010,"Jerson","Medel","Kwan","jerson.kwan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [712010,"Diosdado","Belleza","Collantes Jr.","diosdado.collantesjr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales and Collection Coordinator","Spare Parts","Richmond Ngan","---"],
					  [802054,"Alvin","Basto","Panugao","alvin.panugao@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [907021,"Jojo","Cleofas","Miciano","jojo.miciano@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1007098,"Nicolas","Casio","Mejarito","nicolas.mejarito@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1101105,"Lowell","Garil","Red","lowell.red@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1104029,"Joseph","Casiquin","Suplido","joseph.suplido@mitsukoshimotors.com",7223333,"HEAD OFFICE","Sales Staff","Spare Parts","Richmond Ngan","---"],
					  [1104030,"Peter Charlotte","Arevalo","Cable","petercharlotte.cable@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1105087,"Jorem","Ibañez","Morales","jorem.morales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1107017,"Benedick","Tangonan","Galvez","benedick.galvez@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1111073,"Jonard","Namoc","Intoy","jonard.intoy@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1112084,"Jaime","Felia","Paulino Jr","jaime.paulinojr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1206201,"Leover","Villamena","Pareja","leover.pareja@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1211024,"Jonh Jayson","Caoagdan","Ignacio","jonhjayson.ignacio@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1211155,"Gian Fernando","Revilla","Villarmino","gianfernando.villarmino@mitsukoshimotors.com",7223333,"HEAD OFFICE","Encoder","Spare Parts","Richmond Ngan","---"],
					  [1301027,"Vincent Paul","Dimal","Fajardo","vincentpaul.fajardo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Messenger","Spare Parts","Richmond Ngan","---"],
					  [1302184,"Benjie","Guillermo","Nacario","benjie.nacario@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1305081,"Jesica","Caligaya","Andaya","jesica.andaya@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1307026,"Allan Paolo","Quesada","Burce","allanpaolo.burce@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1307079,"Ryan","Fernandez","Bumatay","ryan.bumatay@mitsukoshimotors.com",7223333,"HEAD OFFICE","Warehouseman","Spare Parts","Richmond Ngan","---"],
					  [1309012,"Mickleson","-","Betache","mickleson.betache@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1310023,"Byron","Liporada","Rabanera","byron.rabanera@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1310051,"Wilson","Barahim","Bumanglag","wilson.bumanglag@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1311049,"Sizzle","Balleta","Donguya","sizzle.donguya@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1403127,"Ivan Samuel","Lauta","Nas","ivansamuel.nas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1405172,"Merson","Macopia","Baccay","merson.baccay@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1409119,"Melvin","Turbanada","Lobos","melvin.lobos@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1502125,"Aiza","Enciso","Villareal","aiza.villareal@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1502126,"Francis","Aquino","Trinidad","francis.trinidad@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1504041,"Paolo Roberto","Acosta","Perez","paoloroberto.perez@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1504042,"Mhar Vane","Alvarez","Villaflor","mharvane.villaflor@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Spare Parts","Richmond Ngan","---"],
					  [1504097,"Gerby","Sudoy","Edrial","gerby.edrial@mitsukoshimotors.com",7223333,"HEAD OFFICE","Driver","Spare Parts","Richmond Ngan","---"],
					  [1507052,"Dario","Lacar","Mingala Jr.","dario.mingalajr@mitsukoshimotors.com",7223333,"HEAD OFFICE","Technician","Spare Parts","Richmond Ngan","---"],
					  [8602001,"Pastor","Patricio","Sabino","pastor.sabino@mitsukoshimotors.com",7223333,"HEAD OFFICE","Warehouse Supervisor","Spare Parts","Richmond Ngan","---"],
					  [9305001,"Fernando","Gueriba","Mampo","fernando.mampo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Stockman","Spare Parts","Richmond Ngan","---"],
					  [9811001,"Luzvi","Tenorio","Orozco","luzvi.orozco@mitsukoshimotors.com",7223333,"HEAD OFFICE","Supervisor","Spare Parts","Richmond Ngan","---"],
					  [1407004,"Zemirah","Catubigan","Rodenas","zemirah.rodenas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Training Specialist","Training and Development","Richmond Ngan","---"],
					  [1505144,"Raymond","Quiñones","Onnagan","raymond.onnagan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Training Coordinator","Training and Development","Richmond Ngan","---"],
					  [1509006,"Hannah Grace","Garcia","Maldupana","hannahgrace.maldupana@mitsukoshimotors.com",7223333,"HEAD OFFICE","Training Specialist","Training and Development","Richmond Ngan","---"],
					  [205002,"Ma. Prea","Picar","Collas","maprea.collas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Payables)","Richmond Ngan","---"],
					  [1405017,"Krisialie","Albo","Tacalan","krisialie.tacalan@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Payables)","Richmond Ngan","---"],
					  [1502113,"Gia","Mission","Dy","gia.dy@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Payables)","Richmond Ngan","---"],
					  [105001,"Mae Christy","Bayog","Pido","maechristy.pido@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [506006,"Mary Ann","Tumazar","Gabuat","maryann.gabuat@mitsukoshimotors.com",7223333,"HEAD OFFICE","Team Leader","Treasury (Receivables)","Richmond Ngan","---"],
					  [1103180,"Debbie","Millamina","Magallon","debbie.magallon@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1104046,"Myla","Lavarrete","Doctolero","myla.doctolero@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1204080,"Leah","Artillaga","Silva","leah.silva@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1207193,"Joy","Frondozo","Pelo","joy.pelo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1305079,"Maribel","Mesta","Albao","maribel.albao@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1307107,"Mylene","Venus","Abadilla","mylene.abadilla@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1311048,"Sarah","De Guzman","Dichoso","sarah.dichoso@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1405003,"Charmaine","Gapate","Zaulda","charmaine.zaulda@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1405065,"Sarah Gaile","Sales","Reyes","sarahgaile.reyes@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1408048,"Cherry","Laguna","Alaba","cherry.alaba@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1410031,"Salome","De Guzman","Nevado","salome.nevado@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1501094,"Sheila May","Lazado","Bonilla","sheilamay.bonilla@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1503105,"Evelyn","Garces","Ronquillo","evelyn.ronquillo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1505029,"Hadasa","Encinas","Nueve","hadasa.nueve@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1505032,"Edsa Meriel","Ogsimer","Granadoso","edsameriel.granadoso@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1505104,"Eloisa","Battad","Domingo","eloisa.domingo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1505120,"Enday Nita","Lleno","Gonzales","endaynita.gonzales@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1506001,"Manilyn","Martinez","Villas","manilyn.villas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1506243,"Irene","Reyes","Balay","irene.balay@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1506313,"Margin","Pico","Burce","margin.burce@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1507195,"Pinky","Cea","Arizala","pinky.arizala@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1508128,"Geezel Janz","Acupan","Dimacisil","geezeljanz.dimacisil@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [1509110,"Mary Grace","Rodriguez","Rodelas","marygrace.rodelas@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [9005001,"Marivic","Yarte","Mampo","marivic.mampo@mitsukoshimotors.com",7223333,"HEAD OFFICE","Staff","Treasury (Receivables)","Richmond Ngan","---"],
					  [201001,"Rogelio","Gemina","Grabulan","rogelio.grabulan@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [402008,"Bernie","Idoy","Quirante","bernie.quirante@mitsukoshimotors.com",7223333,"WAREHOUSE","Helper","Warehouse Carmona","Victoriano Unay","---"],
					  [407002,"Ma. Carmhel","Crisostomo","Ero","macarmhel.ero@mitsukoshimotors.com",7223333,"WAREHOUSE","Warehouse Encoder","Warehouse Carmona","Victoriano Unay","---"],
					  [412011,"Julito","Ayuban","Asas","julito.asas@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [507011,"Michael","Catubig","Estampa","michael.estampa@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [607028,"Jeffrey","Sabiniano","Bacani","jeffrey.bacani@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver/Helper","Warehouse Carmona","Victoriano Unay","---"],
					  [804005,"Juderico","Olino","Penero","juderico.penero@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [805042,"Charlon","Senina","Cadotdot","charlon.cadotdot@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver/Helper","Warehouse Carmona","Victoriano Unay","---"],
					  [809039,"Renato","Gatcho","Agrida Jr.","renato.agridajr@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver/Helper","Warehouse Carmona","Victoriano Unay","---"],
					  [910036,"Edwin","Tercan","Altesing","edwin.altesing@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver/Helper","Warehouse Carmona","Victoriano Unay","---"],
					  [1109071,"Ronnel","Gito","Obina","ronnel.obina@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1110031,"Charlie","Abiza","Rañada","charlie.rañada@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1201124,"Roderick","Ayon","Compas","roderick.compas@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1202137,"Nicanor","Corpuz","Garcia Jr","nicanor.garciajr@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1203014,"Andro","Tenebroso","Lagrimas","andro.lagrimas@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1205187,"Mark Joseph","Credo","Picar","markjoseph.picar@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [1205259,"Robert","Santillan","Mangrobang Jr.","robert.mangrobangjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1205272,"Darwin","Cantarona","Isaal","darwin.isaal@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1206050,"Kimwell","Polintan","Torres","kimwell.torres@mitsukoshimotors.com",7223333,"WAREHOUSE","Trucking","Warehouse Carmona","Victoriano Unay","---"],
					  [1304055,"Fernando","Gabas","Dela Peña","fernando.delapeña@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1306077,"Ronald","Padua","Tagalog","ronald.tagalog@mitsukoshimotors.com",7223333,"WAREHOUSE","Warehouse - Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1310115,"Rommel","Lagrosa","Lampara","rommel.lampara@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1401028,"Harvin","Morales","Revilla","harvin.revilla@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1402074,"Rey","Diay","Laos","rey.laos@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1402212,"Markwin","Aranda","Tagle","markwin.tagle@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [1403077,"Ariel","Colets","Vijuan","ariel.vijuan@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1403112,"Ronnie","-","Timkang","ronnie.timkang@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1403200,"Ernie","Flores","Belegantol","ernie.belegantol@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1403203,"Andy","Sedeño","Vega","andy.vega@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1408073,"Pascual","Cabanias","Dagala","pascual.dagala@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1408089,"Renato","Lucelo","Pamanian","renato.pamanian@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1411283,"Juanito","Belen","Castro","juanito.castro@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1412177,"Charisel","Tumaneng","Guillermo","charisel.guillermo@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1412178,"Romaldo","Quivedo","Rivas Jr.","romaldo.rivasjr@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1503107,"Giraulie","Cerezo","Osma","giraulie.osma@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [1503178,"Abundio","Gonzales","Edem","abundio.edem@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1503244,"Edwin","Emano","Manabat","edwin.manabat@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [1503276,"Saturnino","Sain","Fetalvo Jr.","saturnino.fetalvojr@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1505066,"Dennis","Ballenjare","Bumaya","dennis.bumaya@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1505067,"Bernardo","Guris","Pucio","bernardo.pucio@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1505207,"Wilfredo","Levardo","Restrivera","wilfredo.restrivera@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [1506339,"Glenn","Basatan","Reyes","glenn.reyes@mitsukoshimotors.com",7223333,"WAREHOUSE","Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [1507253,"Leoreto","Encabo","Reyes","leoreto.reyes@mitsukoshimotors.com",7223333,"WAREHOUSE","Warehouse Staff","Warehouse Carmona","Victoriano Unay","---"],
					  [7903001,"Tranquilino","Dones","Fullente Jr.","tranquilino.fullentejr@mitsukoshimotors.com",7223333,"WAREHOUSE","Warehouse Supervisor","Warehouse Carmona","Victoriano Unay","---"],
					  [8903001,"Anito","Jumamoy","Sabandal","anito.sabandal@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [9206001,"Alex","Planco","Dones","alex.dones@mitsukoshimotors.com",7223333,"WAREHOUSE","Driver","Warehouse Carmona","Victoriano Unay","---"],
					  [9604001,"Gene","Ponce","Ero","gene.ero@mitsukoshimotors.com",7223333,"WAREHOUSE","Warehouse Assistant/Delivery Helper","Warehouse Carmona","Victoriano Unay","---"]
					];

		
		set_time_limit(1000);
		$this->db->begin();
		for ($i=0; $i < sizeof($data); $i++) 
		{ 

			$my_data = array(
				'employee_id' => $data[$i][0],
				'firs_name' => $data[$i][1],
				'middle_name' => $data[$i][2],
				'last_name' => $data[$i][3],
				'email' => $data[$i][4],
				'contact_num' => $data[$i][5],
				'location' => $data[$i][6],
				'designation' => $data[$i][7],
				'department' => $data[$i][8],
				'immediate_superior' => $data[$i][9],
			);

			$loc = RefLocation::findFirst(array(
					  'conditions' => 'LOWER(name) = ?1',
					  'bind' => array(1=>strtolower($my_data['location']))
				   ));

			if(!$loc){
				$loc = new RefLocation();
				$loc->name = $my_data['location'];
				$loc->description = $my_data['location'];
				$loc->created_by = 'Gene';
				$loc->status_id = 1;
				$loc->date_updated = CURR_DATE;
				if(!$loc->save()){
					foreach ($loc->getMessages() as $msg) {
						echo $msg;
					}
				}
			}

			$dep = RefDepartment::findFirst(array(
						'conditions' => 'LOWER(name) = LOWER(?1)',
						'bind' => array(1=>strtolower($my_data['department']))
				   ));	

			if(!$dep){
				$dep = new RefDepartment();
				$dep->name = $my_data['department'];
				$dep->description = $my_data['department'];
				$dep->created_by = 'Gene';
				$dep->status_id = 1;
				$dep->date_updated = CURR_DATE;
				if(!$dep->save()){
					foreach ($dep->getMessages() as $msg) {
						echo $msg;
					}
				}
			}

			$des = RefDesignation::findFirst(array(
						'conditions' => 'LOWER(name) = LOWER(?1)',
						'bind' => array(1=>strtolower($my_data['designation']))
				   ));	

			if(!$des){
				$des = new RefDesignation();
				$des->name = $my_data['designation'];
				$des->description = $my_data['designation'];
				$des->created_by = 'Gene';
				$des->status_id = 1;
				$des->date_updated = CURR_DATE;
				if(!$des->save()){
					foreach ($des->getMessages() as $msg) {
						echo $msg;
					}
				}
			}


			$my_data['location_id'] = $loc->location_id;
			$my_data['department_id'] = $dep->department_id;
			$my_data['designation_id'] = $des->designation_id;

			$user_access_data = array(
	            'employee_id' => $my_data['employee_id'],
	            "username" => substr($my_data['firs_name'], 0,1).substr($my_data['last_name'], 0,1).$my_data['employee_id'],
	            'password' => $this->helper->randomPassword(),
	            'security_key' => $this->helper->generateToken(),
	            'validated' => 'Sample',
	            'creator_id' => '200513977',
	            'position_id' => 1,
	            'immediate_superior_id' => '200513977',
	            'status_id' => 1,
	            'cluster_mgt' => 1,
	        );
        	$merge_data = array_merge($my_data,$user_access_data);
	    	
	    	$result = new MmpiTableOfOrganization();
	    	if(!$result->save($merge_data)){
	    		$this->db->rollback();
	    		echo 'error at line '.$i."<br>";
	    		$err_msg = "";
	            foreach ($result->getMessages() as $value) {
	                $err_msg .= $value."<br>";
	            }
	    		echo $error;
	    		$this->view->disable();
	    		exit();
	    	}

	        $result = new MmpiUserAccess();
	    	if(!$result->save($merge_data)){
	    		$this->db->rollback();
	    		echo 'error at line '.$i."<br>";
	    		$err_msg = "";
	            foreach ($result->getMessages() as $value) {
	                $err_msg .= $value."<br>";
	            }
	    		echo $error;
	    		$this->view->disable();
	    		exit();
	    	}
	        

		}
		//$this->db->commit();
	    $this->helper->_echoJson(1,'Succes',$data);


    }
    
}

