<?php

class SettingsController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {

    }

    public function addAction()
    {
    	$data = $this->request->getPost();

    	$this->db->begin();

    	$holiday = new RefHoliday();
    	$holiday->holiday_name = $data['holiday_name'];
        $holiday->holiday_date = date('Y-m-d',  strtotime($data['holiday_date']));
    	$holiday_date_to_insert = date('Y-m-d',  strtotime($data['holiday_date']));
        $check = RefHoliday::find("date(holiday_date) = date('$holiday_date_to_insert')");
        
        if(sizeof($check) > 0)
        {
    		$this->helper->_echoJson(0,'Holiday date already defined.');
    		exit();            
        }
            
        
    	if(!$holiday->save()){
    		$this->db->rollback();
    		$err_msg = '';
    		foreach ($holiday->getMessages() as $value) {
    			$err_msg.=$value.'<br>';
    		}
    		$this->helper->_echoJson(0,$err_msg);
    		exit();
    	}


    	$this->db->commit();
    	$this->helper->_echoJson(1,$data['holiday_name'].' was saved successfully.',$data);
    }
    
        public function deleteAction()
    {
    	$data = $this->request->getPost();

    	$this->db->begin();

    	$holiday = new RefHoliday();
    	$holiday->holiday_name = $data['holiday_name'];
        $holiday->hid = $data['hid'];
    	
    	if(!$holiday->delete()){
    		$this->db->rollback();
    		$err_msg = '';
    		foreach ($holiday->getMessages() as $value) {
    			$err_msg.=$value.'<br>';
    		}
    		$this->helper->_echoJson(0,$err_msg);
    		exit();
    	}


    	$this->db->commit();
    	$this->helper->_echoJson(1,$data['holiday_name'].' was deleted successfully.',$data);
    }


    public function updateAction()
    {

    }
}


