<?php


class AdminController extends ControllerBase
{

    protected function initialize()
    {

        //var_dump(EmployeRequest::getRawReports()[0]->total_overdue);die;
        if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        } 
        else {
            $user = MmpiUserAccess::findFirst("employee_id='".EMPLOYEE_ID."'");
            if($user->admin != 1)
            {
                if($user->reports != 1)
                {
                    return $this->response->redirect(BASE_URI);
                }
            }
        }
        
 
    }
    
    public function indexAction()
    {
       
        $searchKey = $this->request->getPost('wf_search_key');
    	$this->view->setVar('page_content','admin/index');
        $workflows = RefWorkflow::getWorkflow($searchKey);
        $this->view->setVar('workflows',$workflows);
        $this->view->setVar('wf_search_key',$searchKey);
        
    }
    public function createAction()
    {

    	$this->view->setMainView('index');
    	$this->view->setVar('page_content','admin/create');
    }
    public function workflowManagementAction($action)
    {
        $this->view->setMainView('index');
        if($action == "suspendWorkflow")
        {
            $workflow_id = $this->request->getPost('workflow_id');
                $this->db->begin();
                
                $to = RefWorkflow::findFirst(array(
                            'conditions' => 'workflow_id = ?1',
                            'bind' => array(1 => $workflow_id))
                );
                $to->status = 3;
             
                if (!$to->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $workflow_id);
         
                    exit();
                }
  
                $this->db->commit();            
            
            
                $this->helper->_echoJson(1, '', $workflow_id);
        }
        else if($action == "activateWorkflow")
        {
            $workflow_id = $this->request->getPost('workflow_id');
                $this->db->begin();

                $to = RefWorkflow::findFirst(array(
                            'conditions' => 'workflow_id = ?1',
                            'bind' => array(1 => $workflow_id))
                );
                $to->status = 1;

                if (!$to->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $workflow_id);
         
                    exit();
                }

                $this->db->commit();            
            
            
                $this->helper->_echoJson(1, '', $employee_id);
        }
    }
    public function userManagementAction($action)
    {


        $this->view->setMainView('index');

        switch ($action) {
            case 'addUser':
                 $locations = RefLocation::find(array(
                        "order" => "name ASC"
                        ));
                 $designation = RefDesignation::find(array(
                        "order" => "name ASC"
                        ));
                 $departments = RefDepartment::find(array(
                        "order" => "name ASC"
                        ));
                 $this->view->setVar('locations',$locations);
                 $this->view->setVar('designation',$designation);
                 $this->view->setVar('departments',$departments);
                 $this->view->setVar('page_content','admin/addUser');
                break;
            case 'editUser':
                 if(!$this->request->isPost()) return $this->response->redirect(BASE_URI.'admin/userManagement');

                 $employee_id = $this->request->getPost('employee_id');
                 $details = MmpiTableOfOrganization::findFirstByEmployeeId($employee_id);
                 

                 if(!$details){
                    die('User not found');
                 }
                 $access = MmpiUserAccess::findFirstByEmployeeId($employee_id);
                 $locations = RefLocation::find(array(
                        "order" => "name ASC"
                        ));
                 $designation = RefDesignation::find(array(
                        "order" => "name ASC"
                        ));
                 $departments = RefDepartment::find(array(
                        "order" => "name ASC"
                        ));
                 $this->view->setVar('locations',$locations);
                 $this->view->setVar('designation',$designation);
                 $this->view->setVar('departments',$departments);
                 $this->view->setVar('page_content','admin/editUser');   
                 $this->view->setVar('details',$details);
                 $this->view->setVar('access',$access);
                break;
	case 'deleteUser':
		$employee_id = $this->request->getPost('employee_id');

                $this->db->begin();

                $to = MmpiTableOfOrganization::findFirst(array(
                            'conditions' => 'employee_id = ?1',
                            'bind' => array(1 => $employee_id))
                );

                $to->status_id = 3;

                if (!$to->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $employee_id);
                    exit();
                }

                $this->db->commit();

                $this->helper->_echoJson(1, '<strong>Deleting User'.$to->firs_name.' '.$to->last_name.  ' </strong>', $employee_id);
                break;
	case 'resetUser':
		$employee_id = $this->request->getPost('employee_id');

                $this->db->begin();

                $to = MmpiTableOfOrganization::findFirst(array(
                            'conditions' => 'employee_id = ?1',
                            'bind' => array(1 => $employee_id))
                );

                $to->status_id = 1;
                
                if (!$to->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $employee_id);
                    exit();
                }
                $user = MmpiUserAccess::findFirst(array(
                            'conditions' => 'employee_id = ?1',
                            'bind' => array(1 => $employee_id))
                );
                $password = $this->helper->randomPassword();
                $user->password= md5($password);
                if (!$user->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $employee_id);
                    exit();
                }
                $this->db->commit();
                //var_dump($user['username']."".$password,$to['email'])
                $this->helper->mailNotifResetPassword($user->username,$password,$to->email);
                $this->helper->_echoJson(1, '<strong>Reset Password for User'.$to->firs_name.' '.$to->last_name.  ' </strong>', $employee_id);
                break;
                                
            default:
                $searchKey = $this->request->getPost('user_search_key');
                $list = MmpiTableOfOrganization::getList($searchKey);
                $this->view->setVar('list',$list);  
                $this->view->setVar('user_search_key',$searchKey); 
                $this->view->setVar('page_content','admin/userManagement');
                break;
        }
       
    }
    public function groupManagementAction($action)
    {


        $this->view->setMainView('index');

        switch ($action) {
            case 'editGroup':
                 if(!$this->request->isPost()) return $this->response->redirect(BASE_URI.'admin/groupManagement');
                 $group_id = $this->request->getPost('group_id');

                 $group_details = RefGroup::getDetails($group_id);
                 if(sizeof($group_details) <= 0) return $this->response->redirect(BASE_URI.'admin/groupManagement');
                 
                 $group_data = RefGroup::getList($searchKey);
                 $group_id = isset($_GET['group_id']) ? $_GET['group_id'] : null;
                 $autocompletesource = RefsController::getSource();
                 $this->view->setVar('page_content','admin/editGroup');
                 $this->view->setVar('autocompletesource',$autocompletesource);
                 $this->view->setVar('group_details',$group_details);
                 $this->view->setVar('group_data',$group_data);
                 $this->view->setVar('group_id',$group_id);
                break;
                
            /*case 'deleteGroup':
                 if(!$this->request->isPost()) return $this->response->redirect(BASE_URI.'admin/groupManagement');
                 $group_id = $this->request->getPost('group_id');

                 //$autocompletesource = RefsController::getSource();
                 //$this->view->setVar('page_content','admin/groupManagement');
                 //$this->view->setVar('autocompletesource',$autocompletesource);
               
                break;*/
                
            case 'createGroup':
                 $autocompletesource = RefsController::getSource();
        
                 $this->view->setVar('page_content','admin/createGroup');
                 $this->view->setVar('autocompletesource',$autocompletesource);
                break;
			
			case 'deleteGroup':
				$group_id = $this->request->getPost('group_id');
				
				$this->db->begin();
				
				$group = RefGroup::findFirst(array(
                                    'conditions' => 'group_id = ?1',
                                    'bind' => array(1 => $group_id))
                                    );
                  
				$group->status_id=0;
				
				if(!$group->save()){
					$this->db->rollback();
					$err_msg = '';
					foreach ($group->getMessages() as $value) {
						$err_msg.=$value.'<br>';
					}
					$this->helper->_echoJson(0,$err_msg,$request_id);
					exit();
				}
				
				$this->db->commit();
				
				$this->helper->_echoJson(1,'<strong>Group </strong> has been deleted',$group_id);
				break;
            
            default:
                $searchKey = $this->request->getPost('group_search_key');
                $group_data = RefGroup::getList($searchKey);
                $group_id = isset($_GET['group_id']) ? $_GET['group_id'] : null;
                $group_details = array();

                if($group_id == null){

                    if(sizeof($group_data)){
                        
                        $group_id = $group_data[0]->group_id;
                        $group_details = RefGroup::getDetails($group_data[0]->group_id);
                    }    
                }else{
                    if(sizeof($group_data)){
                        $group_details = RefGroup::getDetails($group_id);
                    }    
                }
                //var_dump(count($group_data)); die;
                $this->view->setVar('page_content','admin/groupManagement');
                $this->view->setVar('group_data',$group_data);
                $this->view->setVar('group_details',$group_details);
                $this->view->setVar('group_id',$group_id);
                $this->view->setVar('group_search_key',$searchKey);
                break;
        }

    }
    
    public function requestManagementAction($action)
    {
  
        $this->view->setMainView('index');
        
        
        if($action == "deleteRequest")
        {
                
                $request_id = $this->request->getPost('request_id');
               
                $this->db->begin();

                $to = EmployeRequest::findFirst(array(
                            'conditions' => 'request_id = ?1',
                            'bind' => array(1 => $request_id))
                );

                $to->status = 4;

                if (!$to->save()) {
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value . '<br>';
                    }
                    $this->helper->_echoJson(0, $err_msg, $request_id);
                    exit();
                }
                  
                $this->db->commit();
                $this->helper->_echoJson(1,'<strong>Group </strong> has been deleted',$request_id);  
        }
    }      
    public function settingsAction()
    {

        $this->view->setMainView('index');
        $this->view->setVar('page_content','admin/settings');
    }


    public function reportsAction()
    {
        if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }         
        $from = '';
        $to = '';
        $status = 0;
        $initiator_id = 0;
        $department_id = (DEPT_ID > 0) ? DEPT_ID : 0;

        if($this->request->isPost()){
            $data = $this->request->getPost();
            $from = isset($data['from']) ? $data['from'] : '';
            $to = isset($data['to']) ? $data['to'] : '';
            $status = isset($data['status']) ? (int) $data['status'] : 0;
            $initiator_id = isset($data['initiator']) ?  $data['initiator'] : "";
            $workflow_id= isset($data['workflow_name']) ? (int) $data['workflow_name'] : 0;
           
        }
        
        $reports = EmployeRequest::getReports($from,$to,$status,$initiator_id,DEPT_ID,$workflow_id);

        $total_requests = (sizeof($reports) > 0) ? $reports[0]->total : 0;
        $total_completed = (sizeof($reports) > 0) ? $reports[0]->completed : 0;
        $total_in_progress = (sizeof($reports) > 0) ? $reports[0]->in_progress : 0;
        $total_rejected = (sizeof($reports) > 0) ? $reports[0]->rejected : 0;
        $total_overdue = (sizeof($reports) > 0) ? $reports[0]->overdue : 0;
        $completion_days = (sizeof($reports) > 0) ? $reports[0]->completion_days : 0;
        //var_dump( $total_requests);die();
        $initiator = EmployeRequest::getInitiator();
        $report_details = EmployeRequest::getReportsDetails($from,$to,$status,$initiator_id,DEPT_ID,$workflow_id);
        
        $this->view->setVar('avg',$completion_days/sizeof($report_details));
        $this->view->setVar('total_overdue',$total_overdue); 
        $this->view->setVar('total_rejected',$total_rejected);
        $this->view->setVar('total_requests',$total_requests);
        $this->view->setVar('total_in_progress',$total_in_progress);
        $this->view->setVar('total_completed',$total_completed); 
        $this->view->setVar('reports',$report_details);
        $this->view->setVar('from',$from);
        $this->view->setVar('to',$to);
        $this->view->setVar('status',$status);
        $this->view->setVar('initiator_id',$initiator_id);
        $this->view->setVar('initiator',$initiator);
        $this->view->setVar('wfid',$workflow_id);
        $this->view->setMainView('index');
        $this->view->setVar('page_content','admin/reports');
    }
    public function create_workflowAction()
    {

        $workflow_id = ($this->request->getPost('workflow_id') != null) ? (int) $this->request->getPost('workflow_id') : 0;
        
        $workflow_details = RefWorkflow::findFirstByWorkflowId($workflow_id);
        
        $this->view->setVar('workflow_details',$workflow_details);
        $this->view->setVar('workflow_id',$workflow_id);
    
        $grouplist = RefGroup::find("status_id <> 0");

        
        $categories = RefCategory::find();
        $departments = RefDepartment::find(); 
        $dataTypes = RefDataType::find();
        $data = MmpiTableOfOrganization::getList();
		$result =array();
        if(sizeof($data) > 0){
            
            foreach ($data as $value) {
                $list = array();
                $list['label'] = $value->firs_name.' '.$value->last_name.', '.$value->designation;
                $list['href'] = '';
                $list['employee_id'] = $value->employee_id;
                $list['email'] = $value->email;
                $list['designation'] = $value->designation;
                $list['fullname'] = $value->firs_name.' '.$value->last_name;
                $result[] = $list;
            }
        }
        
	$dataGroups = RefGroup::getList();
        $resultGroups = array();
        if (sizeof($dataGroups) > 0) {
            foreach ($data as $value) {
                $list = array();
                $list['group_id'] = $value->group_id;
                $list['group_name'] = $value->name;
                $resultGroups[] = $list;
            }
        }

        $results['data'] = $result;
        $results['dataGroups'] = $resultGroups;
        $this->view->setVar('search_list', json_encode($results));
        $this->view->setVar('categories', $categories);

        $this->view->setVar('grouplist',$grouplist);

        $this->view->setVar('departments', $departments);


        $this->view->setVar('dataTypes', $dataTypes);
        $this->view->setMainView('index');
        $this->view->setVar('page_content', 'admin/create_workflow');
    }
    public function create_from_existing_workflowAction()
    {
        $categories = RefCategory::find();
        $dataTypes = RefDataType::find();
        $data = MmpiTableOfOrganization::getList();
        $result =array();
        if(sizeof($data) > 0){
            
            foreach ($data as $value) {
                $list = array();
                $list['label'] = $value->firs_name.' '.$value->last_name.', '.$value->designation;
                $list['href'] = '';
                $list['employee_id'] = $value->employee_id;
                $list['email'] = $value->email;
                $list['designation'] = $value->designation;
                $list['fullname'] = $value->firs_name.' '.$value->last_name;
                $result[] = $list;
            }
        }
        $results['data'] = $result;
        $this->view->setVar('search_list',json_encode($results)) ;
        $this->view->setVar('categories',$categories);
        $this->view->setVar('dataTypes',$dataTypes);
        $this->view->setMainView('index');
        $this->view->setVar('page_content','admin/create_from_existing_workflow');                   
    }
    public function edit_existing_workflowAction()
    {
        $categories = RefCategory::find();
        $dataTypes = RefDataType::find();
        $data = MmpiTableOfOrganization::getList();
        $result =array();
        if(sizeof($data) > 0){
            
            foreach ($data as $value) {
                $list = array();
                $list['label'] = $value->firs_name.' '.$value->last_name.', '.$value->designation;
                $list['href'] = '';
                $list['employee_id'] = $value->employee_id;
                $list['email'] = $value->email;
                $list['designation'] = $value->designation;
                $list['fullname'] = $value->firs_name.' '.$value->last_name;
                $result[] = $list;
            }
        }
        $results['data'] = $result;
        $this->view->setVar('search_list',json_encode($results)) ;
        $this->view->setVar('categories',$categories);
        $this->view->setVar('dataTypes',$dataTypes);
        $this->view->setMainView('index');
        $this->view->setVar('page_content','admin/edit_existing_workflow');                   
    }
    
    public function requestListAction()    
    {
        

           $request_id = $this->request->getPost('request_id');
           //$request_id = isset($_GET['request_id']) ? $_GET['request_id'] : null;
           //$request_details = EmployeRequest::getRequestDetails($request_id); 
           //$this->session->remove("report_list_session");
           //echo "asdddd"+$request_id;die();
           $this->session->set("report_list_session",$request_id);
           //$this->view->setVar('report_list',$request_id) ;
          // $this->view->setMainView('index');
           //$this->view->setVar('page_content','admin/reports'); 
            $this->view->setVar('report_list',$request_id) ;
            $this->view->setVar('page_content','admin/reports');

            $this->db->begin();

            
            $to = MmpiTableOfOrganization::findFirst("employee_id='".EMPLOYEE_ID."'");
            $to->request_id = $request_id;
            $saving = $to->save();
                 if(!$saving)
                {
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value."<br>";
                    }
                    $this->db->rollback();
                    $this->helper->_echoJson(0,$err_msg);
                    exit();
                }              
            $this->db->commit();
            //echo "1";
            $this->helper->_echoJson($request_id,$message,'');  

    }    
}

