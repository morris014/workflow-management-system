<?php

class ConverterController extends \Phalcon\Mvc\Controller {
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function pdfAction() {

        function cleanData(&$str) {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if (strstr($str, '"'))
                $str = '"' . str_replace('"', '""', $str) . '"';
        }

        $filename = "wms_" . date('Ymd') . ".xls";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        header('Content-Length: ' . filesize($filename));
        header("Pragma: no-cache");
        header("Expires: 0");        
        $data = $_GET['data'];
        $data = json_decode($data, true);

        //var_dump($data);die();
        /*    $new_data = array(
          array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25),
          array("firstname" => "Amanda", "lastname" => "Miller", "age" => 18),
          array("firstname" => "James", "lastname" => "Brown", "age" => 31),
          array("firstname" => "Patricia", "lastname" => "Williams", "age" => 7),
          array("firstname" => "Michael", "lastname" => "Davis", "age" => 43),
          array("firstname" => "Sarah", "lastname" => "Miller", "age" => 24),
          array("firstname" => "Patrick", "lastname" => "Miller", "age" => 27)
          ); */



        $flag = false;
        foreach ($data as $row) {
            if (!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, 'cleanData');
                echo implode("\t", array_values($row)) . "\r\n";
        }

        exit;
    }    
    public function excelAction() {

        function cleanData(&$str) {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if (strstr($str, '"'))
                $str = '"' . str_replace('"', '""', $str) . '"';
        }

        $filename = "wms_" . date('Ymd') . ".xls";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");
        header('Content-Length: ' . filesize($filename));
        header("Pragma: no-cache");
        header("Expires: 0");        
        $data = $_GET['data'];
        $data = json_decode($data, true);

        //var_dump($data);die();
        /*    $new_data = array(
          array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25),
          array("firstname" => "Amanda", "lastname" => "Miller", "age" => 18),
          array("firstname" => "James", "lastname" => "Brown", "age" => 31),
          array("firstname" => "Patricia", "lastname" => "Williams", "age" => 7),
          array("firstname" => "Michael", "lastname" => "Davis", "age" => 43),
          array("firstname" => "Sarah", "lastname" => "Miller", "age" => 24),
          array("firstname" => "Patrick", "lastname" => "Miller", "age" => 27)
          ); */



        $flag = false;
        foreach ($data as $row) {
            if (!$flag) {
                // display field/column names as first row
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, 'cleanData');
                echo implode("\t", array_values($row)) . "\r\n";
        }

        exit;
    }
    
    public function csvAction() {

        function cleanData(&$str) {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if (strstr($str, '"'))
                $str = '"' . str_replace('"', '""', $str) . '"';
        }

        $filename = "wms_" . date('Ymd') . ".csv";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv");
        $data = $_GET['data'];
        $data = json_decode($data, true);


        /*    $new_data = array(
          array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25),
          array("firstname" => "Amanda", "lastname" => "Miller", "age" => 18),
          array("firstname" => "James", "lastname" => "Brown", "age" => 31),
          array("firstname" => "Patricia", "lastname" => "Williams", "age" => 7),
          array("firstname" => "Michael", "lastname" => "Davis", "age" => 43),
          array("firstname" => "Sarah", "lastname" => "Miller", "age" => 24),
          array("firstname" => "Patrick", "lastname" => "Miller", "age" => 27)
          ); */


        header("Content-Type: text/plain");

        $flag = false;
        foreach ($data as $row) {
            if (!$flag) {
                // display field/column names as first row
                echo implode(",", array_keys($row)) . "\r\n";
                $flag = true;
            }
            array_walk($row, 'cleanData');
            echo implode(",", array_values($row)) . "\r\n";
        }

        exit;
    }
}
