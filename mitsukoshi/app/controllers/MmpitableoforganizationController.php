<?php

class MmpitableoforganizationController extends ControllerBase
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {

    }
    public function editAction()
    {
        //if(!$this->request->isPost()) $this->helper->_echoJson(0,'Invalid access',[]);
        $return = 1;
        $data = $this->request->getPost();
        $original_employee_id = $data['original_employee_id'];
        
        $this->db->begin();
        $to = MmpiTableOfOrganization::findFirstByEmployeeId($original_employee_id);
        $email =  $data['email'];
        //echo $email."-".$to->email;die();
        if($data['email'] != $to->email)
        {
            $tos = MmpiTableOfOrganization::findFirst("lower(email)=lower('".$data['email']."')");
            if($tos->email != "")
            {
                $return = 0;
            }else
            {
                $return = 1;
            }
        }
        
        
        if($return == 1)
        {
            $to->firs_name = $data['firs_name'];
            $to->middle_name = $data['middle_name'];
            $to->last_name = $data['last_name'];
            $to->designation_id = $data['designation_id'];
            $to->department_id = $data['department_id'];
            $to->location_id = $data['location_id'];
            $to->email = $data['email'];
            $to->contact_num = 0;








            if(!$to->save()){
                $err_msg = '';
                foreach ($to->getMessages() as $value) {
                    $err_msg.=$value."<br>";
                }
                $this->db->rollback();
                $this->helper->_echoJson(0,$err_msg);
                exit();
            }

            $ua = MmpiUserAccess::findFirstByEmployeeId($original_employee_id);
            $ua->task = $data['task'];
            $ua->request = $data['request'];
            $ua->admin = $data['admin'];
            $ua->user_mgt = $data['user_mgt'];
            $ua->cluster_mgt = 1;
            $ua->flow_mgt = $data['flow_mgt'];
            $ua->settings = $data['settings'];
            $ua->group_mgt = $data['group_mgt'];
            $ua->reports = $data['reports'];

            if(!$ua->save()){
                $err_msg = '';
                foreach ($ua->getMessages() as $value) {
                    $err_msg.=$value."<br>";
                }
                $this->db->rollback();
                $this->helper->_echoJson(0,$err_msg);
                //var_dump($err_msg);die();
                exit();
            }
            $this->db->commit();

            /*
             *  SEND EMAIL
            */
            $this->helper->mailNotifUserRegistration($user_access_data['username'],$user_access_data['password'],$merge_data['email']);
            $this->helper->_echoJson($return,'<strong>Details of '.$data['firs_name'].' '.$data['last_name'].'</strong> has been updated.',$data);
        }else
        {
            $this->helper->_echoJson($return,'This email address already exists.',$data);
        }
        

    }
    public function registerAction()
    {
    	//if(!$this->request->isPost()) $this->helper->_echoJson(0,'Invalid access',[]);
        $return = 1;
        $data = $this->request->getPost();
        $date = new DateTime();
        $employee_id = $date->format('YmdHis');
        
        $temp_username = strtoupper(substr($data['firs_name'], 0,1).$data['last_name']);
        $user = $to = MmpiUserAccess::find(array(
                        'conditions' => "username like '".$temp_username."%'",
                    ));
    
        
        $tos = MmpiTableOfOrganization::findFirst("lower(email)=lower('".$data['email']."')");
        //echo $tos->email;die();
        if($tos->email == "")
        {
            $return = 1;
        }
        else
        {
            $return = 0;
        }
        if($return == 1)
        {
            if(sizeof($user) > 0)
            {
                $username = $temp_username . (sizeof($user)+1);
            }
            else
            {
                $username = $temp_username;
            }
            $password = $this->helper->randomPassword();
            $user_access_data = array(
                'employee_id' => $employee_id,
                "username" => $username,
                'password' => md5($password),
                'security_key' => $this->helper->generateToken(),
                'validated' => 'Sample',
                'creator_id' => '200513977',
                'position_id' => 1,
                'immediate_superior_id' => '200513977',
                'status_id' => 1,//first time
                'cluster_mgt' => 1,
                'contact_num' => 0,
            );

            $merge_data = array_merge($data,$user_access_data);
            $this->db->begin();
            $result = MmpiTableOfOrganization::register($merge_data);
            if(!$result['status']){
                    $this->db->rollback();
                    echo json_encode($result);
                    $this->view->disable();
                    exit();
            }
            $result = MmpiUserAccess::register($merge_data);
            if(!$result['status']){
                $this->db->rollback();
                echo json_encode($result);
                $this->view->disable();
                exit();
            }

            $this->db->commit();
            /*
             *  SEND EMAIL
            */
            $this->helper->mailNotifUserRegistration($user_access_data['username'],$password,$merge_data['email']);

            $this->helper->_echoJson(1,'<strong>'.$merge_data['firs_name'].' '.$merge_data['last_name'].'</strong> is now added to the Workflow Management System.',$data);
        }
        else
        {
            $this->helper->_echoJson(0,'This email address already exists.',$data);
        }
    }
    
    public function isEmailExistingAction()
    {
    	//if(!$this->request->isPost()) $this->helper->_echoJson(0,'Invalid access',[]);

        $data = $this->request->getPost();
        $return = 0; 
        
        if($data['employee_id'] == NULL)
        {
            $to = MmpiTableOfOrganization::findFirst("email='".$data['email']."'");
            if($to->email != "")
            {
                $return = 1;
            }
        }
        else
        {
            //var_dump($data['employee_id'] );die(); 
            $to = MmpiTableOfOrganization::findFirst("email='".$data['email']."' and employee_id='".$data['employee_id']."'");
            if($to->email != "")
            {
                $return = 0;
            }           
            else
            {
                $to = MmpiTableOfOrganization::findFirst("email='".$data['email']."'");
                if($to->email != "")
                {
                    $return = 1;
                }
            }
        }
        $this->helper->_echoJson($return );

    }    
}

