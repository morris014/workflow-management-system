<?php

class RefworkflowController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {
  

    }
    public function saveAction()
    {
    	$data = $this->request->getPost();
        $error = 0;
    	$approvers = isset($data['approvers']) ? $data['approvers'] : [];
    	$fields = isset($data['fields']) ? $data['fields'] : [];
    	$activities = isset($data['activities']) ? $data['activities'] : [];
        
        
    	$total_tat = 0;
    	for ($i=0; $i < sizeof($activities); $i++) { 
    		$total_tat += $activities[$i]['tat'];
    	} 
    	for ($i=0; $i < sizeof($approvers); $i++) { 
    		$total_tat += $approvers[$i]['tat'];
    	} 

        $workflow_id = (int) $data['workflow_id'];
        $dep_id = $data['department_id'];
        $department_access = implode(',',$data['department_access']);
        $advisory_id = $data['advisory'];
        
        if($workflow_id == 0)
        {
            $find_wf = RefWorkflow::findFirst("lower(name)=lower('".$data['name']."')");
        
            if($find_wf->name != "")
            {
                    
                    $this->helper->_echoJson(0,'Workflow name already existing.');
                    //exit();              
            }
        }
        else
        {
            $find_wf = RefWorkflow::findFirst("lower(name)=lower('".$data['name']."') and workflow_id <> $workflow_id");
            if($find_wf->name != "")
            {
                    
                    $this->helper->_echoJson(0,'Workflow name already existing.');
                    //exit();              
            }            
            
        }
        
     
    	//$code = RefCategory::findFirstByCategoryId($data['category_id'])->code;
        $code = RefDepartment::findFirst("department_id = $dep_id")->code;
        $workflow_name = $data['name'];
    	$details = array(
    		'description' => $data['description'],
    		'name' => $data['name'],
    		'category_id' => $data['category_id'],
    		'creator_id' => EMPLOYEE_ID,
    		'status' => 1,
    		'tat'=> $total_tat,
    		'ref_no' =>$code.$this->helper->generateId(),
                'priority' =>  $data['priority'],
                'department_id' => $data['department_id'],
                'department_access' => $department_access
    	);
    	

    	$this->db->begin();
        if($workflow_id == 0){
            $workflow = new RefWorkflow();    

            $details = array(
                    'description' => $data['description'],
                    'name' => $data['name'],
                    'category_id' => $data['category_id'],
                    'creator_id' => EMPLOYEE_ID,
                    'status' => 1,
                    'tat'=> $total_tat,
                    'ref_no' =>$code.$this->helper->generateId(),
                    'priority' =>  $data['priority'],
                    'department_id' => $data['department_id'],
                    'department_access' => $department_access
            );            
        }else{
            $workflow = RefWorkflow::findFirstByWorkflowId($workflow_id);

            $details = array(
                    'description' => $data['description'],
                    'name' => $data['name'],
                    'category_id' => $data['category_id'],
                    'creator_id' => EMPLOYEE_ID,
                    'status' => 1,
                    'tat'=> $total_tat,
                    'ref_no' =>$workflow->ref_no,
                    'priority' =>  $data['priority'],
                    'department_id' => $data['department_id'],
                    'department_access' => $department_access

            );            
            RefWorkflow::deleteRelated($workflow_id);
        }
    	
    	if(!$workflow->save($details)){
    		$this->db->rollback();
    		$err_message = '';
    		foreach ($workflow->getMessages() as $value) {
    			$err_message.=$value.'<br>';
    		}
    		$this->helper->	_echoJson(0,$err_message);	
    		exit();
    	}
    	$workflow_id = $workflow->workflow_id;
        $last_activity_name = $activities[sizeof($activities)-1]['activity_name'];

	for ($i=0; $i < sizeof($activities); $i++) { 
                $array_id = explode("-",$activities[$i]['approver_id']);
                $employee_id_1 = $array_id[0];
                $employee_id_2 = $array_id[1];

                if($employee_id_1 == "TEAM")
                {
                    $activity_details = array(
                            'workflow_id' => $workflow_id,
                            'tat' => $activities[$i]['tat'],
                            'activity_name' => $activities[$i]['activity_name'],
                            'group_id' => $employee_id_2,
                            'sequence' => $activities[$i]['sequence'],
                    );  
                    if($advisory_id == 1)
                    {
                        $member = Members::find("group_id = ".$employee_id_2."");
                        //var_dump($member);die();
                        foreach($member as $value):
                            $members = $member->member_id;
                            $email = MmpiTableOfOrganization::find("employee_id = '$members'");
                            $this->helper->mailNotifCreateWorkflow($email->email,$workflow_name);
                        endforeach;
                    }
                }
                else
                {
                    $activity_details = array(
                            'workflow_id' => $workflow_id,
                            'tat' => $activities[$i]['tat'],
                            'activity_name' => $activities[$i]['activity_name'],
                            'employee_id' => $activities[$i]['approver_id'],
                            'sequence' => $activities[$i]['sequence'],
                    );
                    if($advisory_id == 1)
                    {

                            $email = MmpiTableOfOrganization::find("employee_id = '".$activities[$i]['approver_id']."'");
                            $this->helper->mailNotifCreateWorkflow($email->email,$workflow_name);
                    }                    
                }
    		$activity = new WorkflowActivity();
    		if(!$activity->save($activity_details)){
    			$this->db->rollback();
	    		$err_message = '';
	    		foreach ($activity->getMessages() as $value) {
	    			$err_message.=$value.'<br>';
	    		}
	    		$this->helper->	_echoJson(0,$err_message);	
	    		exit();
    		}
    	} 

        /************************************JAMES*********/
        
        //echo "--->".$last_activity_name;die;
            if($last_activity_name != "Acknowledge Request")
            {
            	$activity_details = array(
    			'workflow_id' => $workflow_id,
    			'tat' => 1,
    			'activity_name' => 'Acknowledge Request',
    			'employee_id' => 0,
    			'sequence' => sizeof($activities)+1,
    		);

            $activity = new WorkflowActivity();
    		if(!$activity->save($activity_details)){
    			$this->db->rollback();
	    		$err_message = '';
	    		foreach ($activity->getMessages() as $value) {
	    			$err_message.=$value.'<br>';
	    		}
	    		$this->helper->	_echoJson(0,$err_message);	
	    		exit();
    		}      
            }
        /****************************************JAMES***********/
	for ($i=0; $i < sizeof($approvers); $i++) { 
            $array_id = explode("-",$approvers[$i]['approver_id']);
            $employee_id_1 = $array_id[0];
            $employee_id_2 = $array_id[1];
            if($employee_id_1 == "A")
            {
    		$approvers_details = array(
    			'workflow_id' => $workflow_id,
    			'tat' => $approvers[$i]['tat'],
    			'approver_mgt_line_id' => $employee_id_2,
    			'sequence' => $approvers[$i]['sequence'],
    		);
            }
            else
            {
     		$approvers_details = array(
    			'workflow_id' => $workflow_id,
    			'tat' => $approvers[$i]['tat'],
    			'approver_id' => $approvers[$i]['approver_id'],
    			'sequence' => $approvers[$i]['sequence'],
    		);               
            }
                if ($advisory_id == 1 && $employee_id_1!="A") {

                    $email = MmpiTableOfOrganization::find("employee_id = '" . $approvers[$i]['approver_id'] . "'");
                    $this->helper->mailNotifCreateWorkflow($email->email, $workflow_name);
                }                   
    		$approver = new WorkflowApprover();
    		if(!$approver->save($approvers_details)){
    			$this->db->rollback();
	    		$err_message = '';
	    		foreach ($approver->getMessages() as $value) {
	    			$err_message.=$value.'<br>';
	    		}
	    		$this->helper->	_echoJson(0,$err_message);	
	    		exit();
    		}
    	} 

    	$table = new WorkflowTable();
    	$table->workflow_id = $workflow_id;
    	if(!$table->save()){
    		$this->db->rollback();
	    	$err_message = '';
    		foreach ($table->getMessages() as $value) {
    			$err_message.=$value.'<br>';
    		}
    		$this->helper->	_echoJson(0,$err_message);	
	    	exit();
    	}
    	$table_id = $table->table_id;

    	for ($i=0; $i < sizeof($fields); $i++) { 
    		$fields_details = array(
    			'table_id' => $table_id,
    			'required' => $fields[$i]['required'],
    			'data_type_id' => $fields[$i]['data_type_id'],
    			'label' => $fields[$i]['label'],
    		);
    		$field = new TableFields();
            $field_items = array();
            if($fields[$i]['data_type_id'] == "3"){
                $items_str = $fields[$i]['items'];
                $item_arr = explode(',', $items_str);
                for ($y=0; $y < sizeof($item_arr) ; $y++) { 
                     $dd = new RefDropdown();
                     $dd->item = $item_arr[$y];
                     $field_items[] = $dd;       
                }
                $field->RefDropdown = $field_items;
            }
            
    		if(!$field->save($fields_details)){
    			$this->db->rollback();
	    		$err_message = '';
	    		foreach ($field->getMessages() as $value) {
	    			$err_message.=$value.'<br>';
	    		}
	    		$this->helper->	_echoJson(0,$err_message);	
	    		exit();
    		}
    	} 

    	$this->db->commit();
    	$this->helper->	_echoJson(1,'Your <strong>'.$workflow->name.'</strong> workflow with <strong>ID#'.$workflow->ref_no.'</strong><br> has been pubilshed',$fields[1]);

    }

}

