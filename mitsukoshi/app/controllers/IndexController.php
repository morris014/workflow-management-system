<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        
        $success = false;
        $error = false;
        $username_error = "";
        $password_error = "";
        $username = "";
        $password = "";
        $page_content = "";

    	if($this->request->isPost()){
    		$username = $this->request->getPost('username');
    		$password = $this->request->getPost('password');

            if(empty($username) || trim($username) == ''){

                $error = true;
                $success = false;
                $username_error = "Please type your USER ID";  
            }else if(empty($password) || trim($password) == ''){
                $error = true;
                $success = false;
                $password_error = "Please enter your PASSWORD"; 
            }else{
                $user = MmpiUserAccess::findFirst(array(
                        'conditions' => 'username = ?1 AND password = ?2',
                        'bind' => array(1 => $username, 2 => md5($password))
                        ));
                        
                if($user){
           
                    $this->registerSession($user);

                    $log = new UserLog();
                    $log->employee_id = $user->employee_id;
                    $log->session_id = session_id();
                    @$log->save(); 

                    $logs = UserLog::find(array(
                            'conditions' => 'employee_id = ?1',
                            'limit' => 3,
                            'order' => 'date_login DESC',
                            'bind' => array(1=>$user->employee_id)
                        ));
                    $this->session->set('logs',$logs);
                    
                    $success = true;
                    $error = false;
                    
                    if($user->admin){
                        $status = MmpiTableOfOrganization::findFirst(array(
                                        'conditions' => 'employee_id = ?1',
                                        'bind' => array(1=>$user->employee_id)
                                    ));
                         $this->session->set('user_name',$username);

                         if($status->status_id == 1)
                         {
                            return $this->response->redirect(BASE_URI.'change_password');
                         }              
                         else
                         {
                            return $this->response->redirect(BASE_URI.'admin');
                         }
                         
                    }else{
                        $status = MmpiTableOfOrganization::findFirst(array(
                                        'conditions' => 'employee_id = ?1',
                                        'bind' => array(1=>$user->employee_id)
                                    ));
                         $this->session->set('user_name',$username);         
                         if($status->status_id == 1)
                         {
                             return $this->response->redirect(BASE_URI.'change_password');
                         }
                         else {
                            return $this->response->redirect(BASE_URI.'task');
                         }
                        
                    }
                }else{
                    $error = true;
                    $username_error = "Invalid Username/Password";
                }    
            }
            

    	}
        $this->view->setVar('username',$username);
        $this->view->setVar('password',$password);
        $this->view->setVar('success',$success);
        $this->view->setVar('error',$error);
        $this->view->setVar('username_error',$username_error);
        $this->view->setVar('password_error',$password_error);
    	$this->view->setVar('page_content','index/index');
    }

    public function logoutAction(){
        $this->session->destroy();
        return $this->response->redirect(BASE_URI);
    }
}




 


/*$size = UserAccount::find();
set_time_limit(200);

for ($i=sizeof($size); $i <= 5000 ; $i++) { 
	$existing = true;
	while($existing){
		$num = rand(201513977,201598756);
		$password = $num;
		if(!UserAccount::findFirstByUsername($num)){
			$acc = new UserAccount();
			$acc->username = $num;
			$acc->password = $password;
			$existing = false;
			$acc->save();
			echo $acc->id. "<br>";
		}
	}
}   */	