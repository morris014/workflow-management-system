<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

	
	public  function registerSession($user){


		$this->session->set('employee_id',$user->employee_id);
		$this->session->set('task', $user->task);
		$this->session->set('request', $user->request);
		$this->session->set('admin', $user->admin);
		$this->session->set('user_mgt', $user->user_mgt);
		$this->session->set('cluster_mgt', $user->cluster_mgt);
		$this->session->set('flow_mgt', $user->flow_mgt);
		$this->session->set('settings', $user->settings);
		$this->session->set('group_mgt', $user->group_mgt);
		$this->session->set('reports', $user->reports);
		$this->session->set('username', $user->username);

	
		$meta = MmpiTableOfOrganization::findFirstByEmployeeId($user->employee_id);
	

		if($meta){
		
			
			$this->session->set('firs_name',$meta->firs_name);
			$this->session->set('last_name',$meta->last_name);
			$this->session->set('email', $meta->email);
			$this->session->set('designation', $meta->RefDesignation->name);
			$this->session->set('dept_code',$meta->RefDepartment->code);
			$this->session->set('department_id',$meta->RefDepartment->department_id);
		}

	}
}
