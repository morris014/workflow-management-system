<?php

class ChangePasswordController extends ControllerBase
{

    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    
    public function indexAction()
    {
        if ($this->request->isPost()){
            $success = false;
            $error = false;
            $password_error = "";
            $user_name = "";
            $oldPassword = $this->request->getPost('old_password');
            $newPassword = $this->request->getPost('new_password');
            $confirmPassword = $this->request->getPost('confirm_password');
            $user_name = $this->session->get('user_name');
            if(empty($oldPassword))
            {
                if(!$error)
                {
                    $error = true;
                    $password_error = "Old Password Must not be empty";
                }
            }
            if(empty($newPassword))
            {
                if(!$error)
                {                
                    $error = true;
                    $password_error = "New Password Must not be empty";
                }
            }   
            if(empty($confirmPassword))
            {
                if(!$error)
                {
                    $error = true;
                    $password_error = "Confirm Password Must not be empty";
                }
            }     
            
            if($newPassword == $oldPassword)
            {
                if(!$error)
                {
                    $error = true;
                    $password_error = "You cannot use your old password";
                }
            }                 
            
            $user = MmpiUserAccess::findFirst(array(
                        'conditions' => 'username = ?1',
                        'bind' => array(1 => $user_name))
                        );
            $employee_id = $user->employee_id;
            $this->session->set('employee_id',$employee_id);
            if($user->password != md5($oldPassword))
            {
                if(!$error)
                {
                    $error = true;
                    $password_error = "Invalid Old Password";
                }               
            }
           
            
            if($newPassword == $confirmPassword && $error == false)
            {
                //update pwd in db
                $success = true;
                
                //$employee_id = substr($user_name, 2);
                
                
                
                $this->db->begin();
                $to = MmpiTableOfOrganization::findFirstByEmployeeId('104002');
                $to->status_id = 2;
 
                $this->db->commit();
      
               

                
                $this->db->begin();
                
                $to = MmpiTableOfOrganization::findFirstByEmployeeId($employee_id);
              
                
                $to->status_id = 2;
                $saving = $to->save();
                
                if(!$saving)
                {
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value."<br>";
                    }
                    $this->db->rollback();
                    $this->helper->_echoJson(0,$err_msg);
                    exit();
                }          
                
                $user->password = md5($confirmPassword);
                if(!$user->save())
                {
                    $err_msg = '';
                    foreach ($to->getMessages() as $value) {
                        $err_msg.=$value."<br>";
                    }
                    $this->db->rollback();
                    $this->helper->_echoJson(0,$err_msg);
                    exit();
                }                  
                
                
                $this->db->commit();
                if($user->admin){
                    return $this->response->redirect(BASE_URI.'admin');
                }
                else
                {
                    return $this->response->redirect(BASE_URI.'task');
                }

            }
            else 
            {
                if(!$error)
                {
                    $error = true;
                    $password_error = "Password does not match";
                }
            }

        }        
        $this->view->setVar('success',$success);
        $this->view->setVar('error',$error);
        $this->view->setVar('password_error',$password_error);
        $this->view->setVar('page_content','change_password/index');
        
    }

}

