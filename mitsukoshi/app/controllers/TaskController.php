<?php

class TaskController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }      else {
            $user = MmpiUserAccess::findFirst("employee_id='".EMPLOYEE_ID."'");
           
            if($user->task != 1)
            {
                return $this->response->redirect(BASE_URI);
            }     
        } 
    }
    public function indexAction($status = '')
    {
        $searchKey = $this->request->getPost('task_search_key');
        $autocompletesource = RefsController::getSource();
        $status = isset($_GET['status']) ? $_GET['status'] : 'overdue';
		$sort = isset($_GET['sort']) ? $_GET['sort'] : '0';
		
		$this->view->setMainView('index');
        $this->view->setVar('page_content','task/index');
        
		$id = isset($_GET['id']) ? $_GET['id'] : null;

        $member = Members::find("member_id = '".EMPLOYEE_ID."'");
        $member_search = "";
        $member_search_approval = "";
        foreach($member as $value):
            $member_search_approval = $member_search_approval . " or ra.approver_id = 'TEAM-".$value->group_id."'";
            $member_search = $member_search . " or raa.approver_id = 'TEAM-".$value->group_id."'";
        endforeach;
        
        $approvals = RequestApproval::getApproval(EMPLOYEE_ID,$status,$searchKey,$sort,$member_search_approval,$member_search);        
        $request_details = array();


        if($id){
            if(sizeof($approvals) > 0){
                $request_details = EmployeRequest::getRequestDetails($id, $sort);
            }    
        }else{
            if(sizeof($approvals) > 0){
                $request_details = EmployeRequest::getRequestDetails($approvals[0]->request_id, $sort);
            }    
        }
        
        
        $this->view->setVar('approvals',$approvals);
        $this->view->setVar('task_search_key',$searchKey);
        $this->view->setVar('status',($status == null) ? 'in-progress' : $status);
        $this->view->setVar('request_details',$request_details);
        $this->view->setVar('request_id',($request_details) ? $request_details->request_id : 0);
        $this->view->setVar('total_in_progress',sizeof($approvals));
        $this->view->setVar('autocompletesource',$autocompletesource);
		$this->view->setVar('sort',$sort);
    }


    public function reassignAction(){
        $request_id = $this->request->getPost('request_id');
        $comments = $this->request->getPost('comments');
        $re_assign_id = $this->request->getPost('re_assign_id');
        
       
        $request_details = EmployeRequest::findFirstByRequestId($request_id);
        if(!$request_details){
            $this->_echoJson(0,'Could not found the request');
            exit();
        }        
        $this->db->begin();

                /* FIND REQUEST APPROVAL */
        $request_approval = RequestApproval::findFirst(array(
                'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
                'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
        ));

        if($request_approval){
            $request_approval->approver_id = $re_assign_id;
            $request_approval->comments = $comments;
            if(!$request_approval->save()){
                $this->db->rollback();
                $err_msg = '';
                foreach ($request_approval->getMessages() as $value) {
                    $err_msg.=$value.'<br>';
                }
                $this->helper->_echoJson(0,$err_msg,$request_id);
                exit();
            }
        }else{
            $request_activity = RequestActivities::findFirst(array(
                'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
                'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
            ));

            if($request_activity){
                $request_activity->comments = $comments;
                $request_activity->approver_id = $re_assign_id;
                if(!$request_activity->save()){
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($request_activity->getMessages() as $value) {
                        $err_msg.=$value.'<br>';
                    }
                    $this->helper->_echoJson(0,$err_msg,$request_id);
                    exit();
                }
                
            }
        }

        $this->db->commit();
        $this->helper->_echoJson(1,'<strong>Request </strong> has been reassign',$request_id);
    }
    public function uploadAction()
    {
    
        if(isset($_GET['files']))
        {  
            $error = false;
            $files = array();

            $uploaddir = APP_PATH.'/public/uploads/requests/';
            $path = $uploaddir.FIRST_NAME.'_'.LAST_NAME.'_'.basename($_FILES[0]['name']);
            foreach($_FILES as $file)
            {
                if(move_uploaded_file($file['tmp_name'], $path))
                {
                    $files[] = array(
                            'name' => FIRST_NAME.'_'.LAST_NAME.'_'.basename($file['name']),
                            'path' => $path
                        );
                }
                else
                {
                    $this->helper->_echoJson(0,'Failed to upload files',$_GET);
                    exit();
                }
            }
            $this->helper->_echoJson(1,'Uploaded Succesfully',$files);
        }

    }
    public function downloadAction($filename)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
        
        $filetype = finfo_file($finfo, $filename);
        //var_dump($filetype);die;
        finfo_close($finfo);
        
        
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        readfile($file);
    }
    public function returnrequestAction(){
        $request_id = $this->request->getPost('request_id');
        $comments = $this->request->getPost('comments');

        $request_details = EmployeRequest::findFirstByRequestId($request_id);
        if(!$request_details){
            $this->_echoJson(0,'Could not found the request');
            exit();
        }

        
        $total_completed = 0;

        $request_details = EmployeRequest::findFirstByRequestId($request_id);
        if(!$request_details){
            $this->_echoJson(0,'Could not found the request');
            exit();
        }


        $this->db->begin();

        /* FIND REQUEST APPROVAL */
        $request_approval = RequestApproval::findFirst(array(
                'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
                'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
        ));

        if($request_approval){
            $request_approval->status = 1;
            $request_approval->date_approved = null;
            $prev_sequence = $request_approval->sequence - 1;
            
            $request_approval->date_started = null;
            if(!$request_approval->save()){
                $this->db->rollback();
                $err_msg = '';
                foreach ($request_approval->getMessages() as $value) {
                    $err_msg.=$value.'<br>';
                }
                $this->helper->_echoJson(0,$err_msg,$request_id);
                exit();
            }

            $total_completed += 1;

            $prev_approval = RequestApproval::findFirst(array(
                'conditions' => 'sequence = ?3 AND date_started is not null AND date_approved is not null AND request_id = ?2 AND (status = 1 OR status = 2)',
                'bind' => array(2=>$request_id,3=>$prev_sequence)
            ));

            if($prev_approval){
                $prev_approval->date_started = CURR_DATETIME;
                $prev_approval->date_approved = null;
                $prev_approval->status = 1;
                $prev_approval->comments = $comments;
                if(!$prev_approval->save()){
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($prev_approval->getMessages() as $value) {
                        $err_msg.=$value.'<br>';
                    }
                    $this->helper->_echoJson(0,$err_msg,$request_id);
                    exit();
                }
            }else{
                $prev_activity = RequestActivities::findFirst(array(
                    'conditions' => 'date_started is not null AND date_approved is not null AND request_id = ?2 AND (status = 1 OR status = 2)',
                    'bind' => array(2=>$request_id),
                    'order' => 'sequence DESC',
                    'limit' => 1
                ));
                if($prev_activity){
                    $prev_activity->date_started = CURR_DATETIME;
                    $prev_activity->date_approved = null;
                    $prev_activity->status = 1;
                    $prev_activity->comments = $comments;
                    if(!$prev_activity->save()){
                        $this->db->rollback();
                        $err_msg = '';
                        foreach ($next_activity->getMessages() as $value) {
                            $err_msg.=$value.'<br>';
                        }
                        $this->helper->_echoJson(0,$err_msg,$request_id);
                        exit();
                    }
                }
            }
            
        }else{
            $request_activity = RequestActivities::findFirst(array(
                'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
                'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
            ));
               
            if($request_activity){
                $request_activity->status = 1;
                $prev_sequence = $request_activity->sequence - 1;
                $request_activity->date_started = null;
                $request_activity->date_approved = null;
                
                if(!$request_activity->save()){
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($request_activity->getMessages() as $value) {
                        $err_msg.=$value.'<br>';
                    }
                    $this->helper->_echoJson(0,$err_msg,$request_id);
                    exit();
                }
                $total_completed += 1;
                $prev_activity = RequestActivities::findFirst(array(
                    'conditions' => 'sequence = ?1 AND date_started is not null AND   request_id = ?2 AND (status = 1 OR status = 2)',
                    'bind' => array(1=>$prev_sequence,2=>$request_id)
                ));
                 //echo $prev_activity->sequence."->".$request_id;die;
                if($prev_activity){
                    $prev_activity->date_started = CURR_DATETIME;
                    $prev_activity->date_approved = null;
                    $prev_activity->status = 1;
                    $prev_activity->comments = $comments;
                    if(!$prev_activity->save()){
                        $this->db->rollback();
                        $err_msg = '';
                        foreach ($next_activity->getMessages() as $value) {
                            $err_msg.=$value.'<br>';
                        }
                        $this->helper->_echoJson(0,$err_msg,$request_id);
                        exit();
                    }
                }
            }
        }

        if($total_completed > 0){

            $new_total = $request_details->total_completed - $total_completed;
            if($new_total >= $request_details->total_tasks){
                $request_details->status = 2;
                $request_details->date_ended = CURR_DATETIME;
            }
            $request_details->total_completed  = $new_total;
            if(!$request_details->save()){
                $this->db->rollback();
                $err_msg = '';
                foreach ($request_details->getMessages() as $value) {
                    $err_msg.=$value.'<br>';
                }
                $this->helper->_echoJson(0,$err_msg,$request_id);
                exit();
            }
            $this->db->commit();
        }

        
        $this->helper->_echoJson(1,'<strong>Request </strong> has been return',$request_id);

    }
    public function approveAction(){
      
    	$request_id = $this->request->getPost('request_id');
        $comments = $this->request->getPost('comments');
        $files = $this->request->getPost('my_files');
        $filename = isset($files['name']) ? $files['name'] : null;
        $path = isset($files['path']) ? $files['path'] : null;
        
    	$total_completed = 0;

    	$request_details = EmployeRequest::findFirstByRequestId($request_id);
    	if(!$request_details){
    		$this->_echoJson(0,'Could not found the request');
    		exit();
    	}

		
    	$this->db->begin();

        /* FIND REQUEST APPROVAL */
    	$request_approval = RequestApproval::findFirst(array(
    			'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
    			'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
    	));

    	if($request_approval){
    		$request_approval->status = 2;
    		$request_approval->date_approved = CURR_DATETIME;
    		$next_sequence = $request_approval->sequence + 1;
            $request_approval->comments = $comments;
            $request_approval->filename= $filename;
            $request_approval->path = $path;
    		if(!$request_approval->save()){
    			$this->db->rollback();
    			$err_msg = '';
    			foreach ($request_approval->getMessages() as $value) {
    				$err_msg.=$value.'<br>';
    			}
				$this->helper->_echoJson(0,$err_msg,$request_id);
				exit();
    		}
			
    		$total_completed += 1;
			
    		$next_approval = RequestApproval::findFirst(array(
    			'conditions' => 'sequence = ?1 AND date_started is null AND date_approved is null AND request_id = ?2 AND (status = 1 OR status = 2)',
    			'bind' => array(1=>$next_sequence,2=>$request_id)
    		));
			

    		if($next_approval){
                // GET EMAIL 
                    $pos = strpos($next_approval->approver_id, "TEAM");
                    if($pos === false)
                    {
                        $email = $next_approval->MmpiTableOfOrganization->email;
                        $this->helper->mailNotifApprover($email);
                    }
                    else
                    {
                        $array_id = explode("-",$next_approval->approver_id);
                        $employee_id_1 = $array_id[0];
                        $employee_id_2 = $array_id[1];

                        $members_id_array = Members::find("group_id = '$next_approval->approver_id'");
                        foreach ($members_id_array as $value) 
                        {
                            $members_id = $members_id_array->member_id;
                            $email = MmpiTableOfOrganization::find("employee_id = '$members_id'");
                            $this->helper->mailNotifApprover($email->email);
                        }                    
                    }
    			$next_approval->date_started = CURR_DATETIME;
    			if(!$next_approval->save()){
    				$this->db->rollback();
	    			$err_msg = '';
	    			foreach ($next_approval->getMessages() as $value) {
	    				$err_msg.=$value.'<br>';
	    			}
					$this->helper->_echoJson(0,$err_msg,$request_id);
					exit();
    			}
    		}else{
    			$next_activity = RequestActivities::findFirst(array(
	    			'conditions' => 'date_started is null AND date_approved is null AND request_id = ?1 AND (status = 1 OR status = 2)',
	    			'bind' => array(1=>$request_id)
    			));
    			if($next_activity){
                            
                            $pos = strpos($next_activity->approver_id, "TEAM");
                            if($pos === false)
                            {
                                $email = $next_activity->MmpiTableOfOrganization->email;
                                $this->helper->mailNotifActivity($email);
                            }
                            else
                            {
                                $array_id = explode("-",$next_activity->approver_id);
                                $employee_id_1 = $array_id[0];
                                $employee_id_2 = $array_id[1];

                                $members_id_array = Members::find("group_id = '$next_activity->approver_id'");
                                foreach ($members_id_array as $value) 
                                {
                                    $members_id = $members_id_array->member_id;
                                    $email = MmpiTableOfOrganization::find("employee_id = '$members_id'");
                                    $this->helper->mailNotifActivity($email->email);
                                }                    
                            }

    				$next_activity->date_started = CURR_DATETIME;
    				if(!$next_activity->save()){
	    				$this->db->rollback();
		    			$err_msg = '';
		    			foreach ($next_activity->getMessages() as $value) {
		    				$err_msg.=$value.'<br>';
		    			}
						$this->helper->_echoJson(0,$err_msg,$request_id);
						exit();
    				}
    			}
    		}
    		
    	}else{
            
                $member = Members::find("member_id = '".EMPLOYEE_ID."'");
                $member_search = "";
                $member_search_approval = "";
                foreach($member as $value):
                    $member_search_approval = $member_search_approval . "or approver_id = 'TEAM-".$value->group_id."'";
                endforeach;
    		/*$request_activity = RequestActivities::findFirst(array(
    			'conditions' => 'date_started is not null AND date_approved is null AND (approver_id = ?1 $member_search_approval) AND request_id = ?2 AND (status = 1 OR status = 2)',
    			'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
	    	));*/
                //$request_activity =  RequestActivities::getRequestActivitiesForComplete(EMPLOYEE_ID,$request_id,$member_search_approval);
                $request_activity = RequestActivities::findFirst("date_started is not null AND date_approved is null AND (approver_id = '".EMPLOYEE_ID."' $member_search_approval) AND request_id = '$request_id' AND (status = 1 OR status = 2)");
	    	
                if($request_activity){
                 
                
                //if($pos)
                //{
                    $request_activity->approver_id = EMPLOYEE_ID;
                //}
                    $request_activity->comments = $comments;
	    		$request_activity->status = 2;
	    		$next_sequence = $request_activity->sequence + 1;
                        $request_activity->filename= $filename;
                        $request_activity->path = $path;
                        $request_activity->date_approved = CURR_DATETIME;
	    		if(!$request_activity->save()){
                                var_dump("asdsdaaaaaaaaaaaaaaaaaa");die();
	    			$this->db->rollback();
	    			$err_msg = '';
	    			foreach ($request_activity->getMessages() as $value) {
	    				$err_msg.=$value.'<br>';
	    			}
					$this->helper->_echoJson(0,$err_msg,$request_id);
					exit();
	    		}
	    		$total_completed += 1;
	    		$next_activity = RequestActivities::findFirst(array(
	    			'conditions' => 'sequence = ?1 AND date_started is null AND date_approved is null AND  request_id = ?2 AND (status = 1 OR status = 2)',
	    			'bind' => array(1=>$next_sequence,2=>$request_id)
    			));
    			if($next_activity){
                            $pos = strpos($next_activity->approver_id, "TEAM");
                            if($pos === false)
                            {
                                $email = $next_activity->MmpiTableOfOrganization->email;
                                $this->helper->mailNotifActivity($email);
                            }
                            else
                            {
                                $array_id = explode("-",$next_activity->approver_id);
                                $employee_id_1 = $array_id[0];
                                $employee_id_2 = $array_id[1];

                                $members_id_array = Members::find("group_id = '$next_activity->approver_id'");
                                foreach ($members_id_array as $value) 
                                {
                                    $members_id = $members_id_array->member_id;
                                    $email = MmpiTableOfOrganization::find("employee_id = '$members_id'");
                                    $this->helper->mailNotifActivity($email->email);
                                }                    
                            }                            
                            //$email = $next_activity->MmpiTableOfOrganization->email;

    				$next_activity->date_started = CURR_DATETIME;
    				if(!$next_activity->save()){
	    				$this->db->rollback();
		    			$err_msg = '';
		    			foreach ($next_activity->getMessages() as $value) {
		    				$err_msg.=$value.'<br>';
		    			}
						$this->helper->_echoJson(0,$err_msg,$request_id);
						exit();
    				}
    			}
	    	}
    	}

    	if($total_completed > 0){

    		$new_total = $request_details->total_completed + $total_completed;
                
    		if($new_total >= $request_details->total_tasks){
                    //
    			$request_details->status = 2;
                        $request_details->date_ended = CURR_DATETIME;
				
				$requestor_id = $request_details->requestor_id;
				$initiator = MmpiTableOfOrganization::findFirst("employee_id = '$requestor_id'");
				$email = $initiator->email;
				$this->helper->mailRequestComplete($email, $request_details->request_id);
			
    		}
    		$request_details->total_completed  = $new_total;
    		if(!$request_details->save()){
    			$this->db->rollback();
    			$err_msg = '';
    			foreach ($request_details->getMessages() as $value) {
    				$err_msg.=$value.'<br>';
    			}
				$this->helper->_echoJson(0,$err_msg,$request_id);
				exit();
    		}
    		$this->db->commit();
    	}

        
        $this->helper->_echoJson(1,'<strong>Request </strong> has been approved',$request_id);


    }
	
	public function rejectAction()
	{
		$request_id = $this->request->getPost('request_id');
        $comments = $this->request->getPost('comments');
        $files = $this->request->getPost('my_files');
        $filename = isset($files['name']) ? $files['name'] : null;
        $path = isset($files['path']) ? $files['path'] : null;
		
		$total_completed = 0;
        
		$request_details = EmployeRequest::findFirstByRequestId($request_id);
    	if(!$request_details){
    		$this->_echoJson(0,'Could not found the request');
    		exit();
    	}


    	$this->db->begin();

		$request_approval = RequestApproval::findFirst(array(
    			'conditions' => 'date_started is not null AND date_approved is null AND approver_id = ?1 AND request_id = ?2 AND (status = 1 OR status = 2)',
    			'bind' => array(1=>EMPLOYEE_ID,2=>$request_id)
    	));

		if($request_approval){
    		$request_approval->status = 3;
    		$request_approval->date_approved = CURR_DATETIME;
    		$next_sequence = $request_approval->sequence + 1;
            $request_approval->comments = $comments;
            $request_approval->filename= $filename;
            $request_approval->path = $path;
    		if(!$request_approval->save()){
    			$this->db->rollback();
    			$err_msg = '';
    			foreach ($request_approval->getMessages() as $value) {
    				$err_msg.=$value.'<br>';
    			}
				$this->helper->_echoJson(0,$err_msg,$request_id);
				exit();
    		}

			$total_completed += 1;
    		
    	}
		
		if($total_completed > 0){
			$request_details->status = 3;
            $request_details->date_ended = CURR_DATETIME;
			$request_details->total_completed  = $new_total;
			
			$requestor_id = $request_details->requestor_id;
			$initiator = MmpiTableOfOrganization::findFirst("employee_id = '$requestor_id'");
			$email = $initiator->email;
			$this->helper->mailRequestRejected($email, $request_details->request_id);
			
			if(!$request_details->save()){
    			$this->db->rollback();
    			$err_msg = '';
    			foreach ($request_details->getMessages() as $value) {
    				$err_msg.=$value.'<br>';
    			}
				$this->helper->_echoJson(0,$err_msg,$request_id);
				exit();
    		}
    		$this->db->commit();
		}
		$this->helper->_echoJson(1,'<strong>Request </strong> has been rejected',$request_id);
	}
        
        public function followupAction()
        {   
            $email = $_POST['email_data'];
//            $email = $this->request->getPost(data);
            $this->helper->mailNotifApprover($email);
        }
}

