<?php

class GroupController extends \Phalcon\Mvc\Controller
{
    protected function initialize()
    {
          if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {

    }

    public function addAction()
    {
    	$data = $this->request->getPost();
    	$members  = $data['members'];

    	$this->db->begin();

    	$group = new RefGroup();
    	$group->name = $data['group_name'];
    	$group->description = $data['group_name'];
    	$group->status_id = 1;
    	$group->created_by = EMPLOYEE_ID;
    	
        $groups = RefGroup::findFirst("lower(name)=lower('".$data['group_name']."')");
        if($groups->name != "")
        {
            $this->helper->_echoJson(0,"Group name already existing.");
            exit();
        }
        
    	if(!$group->save()){
    		$this->db->rollback();
    		$err_msg = '';
    		foreach ($group->getMessages() as $value) {
    			$err_msg.=$value.'<br>';
    		}
    		$this->helper->_echoJson(0,$err_msg);
    		exit();
    	}

    	if(sizeof($members) > 0){
    		for ($i=0; $i < sizeof($members) ; $i++) { 
    			$member = new Members();
    			$member->member_id  = $members[$i]['member_id'];
    			$member->group_id = $group->group_id;
    			if(!$member->save()){
		    		$this->db->rollback();
		    		$err_msg = '';
		    		foreach ($member->getMessages() as $value) {
		    			$err_msg.=$value.'<br>';
		    		}
		    		$this->helper->_echoJson(0,$err_msg);
		    		exit();
		    	}
    		}
    	}
    	$this->db->commit();
    	$this->helper->_echoJson(1,$data['group_name'].' is now added to the Workflow Management System.',$data);
    }


    public function updateAction()
    {
        $data = $this->request->getPost();
        $members  = $data['members'];

        $this->db->begin();

        $group = RefGroup::findFirstByGroupId($data['group_id']);
        if(!$group){
            $this->helper->_echoJson(0,'Could not find the group');
            exit();
        }
        $group_check = RefGroup::findFirst("lower(name)=('".$data['group_name']."') and group_id <> ".$data['group_id'] . " and status_id <> 0" );
        if($group_check->name != "")
        {
            $this->helper->_echoJson(0,"Group name already existing.");
            exit();            
        }
        
        $group->name = $data['group_name'];
        $group->description = $data['group_name'];
        $group->status_id = 1;
        $group->created_by = EMPLOYEE_ID;
        
        if(!$group->save()){
            $this->db->rollback();
            $err_msg = '';
            foreach ($group->getMessages() as $value) {
                $err_msg.=$value.'<br>';
            }
            $this->helper->_echoJson(0,$err_msg);
            exit();
        }

        $existing_members = Members::find(array(
                                'conditions'=>'group_id = ?1',
                                'bind'=>array(1=>$group->group_id)
                            ));
        if(sizeof($existing_members) <= 0){
            $this->helper->_echoJson(0,'Could not find the members');
            exit();   
        }

        foreach ($existing_members as $x){
            if(!$x->delete()){
                $this->db->rollback();
                $err_msg = '';
                foreach ($x->getMessages() as $value) {
                    $err_msg.=$value.'<br>';
                }
                $this->helper->_echoJson(0,$err_msg);
                exit();
            }
        }
        
        if(sizeof($members) > 0){
            for ($i=0; $i < sizeof($members) ; $i++) { 
                $member = new Members();
                $member->member_id  = $members[$i]['member_id'];
                $member->group_id = $group->group_id;
                if(!$member->save()){
                    $this->db->rollback();
                    $err_msg = '';
                    foreach ($member->getMessages() as $value) {
                        $err_msg.=$value.'<br>';
                    }
                    $this->helper->_echoJson(0,$err_msg);
                    exit();
                }
            }
        }
        $this->db->commit();
        $this->helper->_echoJson(1,$data['group_name'].' has been successfully edited.',$data);
    }
}

